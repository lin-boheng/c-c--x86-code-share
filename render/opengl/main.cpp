/**************************
 * Includes
 *
 **************************/

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

#include "MathUtils.h"

/**************************
 * Function Declarations
 *
 **************************/

LRESULT CALLBACK WndProc (HWND hWnd, UINT message,
WPARAM wParam, LPARAM lParam);
void EnableOpenGL (HWND hWnd, HDC *hDC, HGLRC *hRC);
void DisableOpenGL (HWND hWnd, HDC hDC, HGLRC hRC);

inline void glMultMatrixf(float mat[][4]){
    glMultMatrixf((GLfloat*)mat[0]);
}

void glInitMatrix(float mat[][4]){
    mat[0][0] = 1.0f; mat[1][0] = 0.0f; mat[2][0] = 0.0f; mat[3][0] = 0.0f;
    mat[0][1] = 0.0f; mat[1][1] = 1.0f; mat[2][1] = 0.0f; mat[3][1] = 0.0f;
    mat[0][2] = 0.0f; mat[1][2] = 0.0f; mat[2][2] = 1.0f; mat[3][2] = 0.0f;
    mat[0][3] = 0.0f; mat[1][3] = 0.0f; mat[2][3] = 0.0f; mat[3][3] = 1.0f;
}

void glInitMatrix(float mat[][4], Vector3 pos){
    mat[0][0] = 1.0f; mat[1][0] = 0.0f; mat[2][0] = 0.0f; mat[3][0] = pos.x;
    mat[0][1] = 0.0f; mat[1][1] = 1.0f; mat[2][1] = 0.0f; mat[3][1] = pos.y;
    mat[0][2] = 0.0f; mat[1][2] = 0.0f; mat[2][2] = 1.0f; mat[3][2] = pos.z;
    mat[0][3] = 0.0f; mat[1][3] = 0.0f; mat[2][3] = 0.0f; mat[3][3] = 1.0f;
}

void glInitMatrix(float mat[][4], Quaternion q){
    float x2 = q.x * q.x, y2 = q.y * q.y, z2 = q.z * q.z;
    float xy = q.x * q.y, yz = q.y * q.z, zw = q.z * q.w;
    float xz = q.x * q.z, yw = q.y * q.w, xw = q.x * q.w;
    mat[0][0] = 1.0f - 2.0f * (y2 + z2);
    mat[0][1] = 2.0f * (xy - zw);
    mat[0][2] = 2.0f * (xz - yw);
    mat[0][3] = 0.0f;
    mat[1][0] = 2.0f * (xy + zw);
    mat[1][1] = 1.0f - 2.0f * (z2 + x2);
    mat[1][2] = 2.0f * (yz - xw);
    mat[1][3] = 0.0f;
    mat[2][0] = 2.0f * (xz - yw);
    mat[2][1] = 2.0f * (yz + xw);
    mat[2][2] = 1.0f - 2.0f * (x2 + y2);
    mat[2][3] = 0.0f;
    mat[3][0] = 0.0f;
    mat[3][1] = 0.0f;
    mat[3][2] = 0.0f;
    mat[3][3] = 1.0f;
}

void glPrespective(float angle, float rate, float min, float max){
    float mat[4][4]{};
    float height = __builtin_tanf(angle) * min;
    float width = height * rate;
    mat[0][0] = 1.0f / width;
    mat[1][1] = -1.0f / height;
    mat[2][2] = 2.0f / (min - max);
    mat[3][2] = -(min + max) / (min - max);
    mat[3][3] = 1.0f;
    glMultMatrixf(mat);
    mat[0][0] = min;
    mat[1][1] = min;
    mat[2][2] = min + max;
    mat[3][2] = -min * max;
    mat[2][3] = 1.0f;
    mat[3][3] = 0.0f;
    glMultMatrixf(mat);
}

inline void glTranslatef(Vector3 v){
    glTranslatef(v.x, v.y, v.z);
}

inline void glRotatef(Quaternion q){
    float mat[4][4];
    glInitMatrix(mat, q);
    glMultMatrixf(mat);
    //Vector3 axis = q.GetAxis().Normal();
    //glRotatef(ToAngle(2.0f * __builtin_acosf(q.w)), axis.x, axis.y, axis.z);
}

Vector3 camPos(0.0f, -1.6f, 2.0f);
Quaternion camRot(Quaternion::FromTo(Vector3::back, -camPos));

void glInitCamera(){
    //glPrespective(ToRadian(45.0f), 1.0f, 0.2f, 10.0f);
    glMatrixMode(GL_PROJECTION);
    gluPerspective(45.0, 1.0, 0.1, 10.0);
    //glScalef(1.0f, 1.0f, 1.0f);
    glRotatef(Quaternion(camRot.GetAxis(), -camRot.w));
    glTranslatef(-camPos);
}

/**************************
 * WinMain
 *
 **************************/

int WINAPI WinMain (HINSTANCE hInstance,
                    HINSTANCE hPrevInstance,
                    LPSTR lpCmdLine,
                    int iCmdShow)
{
    WNDCLASS wc;
    HWND hWnd;
    HDC hDC;
    HGLRC hRC;        
    MSG msg;
    BOOL bQuit = FALSE;
    float theta = 0.0f;

    /* register window class */
    wc.style = CS_OWNDC;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor (NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH) GetStockObject (BLACK_BRUSH);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = "GLSample";
    RegisterClass (&wc);

    /* create main window */
    hWnd = CreateWindowEx (
      0,
      "GLSample", "OpenGL Sample", 
      WS_CAPTION | WS_POPUPWINDOW | WS_VISIBLE,
      0, 0, 256, 256,
      NULL, NULL, hInstance, NULL);

    /* enable OpenGL for the window */
    EnableOpenGL (hWnd, &hDC, &hRC);

    glEnable(GL_DEPTH_TEST);

    glMatrixMode(GL_PROJECTION);  
    glLoadIdentity();  
    gluPerspective(60.0, 1.0, 0.01, 20.0);  
  
    glMatrixMode(GL_MODELVIEW);  
    glLoadIdentity();  
    gluLookAt(0.0, 0.0, -2.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);  

    /* program main loop */
    while (!bQuit)
    {
        /* check for messages */
        if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
        {
            /* handle or dispatch messages */
            if (msg.message == WM_QUIT)
            {
                bQuit = TRUE;
            }
            else
            {
                TranslateMessage (&msg);
                DispatchMessage (&msg);
            }
        }
        else
        {
            /* OpenGL animation code goes here */
            glClearColor(0.0,0.0,0.0,1.0);  
            glClearDepth(1.0);
        
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  
        
            // 画左眼
            glMatrixMode(GL_PROJECTION);  
            glPushMatrix();
            float mat[16];  
            glGetFloatv(GL_PROJECTION_MATRIX,mat);  
            glLoadIdentity();
        
            glMultMatrixf(mat);
        
            glMatrixMode(GL_MODELVIEW);  
            glPushMatrix();  
            glGetFloatv(GL_MODELVIEW_MATRIX,mat);  
            glLoadIdentity();
        
            glMultMatrixf(mat);
            glColorMask(1.0f, 0.5f, 0.0f, 1.0f);
            glTranslatef(0.1f, 0.0f, 0.0f);
            glRotatef(theta, 0.0, 1.0, 0.0);
            glBegin(GL_TRIANGLES);
            glColor3f(1.0f, 1.0f, 1.0f);
            glVertex2f(0.0f, 1.0f);
            glVertex2f(-0.87f, -0.5f);
            glVertex2f(0.87f, -0.5f);
            glEnd();
            glColorMask(1.0f, 1.0f, 1.0f, 1.0f);
            glPopMatrix();
        
            glMatrixMode(GL_PROJECTION);
            glPopMatrix();
        
            glFlush();  
        
            //画右眼  
            glClearDepth(1.0);  
            glClear(GL_DEPTH_BUFFER_BIT);  
        
            glMatrixMode(GL_PROJECTION);  
            glPushMatrix();  
            glGetFloatv(GL_PROJECTION_MATRIX,mat);  
            glLoadIdentity();  
        
            glMultMatrixf(mat);  
        
            glMatrixMode(GL_MODELVIEW);  
            glPushMatrix();  
            glGetFloatv(GL_MODELVIEW_MATRIX,mat);  
            glLoadIdentity();  
        
            glMultMatrixf(mat);
            glColorMask(0.0f, 0.5f, 1.0f, 1.0f);
            glTranslatef(-0.1f, 0.0f, 0.0f);
            glRotatef(theta, 0.0, 1.0, 0.0);
            glBegin(GL_TRIANGLES);
            glColor3f(1.0f, 1.0f, 1.0f);
            glVertex2f(0.0f, 1.0f);
            glVertex2f(-0.87f, -0.5f);
            glVertex2f(0.87f, -0.5f);
            glEnd();
            glColorMask(1.0f, 1.0f, 1.0f, 1.0f);
            glPopMatrix();  
        
            glMatrixMode(GL_PROJECTION);  
            glPopMatrix();  
            glFlush();

            SwapBuffers (hDC);

            theta += 1.0f;
            Sleep (1);
        }
    }

    /* shutdown OpenGL */
    DisableOpenGL (hWnd, hDC, hRC);

    /* destroy the window explicitly */
    DestroyWindow (hWnd);

    return msg.wParam;
}


/********************
 * Window Procedure
 *
 ********************/

LRESULT CALLBACK WndProc (HWND hWnd, UINT message,
                          WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
    case WM_CREATE:
        return 0;
    case WM_CLOSE:
        PostQuitMessage (0);
        return 0;

    case WM_DESTROY:
        return 0;

    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_ESCAPE:
            PostQuitMessage(0);
            return 0;
        }
        return 0;

    default:
        return DefWindowProc (hWnd, message, wParam, lParam);
    }
}


/*******************
 * Enable OpenGL
 *
 *******************/

void EnableOpenGL (HWND hWnd, HDC *hDC, HGLRC *hRC)
{
    PIXELFORMATDESCRIPTOR pfd;
    int iFormat;

    /* get the device context (DC) */
    *hDC = GetDC (hWnd);

    /* set the pixel format for the DC */
    ZeroMemory (&pfd, sizeof (pfd));
    pfd.nSize = sizeof (pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | 
      PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;
    iFormat = ChoosePixelFormat (*hDC, &pfd);
    SetPixelFormat (*hDC, iFormat, &pfd);

    /* create and enable the render context (RC) */
    *hRC = wglCreateContext( *hDC );
    wglMakeCurrent( *hDC, *hRC );

}


/******************
 * Disable OpenGL
 *
 ******************/

void DisableOpenGL (HWND hWnd, HDC hDC, HGLRC hRC)
{
    wglMakeCurrent (NULL, NULL);
    wglDeleteContext (hRC);
    ReleaseDC (hWnd, hDC);
}
