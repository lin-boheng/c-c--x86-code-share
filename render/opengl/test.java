import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class test {
    public static void main(String[] args){
        try {
            File data = new File("data.SP3");
            Scanner sn = new Scanner(new FileInputStream(data));
            String time = null;
            while(sn.hasNextLine()){
                String s = sn.nextLine();
                if(s.startsWith("*")){
                    time = s.substring(3, 19);
                }
                if(time == null)
                    continue;
                if(s.startsWith("P")){
                    String name;
                    float x, y, z;
                    name = s.substring(1, 4);
                    x = Float.parseFloat(s.substring(5, 18));
                    y = Float.parseFloat(s.substring(19, 32));
                    z = Float.parseFloat(s.substring(33, 46));
                    System.out.println(time + " " + name + ": " + Math.atan2(y, x) * 180.0 / Math.PI + " " + Math.atan2(z, Math.sqrt(x * x + y * y)) * 180.0 / Math.PI);
                }
            }
            sn.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}