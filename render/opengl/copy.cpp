/**************************
 * Includes
 *
 **************************/

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

#include "MathUtils.h"

#include <iostream>
#include <fstream>
#include <vector>

class Object3D {
public:
    struct Fragment {
        int vertex[3];
        int texCoord[3];
        int normal[3];
        bool hasTexCoord;
        bool hasNormal;
    };
private:
    std::vector<Quaternion> Vertices;
    std::vector<Vector3> TexCoords;
    std::vector<Vector3> Normals;
    std::vector<Fragment> Fragments;
public:
    Object3D(){}

    Object3D(const char* filename){
        Load(filename);
    }

    ~Object3D(){}

    void Load(const char* filename){
        std::ifstream file(filename);
        char line[MAX_PATH + 1];
        char clip[3][40];
        if(!file)
            throw std::runtime_error("File Not Found!");
        while(!file.eof()){
            file.getline(line, MAX_PATH);
            if(*line == '#' || *line == '\0')
                continue;
            if(!strncmp(line, "v ", 2)){
                Quaternion v;
                int n = sscanf(line + 2, "%f %f %f %f", &v.x, &v.y, &v.z, &v.w);
                if(n == 3){
                    v.w = 1.0f;
                }else if(n != 4)
                    throw std::runtime_error("Vertex Format Error!");
                Vertices.push_back(v);
            }else if(!strncmp(line, "vt ", 3)){
                Vector3 c;
                int n = sscanf(line + 3, "%f %f %f", &c.x, &c.y, &c.z);
                if(n == 2){
                    c.z = 0.0f;
                }else if(n != 3)
                    throw std::runtime_error("Texture Coordination Format Error!");
                TexCoords.push_back(c);
            }else if(!strncmp(line, "vn ", 3)){
                Vector3 v;
                int n = sscanf(line + 3, "%f %f %f", &v.x, &v.y, &v.z);
                if(n != 3)
                    throw std::runtime_error("Normal Format Error!");
                Normals.push_back(v);
            }else if(!strncmp(line, "f ", 2)){
                Fragment f = {};
                if(sscanf(line + 2, "%s %s %s", clip[0], clip[1], clip[2]) != 3)
                    throw std::runtime_error("Fragment Format Error!");
                char* p = strstr(clip[0], "//");
                if(p){
                    f.hasTexCoord = false;
                    f.hasNormal = true;
                    for(int i = 0; i < 3; i++)
                        if(sscanf(clip[i], "%d//%d", f.vertex + i, f.normal + i) != 2)
                            throw std::runtime_error("Fragment [v//vn] Format Error!");
                }else{
                    p = strstr(clip[0], "/");
                    if(!p){
                        f.hasTexCoord = false;
                        f.hasNormal = false;
                        for(int i = 0; i < 3; i++)
                            if(sscanf(clip[i], "%d", f.vertex + i) != 1)
                                throw std::runtime_error("Fragment [v] Format Error!");
                    }else if(strstr(p + 1, "/")){
                        f.hasTexCoord = true;
                        f.hasNormal = true;
                        for(int i = 0; i < 3; i++)
                            if(sscanf(clip[i], "%d/%d/%d", f.vertex + i, f.texCoord + i, f.normal + i) != 3)
                                throw std::runtime_error("Fragment [v/vt/vn] Format Error!");
                    }else{
                        f.hasTexCoord = true;
                        f.hasNormal = false;
                        for(int i = 0; i < 3; i++)
                            if(sscanf(clip[i], "%d/%d", f.vertex + i, f.texCoord + i) != 2)
                                throw std::runtime_error("Fragment [v/vt] Format Error!");
                    }
                }
                Fragments.push_back(f);
            }else if(!strncmp(line, "o ", 2)){
                std::string name = line + 2;
                continue;
            }else if(!strncmp(line, "s ", 2)){
                continue;
            }else{
                std::string err = "Unrecognized Sign: ";
                err += line;
                throw std::runtime_error(err);
            }
        }
    }

    void Store(const char* filename){

    }

    void Draw(){
        glBegin(GL_TRIANGLES);
        for(Fragment f : Fragments){
            if(f.hasNormal){
                int index = (f.normal[0] < 0 ? Normals.size() + f.normal[0] : f.normal[0] - 1);
                glNormal3f(Normals[index].x, Normals[index].y, Normals[index].z);
            }
            for(int i = 0; i < 3; i++){
                int index = (f.vertex[i] < 0 ? Vertices.size() + f.vertex[i] : f.vertex[i] - 1);
                glVertex4f(Vertices[index].x, Vertices[index].y, Vertices[index].z, Vertices[index].w);
            }
        }
        glEnd();
    }
};

Object3D obj;

/**************************
 * Function Declarations
 *
 **************************/

LRESULT CALLBACK WndProc (HWND hWnd, UINT message,
WPARAM wParam, LPARAM lParam);
void EnableOpenGL (HWND hWnd, HDC *hDC, HGLRC *hRC);
void DisableOpenGL (HWND hWnd, HDC hDC, HGLRC hRC);

inline void glMultMatrixf(float mat[][4]){
    glMultMatrixf((GLfloat*)mat[0]);
}

inline void glTranslatef(Vector3 v){
    glTranslatef(v.x, v.y, v.z);
}

inline void glRotatef(Quaternion q){
    float mat[4][4];
    float x2 = q.x * q.x, y2 = q.y * q.y, z2 = q.z * q.z;
    float xy = q.x * q.y, yz = q.y * q.z, zw = q.z * q.w;
    float xz = q.x * q.z, yw = q.y * q.w, xw = q.x * q.w;
    mat[0][0] = 1.0f - 2.0f * (y2 + z2);
    mat[0][1] = 2.0f * (xy - zw);
    mat[0][2] = 2.0f * (xz - yw);
    mat[0][3] = 0.0f;
    mat[1][0] = 2.0f * (xy + zw);
    mat[1][1] = 1.0f - 2.0f * (z2 + x2);
    mat[1][2] = 2.0f * (yz - xw);
    mat[1][3] = 0.0f;
    mat[2][0] = 2.0f * (xz - yw);
    mat[2][1] = 2.0f * (yz + xw);
    mat[2][2] = 1.0f - 2.0f * (x2 + y2);
    mat[2][3] = 0.0f;
    mat[3][0] = 0.0f;
    mat[3][1] = 0.0f;
    mat[3][2] = 0.0f;
    mat[3][3] = 1.0f;
    glMultMatrixf(mat);
}

Vector3 camPos(0.0f, -3.0f, 1.0f);
Quaternion camRot(Quaternion::FromTo(Vector3::back, -camPos));

void glInitCamera(){
    glMatrixMode(GL_PROJECTION);  
    glPushMatrix();
    float mat[16];
    glGetFloatv(GL_PROJECTION_MATRIX,mat);  
    glLoadIdentity();

    glMultMatrixf(mat);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glGetFloatv(GL_MODELVIEW_MATRIX,mat);  
    glLoadIdentity();

    glMultMatrixf(mat);
}

void glInitMat(HWND hwnd, int no){
    RECT rect;

    GetWindowRect(hwnd, &rect);

    int height = (rect.bottom - rect.top) >> 1;
    int width = (rect.right - rect.left) >> 1;

    glScissor((no & 1 ? width : 0), (no & 2 ? height : 0), width, height);
    glTranslatef(-0.5f + (no & 1 ? 1.0f : 0.0f), 0.0f, -0.5f + (no & 2 ? 1.0f : 0.0f));
    glScalef(0.5f, 1.0f, 0.5f);
}

//先画右眼
void draw(bool left){
    static float theta = 0.0f;

    if(!left)
        theta += 0.1f;

    glPushMatrix();
    glTranslatef(__builtin_cosf(theta * 0.1f), 5.0f * __builtin_sinf(theta * 0.1f) + 3.0f, 0.0f);
    glRotatef(theta, 0.0f, 0.0f, 1.0f);
    glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
    obj.Draw();
    glPopMatrix();
}

void render(){
    Vector3 lookAt = -camPos;
    Vector3 up = Vector3::up;
    Vector3 horn = Vector3::Cross(lookAt, up).Normal();

    glPushMatrix ();
    glInitCamera();
    glTranslatef(horn * -0.1f);
    glColorMask(1.0f, 0.0f, 0.0f, 1.0f);
    draw(false);
    glPopMatrix ();

    glClear(GL_DEPTH_BUFFER_BIT);

    glPushMatrix ();
    glInitCamera();
    glTranslatef(horn * 0.1f);
    glColorMask(0.0f, 0.0f, 1.0f, 1.0f);
    draw(true);
    glPopMatrix ();
}

void setViewport(HWND hWnd, float left, float right, float top, float bottom){
    RECT rect;
    LONG width, height;
    GetClientRect(hWnd, &rect);
    width = rect.right - rect.left;
    height = rect.bottom - rect.top;
    glViewport((GLint)(left * width), (GLint)(bottom * height),
               (GLint)((right - left) * width), (GLint)((top - bottom) * height));
}

/**************************
 * WinMain
 *
 **************************/

int WINAPI WinMain (HINSTANCE hInstance,
                    HINSTANCE hPrevInstance,
                    LPSTR lpCmdLine,
                    int iCmdShow)
{
    WNDCLASS wc;
    HWND hWnd;
    HDC hDC;
    HGLRC hRC;        
    MSG msg;
    BOOL bQuit = FALSE;

    try{
        obj.Load("test.obj");
    }catch(std::runtime_error e){
        MessageBoxA(NULL, e.what(), "Exception", MB_OKCANCEL);
        exit(1);
    }

    std::cout << "Object Load Success!" << std::endl;

    /* register window class */
    wc.style = CS_OWNDC;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor (NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH) GetStockObject (BLACK_BRUSH);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = "GLSample";
    RegisterClass (&wc);

    /* create main window */
    hWnd = CreateWindowEx (
        WS_EX_LAYERED | WS_EX_TOPMOST,
        "GLSample", "OpenGL Sample", 
        WS_POPUPWINDOW | WS_VISIBLE | WS_MAXIMIZE,
        0, 0, 256, 256,
        NULL, NULL, hInstance, NULL);
    
    ShowCursor(TRUE);
    SetLayeredWindowAttributes(hWnd, RGB(0, 0, 0), TRUE, LWA_COLORKEY);

    /* enable OpenGL for the window */
    EnableOpenGL (hWnd, &hDC, &hRC);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glEnable(GL_ALPHA_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);
    //glEnable(GL_SCISSOR_TEST);

    glEnable(GL_SMOOTH);
    glEnable(GL_POINT_SMOOTH);
    glHint(GL_POINT_SMOOTH, GL_NICEST);
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH, GL_NICEST);
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_POLYGON_SMOOTH, GL_NICEST);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

    glCullFace(GL_BACK);

    glBlendFunc(GL_ONE, GL_ONE);

    GLfloat emis[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat ambi[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat diff[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat spec[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	glMaterialfv(GL_FRONT, GL_EMISSION, emis);
	glMaterialfv(GL_FRONT, GL_AMBIENT, ambi);
	glMaterialfv(GL_FRONT, GL_DIFFUSE , diff);
	glMaterialfv(GL_FRONT, GL_SPECULAR, spec);
	glMaterialf(GL_FRONT, GL_SHININESS, 10.0f);

	GLfloat ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);

	GLfloat position[] = { 0.0f, 0.0f, 1.0f, 0.0f };

	glLightfv(GL_LIGHT0, GL_POSITION, position);

    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0);

    glEnable(GL_LIGHT0);

    glClearColor (0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0);

    setViewport(hWnd, 0.25f, 0.75f, 1.0f, 0.75f);

    /* program main loop */
    while (!bQuit)
    {
        /* check for messages */
        if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
        {
            /* handle or dispatch messages */
            if (msg.message == WM_QUIT)
            {
                bQuit = TRUE;
            }
            else
            {
                TranslateMessage (&msg);
                DispatchMessage (&msg);
            }
        }
        else
        {
            /* OpenGL animation code goes here */
            static int i = 0; i++;
            if(i >= 4)
                i = 0;

            RECT wndRect;
            GetWindowRect(hWnd, &wndRect);

            //Vector3 camPos = camPos.RotateY(i * 90.0f);

            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            gluPerspective(45.0, (GLdouble)(wndRect.right - wndRect.left) / (wndRect.bottom - wndRect.top), 0.1, 20.0);
        
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
            gluLookAt(camPos.x, camPos.y, camPos.z, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

            glColorMask (1.0f, 1.0f, 1.0f, 1.0f);
            glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            render();

            SwapBuffers (hDC);
            Sleep (1);
        }
    }

    /* shutdown OpenGL */
    DisableOpenGL (hWnd, hDC, hRC);

    /* destroy the window explicitly */
    DestroyWindow (hWnd);

    return msg.wParam;
}


/********************
 * Window Procedure
 *
 ********************/

LRESULT CALLBACK WndProc (HWND hWnd, UINT message,
                          WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
    case WM_CREATE:
        return 0;
    case WM_CLOSE:
        PostQuitMessage (0);
        return 0;

    case WM_DESTROY:
        return 0;

    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_ESCAPE:
            PostQuitMessage(0);
            return 0;
        }
        return 0;

    default:
        return DefWindowProc (hWnd, message, wParam, lParam);
    }
}


/*******************
 * Enable OpenGL
 *
 *******************/

void EnableOpenGL (HWND hWnd, HDC *hDC, HGLRC *hRC)
{
    PIXELFORMATDESCRIPTOR pfd;
    int iFormat;

    /* get the device context (DC) */
    *hDC = GetDC (hWnd);

    /* set the pixel format for the DC */
    ZeroMemory (&pfd, sizeof (pfd));
    pfd.nSize = sizeof (pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | 
      PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;
    iFormat = ChoosePixelFormat (*hDC, &pfd);
    SetPixelFormat (*hDC, iFormat, &pfd);

    /* create and enable the render context (RC) */
    *hRC = wglCreateContext( *hDC );
    wglMakeCurrent( *hDC, *hRC );
}


/******************
 * Disable OpenGL
 *
 ******************/

void DisableOpenGL (HWND hWnd, HDC hDC, HGLRC hRC)
{
    wglMakeCurrent (NULL, NULL);
    wglDeleteContext (hRC);
    ReleaseDC (hWnd, hDC);
}
