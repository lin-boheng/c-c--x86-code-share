#define PI 3.1415926535897932384626433832795

#define Sqrt(x) ((float)__builtin_sqrt(x))
#define Sin(x) ((float)__builtin_sin(x))
#define Cos(x) ((float)__builtin_cos(x))

#define ToRadian(x) (x * 0.017453292519f)
#define ToAngle(x) (x * 57.29577951308f)

class Vector3;
class Quaternion;

class Vector3
{
public:
    float x;
    float y;
    float z;

    static const Vector3 up;
    static const Vector3 down;
    static const Vector3 left;
    static const Vector3 right;
    static const Vector3 forward;
    static const Vector3 back;

    Vector3();
    Vector3(Vector3 &&) = default;
    Vector3(const Vector3 &) = default;
    Vector3 &operator=(Vector3 &&) = default;
    Vector3 &operator=(const Vector3 &) = default;
    ~Vector3();
    Vector3(float, float, float);
    Vector3 operator+(Vector3) const;
    Vector3 operator-(Vector3) const;
    Vector3 operator*(float) const;
    Vector3 operator/(float) const;
    Vector3 &operator+=(Vector3);
    Vector3 &operator-=(Vector3);
    Vector3 &operator*=(float);
    Vector3 &operator/=(float);
    Vector3 operator-() const;
    Vector3 &operator*=(Quaternion);
    Vector3 &operator/=(Quaternion);
    
    static float Dot(Vector3, Vector3);
    static Vector3 Cross(Vector3, Vector3);

    Vector3 Normal() const;
    Vector3 &Normalize();
    float Magnitude() const;
    float SqrMagnitude() const;
    Vector3 Reflect(Vector3) const;
    Vector3 RotateX(float) const;
    Vector3 RotateY(float) const;
    Vector3 RotateZ(float) const;
};

class Quaternion
{
public:
    float x;
    float y;
    float z;
    float w;

    Quaternion();
    Quaternion(Quaternion &&) = default;
    Quaternion(const Quaternion &) = default;
    Quaternion &operator=(Quaternion &&) = default;
    Quaternion &operator=(const Quaternion &) = default;
    ~Quaternion();
    Quaternion(float, float, float, float);
    Quaternion(Vector3, float);
    Quaternion operator*(Quaternion) const;
    Quaternion operator*(float) const;
    Quaternion operator/(Quaternion) const;
    Quaternion operator/(float) const;
    Quaternion &operator*=(Quaternion);
    Quaternion &operator*=(float);
    Quaternion &operator/=(Quaternion);
    Quaternion &operator/=(float);
    Quaternion operator-() const;
    Vector3 operator*(Vector3) const;

    static Quaternion FromTo(Vector3, Vector3);
    static Quaternion Reflection(Vector3, Vector3);
    static Quaternion AxisAngle(Vector3, float);
    static Quaternion EulerZXY(float, float, float);
    static Quaternion EulerZXY(Vector3);

    Quaternion Conjugate() const;
    Quaternion Inverse() const;
    Quaternion Normal() const;
    Quaternion &Normalize();
    float Magnitude() const;
    float SqrMagnitude() const;
    Vector3 GetAxis() const;
};

Vector3::Vector3()
{
}

Vector3::~Vector3()
{
}

Vector3::Vector3(float x, float y, float z):x(x), y(y), z(z)
{
}

const Vector3 Vector3::up       = { 0.0F, 1.0F, 0.0F};
const Vector3 Vector3::down     = { 0.0F,-1.0F, 0.0F};
const Vector3 Vector3::left     = {-1.0F, 0.0F, 0.0F};
const Vector3 Vector3::right    = { 1.0F, 0.0F, 0.0F};
const Vector3 Vector3::forward  = { 0.0F, 0.0F, 1.0F};
const Vector3 Vector3::back     = { 0.0F, 0.0F,-1.0F};

Vector3 Vector3::operator*(float w) const
{
    return Vector3(x * w, y * w, z * w);
}

Vector3 &Vector3::operator*=(float w)
{
    x *= w; y *= w; z *= w;
    return *this;
}

Vector3 Vector3::operator+(Vector3 v) const
{
    return Vector3(x + v.x, y + v.y, z + v.z);
}

Vector3 &Vector3::operator+=(Vector3 v)
{
    x += v.x; y += v.y; z += v.z;
    return *this;
}

Vector3 Vector3::operator-() const
{
    return Vector3(-x, -y, -z);
}

Vector3 Vector3::operator-(Vector3 v) const
{
    return Vector3(x - v.x, y - v.y, z - v.z);
}

Vector3 &Vector3::operator-=(Vector3 v)
{
    x -= v.x; y -= v.y; z -= v.z;
    return *this;
}

Vector3 Vector3::operator/(float w) const
{
    w = 1.0F / w;
    return Vector3(x * w, y * w, z * w);
}

Vector3 &Vector3::operator/=(float w)
{
    w = 1.0F / w;
    x *= w; y *= w; z *= w;
    return *this;
}

Vector3 &Vector3::operator*=(Quaternion q)
{
    return (*this = q * *this);
}

Vector3 &Vector3::operator/=(Quaternion q)
{
    return (*this = q.Inverse() * *this);
}

Vector3 Vector3::Cross(Vector3 v1, Vector3 v2)
{
    return Vector3( v1.y * v2.z - v1.z * v2.y,
                    v1.z * v2.x - v1.x * v2.z,
                    v1.x * v2.y - v1.y * v2.x);
}

float Vector3::Dot(Vector3 v1, Vector3 v2)
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

float Vector3::Magnitude() const
{
    return Sqrt(x * x + y * y + z * z);
}

float Vector3::SqrMagnitude() const
{
    return x * x + y * y + z * z;
}

Vector3 Vector3::Normal() const
{
    return *this / this->Magnitude();
}

Vector3 &Vector3::Normalize()
{
    return (*this /= this->Magnitude());
}

Vector3 Vector3::Reflect(Vector3 v) const
{
    return Quaternion(Normal(), 0.0F) / Quaternion(v.Normal(), 0.0F) * v;
}

Vector3 Vector3::RotateX(float angle) const
{
    float vsin, vcos;
    __builtin_sincosf(angle * 0.01745329251994329547f, &vsin, &vcos);
    return Vector3(x, z * vsin + y * vcos, z * vcos - y * vsin);
}

Vector3 Vector3::RotateY(float angle) const
{
    float vsin, vcos;
    __builtin_sincosf(angle * 0.01745329251994329547f, &vsin, &vcos);
    return Vector3(z * vsin + x * vcos, y, z * vcos - x * vsin);
}

Vector3 Vector3::RotateZ(float angle) const
{
    float vsin, vcos;
    __builtin_sincosf(angle * 0.01745329251994329547f, &vsin, &vcos);
    return Vector3(x * vcos - y * vsin, x * vsin + y * vcos, z);
}

Quaternion::Quaternion()
{
}

Quaternion::~Quaternion()
{
}

Quaternion::Quaternion(float x, float y, float z, float w):x(x), y(y), z(z), w(w)
{
}

Quaternion::Quaternion(Vector3 v, float w):x(v.x), y(v.y), z(v.z), w(w)
{
}

Quaternion Quaternion::operator*(Quaternion q) const
{
    return Quaternion(  w * q.x + x * q.w + y * q.z - z * q.y,
                        w * q.y - x * q.z + y * q.w + z * q.x,
                        w * q.z + x * q.y - y * q.x + z * q.w,
                        w * q.w - x * q.x - y * q.y - z * q.z);//(w,z,-y,-x)*(-x,-y,-z,w)=(x2+w2-z2-y2,xy+wz-yw+yx,xz-wy+zx-yw,0)
}

Quaternion Quaternion::operator*(float s) const
{
    return Quaternion(x * s, y * s, z * s, w * s);
}

Quaternion Quaternion::operator/(Quaternion q) const
{
    return *this * q.Inverse();
}

Quaternion Quaternion::operator/(float s) const
{
    s = 1.0F / s;
    return Quaternion(x * s, y * s, z * s, w * s);
}

Quaternion &Quaternion::operator*=(Quaternion q)
{
    return (*this = *this * q);
}

Quaternion &Quaternion::operator*=(float s)
{
    x *= s; y *= s; z *= s; w *= s;
    return *this;
}

Quaternion &Quaternion::operator/=(Quaternion q)
{
    return (*this *= *this * q.Inverse());
}

Quaternion &Quaternion::operator/=(float s)
{
    s = 1.0F / s;
    x *= s; y *= s; z *= s; w *= s;
    return *this;
}

Quaternion Quaternion::operator-() const
{
    return Quaternion(-x, -y, -z, -w);
}

Vector3 Quaternion::operator*(Vector3 v) const
{
    return (*this * Quaternion(v, 0.0F) * this->Inverse()).GetAxis();
}

Quaternion Quaternion::FromTo(Vector3 from, Vector3 to)
{
    from.Normalize();
    to.Normalize();
    return Quaternion((from + to).Normal(), 0.0F) / Quaternion(from, 0.0F);
}

Quaternion Quaternion::Reflection(Vector3 axis, Vector3 v)
{
    return Quaternion(axis.Normal(), 0.0F) / Quaternion(v.Normal(), 0.0F);
}

Quaternion Quaternion::AxisAngle(Vector3 axis, float angle)
{
    angle *= 0.00872664625997164788461845384244F;
    return Quaternion(axis.Normal() * Sin(angle), Cos(angle));
}

Quaternion Quaternion::EulerZXY(Vector3 angle)
{
    return  Quaternion::AxisAngle(Vector3::up, angle.y) *
            Quaternion::AxisAngle(Vector3::right, angle.x) *
            Quaternion::AxisAngle(Vector3::forward, angle.z);
}

Quaternion Quaternion::EulerZXY(float x, float y, float z)
{
    return  Quaternion::AxisAngle(Vector3::up, y) *
            Quaternion::AxisAngle(Vector3::right, x) *
            Quaternion::AxisAngle(Vector3::forward, z);
}

Quaternion Quaternion::Conjugate() const
{
    return Quaternion(-x, -y, -z, w);
}

Quaternion Quaternion::Inverse() const
{
    return Quaternion(-x, -y, -z, w) / this->SqrMagnitude();
}

Quaternion Quaternion::Normal() const
{
    return *this / this->Magnitude();
}

Quaternion &Quaternion::Normalize()
{
    return (*this /= this->Magnitude());
}

float Quaternion::Magnitude() const
{
    return Sqrt(x * x + y * y + z * z + w * w);
}

float Quaternion::SqrMagnitude() const
{
    return x * x + y * y + z * z + w * w;
}

Vector3 Quaternion::GetAxis() const
{
    return Vector3(x, y, z);
}
