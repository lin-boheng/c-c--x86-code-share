﻿Shader "Unlit/Test"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="AlphaTest" }
		LOD 100

		Pass
		{
            ZWrite Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD1;
                float4 worldPos : TEXCOORD2;
                float3 normal : NORMAL;
			};

			sampler2D _MainTex;
            float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
                o.worldPos = v.vertex;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
                o.normal = v.normal;
                o.screenPos = ComputeScreenPos(o.vertex);
                COMPUTE_EYEDEPTH(o.screenPos.z);
				return o;
			}

            float4 rotation(float3 v, float rot){
                return float4(cos(rot * 0.5), v * sin(rot * 0.5));
            }

            float4 mul(float4 q1, float4 q2){
                return float4(q1.w * q2.x + q1.x * q2.w + q1.y * q2.z - q1.z * q2.y,
                              q1.w * q2.y - q1.x * q2.z + q1.y * q2.w + q1.z * q2.x,
                              q1.w * q2.z + q1.x * q2.y - q1.y * q2.x + q1.z * q2.w,
                              q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z);
            }

            float3 rotate(float4 quat, float3 v){
                quat = normalize(quat);
                return mul(quat, mul(float4(0, v), float4(quat.x, -quat.yzw))).yzw;
            }
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
                float3 look = i.worldPos - _WorldSpaceCameraPos;
                float3 refl = reflect(-look, i.normal);
                refl = rotate(rotation(i.normal, sin(_Time.w) * 0.5), refl);
                col = col * 0.5 + UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, refl) * 0.5;
                float tmp;
                tmp = saturate((i.worldPos.z - i.screenPos.z + 5) * 0.2);
                col.r += min(tmp, 1.0 - tmp);
                tmp = saturate((i.worldPos.z - i.screenPos.z + 10) * 0.2);
                col.g += min(tmp, 1.0 - tmp);
                tmp = saturate((i.worldPos.z - i.screenPos.z + 15) * 0.2);
                col.b += min(tmp, 1.0 - tmp);
                col.a = 1;
				return col;
			}
			ENDCG
		}
	}
}
