#include <jni.h>
#include <windows.h>

#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glext.h>
#include <gl/wgl.h>

#include "MathUtils.h"

struct Transform {
    Vector3 pos;
    Quaternion rot;
    Vector3 scale;
};

LRESULT CALLBACK WndProc(HWND hWnd, UINT message,
                         WPARAM wParam, LPARAM lParam);
void EnableOpenGL(HWND hWnd, HDC *hDC, HGLRC *hRC);
void DisableOpenGL(HWND hWnd, HDC hDC, HGLRC hRC);
ATOM InitWindowClass();
void RenderFrame(HDC hDC);

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int iCmdShow)
{
    HWND hWnd;
    HDC hDC;
    HGLRC hRC;
    MSG msg;
    BOOL bQuit = FALSE;
    JavaVM* jvm;
    JNIEnv* jenv;
    JavaVMInitArgs jargs;
    JavaVMOption options[1];

    InitWindowClass();

    /* create main window */
    hWnd = CreateWindow(
        TEXT("GLSample"), TEXT("OpenGL Sample"),
        WS_CAPTION | WS_POPUPWINDOW | WS_VISIBLE,
        0, 0, 256, 256,
        NULL, NULL, hInstance, NULL);

    EnableOpenGL(hWnd, &hDC, &hRC);
    glEnable(GL_DEPTH_TEST);

    // options[0].extraInfo = NULL;
    // options[0].optionString = const_cast<char*>("-Djava.class.path=.");
    // jargs.version = 1;
    // jargs.ignoreUnrecognized = TRUE;
    // jargs.nOptions = 1;
    // jargs.options = options;
    // JNI_CreateJavaVM(&jvm, (void**)&jenv, &jargs);

    while (!bQuit)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                bQuit = TRUE;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            RenderFrame(hDC);
        }
    }
    DisableOpenGL(hWnd, hDC, hRC);
    DestroyWindow(hWnd);
    return msg.wParam;
}

ATOM InitWindowClass()
{
    WNDCLASS wc;
    wc.style = CS_OWNDC;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = GetModuleHandle(NULL);
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = TEXT("GLSample");
    return RegisterClass(&wc);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message,
                         WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:
        return 0;
    case WM_CLOSE:
        PostQuitMessage(0);
        return 0;

    case WM_DESTROY:
        return 0;

    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_ESCAPE:
            PostQuitMessage(0);
            return 0;
        }
        return 0;

    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
}

static inline void GetRotatedVector3(Vector3 &v, float angle){
    __builtin_sincosf(angle * 0.017453292f, &v.y, &v.x);
    v.z = 0.0f;
}

static inline void glRotateq(Quaternion q){
    Vector3 v = q.GetAxis().Normalize();
    glRotatef(__builtin_acos(q.w) * 114.59155902f, v.x, v.y, v.z);
}

static inline void glVertex3v(Vector3 v){
    glVertex3f(v.x, v.y, v.z);
}

static inline void glTransform(Transform& trans){
    glRotateq(trans.rot);
    glScalef(trans.scale.x, trans.scale.y, trans.scale.z);
    glTranslatef(trans.pos.x, trans.pos.y, trans.pos.z);
}

void RenderFrame(HDC hDC)
{
    static float theta = 0.0f;

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);

    glPushMatrix();
    //OpenGL Viewport Z[-1,1]
    //Projection (x,y)*=z
    glPushMatrix();
    //Camera View
    glRotatef(theta * 0.1f, 0.0f, 1.0f, 0.0f);
    glTranslatef(0.0f, 0.0f, 1.0f);//cam 0.0 0.0 -1.0
    glPushMatrix();
    //World Space
    glRotatef(theta, 0.0f, 0.0f, 1.0f);
    glBegin(GL_TRIANGLES);
    //Local Space
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3v(Vector3::up.RotateZ(0.0f));
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3v(Vector3::up.RotateZ(120.0f));
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3v(Vector3::up.RotateZ(240.0f));
    glEnd();
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();

    SwapBuffers(hDC);

    theta += 1.0f;
    Sleep(1);
}

void EnableOpenGL(HWND hWnd, HDC *hDC, HGLRC *hRC)
{
    PIXELFORMATDESCRIPTOR pfd;
    int iFormat;

    *hDC = GetDC(hWnd);

    ZeroMemory(&pfd, sizeof(pfd));
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW |
                  PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;
    iFormat = ChoosePixelFormat(*hDC, &pfd);
    SetPixelFormat(*hDC, iFormat, &pfd);

    *hRC = wglCreateContext(*hDC);
    wglMakeCurrent(*hDC, *hRC);
}

void DisableOpenGL(HWND hWnd, HDC hDC, HGLRC hRC)
{
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(hRC);
    ReleaseDC(hWnd, hDC);
}
