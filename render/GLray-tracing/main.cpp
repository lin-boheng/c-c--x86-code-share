#include <windows.h>

#include <gl/gl.h>

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
    switch (uMsg)
    {
    case WM_CREATE:
        return 0;
    case WM_CLOSE:
        PostQuitMessage(0);
        return 0;
    case WM_QUIT:
        return 0;
    case WM_DESTROY:
        return 0;
    case WM_KEYDOWN:
        switch (wParam){
        case VK_ESCAPE:
            PostQuitMessage(0);
            return 0;
        }
        return 0;
    }
    return DefWindowProcA(hWnd, uMsg, wParam, lParam);
}

ATOM regClass(HINSTANCE hInstance){
    WNDCLASSEXA wc;

    wc.cbSize = sizeof(WNDCLASSEXA);
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wc.hCursor = LoadCursorA(NULL, IDC_ARROW);
    wc.hIcon = LoadIconA(NULL, IDI_APPLICATION);
    wc.hIconSm = LoadIconA(NULL, IDI_APPLICATION);
    wc.hInstance = hInstance;
    wc.lpfnWndProc = WndProc;
    wc.lpszClassName = "OpenGL RT Test";
    wc.lpszMenuName = "";
    wc.style = 0;

    return RegisterClassExA(&wc);
}

void EnableOpenGL(HWND hWnd, HDC* hDC, HGLRC* hRC){
    PIXELFORMATDESCRIPTOR pfd;

    *hDC = GetDC(hWnd);

    RtlZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iLayerType = PFD_MAIN_PLANE;

    int iFormat = ChoosePixelFormat(*hDC, &pfd);
    SetPixelFormat(*hDC, iFormat, &pfd);

    *hRC = wglCreateContext(*hDC);
    wglMakeCurrent(*hDC, *hRC);
}

void DisableOpenGL(HWND hWnd, HDC hDC, HGLRC hRC){
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(hRC);
    ReleaseDC(hWnd, hDC);
}

//#include "MathUtils.h"
#include "Shapes.h"

class TargetObject {
public:
    float rate = 1.0F;

    virtual bool RayCast(Vector3 ori, Vector3 dir, Vector3& result) = 0;
};

class Ball : TargetObject {
public:
    Vector3 pos;
    float r;

    Ball(Vector3 pos, float r) : pos(pos), r(r) {}

    virtual bool RayCast(Vector3 ori, Vector3 dir, Vector3& result){
        Vector3 rel = pos - ori;
        float val = Vector3::Cosine(rel, dir);
        float val2 = Sqrt(1.0F - val * val);
        float dis = rel.Magnitude() * val2;
        if (dis > r){
            return false;
        }
        float tar = Vector3::Dot(rel, dir) / dir.SqrMagnitude();
        float off = Sqrt(r * r - dis * dis);
        if (tar <= -off){
            return false;
        }
        if (tar <= off){
            result = dir.Normal() * (tar + off);
            return true;
        }
        result = dir.Normal() * (tar - off);
        return true;
    }

    Vector3 Normal(Vector3 p){
        return (p - pos) / r;
    }
};

Ball test(Vector3(-2.0, 0.0, 2.0), 1.4);
Ball test2(Vector3(2.0, -0.8, 2.0), 1.0);
Ball test3(Vector3(0.5, 1.0, 2.8), 0.8);
Ball test4(Vector3(-1.5, 1.5, 1.0), 0.1);

float Clamp(float x, float Min, float Max){
    if (x >= Max){
        return Max;
    }
    if (x <= Min){
        return Min;
    }
    return x;
}

Vector3 Clamp(Vector3 v, float Min, float Max){
    return Vector3(Clamp(v.x, Min, Max), Clamp(v.y, Min, Max), Clamp(v.z, Min, Max));
}

float Fresnel(Vector3 dir, Vector3 n){
    return Pow(1.0F - Abs(Vector3::Cosine(-dir, n)), 5.0F);
}

Vector3 GetCubeColor(Vector3 dir){
    dir.Normalize();
    Vector3 col(Vector3(0.0F, 0.3F, 1.0F) * Clamp(dir.y + 0.5F, 0.0F, 1.0F));
    if (Vector3::Cosine(dir, Vector3(-0.5F, 0.2F, 0.8F)) > 0.97F){
        col += Vector3(1.0F, 0.7F, 0.0F);
    }
    return col;
}

Vector3 GetColor(Vector3 ori, Vector3 dir, unsigned int step){
    Vector3 res;
    if (step >= 5){
        return GetCubeColor(dir);
    }
    if (test.RayCast(ori, dir, res)){
        Vector3 p = ori + res;
        Vector3 n = test.Normal(p);
        float num = Fresnel(dir, n);
        if (Vector3::Dot(dir, n) <= 0.0F){
            return GetColor(p, dir.Reflect(n), step + 1) * (0.2F + num * 0.6F) + GetColor(p, dir.Refract(n, 2.0F), step + 1) * ((1.0F - num) * 0.6F);
        } else {
            return Vector3(0.05F, 0.05F, 0.05F) + GetColor(p, dir.Refract(n, 2.0F), step + 1) * ((1.0F - num) * 0.6F);
        }
    }
    if (test2.RayCast(ori, dir, res)){
        Vector3 p = ori + res;
        Vector3 n = test2.Normal(p);
        float num = Fresnel(dir, n);
        return Vector3(0.2F, 0.2F, 0.2F) + GetColor(p, dir.Reflect(n), step + 1) * (0.8F + num * 0.2F);
    }
    if (test3.RayCast(ori, dir, res)){
        Vector3 p = ori + res;
        Vector3 n = test3.Normal(p);
        float num = Fresnel(dir, n);
        return Vector3(0.2F, 0.2F, 0.2F) + GetColor(p, dir.Reflect(n), step + 1) * (0.8F + num * 0.2F);
    }
    if (test4.RayCast(ori, dir, res)){
        Vector3 p = ori + res;
        Vector3 n = test4.Normal(p);
        float num = Fresnel(dir, n);
        return Vector3(2.0F, 2.0F, 1.5F);
    }
    return GetCubeColor(dir);
}

struct Byte4 {
    unsigned char r, g, b, a;
};

static Byte4 texture[1 << 18];

inline ColorRGB Shade(float u, float v){
    int x = (int)(u * 512.0F) & 0x1FF;
    int y = (int)(v * 512.0F) & 0x1FF;
    int index = (y << 9) | x;
    return Vector3(1.0F - texture[index].g / 255.0F, texture[index].b / 255.0F, texture[index].r / 255.0F);
}

inline bool TraceTri(Point3D ori, Vec3D dir, float& ou, float& ov, Point3D a, Point3D b, Point3D c){
    Vec3D vec0 = b - a;
    Vec3D vec1 = c - a;
    Vec3D tar = ori - a;
    int div = det(vec0, vec1, -dir);
    if(div == 0){
        return false;
    }
    int u, v, t;
    if(div > 0){
        u = det(tar, vec1, -dir);
        if(u < 0 || u > div){
            return false;
        }
        v = det(vec0, tar, -dir);
        if(v < 0 || u + v > div){
            return false;
        }
        t = det(vec0, vec1, tar);
        if(t <= 0){
            return false;
        }
    }else{
        u = det(tar, vec1, -dir);
        if(u > 0 || u < div){
            return false;
        }
        v = det(vec0, tar, -dir);
        if(v > 0 || u + v < div){
            return false;
        }
        t = det(vec0, vec1, tar);
        if(t >= 0){
            return false;
        }
    }
    ou = (float)u / div;
    ov = (float)v / div;
    return true;
}

inline ColorRGB GetColor2(Point3D ori, Vec3D dir){
    float u, v;
    if(TraceTri(ori, dir, u, v, Point3D(-500, 0, 512), Point3D(0, -500, 512), Point3D(0, 500, 512))){
        return Shade(u, v);
    }
    return GetCubeColor(Vector3(dir.x, dir.y, dir.z));
}

void draw(){
    glColor3f(1.0F, 1.0F, 1.0F);

    static float angle = 0.0f; angle += 0.1f;

    Vector3 pos(0.0, 0.0, -2.0);
    glBegin(GL_POINTS);
    for (double i = -1.0; i < 1.0; i += 0.00390625){
        for (double j = -1.0; j < 1.0; j += 0.00390625){
            Vector3 ret = GetColor(pos, Vector3(i, j, 1.0).RotateY(Sin(angle)), 0);
            glColor3f(ret.x, ret.y, ret.z);
            glVertex2f(i, j);
        }
    }
    glEnd();
}

void RenderInt(){
    static int delta = 0; delta++;
    int val = (((delta / 100) & 1) ? 100 - delta % 100 : delta % 100) - 50;
    for (int i = -256; i < 256; i++){
        for (int j = -256; j < 256; j++){
            ColorRGB col = GetColor2(Point3D(0, 0, 0), Vec3D(i + val, j, 256));
            int pos = ((i + 256) << 9) | (j + 256);
            texture[pos].b = Clamp(col.z, 0.0F, 1.0F) * 255;
            texture[pos].g = Clamp(col.y, 0.0F, 1.0F) * 255;
            texture[pos].r = Clamp(col.x, 0.0F, 1.0F) * 255;
        }
    }
}

void drawInt(){
    RenderInt();
    static int delta = 0; delta++;
    int val = (((delta / 100) & 1) ? 100 - delta % 100 : delta % 100) - 50;
    glBegin(GL_POINTS);
    for (int i = -256; i < 256; i++){
        for (int j = -256; j < 256; j++){
            ColorRGB col = GetColor2(Point3D(0, 0, 0), Vec3D(i + val, j, 256));
            glColor3f(col.x, col.y, col.z);
            glVertex2f(i * 0.00390625f, j * 0.00390625f);
        }
    }
    glEnd();
}

double GetTime(){
    LARGE_INTEGER time;
    LARGE_INTEGER freq;
    QueryPerformanceCounter(&time);
    QueryPerformanceFrequency(&freq);
    return (double)time.QuadPart / freq.QuadPart;
}

void RenderToBitmap(const char* file, int resolution){
    BITMAPFILEHEADER fileHdr;
    fileHdr.bfSize = sizeof(BITMAPFILEHEADER);
    fileHdr.bfType = 'B' + ('M' << 8);
    fileHdr.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
    fileHdr.bfReserved1 = 0;
    fileHdr.bfReserved2 = 0;
    BITMAPINFOHEADER hdr;
    hdr.biBitCount = 24;
    hdr.biClrImportant = 0;
    hdr.biClrUsed = 0;
    hdr.biCompression = FALSE;
    hdr.biHeight = resolution << 1;
    hdr.biWidth = resolution << 1;
    hdr.biPlanes = 1;
    hdr.biSize = sizeof(BITMAPINFOHEADER);
    hdr.biSizeImage = resolution * resolution * 12;
    hdr.biXPelsPerMeter = 0;
    hdr.biYPelsPerMeter = 0;
    HANDLE hFile = CreateFileA(
        file,
        FILE_GENERIC_READ | FILE_GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL,
        NULL
    );
    WriteFile(hFile, &fileHdr, sizeof(BITMAPFILEHEADER), NULL, NULL);
    WriteFile(hFile, &hdr, sizeof(BITMAPINFOHEADER), NULL, NULL);
    Vector3 pos(0.0, 0.0, -2.0);
    static unsigned char* bitmap;
    bitmap = new unsigned char[resolution * resolution * 12];
    unsigned int p = 0;
    for (int i = -resolution; i < resolution; i++){
        for (int j = -resolution; j < resolution; j++){
            Vector3 ret = GetColor(pos, Vector3(j / (double)resolution, i / (double)resolution, 1.0), 0);
            bitmap[p++] = Clamp(ret.z, 0.0F, 1.0F) * 255;
            bitmap[p++] = Clamp(ret.y, 0.0F, 1.0F) * 255;
            bitmap[p++] = Clamp(ret.x, 0.0F, 1.0F) * 255;
        }
    }
    WriteFile(hFile, bitmap, resolution * resolution * 12, NULL, NULL);
    delete bitmap;
    CloseHandle(hFile);
}

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
    HWND hWnd;
    HDC hDC;
    HGLRC hRC;
    MSG Msg;

    regClass(hInstance);

    hWnd = CreateWindowExA(
        0,
        "OpenGL RT Test",
        "OpenGL Test",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT,
        512, 512,
        NULL,
        NULL,
        hInstance,
        NULL
    );

    ShowWindow(hWnd, SW_SHOW);
    UpdateWindow(hWnd);

    EnableOpenGL(hWnd, &hDC, &hRC);

    glEnable(GL_DEPTH_TEST);

    while (TRUE){
        if (PeekMessageA(&Msg, NULL, 0, 0, PM_REMOVE)){
            if (Msg.message == WM_QUIT){
                break;
            } else {
                TranslateMessage(&Msg);
                DispatchMessageA(&Msg);
            }
        } else {
            // static bool drawed = false;
            // if (drawed){
            //     Sleep(1);
            //     continue;
            // }

            //double st = GetTime();
            
            glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glPushMatrix();

            drawInt();

            glPopMatrix();

            SwapBuffers(hDC);
            //drawed = true;

            //RenderToBitmap("HD.bmp", 4096);

            // char s[20];
            // wsprintfA(s, "%d", (int)((GetTime() - st) * 1e9));
            // MessageBoxA(hWnd, s, "Time", MB_OK);
        }
    }

    DisableOpenGL(hWnd, hDC, hRC);

    DestroyWindow(hWnd);
    
    return Msg.wParam;
}