#include "MathUtils.h"

#include <fstream>
#include <stdexcept>
#include <cstring>
#include <vector>

class Object3D {
public:
    struct Fragment {
        int vertex[3];
        int texCoord[3];
        int normal[3];
        bool hasTexCoord;
        bool hasNormal;
    };
private:
    std::vector<Quaternion> Vertices;
    std::vector<Vector3> TexCoords;
    std::vector<Vector3> Normals;
    std::vector<Fragment> Fragments;
public:
    Object3D(){}

    Object3D(const char* filename){
        Load(filename);
    }

    ~Object3D(){}

    void Load(const char* filename){
        std::ifstream file(filename);
        char line[MAX_PATH + 1];
        char clip[3][40];
        if(!file)
            throw std::runtime_error("File Not Found!");
        while(!file.eof()){
            file.getline(line, MAX_PATH);
            if(*line == '#' || *line == '\0')
                continue;
            if(!strncmp(line, "v ", 2)){
                Quaternion v;
                int n = sscanf(line + 2, "%f %f %f %f", &v.x, &v.y, &v.z, &v.w);
                if(n == 3){
                    v.w = 1.0f;
                }else if(n != 4)
                    throw std::runtime_error("Vertex Format Error!");
                Vertices.push_back(v);
            }else if(!strncmp(line, "vt ", 3)){
                Vector3 c;
                int n = sscanf(line + 3, "%f %f %f", &c.x, &c.y, &c.z);
                if(n == 2){
                    c.z = 0.0f;
                }else if(n != 3)
                    throw std::runtime_error("Texture Coordination Format Error!");
                TexCoords.push_back(c);
            }else if(!strncmp(line, "vn ", 3)){
                Vector3 v;
                int n = sscanf(line + 3, "%f %f %f", &v.x, &v.y, &v.z);
                if(n != 3)
                    throw std::runtime_error("Normal Format Error!");
                Normals.push_back(v);
            }else if(!strncmp(line, "f ", 2)){
                Fragment f = {};
                if(sscanf(line + 2, "%s %s %s", clip[0], clip[1], clip[2]) != 3)
                    throw std::runtime_error("Fragment Format Error!");
                char* p = strstr(clip[0], "//");
                if(p){
                    f.hasTexCoord = false;
                    f.hasNormal = true;
                    for(int i = 0; i < 3; i++)
                        if(sscanf(clip[i], "%d//%d", f.vertex + i, f.normal + i) != 2)
                            throw std::runtime_error("Fragment [v//vn] Format Error!");
                }else{
                    p = strstr(clip[0], "/");
                    if(!p){
                        f.hasTexCoord = false;
                        f.hasNormal = false;
                        for(int i = 0; i < 3; i++)
                            if(sscanf(clip[i], "%d", f.vertex + i) != 1)
                                throw std::runtime_error("Fragment [v] Format Error!");
                    }else if(strstr(p + 1, "/")){
                        f.hasTexCoord = true;
                        f.hasNormal = true;
                        for(int i = 0; i < 3; i++)
                            if(sscanf(clip[i], "%d/%d/%d", f.vertex + i, f.texCoord + i, f.normal + i) != 3)
                                throw std::runtime_error("Fragment [v/vt/vn] Format Error!");
                    }else{
                        f.hasTexCoord = true;
                        f.hasNormal = false;
                        for(int i = 0; i < 3; i++)
                            if(sscanf(clip[i], "%d/%d", f.vertex + i, f.texCoord + i) != 2)
                                throw std::runtime_error("Fragment [v/vt] Format Error!");
                    }
                }
                Fragments.push_back(f);
            }else if(!strncmp(line, "o ", 2)){
                std::string name = line + 2;
                continue;
            }else if(!strncmp(line, "s ", 2)){
                continue;
            }else{
                std::string err = "Unrecognized Sign: ";
                err += line;
                throw std::runtime_error(err);
            }
        }
    }

    void Store(const char* filename){

    }

    void Draw(){
        glBegin(GL_TRIANGLES);
        for(Fragment f : Fragments){
            if(f.hasNormal){
                int index = (f.normal[0] < 0 ? Normals.size() + f.normal[0] : f.normal[0] - 1);
                glNormal3f(Normals[index].x, Normals[index].y, Normals[index].z);
            }
            for(int i = 0; i < 3; i++){
                int index = (f.vertex[i] < 0 ? Vertices.size() + f.vertex[i] : f.vertex[i] - 1);
                glVertex4f(Vertices[index].x, Vertices[index].y, Vertices[index].z, Vertices[index].w);
            }
        }
        glEnd();
    }
};

typedef struct Point3D {
    int x;
    int y;
    int z;

    Point3D(){}
    Point3D(int x, int y, int z) : x(x), y(y), z(z) {}
} Vec3D;

Point3D operator+(Point3D a, Point3D b){
    return Point3D(a.x + b.x, a.y + b.y, a.z + b.z);
}

Point3D operator-(Point3D a, Point3D b){
    return Point3D(a.x - b.x, a.y - b.y, a.z - b.z);
}

Point3D operator-(Point3D a){
    return Point3D(-a.x, -a.y, -a.z);
}

typedef Vector3 ColorRGB;

typedef struct {
    float u, v;
} TexCoord2D;

class BasicShape {
public:
    virtual bool intersect(Point3D ori, Vec3D dir, TexCoord2D& result) = 0;
    virtual bool belongTo(int x, int y, int z, int size) = 0;
};

// trace bresenham
// sx, sy, sz += max(x, y, z)

void Trace(Point3D ori, Vec3D dir){
    int mode = (dir.y > dir.x ? (dir.z > dir.y ? 2 : 1) : (dir.z > dir.x ? 2 : 0));
    int sx = 0, sy = 0, sz = 0;// int3
    int delta = (mode == 0 ? dir.x : (mode == 1 ? dir.y : dir.z));
    // tar - ori.x
    // 1 << n ...
    // box 0 0 0 1<<n 1<<n 1<<n
}

class Octree {
private:
    struct Domain {
        bool hasChild;
        Domain* children[8];
        std::vector<BasicShape*> shapes;
    };

    Domain* root;
    int size;

public:
    Octree(){
        size = 1;
        root = new Domain;
        root->hasChild = false;
    }
    
    ~Octree(){}

    void div(Domain* cur, int x, int y, int z, int s){
        while (cur->shapes.size() >= 4 && s > 0){
            std::vector<BasicShape*> rem;
            for (int j = 0; j < cur->shapes.size(); j++){
                for (int i = 0; i < 8; i++){
                    if (cur->shapes[i]->belongTo(x + (i & 0b001 ? s : 0), y + (i & 0b010 ? s : 0), z + (i & 0b100 ? s : 0), ~(size - 1))){
                        x += (i & 0b001 ? s : 0);
                        y += (i & 0b010 ? s : 0);
                        z += (i & 0b100 ? s : 0);
                        s >>= 1;
                        if (!cur->hasChild){
                            cur->hasChild = true;
                            for (int i = 0; i < 0b1000; i++){
                                cur->children[i] = new Domain;
                            }
                        }
                        cur->children[i]->shapes.push_back(cur->shapes[j]);
                        if (j == cur->shapes.size() - 1){
                            cur = cur->children[i];
                        }
                        goto conti2;
                    }
                }
                rem.push_back(cur->shapes[j]);
                if (j == cur->shapes.size() - 1){
                    return;
                }
                conti2:;
            }
            cur->shapes.clear();
            for (int i = 0; i < rem.size(); i++){
                cur->shapes.push_back(rem[i]);
            }
        }
    }

    void add(BasicShape* shape){
        while (!shape->belongTo(0, 0, 0, ~(size - 1))){
            size <<= 1;
            if (root->hasChild){
                Domain* rt = new Domain;
                rt->hasChild = true;
                rt->children[0b000] = root;
                root = rt;
                for (int i = 0b001; i < 0b1000; i++){
                    root->children[i] = new Domain;
                    root->children[i]->hasChild = false;
                }
            }
        }
        int x = 0, y = 0, z = 0;
        int s = size >> 1;
        Domain* cur = root;
        while (true){
            conti:;
            if (cur->hasChild){
                for (int i = 0; i < 8; i++){
                    if (shape->belongTo(x + (i & 0b001 ? s : 0), y + (i & 0b010 ? s : 0), z + (i & 0b100 ? s : 0), ~(size - 1))){
                        x += (i & 0b001 ? s : 0);
                        y += (i & 0b010 ? s : 0);
                        z += (i & 0b100 ? s : 0);
                        s >>= 1;
                        cur = cur->children[i];
                        goto conti;
                    }
                }
            }
            cur->shapes.push_back(shape);
            div(cur, x, y, z, s);
            break;
        }
    }

    void intersect(int x, int y, int z, int dx, int dy, int dz){
        int mode = 0;
        if (dy > dx && dy > dz){
            mode = 1;
        } else if(dz > dx && dz > dy){
            mode = 2;
        }

    }
};

inline int det(Vec3D a, Vec3D b, Vec3D c){
    return a.x * (b.y * c.z - b.z * c.y)
            + a.y * (b.z * c.x - b.x * c.z)
            + a.z * (b.x * c.y - b.y * c.x);
}

class Triangle : public BasicShape {
public:
    Point3D a, b, c;

    Triangle(Point3D a, Point3D b, Point3D c) : a(a), b(b), c(c) {}

    virtual bool intersect(Point3D ori, Vec3D dir, TexCoord2D& result){
        Vec3D tar = ori - a;
        Vec3D vec0 = b - a;
        Vec3D vec1 = c - a;
        int div = det(v1, v2, dir);
        if (!div){
            return false;
        }
        int u, v, t;
        if(div > 0){
            u = det(tar, vec1, -dir);
            if(u < 0 || u > div){
                return false;
            }
            v = det(vec0, tar, -dir);
            if(v < 0 || u + v > div){
                return false;
            }
            t = det(vec0, vec1, tar);
            if(t <= 0){
                return false;
            }
        }else{
            u = det(tar, vec1, -dir);
            if(u > 0 || u < div){
                return false;
            }
            v = det(vec0, tar, -dir);
            if(v > 0 || u + v < div){
                return false;
            }
            t = det(vec0, vec1, tar);
            if(t >= 0){
                return false;
            }
        }
        result.u = (float)u / div;
        result.v = (float)v / div;
        return true;
    }

    virtual bool belongTo(int x, int y, int z, int size){
        return (a.x & size) == x && (a.y & size) == y && (a.z & size) == z &&
                (b.x & size) == x && (b.y & size) == y && (b.z & size) == z &&
                (c.x & size) == x && (c.y & size) == y && (c.z & size) == z;
    }
};