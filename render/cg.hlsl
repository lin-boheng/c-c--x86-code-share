struct INPUT{
    float2 pos : SV_POSITION;
    float4 col0 : COLOR0;
    float2 uv_Tex : TEXCOORD0;
};

float4 pixel(INPUT i) : COLOR0 {
    float4 col = float4(0.5, 0.5, 0.5, 1.0);
    float2 coord = float2(i.pos.x - 400.0, i.pos.y - 300.0);
    //coord *= 0.005;
    //coord = saturate(coord + float2(0.5, 0.5));
    col.rg += sin(coord.xy * 0.1) * 0.5;
    return col * i.col0;
}
