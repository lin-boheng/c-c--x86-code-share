﻿Shader "Unlit/sea"
{
    Properties
    {
        _ColorOffset("Color Offset", Range(1.0, 2.0)) = 1.5
        _WaveHeight("Wave Height", Range(0.0, 20.0)) = 1.0
        _WaveFrequency("Wave Frequency", Range(0.0, 10.0)) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;							
            };

            struct v2f
            {
                UNITY_FOG_COORDS(3)
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD1;
                float3 worldPos : TEXCOORD2;
            };

            float _ColorOffset;
            float _WaveHeight;
            float _WaveFrequency;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;	
                o.screenPos = ComputeScreenPos(o.vertex);
                COMPUTE_EYEDEPTH(o.screenPos.z);
                return o;
            }
            //利用cos生成的渐变色，使用网站：https://sp4ghet.github.io/grad/
            fixed4 cosine_gradient(float x,  fixed4 phase, fixed4 amp, fixed4 freq, fixed4 offset){
                const float TAU = 2. * 3.14159265 * _WaveFrequency;
                phase *= TAU;
                x *= TAU;

                return fixed4(
                    offset.r + amp.r * 0.5 * cos(x * freq.r + phase.r) + 0.5,
                    offset.g + amp.g * 0.5 * cos(x * freq.g + phase.g) + 0.5,
                    offset.b + amp.b * 0.5 * cos(x * freq.b + phase.b) + 0.5,
                    offset.a + amp.a * 0.5 * cos(x * freq.a + phase.a) + 0.5
                );
            }
            fixed3 toRGB(fixed3 grad){
                return grad.rgb;
            }
            //噪声图生成
            float2 rand(float2 st, int seed)
            {
                float2 s = float2(dot(st, float2(127.1, 311.7)) + seed, dot(st, float2(269.5, 183.3)) + seed);
                return -1 + 2 * frac(sin(s) * 43758.5453123);
            }
            float noise(float2 st, int seed)
            {
                st.y += _Time.y;

                float2 p = floor(st);
                float2 f = frac(st);
                
                float w00 = dot(rand(p, seed), f);
                float w10 = dot(rand(p + float2(1, 0), seed), f - float2(1, 0));
                float w01 = dot(rand(p + float2(0, 1), seed), f - float2(0, 1));
                float w11 = dot(rand(p + float2(1, 1), seed), f - float2(1, 1));
                
                float2 u = f * f * (3 - 2 * f);
                
                return lerp(lerp(w00, w10, u.x), lerp(w01, w11, u.x), u.y);
            }
            //海浪的涌起法线计算
            float3 swell( float3 pos , float anisotropy){
                float3 normal;
                float height = noise(pos.xz * 0.1, 0);
                height *= anisotropy * _WaveHeight;//使距离地平线近的区域的海浪高度降低
                normal = normalize(
                    cross (
                        float3(0, ddy(height), 1),
                        float3(1, ddx(height), 0)
                    )//两片元间高度差值得到梯度
                );
                return normal;
            }

            UNITY_DECLARE_DEPTH_TEXTURE(_CameraDepthTexture);
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = fixed4(1.0, 1.0, 1.0, 1.0);
                float sceneZ = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos)));
                float partZ = i.screenPos.z;
                float diffZ = saturate( (sceneZ - partZ)/5.0f);//片元深度与场景深度的差值
                const fixed4 phases = fixed4(0.28, 0.50, 0.07, 0);//相位
                const fixed4 amplitudes = fixed4(4.02, 0.34, 0.65, 0);//振幅
                const fixed4 frequencies = fixed4(0.00, 0.48, 0.08, 0);//频率
                const fixed4 offsets = fixed4(0.00, 0.16, 0.00, 0);//偏移
                //按照距离海滩远近叠加渐变色
                fixed4 cos_grad = cosine_gradient(saturate(_ColorOffset - diffZ), phases, amplitudes, frequencies, offsets);
                cos_grad = clamp(cos_grad, 0, 1);
                col.rgb = toRGB(cos_grad);
                //海浪波动
                half3 worldViewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
                float3 v = i.worldPos - _WorldSpaceCameraPos;
                float anisotropy = saturate(1/ddy(length(v.xz))/10);//通过临近像素点间摄像机到片元位置差值来计算哪里是接近地平线的部分
                float3 swelledNormal = swell( i.worldPos , anisotropy);

                // 只反射天空盒
                half3 reflDir = reflect(-worldViewDir, swelledNormal);
                fixed4 reflectionColor = UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, reflDir);

                // 菲涅尔反射
                float f0 = 0.02;
                float vReflect = f0 + (1-f0) * pow(1 - dot(worldViewDir,swelledNormal),5);
                vReflect = saturate(vReflect * 2.0);
                col = lerp(col , reflectionColor , vReflect);
                //接近海滩部分更透明
                float alpha = saturate(diffZ);
                col.a = alpha;

                return col;
            }
            ENDCG
        }
    }
}