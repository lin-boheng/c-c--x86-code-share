ps.1.0
//Version PixelShader 1.0
//General Purpose: r0 - r1
//Constant: c0 - c7
//Texture: t0 - t3
//Color: v0 - v1
/*
def c0,0.5,0.5,0.5,1.0
def c1,0.7,0.8,0.1,0.6
mov r0,c0
mov r1,c1
mad_sat r0,r0,r1,r0
mul r1,r1,r0
mul_sat r1,r0,r1
*/
tex t0
mul_sat r0,t0,v0
