#include "Edit.h"

LRESULT Edit::OnMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam){
    if(this->hWnd != hWnd)
        return 0;
    EditEventArg arg = {
        .self = this,
        .hWnd = hWnd,
        .wParam = wParam,
        .lParam = lParam
    };
    switch (Msg) {
    case WM_CREATE:
        break;
    case WM_DESTROY:
        break;
    case WM_PAINT:{
        LPPAINTSTRUCT ps;
        HBRUSH hBrush;
        Debug::log(TEXT("Paint\n"));
        ps = (LPPAINTSTRUCT)lParam;
        hBrush = CreateSolidBrush(focus ? 0x909090 : 0x808080);
        FillRect(ps->hdc, &rect, hBrush);
        DeleteObject(hBrush);
        SetBkMode(ps->hdc, TRANSPARENT);
        DrawText(ps->hdc, szText, dwSize, &rect, DT_LEFT);
    }
        return 0;
#ifdef UNICODE
    case WM_UNICHAR:
#else
    case WM_CHAR:
#endif
        if(!focus)
            break;
        if(wParam == VK_RETURN){
            if(OnEnter)
                OnEnter(&arg);
            if(bSingleLine)
                break;
        }
        if(wParam == VK_BACK){
            if(dwSize)
                dwSize--;
            InvalidateRect(hWnd, &rect, FALSE);
            break;
        }
        if(dwSize >= MAX_PATH)
            break;
        szText[dwSize++] = wParam;
        InvalidateRect(hWnd, &rect, FALSE);
        break;
    case WM_LBUTTONDOWN:
        if(focus != hover)
            InvalidateRect(hWnd, &rect, FALSE);
        focus = hover;
        break;
    case WM_LBUTTONUP:
        break;
    case WM_MOUSEMOVE:
        hover = InRect(lParam);
        break;
    default:
        return 0;
    }
    return 0;
}

bool Edit::InRect(LPARAM lParam){
    return (LOWORD(lParam) >= rect.left) && (LOWORD(lParam) <= rect.right) &&
            (HIWORD(lParam) >= rect.top) && (HIWORD(lParam) <= rect.bottom);
}

Edit::Edit(HWND hParent) {
    rect = {
        .left = 0,
        .top = 0,
        .right = 100,
        .bottom = 100
    };
    hWnd = hParent;
    szText = new TCHAR[MAX_PATH + 1];
    dwSize = 0;
    hover = false;
    focus = false;
    RegComponent();
}

Edit::~Edit(){
    DelComponent();
    InvalidateRect(hWnd, &rect, TRUE);
    delete[] szText;
}

LPCTSTR Edit::GetText() const{
    szText[dwSize] = '\0';
    return szText;
}

void Edit::SetText(LPCTSTR szText){
    dwSize = tstrnlen(szText, MAX_PATH);
    tstrncpy(this->szText, szText, dwSize);
    InvalidateRect(hWnd, &rect, FALSE);
}
