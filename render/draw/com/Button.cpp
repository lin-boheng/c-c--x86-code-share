#include "Button.h"

LRESULT Button::OnMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam){
    if(this->hWnd != hWnd)
        return 0;
    ButtonEventArg arg = {
        .self = this,
        .hWnd = hWnd,
        .wParam = wParam,
        .lParam = lParam
    };
    switch (Msg) {
    case WM_CREATE:
        if(OnCreate)
            OnCreate(&arg);
        break;
    case WM_DESTROY:
        if(OnDestroy)
            OnDestroy(&arg);
        break;
    case WM_PAINT:{
        LPPAINTSTRUCT ps;
        HBRUSH hBrush;
        Debug::log(TEXT("Paint\n"));
        ps = (LPPAINTSTRUCT)lParam;
        hBrush = CreateSolidBrush(hover ? 0xC0C0C0 : 0xFFFFFF);
        FillRect(ps->hdc, &rect, hBrush);
        DeleteObject(hBrush);
        SetBkMode(ps->hdc, TRANSPARENT);
        DrawText(ps->hdc, szCaption, -1, &rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
    }
        return 0;
    case WM_LBUTTONDOWN:
        if(!InRect(lParam))
            break;
        mouseState = true;
        break;
    case WM_LBUTTONUP:
        if(!InRect(lParam))
            break;
        if(OnClick && mouseState)
            OnClick(&arg);
        mouseState = false;
        break;
    case WM_MOUSEMOVE:{
        Debug::log(TEXT("Position: %hu %hu\n"), LOWORD(lParam), HIWORD(lParam));
        bool newb = InRect(lParam);
        if(newb != hover)
            InvalidateRect(hWnd, &rect, FALSE);
        hover = newb;
        Debug::log(hover ? TEXT("Hover: TRUE\n") : TEXT("Hover: FALSE\n"));
    }
        break;
    default:
        return 0;
    }
    return 0;
}

bool Button::InRect(LPARAM lParam){
    return (LOWORD(lParam) >= rect.left) && (LOWORD(lParam) <= rect.right) &&
            (HIWORD(lParam) >= rect.top) && (HIWORD(lParam) <= rect.bottom);
}

Button::Button(HWND hParent) {
    rect = {
        .left = 0,
        .top = 0,
        .right = 100,
        .bottom = 100
    };
    hWnd = hParent;
    mouseState = false;
    hover = false;
    szCaption = TEXT("");
    RegComponent();
}

Button::~Button(){
    DelComponent();
    InvalidateRect(hWnd, &rect, TRUE);
}

void Button::BeginUpdate(){
    InvalidateRect(hWnd, &rect, FALSE);
}
