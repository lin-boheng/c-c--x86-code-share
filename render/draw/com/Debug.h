#ifndef __DEBUG__
#define __DEBUG__

#include <windows.h>
#include <stdio.h>
#include "..\lib\TString.h"

class Debug {
public:
    static DWORD log(LPCTSTR szFormat, ...);
};

#endif