#include "Base.h"

struct TimerEventArg;
class Timer;
typedef void(*TimerEvent)(TimerEventArg*);

struct TimerEventArg {
    Timer* self;
    HWND hWnd;
    WPARAM wParam;
    LPARAM lParam;
};

class Timer : public Base {
private:
    UINT uID, uTimeout;
    virtual LRESULT OnMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) override;
public:
    TimerEvent OnTimeout = NULL;
    Timer(HWND hParent, UINT uTimeout);
    ~Timer();
};