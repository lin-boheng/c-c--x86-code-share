#include "Timer.h"

LRESULT Timer::OnMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam){
    if(this->hWnd != hWnd)
        return 0;
    TimerEventArg arg = {
        .self = this,
        .hWnd = hWnd,
        .wParam = wParam,
        .lParam = lParam
    };
    switch (Msg) {
    case WM_CREATE:
        SetTimer(hWnd, uID, uTimeout, NULL);
        break;
    case WM_TIMER:
        if(this->uID != wParam)
            return 0;
        if(OnTimeout)
            OnTimeout(&arg);
        break;
    case WM_DESTROY:
        KillTimer(hWnd, uID);
        break;
    }
    return 0;
}

Timer::Timer(HWND hParent, UINT uTimeout) {
    static UINT incID = 0;
    hWnd = hParent;
    this->uID = ++incID;
    this->uTimeout = uTimeout;
    RegComponent();
}

Timer::~Timer(){
    DelComponent();
}
