#include "Base.h"

struct EditEventArg;
class Edit;
typedef void(*EditEvent)(EditEventArg*);

struct EditEventArg {
    Edit* self;
    HWND hWnd;
    WPARAM wParam;
    LPARAM lParam;
};

class Edit : public Base {
private:
    LPTSTR szText;
    DWORD dwSize;
    bool hover, focus;
    virtual LRESULT OnMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
    bool InRect(LPARAM lParam);
public:
    RECT rect;
    bool bSingleLine = false;
    EditEvent OnEnter = NULL;
    Edit(HWND hParent);
    ~Edit();
    LPCTSTR GetText() const;
    void SetText(LPCTSTR szText);
};
