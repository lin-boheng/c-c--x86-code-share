#include "Base.h"

Pair::Pair(Base* lpCom, Pair* lpNext){
    this->lpCom = lpCom;
    this->lpNext = lpNext;
}

Base::Base(){
    this->szName = "";
}

Base::~Base(){}

static Pair* lpMap;

static LPVOID Reg[MAX_PATH], Del[MAX_PATH];
static DWORD dwReg, dwDel;

void Base::RegComponent(){
    lpMap = new Pair(this, lpMap);
    Reg[dwReg++] = this;
}

void Base::DelComponent(){
    Pair** lpCur = &lpMap;
    while(*lpCur != NULL){
        if((*lpCur)->lpCom == this){
            (*lpCur) = (*lpCur)->lpNext;
            Del[dwDel++] = this;
            delete *lpCur;
            return;
        }
        lpCur = &(*lpCur)->lpNext;
    }
}

LRESULT Base::OnMessageStatic(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam){
    Pair* lpCur = lpMap;
    while(dwReg)
        ((Base*)Reg[--dwReg])->OnMessage(hWnd, WM_CREATE, 0, 0);
    while(dwDel)
        ((Base*)Del[--dwDel])->OnMessage(hWnd, WM_DESTROY, 0, 0);
    switch (Msg) {
    case WM_CREATE:
        return 0;
    case WM_QUIT:
        DestroyWindow(hWnd);
        return 0;
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    case WM_PAINT:{
        PAINTSTRUCT ps;
        BeginPaint(hWnd, &ps);
        while(lpCur != NULL){
            lpCur->lpCom->OnMessage(hWnd, WM_PAINT, 0, (LPARAM)&ps);
            lpCur = lpCur->lpNext;
        }
        EndPaint(hWnd, &ps);
    }
        return 0;
    }
    while(lpCur != NULL){
        lpCur->lpCom->OnMessage(hWnd, Msg, wParam, lParam);
        lpCur = lpCur->lpNext;
    }
    return DefWindowProc(hWnd, Msg, wParam, lParam);
}

Base* Base::QueryComponent(LPCTSTR szName){
    Pair* lpCur = lpMap;
    Debug::log(TEXT("Query: %s\n"), szName);
    while(lpCur != NULL){
        if(!strcmp(lpCur->lpCom->szName, szName))
            return lpCur->lpCom;
        lpCur = lpCur->lpNext;
    }
    return NULL;
}

ATOM Utils::RegClassEx(HINSTANCE hInst, LPCTSTR szClassName, WNDPROC WndProc){
    WNDCLASSEX wcex;

    wcex.cbSize        = sizeof(WNDCLASSEX);
    wcex.style         = 0;
    wcex.lpfnWndProc   = WndProc;
    wcex.cbClsExtra    = 0;
    wcex.cbWndExtra    = 0;
    wcex.hInstance     = hInst;
    wcex.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wcex.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName  = NULL;
    wcex.lpszClassName = szClassName;
    wcex.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

    return RegisterClassEx(&wcex);
}

HWND Utils::Init(HINSTANCE hInstance, DWORD dwWidth, DWORD dwHeight){
    static ATOM state = RegClassEx(hInstance, TEXT("component.cpp"), Base::OnMessageStatic);
    lpMap = NULL;
    dwReg = dwDel = 0;
    return CreateWindowEx(
        WS_EX_CLIENTEDGE | WS_EX_LAYERED,
        TEXT("component.cpp"),
        TEXT("TEST"),
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT,
        dwWidth, dwHeight,
        NULL,
        NULL,
        hInstance,
        NULL
    );
}

Utils::Utils(){}

Utils::~Utils(){}
