#include "Base.h"

struct ButtonEventArg;
class Button;
typedef void(*ButtonEvent)(ButtonEventArg*);

struct ButtonEventArg {
    Button* self;
    HWND hWnd;
    WPARAM wParam;
    LPARAM lParam;
};

class Button : public Base {
private:
    bool mouseState;
    bool hover;
    virtual LRESULT OnMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) override;
    bool InRect(LPARAM lParam);
public:
    RECT rect;
    LPCTSTR szCaption;
    ButtonEvent OnCreate = NULL;
    ButtonEvent OnDestroy = NULL;
    ButtonEvent OnClick = NULL;
    Button(HWND hParent);
    ~Button();
    void BeginUpdate();
};
