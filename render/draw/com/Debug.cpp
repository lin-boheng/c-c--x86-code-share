#include "Debug.h"

DWORD Debug::log(LPCTSTR szFormat, ...){
    static TCHAR str[MAX_PATH];
    static HANDLE hFile = CreateFile(
        TEXT(".\\debug.txt"),
        FILE_GENERIC_READ | FILE_GENERIC_WRITE,
        0,
        NULL,
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL,
        NULL
    );
    DWORD dwLen;
    va_list lpList;
    va_start(lpList, szFormat);
    dwLen = tvsnprintf(str, MAX_PATH, szFormat, lpList);
    va_end(lpList);
    WriteFile(hFile, str, dwLen, &dwLen, NULL);
    return dwLen;
}