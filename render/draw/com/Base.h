#ifndef __BASE__
#define __BASE__

#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include "Debug.h"

#define GET_X_LPARAM LOWORD
#define GET_Y_LPARAM HIWORD

struct Pair;
class Base;

struct Pair{
    Base* lpCom;
    Pair* lpNext;
    Pair(Base* lpCom, Pair* lpNext);
};

class Base {
private:
    virtual LRESULT OnMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) = 0;
protected:
    HWND hWnd;
    Base();
    ~Base();
    void DelComponent();
    void RegComponent();
public:
    LPCTSTR szName;
    static LRESULT OnMessageStatic(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
    static Base* QueryComponent(LPCTSTR szName);
};

class Utils {
public:
    Utils();
    ~Utils();
    static HWND Init(HINSTANCE hInstance, DWORD dwWidth, DWORD dwHeight);
    static ATOM RegClassEx(HINSTANCE hInst, LPCTSTR szClassName, WNDPROC WndProc);
};

#endif
