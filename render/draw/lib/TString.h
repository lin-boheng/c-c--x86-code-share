#ifndef __TSTRING__
#define __TSTRING__

#include <string.h>

#ifdef UNICODE
#define NAME_AW(x) wcs##x
#define tprintf wprintf
#define tvsprintf wvsprintf
#define tvsnprintf vsnwprintf
#define tsnprintf snwprintf
#define tsprintf swprintf
#else
#define NAME_AW(x) str##x
#define tprintf printf
#define tvsprintf vsprintf
#define tvsnprintf vsnprintf
#define tsnprintf snprintf
#define tsprintf sprintf
#endif

#define tstrlen NAME_AW(len)
#define tstrstr NAME_AW(str)
#define tstrcat NAME_AW(cat)
#define tstrcpy NAME_AW(cpy)
#define tstrchr NAME_AW(chr)
#define tstrrchr NAME_AW(rchr)
#define tstrcoll NAME_AW(coll)
#define tstrcmp NAME_AW(cmp)
#define tstrcspn NAME_AW(cspn)
#define tstrspn NAME_AW(spn)
#define tstrtok NAME_AW(tok)
#define tstrxfrm NAME_AW(xfrm)
#define tstrtod NAME_AW(tod)
#define tstrtol NAME_AW(tol)
#define tstrtold NAME_AW(told)
#define tstrtoll NAME_AW(toll)
#define tstrtof NAME_AW(tof)
#define tstrtoul NAME_AW(toul)
#define tstrtoull NAME_AW(toull)
#define tstrpbrk NAME_AW(pbrk)
#define tstrcmpi NAME_AW(cmpi)
#define tstrdup NAME_AW(dup)
#define tstrerror NAME_AW(error)
#define tstricmp NAME_AW(icmp)
#define tstrlwr NAME_AW(lwr)
#define tstrnicmp NAME_AW(nicmp)
#define tstrnset NAME_AW(nset)
#define tstrrev NAME_AW(rev)
#define tstrupr NAME_AW(upr)
#define tstrset NAME_AW(set)
#define tstrnlen NAME_AW(nlen)
#define tstrncmp NAME_AW(ncmp)
#define tstrncpy NAME_AW(ncpy)
#define tstrncat NAME_AW(ncat)

#define tstrcat_s NAME_AW(cat_s)
#define tstrcpy_s NAME_AW(cpy_s)
#define tstrerror_s NAME_AW(error_s)
#define tstrncat_s NAME_AW(ncat_s)
#define tstrnlen_s NAME_AW(nlen_s)
#define tstrtok_s NAME_AW(tok_s)
#define strncpy_s NAME_AW(ncpy_s)

#endif
