#include "main.h"

LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam){
    switch (Msg) {
    case WM_CREATE:
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        ExitProcess(0);
        break;
    default:
        return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return TRUE;
}

#ifdef UNICODE
#define tWinMain wWinMain
#else
#define tWinMain WinMain
#endif

int tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nShowCmd){
    HWND hWnd;
    MSG Msg;

    hWnd = Utils::Init(hInstance, 200, 200);

    UpdateWindow(hWnd);
    ShowWindow(hWnd, SW_SHOW);

    Button b(hWnd);
    Timer t(hWnd, 1000);
    Timer t2(hWnd, 10000);
    Edit e(hWnd);
    b.rect = {
        .left = 70,
        .top = 100,
        .right = 130,
        .bottom = 140
    };
    e.rect = {
        .left = 0,
        .top = 0,
        .right = 200,
        .bottom = 100
    };
    e.szName = TEXT("Edit1");
    b.szCaption = TEXT("Button");
    b.szName = TEXT("Button1");
    b.OnClick = [](ButtonEventArg* e){
        MessageBox(e->hWnd, TEXT("Hello!"), TEXT("tips"), MB_OK);
    };
    b.OnCreate = [](ButtonEventArg* e){};
    t.OnTimeout = [](TimerEventArg* e){
        Button* b = (Button*)Base::QueryComponent(TEXT("Button1"));
        b->BeginUpdate();
        b->rect.left--;
    };
    t2.OnTimeout = [](TimerEventArg* e){
        Edit* ed = (Edit*)Base::QueryComponent(TEXT("Edit1"));
        ed->SetText("Timer2");
    };
    e.OnEnter = [](EditEventArg* e){
        MessageBox(e->hWnd, e->self->GetText(), TEXT("Edit"), MB_OK);
    };

    SetLayeredWindowAttributes(hWnd, RGB(0, 0, 0), 0x80, LWA_ALPHA);
    SetWindowDisplayAffinity(hWnd, WDA_NONE);

    while(GetMessage(&Msg, NULL, 0, 0)){
        Debug::log(TEXT("%08X %08X %08X %08X\n"), hWnd, Msg.message, Msg.wParam, Msg.lParam);
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }

    ExitProcess(0);
}