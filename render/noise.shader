Shader "Unlit/noise"
{
    Properties{
        _Noise ("Noise Texture", 2D) = "white" {}
    }
    SubShader{
        LOD 100
        Tags{"RenderType"="Opaque"}
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag

        struct appinfo{
            float4 pos : POSITION;
            float2 uv_noise : TEXCOORD0;
        }

        struct v2f{
            float4 uv_pos : SV_POSITION;
            float2 uv_noise : TEXCOORD0;
            float4 worldPos : TEXCOORD1;
            float4 screenPos : TEXCOORD2;
        }

        sampler2D _Noise;

        v2f vert(appinfo i){
            v2f o;
            o.uv_pos = UnityObjectToClipPos(i.pos);
            o.worldPos = mul(unity_ObjectToWorld, i.pos);
            o.screenPos = ComputeScreenPos(o.uv_pos);
            o.uv_noise = i.uv_noise;
            return o;
        }

        fixed4 frag(v2f i){
            fixed4 col = tex2D(_Noise, i.uv_noise);
            return col;
        }

        ENDCG
    }
}