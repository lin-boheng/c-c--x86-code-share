#include <windows.h>

int main(){
    HANDLE hSerial;
    DCB commDCB;

    hSerial = CreateFileW(
        L"\\\\.\\COM1",
        GENERIC_READ | GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        0,
        NULL
    );
    commDCB.BaudRate = 9600;
    commDCB.ByteSize = 8;
    commDCB.StopBits = 0;//STOPBITS_10
    commDCB.fParity = TRUE;
    commDCB.Parity = 2;//PARITY_EVEN

    commDCB.fDtrControl = FALSE;
    commDCB.fDtrControl = DTR_CONTROL_ENABLE;

    commDCB.fRtsControl = RTS_CONTROL_ENABLE;
    commDCB.fOutxCtsFlow = FALSE;
    commDCB.fOutX = TRUE;
    commDCB.fInX = TRUE;

    SetCommState(hSerial, &commDCB);

    CloseHandle(hSerial);
    
    ExitProcess(0);
}