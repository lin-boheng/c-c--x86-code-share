#include <cstdio>
#include <fstream>
#include <windows.h>

int main(){
    HWAVEIN hwi;
    WAVEFORMATEX waveFormat;
    MMRESULT mmRet;
    HANDLE event;
    WAVEHDR whdr;
    FILE* output;
    BYTE* buffer;

    waveFormat.cbSize = 0;
    waveFormat.nAvgBytesPerSec = 44100;
    waveFormat.nBlockAlign = 1;
    waveFormat.nChannels = 1;
    waveFormat.nSamplesPerSec = 44100;
    waveFormat.wBitsPerSample = 8;
    waveFormat.wFormatTag = WAVE_FORMAT_PCM;

    event = CreateEvent(NULL, FALSE, FALSE, NULL);

    mmRet = waveInOpen(&hwi, WAVE_MAPPER, &waveFormat, (DWORD_PTR)event, 0, CALLBACK_EVENT);

    if(mmRet != MMSYSERR_NOERROR){
        CHAR msg[0x100];
        RtlZeroMemory(msg, 0x100);
        waveInGetErrorTextA(mmRet, msg, 0x100);
        MessageBoxA(NULL, msg, "waveInOpen Error!", MB_OK | MB_ICONERROR);
        return 0;
    }

    const DWORD size = 0x100000;

    BYTE* Buffers[0x10] = {NULL};

for(DWORD i=0;i<10;i++){
    if(Buffers[i & 0xF] != NULL)
        delete[] Buffers[i & 0xF];

    Buffers[i & 0xF] = buffer = new BYTE[size];

    struct{
		char riff[4]={'R','I','F','F'};
		DWORD RiffSize;
		char wave[4]={'W','A','V','E'};
		char fmt[4]={'f','m','t',' '};
		DWORD fmtLen=0x10;
		SHORT tag=1;
		SHORT channels=1;
		DWORD samplesPerSec=44100;
		DWORD bytesPerSec=44100;
		SHORT blockAlign=1;
		SHORT bitsPerSample=8;
		char data[4]={'d','a','t','a'};
		DWORD dataSize;
	}wav;

    whdr.lpData = (LPSTR)(buffer + sizeof(wav));
    whdr.dwBufferLength = size;
    whdr.dwBytesRecorded = 0;
    whdr.dwUser = 0;
    whdr.dwFlags = 0;
    whdr.dwLoops = 1;

    waveInPrepareHeader(hwi, &whdr, sizeof(WAVEHDR));
    waveInAddBuffer(hwi, &whdr, sizeof(WAVEHDR));
    waveInStart(hwi);
    Sleep(10000);
    waveInReset(hwi);

    wav.RiffSize = whdr.dwBytesRecorded + 36;
    wav.dataSize = whdr.dwBytesRecorded;
    memcpy(buffer, &wav, sizeof(wav));

    PlaySoundA((LPCSTR)buffer, NULL, SND_ASYNC | SND_MEMORY);

    FILE* f = fopen("wave.wav", "wb");
    fwrite(buffer, sizeof(BYTE), whdr.dwBytesRecorded + 36, f);
    fclose(f);
}

    return 0;
}