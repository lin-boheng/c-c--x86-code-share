#include <iostream>
#include <windows.h>
using namespace std;
char Buffer[0x100000];
char Cmp[0x100000];
int main(){
    HANDLE hDisk=CreateFileA(
        "\\\\.\\E:",
        GENERIC_READ|GENERIC_WRITE,
        FILE_SHARE_READ|FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        0,
        NULL);
    if(hDisk==INVALID_HANDLE_VALUE){
        printf("Error:%d.",GetLastError());
        getchar();
        return 0;
    }
    FILE *MBR = fopen("USB.bin", "rb");
    printf("Handle:%016llx\n",hDisk);
    fread(Buffer, sizeof(char), 0x100000, MBR);
    DWORD Len = 0x100000;
    WriteFile(hDisk, Buffer, 0x100000, &Len, NULL);
    printf("Write %d Bytes.\n", Len);
    printf("Comparing...\n");
    ReadFile(hDisk, Cmp, Len, &Len, NULL);
    DWORD cnt = 0;
    for(DWORD i=0;i<Len;i++)
        if(Cmp[i]!=Buffer[i] && (++cnt))
            printf("error at %x.\n",i);
    printf("total errors: %x\n",cnt);
    fclose(MBR);
    getchar();
    return 0;
}