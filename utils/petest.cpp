#include <iostream>
#include <fstream>
//#define DWORD DWORD32
#include <windows.h>

using namespace std;

int main(int argc, char** argv){
    if(argc != 2){
        puts("arg count should be 2!");
        return 0;
    }
    FILE* pe = fopen(argv[1], "rb");
    IMAGE_DOS_HEADER dosHdr;
    fread(&dosHdr, sizeof(IMAGE_DOS_HEADER), 1, pe);
    printf("DOS header magic: %c%c\n", (char)dosHdr.e_magic, (char)(dosHdr.e_magic >> 8));
    if(dosHdr.e_magic != IMAGE_DOS_SIGNATURE){
        puts("Error!");
        return 0;
    }
    fseek(pe, dosHdr.e_lfanew, FILE_BEGIN);
    DWORD sign;
    IMAGE_FILE_HEADER fileHdr;
    fread(&sign, sizeof(DWORD), 1, pe);
    printf("NT header magic: %c%c\n", (char)sign, (char)(sign >> 8));
    if(sign != IMAGE_NT_SIGNATURE){
        puts("Error!");
        return 0;
    }
    fread(&fileHdr, sizeof(IMAGE_FILE_HEADER), 1, pe);
    if(fileHdr.SizeOfOptionalHeader == sizeof(IMAGE_OPTIONAL_HEADER32)){
        puts("32bit");
        IMAGE_OPTIONAL_HEADER32 optHdr;
        fread(&optHdr, sizeof(IMAGE_OPTIONAL_HEADER32), 1, pe);
        printf("Optional Header Sign: %04X\n", optHdr.Magic);
        printf("Execution Entry Point: %08X\n", optHdr.AddressOfEntryPoint);
        printf("Number of RVA and Sizes: %08X\n", optHdr.NumberOfRvaAndSizes);
        printf("Number of Sections: %04X\n", fileHdr.NumberOfSections);
        (int)FIELD_OFFSET(IMAGE_NT_HEADERS32,OptionalHeader.AddressOfEntryPoint);
        IMAGE_DATA_DIRECTORY dataDir;
        for(DWORD i=0;i<optHdr.NumberOfRvaAndSizes;i++){
            fread(&dataDir, sizeof(IMAGE_DATA_DIRECTORY), 1, pe);
            printf("%08X:VirtualAddress %08X Size %08X\n", i, dataDir.VirtualAddress, dataDir.Size);
        }
        fseek(pe, dosHdr.e_lfanew + sizeof(IMAGE_NT_HEADERS32), FILE_BEGIN);
        IMAGE_SECTION_HEADER secHdr;
        for(DWORD i=0;i<fileHdr.NumberOfSections;i++){
            fread(&secHdr, IMAGE_SIZEOF_SECTION_HEADER, 1, pe);
            printf("Segment %04X\n", i);
            printf("Name: ");
            for(DWORD j=0;j<8;j++)
                putchar(secHdr.Name[j]);
            putchar('\n');
            printf("Virtual Address: %08X\n", secHdr.VirtualAddress);
            printf("Size of Raw Data: %08X\n", secHdr.SizeOfRawData);
            printf("Pointer to Raw Data: %08X\n", secHdr.PointerToRawData);
        }
    }else if(fileHdr.SizeOfOptionalHeader == sizeof(IMAGE_OPTIONAL_HEADER64)){
        puts("64bit");
        IMAGE_OPTIONAL_HEADER64 optHdr;
        fread(&optHdr, sizeof(IMAGE_OPTIONAL_HEADER64), 1, pe);
        printf("Optional Header Sign: %04X\n", optHdr.Magic);
        printf("Execution Entry Point: %08X\n", optHdr.AddressOfEntryPoint);
        printf("Number of RVA and Sizes: %08X\n", optHdr.NumberOfRvaAndSizes);
        printf("Number of Sections: %04X\n", fileHdr.NumberOfSections);
        IMAGE_DATA_DIRECTORY dataDir;
        for(DWORD i=0;i<optHdr.NumberOfRvaAndSizes;i++){
            fread(&dataDir, sizeof(IMAGE_DATA_DIRECTORY), 1, pe);
            printf("%08X:VirtualAddress %08X Size %08X\n", i, dataDir.VirtualAddress, dataDir.Size);
        }
        fseek(pe, dosHdr.e_lfanew + sizeof(IMAGE_NT_HEADERS64), FILE_BEGIN);
        IMAGE_SECTION_HEADER secHdr;
        for(DWORD i=0;i<fileHdr.NumberOfSections;i++){
            fread(&secHdr, IMAGE_SIZEOF_SECTION_HEADER, 1, pe);
            printf("Segment %04X\n", i);
            printf("Name: ");
            for(DWORD j=0;j<8;j++)
                putchar(secHdr.Name[j]);
            putchar('\n');
            printf("Virtual Address: %08X\n", secHdr.VirtualAddress);
            printf("Size of Raw Data: %08X\n", secHdr.SizeOfRawData);
            printf("Pointer to Raw Data: %08X\n", secHdr.PointerToRawData);
        }
    }else{
        puts("Error!");
        return 0;
    }
    return 0;
}