#include <string.h>
#include <windows.h>
#include <mmsystem.h>

#define GOOD_HANDLE(x) (x != INVALID_HANDLE_VALUE && x != NULL)

int main(){
    CHAR lpszPath[0x200];
    CHAR lpszFile[0x400];
    WIN32_FIND_DATAA FindData;
    HANDLE hFiles;
    HANDLE hOut;
    HINSTANCE hInst;

    hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    hInst = GetModuleHandleA(NULL);
    GetCurrentDirectoryA(MAX_PATH, lpszPath);
    wsprintfA(lpszPath, "%s\\AudioClip\\*.wav", lpszPath);
    hFiles = FindFirstFileA(lpszPath, &FindData);
    if(!GOOD_HANDLE(hFiles))
        ExitProcess(1);
    GetCurrentDirectoryA(MAX_PATH, lpszPath);
    WriteFile(hOut, FindData.cFileName, strlen(FindData.cFileName), NULL, NULL);
    WriteFile(hOut, "\n", 1, NULL, NULL);
    wsprintfA(lpszFile, "%s\\AudioClip\\%s", lpszPath, FindData.cFileName);
    PlaySoundA(lpszFile, hInst, SND_SYNC | SND_FILENAME | SND_NODEFAULT);
    while(FindNextFileA(hFiles, &FindData)){
        WriteFile(hOut, FindData.cFileName, strlen(FindData.cFileName), NULL, NULL);
        WriteFile(hOut, "\n", 1, NULL, NULL);
        wsprintfA(lpszFile, "%s\\AudioClip\\%s", lpszPath, FindData.cFileName);
        PlaySoundA(lpszFile, hInst, SND_SYNC | SND_FILENAME | SND_NODEFAULT);
    }
    FindClose(hFiles);
    ExitProcess(0);
}
