#include <windows.h>

VOID ManagerRun(LPCSTR exe,LPCSTR param,INT nShow)  
{ //注意：会跳出提示。
 SHELLEXECUTEINFO ShExecInfo; 
 ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
 ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
 ShExecInfo.hwnd = NULL;  
 ShExecInfo.lpVerb = "runas";  
 ShExecInfo.lpFile = exe; 
 ShExecInfo.lpParameters = param;
 ShExecInfo.lpDirectory = NULL;  
 ShExecInfo.nShow = nShow;  
 ShExecInfo.hInstApp = NULL;   
 BOOL ret = ShellExecuteEx(&ShExecInfo);
 //等不及了，不等了。
 CloseHandle(ShExecInfo.hProcess);
 return;
}

int main(int argc, char** argv){
    if(argc == 1) //初次运行，即双击EXE
    {
       ShowWindow(GetConsoleWindow(), SW_HIDE);
       ManagerRun(argv[0],"2", SW_HIDE);
       return 1;
    }else if(argc == 2) //再次运行,即上面那个ManagerRun
    {
      /*你的程序主代码在此*/
    }
    return 0;
}
