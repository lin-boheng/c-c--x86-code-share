#include <algorithm>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <stdio.h>

typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned int DWORD;
typedef unsigned long long QWORD;

#define CLASS_FILE_MAGIC 0xCAFEBABE

#define CLASS_CONSTANT_UTF8 1
#define CLASS_CONSTANT_INTEGER 3
#define CLASS_CONSTANT_FLOAT 4
#define CLASS_CONSTANT_LONG 5
#define CLASS_CONSTANT_DOUBLE 6
#define CLASS_CONSTANT_CLASS_REF 7
#define CLASS_CONSTANT_STRING 8
#define CLASS_CONSTANT_FIELD_REF 9
#define CLASS_CONSTANT_METHOD_REF 10
#define CLASS_CONSTANT_INTERFACE_METHOD_REF 11
#define CLASS_CONSTANT_NAME_AND_TYPE 12
#define CLASS_CONSTANT_METHOD_HANDLE 15
#define CLASS_CONSTANT_METHOD_TYPE 16
#define CLASS_CONSTANT_INVOKE_DYNAMIC 18

#define CLASS_ACC_PUBLIC 0x0001
#define CLASS_ACC_FINAL 0x0010
#define CLASS_ACC_SUPER 0x0020
#define CLASS_ACC_INTERFACE 0x0200
#define CLASS_ACC_ABSTRACT 0x0400
#define CLASS_ACC_SYNTHETIC 0x1000
#define CLASS_ACC_ANNOTATION 0x2000
#define CLASS_ACC_ENUM 0x4000

#define FIELD_ACC_PUBLIC 0x0001
#define FIELD_ACC_PRIVATE 0x0002
#define FIELD_ACC_PROTECTED 0x0004
#define FIELD_ACC_STATIC 0x0008
#define FIELD_ACC_FINAL 0x0010
#define FIELD_ACC_VOLATILE 0x0040
#define FIELD_ACC_TRANSIENT 0x0080
#define FIELD_ACC_SYNTHETIC 0x0100
#define FIELD_ACC_ENUM 0x0400

#define METHOD_ACC_PUBLIC 0x0001
#define METHOD_ACC_PRIVATE 0x0002
#define METHOD_ACC_PROTECTED 0x0004
#define METHOD_ACC_STATIC 0x0008
#define METHOD_ACC_FINAL 0x0010
#define METHOD_ACC_SYNCHRONIZED 0x0020
#define METHOD_ACC_BRIDGE 0x0040
#define METHOD_ACC_VARARGS 0x0080
#define METHOD_ACC_NATIVE 0x0100
#define METHOD_ACC_ABSTRACT 0x0400
#define METHOD_ACC_STRICTFP 0x0800
#define METHOD_ACC_SYNTHETIC 0x1000

WORD swapWord(WORD x) { return (x << 8) | (x >> 8); }
DWORD swapDword(DWORD x) {
  return ((DWORD)swapWord(x) << 16) | swapWord(x >> 16);
}
QWORD swapQword(QWORD x) {
  return ((QWORD)swapDword(x) << 32) | swapDword(x >> 32);
}
#define SWAP_WORD_BYTES(x) (*((WORD *)&x) = swapWord(*((WORD *)&x)))
#define SWAP_DWORD_BYTES(x) (*((DWORD *)&x) = swapDword(*((DWORD *)&x)))
#define SWAP_QWORD_BYTES(x) (*((QWORD *)&x) = swapQword(*((QWORD *)&x)))

#define SECURE_STRING(x) (x == NULL ? "NULL" : (const char *)x)

#define MAX_PATH 260

struct CLASS_FILE_HEADER {
  DWORD magic;
  WORD minor_version;
  WORD major_version;
};

BYTE readByte(FILE *file) {
  BYTE bVal;
  fread(&bVal, sizeof(BYTE), 1, file);
  return bVal;
}

WORD readSwapWord(FILE *file) {
  WORD wVal;
  fread(&wVal, sizeof(WORD), 1, file);
  return swapWord(wVal);
}

DWORD readSwapDword(FILE *file) {
  DWORD dwVal;
  fread(&dwVal, sizeof(DWORD), 1, file);
  return swapDword(dwVal);
}

QWORD readSwapQword(FILE *file) {
  QWORD qwVal;
  fread(&qwVal, sizeof(QWORD), 1, file);
  return swapQword(qwVal);
}

#define getByte(x) (x = readByte(classFile))
#define getWord(x) (x = readSwapWord(classFile))
#define getDword(x) (x = readSwapDword(classFile))
#define getQword(x) (x = readSwapQword(classFile))

const char *getStr(BYTE **indexStr, WORD index) {
  return SECURE_STRING(indexStr[index]);
}

#define String(index) getStr(indexStr, index)

using namespace std;

void parseClassAcc(WORD flag) {
  if (flag & CLASS_ACC_PUBLIC)
    cout << "public ";
  if (flag & CLASS_ACC_FINAL)
    cout << "final ";
  if (flag & CLASS_ACC_SUPER)
    cout << "super ";
  if (flag & CLASS_ACC_INTERFACE)
    cout << "interface ";
  if (flag & CLASS_ACC_ABSTRACT)
    cout << "abstract ";
  if (flag & CLASS_ACC_SYNTHETIC)
    cout << "synthetic ";
  if (flag & CLASS_ACC_ANNOTATION)
    cout << "annotation ";
  if (flag & CLASS_ACC_ENUM)
    cout << "enum";
  putchar('\n');
}

void parseFieldAcc(WORD flag) {
  if (flag & FIELD_ACC_PUBLIC)
    cout << "public ";
  if (flag & FIELD_ACC_PRIVATE)
    cout << "private ";
  if (flag & FIELD_ACC_PROTECTED)
    cout << "protected ";
  if (flag & FIELD_ACC_STATIC)
    cout << "static ";
  if (flag & FIELD_ACC_FINAL)
    cout << "final ";
  if (flag & FIELD_ACC_VOLATILE)
    cout << "volatile ";
  if (flag & FIELD_ACC_TRANSIENT)
    cout << "transient ";
  if (flag & FIELD_ACC_SYNTHETIC)
    cout << "synthetic ";
  if (flag & FIELD_ACC_ENUM)
    cout << "enum ";
  putchar('\n');
}

void parseMethodAcc(WORD flag) {
  if (flag & METHOD_ACC_PUBLIC)
    cout << "public ";
  if (flag & METHOD_ACC_PRIVATE)
    cout << "private ";
  if (flag & METHOD_ACC_PROTECTED)
    cout << "protected ";
  if (flag & METHOD_ACC_STATIC)
    cout << "static ";
  if (flag & METHOD_ACC_FINAL)
    cout << "final ";
  if (flag & METHOD_ACC_SYNCHRONIZED)
    cout << "synchronized ";
  if (flag & METHOD_ACC_BRIDGE)
    cout << "bridge ";
  if (flag & METHOD_ACC_VARARGS)
    cout << "varargs ";
  if (flag & METHOD_ACC_NATIVE)
    cout << "native ";
  if (flag & METHOD_ACC_ABSTRACT)
    cout << "abstract ";
  if (flag & METHOD_ACC_STRICTFP)
    cout << "strictfp ";
  if (flag & METHOD_ACC_SYNTHETIC)
    cout << "synthetic ";
  putchar('\n');
}

void errorSeek(FILE *classFile, DWORD len) {
  puts("This attribute can't be resolved in this program now.");
  fseek(classFile, len, ios::cur);
}

void initAssemblyInfo(const char **sign, const char **arg) {
  static bool initialized = false;
  if (!initialized) {
    enum {
      nop = 0x00,
      aconst_null = 0x01,
      iconst_m1 = 0x02,
      iconst_0 = 0x03,
      iconst_1 = 0x04,
      iconst_2 = 0x05,
      iconst_3 = 0x06,
      iconst_4 = 0x07,
      iconst_5 = 0x08,
      lconst_0 = 0x09,
      lconst_1 = 0x0A,
      fconst_0 = 0x0B,
      fconst_1 = 0x0C,
      fconst_2 = 0x0D,
      dconst_0 = 0x0E,
      dconst_1 = 0x0F,
      bipush = 0x10,
      sipush = 0x11,
      ldc = 0x12,
      ldc_w = 0x13,
      ldc2_w = 0x14,
      aload = 0x19,
      aload_0 = 0x2A,
      aload_1 = 0x2B,
      aload_2 = 0x2C,
      aload_3 = 0x2D,
      iload = 0x15,
      iload_0 = 0x1A,
      iload_1 = 0x1B,
      iload_2 = 0x1C,
      iload_3 = 0x1D,
      lload = 0x16,
      lload_0 = 0x1E,
      lload_1 = 0x1F,
      lload_2 = 0x20,
      lload_3 = 0x21,
      fload = 0x17,
      fload_0 = 0x22,
      fload_1 = 0x23,
      fload_2 = 0x24,
      fload_3 = 0x25,
      dload = 0x18,
      dload_0 = 0x26,
      dload_1 = 0x27,
      dload_2 = 0x28,
      dload_3 = 0x29,
      aaload = 0x32,
      iaload = 0x2E,
      laload = 0x2F,
      faload = 0x30,
      daload = 0x31,
      baload = 0x33,
      caload = 0x34,
      saload = 0x35,
      astore = 0x3A,
      astore_0 = 0x4B,
      astore_1 = 0x4C,
      astore_2 = 0x4D,
      astore_3 = 0x4E,
      istore = 0x36,
      istore_0 = 0x3B,
      istore_1 = 0x3C,
      istore_2 = 0x3D,
      istore_3 = 0x3E,
      lstore = 0x37,
      lstore_0 = 0x3F,
      lstore_1 = 0x40,
      lstore_2 = 0x41,
      lstore_3 = 0x42,
      fstore = 0x38,
      fstore_0 = 0x43,
      fstore_1 = 0x44,
      fstore_2 = 0x45,
      fstore_3 = 0x46,
      dstore = 0x39,
      dstore_0 = 0x47,
      dstore_1 = 0x48,
      dstore_2 = 0x49,
      dstore_3 = 0x4A,
      aastore = 0x53,
      iastore = 0x4F,
      lastore = 0x50,
      fastore = 0x51,
      dastore = 0x52,
      bastore = 0x54,
      castore = 0x55,
      sastore = 0x56,
      pop = 0x57,
      pop2 = 0x58,
      dup = 0x59,
      dup_x1 = 0x5A,
      dup_x2 = 0x5B,
      dup2 = 0x5C,
      dup2_x1 = 0x5D,
      dup2_x2 = 0x5E,
      swap = 0x5F,
      iadd = 0x60,
      ladd = 0x61,
      fadd = 0x62,
      dadd = 0x63,
      isub = 0x64,
      lsub = 0x65,
      fsub = 0x66,
      dsub = 0x67,
      imul = 0x68,
      lmul = 0x69,
      fmul = 0x6A,
      dmul = 0x6B,
      idiv = 0x6C,
      ldiv_ = 0x6D,
      fdiv = 0x6E,
      ddiv = 0x6F,
      irem = 0x70,
      lrem = 0x71,
      frem = 0x72,
      drem = 0x73,
      ineg = 0x74,
      lneg = 0x75,
      fneg = 0x76,
      dneg = 0x77,
      ishl = 0x78,
      lshl = 0x79,
      ishr = 0x7A,
      lshr = 0x7B,
      iushr = 0x7C,
      lushr = 0x7D,
      iand = 0x7E,
      land = 0x7F,
      ior = 0x80,
      lor = 0x81,
      ixor = 0x82,
      lxor = 0x83,
      iinc = 0x84,
      i2l = 0x85,
      i2f = 0x86,
      i2d = 0x87,
      l2i = 0x88,
      l2f = 0x89,
      l2d = 0x8A,
      f2i = 0x8B,
      f2l = 0x8C,
      f2d = 0x8D,
      d2i = 0x8E,
      d2l = 0x8F,
      d2f = 0x90,
      i2b = 0x91,
      i2c = 0x92,
      i2s = 0x93,
      lcmp = 0x94,
      fcmpl = 0x95,
      fcmpg = 0x96,
      dcmpl = 0x97,
      dcmpg = 0x98,
      ifeq = 0x99,
      ifne = 0x9A,
      iflt = 0x9B,
      ifge = 0x9C,
      ifgt = 0x9D,
      ifle = 0x9E,
      if_icmpeq = 0x9F,
      if_icmpne = 0xA0,
      if_icmplt = 0xA1,
      if_icmpge = 0xA2,
      if_icmpgt = 0xA3,
      if_icmple = 0xA4,
      if_acmpeq = 0xA5,
      if_acmpne = 0xA6,
      goto_ = 0xA7,
      jsr = 0xA8,
      ret = 0xA9,
      tableswitch = 0xAA,
      lookupswitch = 0xAB,
      ireturn = 0xAC,
      lreturn = 0xAD,
      freturn = 0xAE,
      dreturn = 0xAF,
      areturn = 0xB0,
      return_ = 0xB1,
      getstatic = 0xB2,
      putstatic = 0xB3,
      getfield = 0xB4,
      putfield = 0xB5,
      invokevirtual = 0xB6,
      invokespecial = 0xB7,
      invokestatic = 0xB8,
      invokeinterface = 0xB9,
      new_ = 0xBB,
      newarray = 0xBC,
      anewarray = 0xBD,
      arraylength = 0xBE,
      athrow = 0xBF,
      checkcast = 0xC0,
      instanceof
      = 0xC1,
      monitorenter = 0xC2,
      monitorexit = 0xC3,
      wide = 0xC4,
      multianewarray = 0xC5,
      ifnull = 0xC6,
      ifnonnull = 0xC7,
      goto_w = 0xC8,
      jsr_w = 0xC9,
    };
    memset(sign, 0, 0x100);
    memset(arg, 0, 0x100);
    sign[nop] = "nop", sign[aconst_null] = "aconst_null",
    sign[iconst_m1] = "iconst_m1", sign[iconst_0] = "iconst_0",
    sign[iconst_1] = "iconst_1", sign[iconst_2] = "iconst_2",
    sign[iconst_3] = "iconst_3", sign[iconst_4] = "iconst_4",
    sign[iconst_5] = "iconst_5", sign[lconst_0] = "lconst_0",
    sign[lconst_1] = "lconst_1", sign[fconst_0] = "fconst_0",
    sign[fconst_1] = "fconst_1", sign[fconst_2] = "fconst_2",
    sign[dconst_0] = "dconst_0", sign[dconst_1] = "dconst_1",
    sign[bipush] = "bipush", sign[sipush] = "sipush", sign[ldc] = "ldc",
    sign[ldc_w] = "ldc_w", sign[ldc2_w] = "ldc2_w", sign[aload] = "aload",
    sign[aload_0] = "aload_0", sign[aload_1] = "aload_1",
    sign[aload_2] = "aload_2", sign[aload_3] = "aload_3", sign[iload] = "iload",
    sign[iload_0] = "iload_0", sign[iload_1] = "iload_1",
    sign[iload_2] = "iload_2", sign[iload_3] = "iload_3", sign[lload] = "lload",
    sign[lload_0] = "lload_0", sign[lload_1] = "lload_1",
    sign[lload_2] = "lload_2", sign[lload_3] = "lload_3", sign[fload] = "fload",
    sign[fload_0] = "fload_0", sign[fload_1] = "fload_1",
    sign[fload_2] = "fload_2", sign[fload_3] = "fload_3", sign[dload] = "dload",
    sign[dload_0] = "dload_0", sign[dload_1] = "dload_1",
    sign[dload_2] = "dload_2", sign[dload_3] = "dload_3",
    sign[aaload] = "aaload", sign[iaload] = "iaload", sign[laload] = "laload",
    sign[faload] = "faload", sign[daload] = "daload", sign[baload] = "baload",
    sign[caload] = "caload", sign[saload] = "saload", sign[astore] = "astore",
    sign[astore_0] = "astore_0", sign[astore_1] = "astore_1",
    sign[astore_2] = "astore_2", sign[astore_3] = "astore_3",
    sign[istore] = "istore", sign[istore_0] = "istore_0",
    sign[istore_1] = "istore_1", sign[istore_2] = "istore_2",
    sign[istore_3] = "istore_3", sign[lstore] = "lstore",
    sign[lstore_0] = "lstore_0", sign[lstore_1] = "lstore_1",
    sign[lstore_2] = "lstore_2", sign[lstore_3] = "lstore_3",
    sign[fstore] = "fstore", sign[fstore_0] = "fstore_0",
    sign[fstore_1] = "fstore_1", sign[fstore_2] = "fstore_2",
    sign[fstore_3] = "fstore_3", sign[dstore] = "dstore",
    sign[dstore_0] = "dstore_0", sign[dstore_1] = "dstore_1",
    sign[dstore_2] = "dstore_2", sign[dstore_3] = "dstore_3",
    sign[aastore] = "aastore", sign[iastore] = "iastore",
    sign[lastore] = "lastore", sign[fastore] = "fastore",
    sign[dastore] = "dastore", sign[bastore] = "bastore",
    sign[castore] = "castore", sign[sastore] = "sastore", sign[pop] = "pop",
    sign[pop2] = "pop2", sign[dup] = "dup", sign[dup_x1] = "dup_x1",
    sign[dup_x2] = "dup_x2", sign[dup2] = "dup2", sign[dup2_x1] = "dup2_x1",
    sign[dup2_x2] = "dup2_x2", sign[swap] = "swap", sign[iadd] = "iadd",
    sign[ladd] = "ladd", sign[fadd] = "fadd", sign[dadd] = "dadd",
    sign[isub] = "isub", sign[lsub] = "lsub", sign[fsub] = "fsub",
    sign[dsub] = "dsub", sign[imul] = "imul", sign[lmul] = "lmul",
    sign[fmul] = "fmul", sign[dmul] = "dmul", sign[idiv] = "idiv",
    sign[ldiv_] = "ldiv_", sign[fdiv] = "fdiv", sign[ddiv] = "ddiv",
    sign[irem] = "irem", sign[lrem] = "lrem", sign[frem] = "frem",
    sign[drem] = "drem", sign[ineg] = "ineg", sign[lneg] = "lneg",
    sign[fneg] = "fneg", sign[dneg] = "dneg", sign[ishl] = "ishl",
    sign[lshl] = "lshl", sign[ishr] = "ishr", sign[lshr] = "lshr",
    sign[iushr] = "iushr", sign[lushr] = "lushr", sign[iand] = "iand",
    sign[land] = "land", sign[ior] = "ior", sign[lor] = "lor",
    sign[ixor] = "ixor", sign[lxor] = "lxor", sign[i2l] = "i2l",
    sign[i2f] = "i2f", sign[i2d] = "i2d", sign[l2i] = "l2i", sign[l2f] = "l2f",
    sign[l2d] = "l2d", sign[f2i] = "f2i", sign[f2l] = "f2l", sign[f2d] = "f2d",
    sign[d2i] = "d2i", sign[d2l] = "d2l", sign[d2f] = "d2f", sign[i2b] = "i2b",
    sign[i2c] = "i2c", sign[i2s] = "i2s", sign[lcmp] = "lcmp",
    sign[fcmpl] = "fcmpl", sign[fcmpg] = "fcmpg", sign[dcmpl] = "dcmpl",
    sign[dcmpg] = "dcmpg", sign[ifeq] = "ifeq", sign[ifne] = "ifne",
    sign[iflt] = "iflt", sign[ifge] = "ifge", sign[ifgt] = "ifgt",
    sign[ifle] = "ifle", sign[if_icmpeq] = "if_icmpeq",
    sign[if_icmpne] = "if_icmpne", sign[if_icmplt] = "if_icmplt",
    sign[if_icmpge] = "if_icmpge", sign[if_icmpgt] = "if_icmpgt",
    sign[if_icmple] = "if_icmple", sign[if_acmpeq] = "if_acmpeq",
    sign[if_acmpne] = "if_acmpne", sign[goto_] = "goto_", sign[jsr] = "jsr",
    sign[ret] = "ret", sign[tableswitch] = "tableswitch",
    sign[lookupswitch] = "lookupswitch", sign[ireturn] = "ireturn",
    sign[lreturn] = "lreturn", sign[freturn] = "freturn",
    sign[dreturn] = "dreturn", sign[areturn] = "areturn",
    sign[return_] = "return_", sign[getstatic] = "getstatic",
    sign[putstatic] = "putstatic", sign[getfield] = "getfield",
    sign[putfield] = "putfield", sign[invokevirtual] = "invokevirtual",
    sign[invokespecial] = "invokespecial", sign[invokestatic] = "invokestatic",
    sign[invokeinterface] = "invokeinterface", sign[new_] = "new_",
    sign[newarray] = "newarray", sign[anewarray] = "anewarray",
    sign[arraylength] = "arraylength", sign[athrow] = "athrow",
    sign[checkcast] = "checkcast", sign[ instanceof ] = "instanceof",
    sign[monitorenter] = "monitorenter", sign[monitorexit] = "monitorexit",
    sign[wide] = "wide", sign[multianewarray] = "multianewarray",
    sign[ifnull] = "ifnull", sign[ifnonnull] = "ifnonnull",
    sign[goto_w] = "goto_w", sign[jsr_w] = "jsr_w";
    arg[bipush] = "\x81", arg[sipush] = "\x82", arg[ldc] = "\x01",
    arg[ldc_w] = "\x02", arg[ldc2_w] = "\x02", arg[aload] = "\x01",
    arg[iload] = "\x01", arg[lload] = "\x01", arg[fload] = "\x01",
    arg[dload] = "\x01", arg[astore] = "\x01", arg[istore] = "\x01",
    arg[lstore] = "\x01", arg[fstore] = "\x01", arg[dstore] = "\x01",
    arg[iinc] = "\x01\x81", arg[ifeq] = "\x02", arg[ifne] = "\x02",
    arg[iflt] = "\x02", arg[ifge] = "\x02", arg[ifgt] = "\x02",
    arg[ifle] = "\x02", arg[if_icmpeq] = "\x02", arg[if_icmpne] = "\x02",
    arg[if_icmplt] = "\x02", arg[if_icmpge] = "\x02", arg[if_icmpgt] = "\x02",
    arg[if_icmple] = "\x02", arg[if_acmpeq] = "\x02", arg[if_acmpne] = "\x02",
    arg[goto_] = "\x02", arg[goto_w] = "\x04";
    initialized = true;
  }
}

void parseAssemblyCode(BYTE *code, DWORD len) {
  static const char *sign[0x100];
  static const char *arg[0x100];
  initAssemblyInfo(sign, arg);
  for (DWORD i = 0; i < len;) {
    const BYTE ins = code[i++];
    printf("%08X: %s", i, sign[ins] == NULL ? "unknown" : sign[ins]);
    if (arg[ins] == NULL) {
      putchar('\n');
      continue;
    }
    for (DWORD j = 0; arg[ins][j]; j++) {
      BYTE type = (BYTE)arg[ins][j];
      switch (type) {
      case 0x01:
        printf(" %hhu", code[i++]);
        break;
      case 0x02:
        printf(" %hu", swapWord(*(WORD *)&code[i]));
        i += 2;
        break;
      case 0x04:
        printf(" %u", swapDword(*(DWORD *)&code[i]));
        i += 4;
        break;
      case 0x81:
        printf(" %hhd", (char)code[i++]);
        break;
      case 0x82:
        printf(" %hd", (short)swapWord(*(WORD *)&code[i]));
        i += 2;
        break;
      case 0x84:
        printf(" %d", (int)swapDword(*(WORD *)&code[i]));
        i += 4;
        break;
      default:
        printf(" [invaild size]");
        i += (type & 0x7F);
        break;
      }
    }
    putchar('\n');
  }
}

int parseAttributeTable(BYTE **indexStr, FILE *classFile,
                        WORD indexArg[0x10000][2], bool disAsm,
                        FILE *codeOutput = NULL) {
  WORD name;
  DWORD len;
  WORD wVal;
  DWORD dwVal;
  BYTE *buffer;
  const char *nameStr;
  getWord(name);
  getDword(len);
  nameStr = String(name);
  printf("Attribute name: %s\n", nameStr);
  printf("Attribute length: %u\n", len);
  if (!strcmp(nameStr, "NULL")) {
    puts("Critical Error: attribute name not found!");
    puts("Press 'Enter' to continue.");
    fseek(classFile, len, ios::cur);
    getchar();
    return 0;
  }
  if (!strcmp(nameStr, "Code")) {
    getWord(wVal);
    printf("Max stack: %hu\n", wVal);
    getWord(wVal);
    printf("Max locals: %hu\n", wVal);
    getDword(dwVal);
    printf("Code length: %u\n", dwVal);
    buffer = new BYTE[dwVal];
    fread(buffer, 1, dwVal, classFile);
    if (codeOutput) {
      fwrite(buffer, 1, dwVal, codeOutput);
    }
    for (DWORD i = 0; i < dwVal; i++)
      printf("%02X ", buffer[i]);
    putchar('\n');
    if (disAsm)
      parseAssemblyCode(buffer, dwVal);
    delete buffer;
    getWord(wVal);
    printf("Exception table length: %hu\n", wVal);
    WORD res1;
    for (DWORD i = 0; i < wVal; i++) {
      getWord(res1);
      printf("Start pc: %hu\n", res1);
      getWord(res1);
      printf("End pc: %hu\n", res1);
      getWord(res1);
      printf("Handle pc: %hu\n", res1);
      getWord(res1);
      printf("Catch type: %hu\n", res1);
    }
    getWord(wVal);
    printf("Attribute count: %hu\n", wVal);
    int totalAdjust = 0;
    for (DWORD i = 0; i < wVal; i++)
      totalAdjust += parseAttributeTable(indexStr, classFile, indexArg, disAsm,
                                         codeOutput);
    return totalAdjust;
  } else if (!strcmp(nameStr, "ConstantValue")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "Deprecated")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "Exception")) {
    getWord(wVal);
    printf("Number of exceptions: %hu\n", wVal);
    WORD classIndex;
    for (DWORD i = 0; i < wVal; i++) {
      getWord(classIndex);
      printf("Exception class: %s\n", String(classIndex));
    }
  } else if (!strcmp(nameStr, "Exceptions")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "EnclosingMethod")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "InnerClasses")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "LineNumberTable")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "LocalVariableTable")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "StackMapTable")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "Signature")) {
    getWord(wVal);
    printf("Signature: %s\n", String(wVal));
  } else if (!strcmp(nameStr, "SourceFile")) {
    getWord(wVal);
    printf("Source file: %s\n", String(wVal));
  } else if (!strcmp(nameStr, "SourceDebugExtension")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "Synthetic")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "LocalVariableTypeTable")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "RuntimeVisibleAnnotations")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "RuntimeInvisibleAnnotations")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "RuntimeVisibleParameterAnnotations")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "RuntimeInvisibleParameterAnnotations")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "AnnotationDefault")) {
    errorSeek(classFile, len);
  } else if (!strcmp(nameStr, "BootstrapMethods")) {
    errorSeek(classFile, len);
  } else {
    puts("This attribute can't be resolved in this program now.");
    puts("Unknown attribute");
    fseek(classFile, len, ios::cur);
  }
  return 0;
}

int main(int argc, char **argv) {
  if (argc < 2) {
    puts("usage: ParseClass.exe [options] [ClassFile]");
    getchar();
    return 0;
  }
  FILE *classFile = NULL;
  bool disAsm = false;
  for (DWORD i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      if (!strcmp(argv[i] + 1, "d")) {
        disAsm = true;
      } else {
        printf("unknown option '%s'\n", argv[i]);
        getchar();
        return 0;
      }
    } else {
      if (classFile) {
        puts("Only allow one input file!");
        getchar();
        return 0;
      }
      classFile = fopen(argv[i], "rb");
    }
  }
  if (!classFile) {
    puts("File not found!");
    getchar();
    return 0;
  }
  static BYTE *utf8Str[0x10000] = {NULL};
  static BYTE *indexStr[0x10000] = {NULL};
  static BYTE constType[0x10000] = {0};
  static WORD indexArg[0x10000][2] = {0};
  CLASS_FILE_HEADER classHdr;
  getDword(classHdr.magic);
  getWord(classHdr.minor_version);
  getWord(classHdr.major_version);
  printf("magic: %08X\n", classHdr.magic);
  printf("version: %hu.%hu\n", classHdr.major_version, classHdr.minor_version);
  if (classHdr.magic != CLASS_FILE_MAGIC) {
    puts("class magic error!");
    getchar();
    return 0;
  }
  WORD constCnt;
  getWord(constCnt);
  printf("constant count: %d\n", constCnt - 1);
  for (DWORD i = 1; i < constCnt; i++) {
    BYTE tag;
    BYTE bVal;
    WORD res1, res2;
    DWORD dwVal;
    QWORD qwVal;
    float real4;
    double real8;
    printf("%u\n", i);
    fread(&tag, 1, 1, classFile);
    constType[i] = tag;
    switch (tag) {
    case CLASS_CONSTANT_UTF8:
      getWord(res1);
      utf8Str[i] = new BYTE[res1 + 1];
      fread(utf8Str[i], 1, res1, classFile);
      utf8Str[i][res1] = '\000';
      indexStr[i] = utf8Str[i];
      printf("UTF-8 String: %s\n", utf8Str[i]);
      break;
    case CLASS_CONSTANT_INTEGER:
      getDword(dwVal);
      printf("Integer: %d\n", dwVal);
      break;
    case CLASS_CONSTANT_FLOAT:
      getDword(*(DWORD *)&real4);
      printf("Float: %f\n", real4);
      break;
    case CLASS_CONSTANT_LONG:
      getQword(qwVal);
      printf("Long: %lld\n", qwVal);
      break;
    case CLASS_CONSTANT_DOUBLE:
      getQword(*(QWORD *)&real8);
      printf("Double: %lf\n", real8);
      i++;
      break;
    case CLASS_CONSTANT_CLASS_REF:
      getWord(res1);
      indexArg[i][0] = res1;
      printf("Class index: %hu\n", res1);
      break;
    case CLASS_CONSTANT_STRING:
      getWord(res1);
      indexArg[i][0] = res1;
      printf("String index: %hu\n", res1);
      break;
    case CLASS_CONSTANT_FIELD_REF:
      getWord(res1);
      getWord(res2);
      indexArg[i][0] = res1;
      indexArg[i][1] = res2;
      printf("Field index [class]: %hu [name and type]: %hu\n", res1, res2);
      break;
    case CLASS_CONSTANT_METHOD_REF:
      getWord(res1);
      getWord(res2);
      indexArg[i][0] = res1;
      indexArg[i][1] = res2;
      printf("Method index [class]: %hu [name and type]: %hu\n", res1, res2);
      break;
    case CLASS_CONSTANT_INTERFACE_METHOD_REF:
      getWord(res1);
      getWord(res2);
      indexArg[i][0] = res1;
      indexArg[i][1] = res2;
      printf("Interface method index [class]: %hu [name and type]: %hu\n", res1,
             res2);
      break;
    case CLASS_CONSTANT_NAME_AND_TYPE:
      getWord(res1);
      getWord(res2);
      indexArg[i][0] = res1;
      indexArg[i][1] = res2;
      printf("Name and type index [name]: %hu [type descriptor]: %hu\n", res1,
             res2);
      break;
    case CLASS_CONSTANT_METHOD_HANDLE:
      getByte(bVal);
      getWord(res1);
      indexArg[i][0] = (WORD)bVal;
      indexArg[i][1] = res1;
      printf("Method handle [kind]: %hhu [index]: %hu\n", bVal, res1);
      break;
    case CLASS_CONSTANT_METHOD_TYPE:
      getWord(res1);
      indexArg[i][0] = res1;
      printf("Method type index: %hu\n", res1);
      break;
    case CLASS_CONSTANT_INVOKE_DYNAMIC:
      getWord(res1);
      getWord(res2);
      indexArg[i][0] = res1;
      indexArg[i][1] = res2;
      printf(
          "Invoke dynamic [method attribute index]: %hu [name and type]: %hu\n",
          res1, res2);
      break;
    default:
      printf("Unknown constant type: %hhu\n", tag);
      break;
    }
  }
  for (DWORD i = 1; i < constCnt; i++)
    if (constType[i] == CLASS_CONSTANT_STRING)
      indexStr[i] = utf8Str[indexArg[i][0]];
  for (DWORD i = 1; i < constCnt; i++)
    if (constType[i] == CLASS_CONSTANT_CLASS_REF)
      indexStr[i] = indexStr[indexArg[i][0]];
  fseek(classFile, 10, ios::beg); // FILE_BEGIN = 0
  for (DWORD i = 1; i < constCnt; i++) {
    BYTE tag;
    BYTE bVal;
    WORD res1, res2;
    DWORD dwVal;
    QWORD qwVal;
    float real4;
    double real8;
    fread(&tag, 1, 1, classFile);
    switch (tag) {
    case CLASS_CONSTANT_UTF8:
      getWord(res1);
      utf8Str[i] = new BYTE[res1 + 1];
      fread(utf8Str[i], 1, res1, classFile);
      utf8Str[i][res1] = '\000';
      printf("UTF-8 String: %s\n", utf8Str[i]);
      break;
    case CLASS_CONSTANT_INTEGER:
      getDword(dwVal);
      printf("Integer: %d\n", dwVal);
      break;
    case CLASS_CONSTANT_FLOAT:
      getDword(*(DWORD *)&real4);
      printf("Float: %f\n", real4);
      break;
    case CLASS_CONSTANT_LONG:
      getQword(qwVal);
      printf("Long: %lld\n", qwVal);
      break;
    case CLASS_CONSTANT_DOUBLE:
      getQword(*(QWORD *)&real8);
      printf("Double: %lf\n", real8);
      i++;
      break;
    case CLASS_CONSTANT_CLASS_REF:
      getWord(res1);
      printf("Class: %s\n", String(res1));
      break;
    case CLASS_CONSTANT_STRING:
      getWord(res1);
      printf("String: %s\n", String(res1));
      break;
    case CLASS_CONSTANT_FIELD_REF:
      getWord(res1);
      getWord(res2);
      printf("Field\n[class]: %s\n[name]: %s\n[type]: %s\n", String(res1),
             String(indexArg[res2][0]), String(indexArg[res2][1]));
      break;
    case CLASS_CONSTANT_METHOD_REF:
      getWord(res1);
      getWord(res2);
      printf("Method\n[class]: %s\n[name]: %s\n[type]: %s\n", String(res1),
             String(indexArg[res2][0]), String(indexArg[res2][1]));
      break;
    case CLASS_CONSTANT_INTERFACE_METHOD_REF:
      getWord(res1);
      getWord(res2);
      printf("Interface method\n[class]: %s\n[name]: %s\n[type]: %s\n",
             String(res1), String(indexArg[res2][0]),
             String(indexArg[res2][1]));
      break;
    case CLASS_CONSTANT_NAME_AND_TYPE:
      getWord(res1);
      getWord(res2);
      printf("Name and type\n[name]: %s\n[type descriptor]: %s\n", String(res1),
             String(res2));
      break;
    case CLASS_CONSTANT_METHOD_HANDLE:
      getByte(bVal);
      getWord(res1);
      printf("Method handle\n[kind]: %hhu\n[descriptor]: %s\n", bVal,
             String(res1));
      break;
    case CLASS_CONSTANT_METHOD_TYPE:
      getWord(res1);
      printf("Method type: %s\n", String(res1));
      break;
    case CLASS_CONSTANT_INVOKE_DYNAMIC:
      getWord(res1);
      getWord(res2);
      printf("Invoke dynamic\n[method attribute index]: %hu\n[name]: "
             "%s\n[type]: %s\n",
             res1, String(indexArg[res2][0]), String(indexArg[res2][1]));
      break;
    default:
      printf("Unknown constant type: %hhu\n", tag);
      printf("Debug message:\n");
      printf("Constant count: %d\n", constCnt - 1);
      printf("UTF-8 String: %lld\n",
             count(constType + 1, constType + constCnt, CLASS_CONSTANT_UTF8));
      printf("Integer: %lld\n", count(constType + 1, constType + constCnt,
                                      CLASS_CONSTANT_INTEGER));
      printf("Float: %lld\n",
             count(constType + 1, constType + constCnt, CLASS_CONSTANT_FLOAT));
      printf("Long: %lld\n",
             count(constType + 1, constType + constCnt, CLASS_CONSTANT_LONG));
      printf("Double: %lld\n",
             count(constType + 1, constType + constCnt, CLASS_CONSTANT_DOUBLE));
      printf("Class: %lld\n", count(constType + 1, constType + constCnt,
                                    CLASS_CONSTANT_CLASS_REF));
      printf("String: %lld\n",
             count(constType + 1, constType + constCnt, CLASS_CONSTANT_STRING));
      printf("Field: %lld\n", count(constType + 1, constType + constCnt,
                                    CLASS_CONSTANT_FIELD_REF));
      printf("Method: %lld\n", count(constType + 1, constType + constCnt,
                                     CLASS_CONSTANT_METHOD_REF));
      printf("Interface method: %lld\n",
             count(constType + 1, constType + constCnt,
                   CLASS_CONSTANT_INTERFACE_METHOD_REF));
      printf("Name and type: %lld\n", count(constType + 1, constType + constCnt,
                                            CLASS_CONSTANT_NAME_AND_TYPE));
      printf("Method handle: %lld\n", count(constType + 1, constType + constCnt,
                                            CLASS_CONSTANT_METHOD_HANDLE));
      printf("Method type: %lld\n", count(constType + 1, constType + constCnt,
                                          CLASS_CONSTANT_METHOD_TYPE));
      printf("Invoke dynamic: %lld\n",
             count(constType + 1, constType + constCnt,
                   CLASS_CONSTANT_INVOKE_DYNAMIC));
      getchar();
      return 0;
    }
  }
  {
    WORD accessFlags, thisClass, superClass;
    getWord(accessFlags);
    getWord(thisClass);
    getWord(superClass);
    printf("Access flag: ");
    parseClassAcc(accessFlags);
    printf("This class: %s\n", String(thisClass));
    printf("Super class: %s\n", String(superClass));
  }
  {
    WORD interfaceCnt;
    getWord(interfaceCnt);
    printf("Interface count: %hu\n", interfaceCnt);
    WORD res1;
    for (DWORD i = 0; i < interfaceCnt; i++) {
      getWord(res1);
      printf("Name: %s\n", String(res1));
    }
  }
  {
    WORD fieldCnt;
    getWord(fieldCnt);
    printf("Field count: %hu\n", fieldCnt);
    WORD res1;
    for (DWORD i = 0; i < fieldCnt; i++) {
      getWord(res1);
      printf("Access flag: ");
      parseFieldAcc(res1);
      getWord(res1);
      printf("Name: %s\n", String(res1));
      getWord(res1);
      printf("Descriptor: %s\n", String(res1));
      getWord(res1);
      printf("Attribute count (Field): %hu\n", res1);
      for (DWORD j = 0; j < res1; j++) {
        parseAttributeTable(indexStr, classFile, indexArg, disAsm);
      }
    }
  }
  {
    WORD methodCnt;
    getWord(methodCnt);
    WORD res1, res2;
    for (DWORD i = 0; i < methodCnt; i++) {
      getWord(res1);
      printf("Access flag: ");
      parseMethodAcc(res1);
      getWord(res1);
      printf("Name: %s\n", String(res1));
      getWord(res2);
      printf("Descriptor: %s\n", String(res2));
      static char fileName[MAX_PATH + 1];
      fileName[MAX_PATH] = '\000';
      if (!strcmp(String(res1), "<init>"))
        snprintf(fileName, MAX_PATH, "(init)%s", String(res2));
      else
        snprintf(fileName, MAX_PATH, "%s%s", String(res1), String(res2));
      replace(fileName, fileName + MAX_PATH, '/', '.');
      FILE *codeFile = fopen(fileName, "wb");
      if (!codeFile)
        puts("Open file error!");
      getWord(res1);
      printf("Attribute count (Method): %hu\n", res1);
      for (DWORD j = 0; j < res1; j++) {
        parseAttributeTable(indexStr, classFile, indexArg, disAsm, codeFile);
      }
      if (codeFile)
        fclose(codeFile);
    }
  }
  {
    WORD attrCnt;
    getWord(attrCnt);
    printf("Attribute count (Global): %hu\n", attrCnt);
    for (DWORD i = 0; i < attrCnt; i++) {
      parseAttributeTable(indexStr, classFile, indexArg, disAsm);
    }
  }
  fclose(classFile);
  getchar();
  return 0;
}