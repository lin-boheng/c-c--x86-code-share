#include <windows.h>
#include <iostream>
  
#include "ATLProject1_i.h"
#include "ATLProject1_i.c"

int TestAdd(LONG num1, LONG num2, PLONG sum)
{
    CoInitialize(NULL);
    MULTI_QI     qi;
    long         hr;
    ZeroMemory(&qi, sizeof(qi));
    qi.pIID = &IID_IATLSimpleObject;
    qi.pItf = NULL;
    hr = CoCreateInstanceEx(CLSID_ATLSimpleObject, NULL, CLSCTX_LOCAL_SERVER, NULL, 1, &qi);
    if(FAILED(hr) || FAILED(qi.hr)){
        std::cout<<hr<<std::endl;
        std::cout<<qi.hr<<std::endl;
        //CO_E_SERVER_EXEC_FAILURE;
        return -1;  //连接服务器失败
    }
    IATLSimpleObject* pT = NULL;
    qi.pItf->QueryInterface(&pT);
    qi.pItf->Release();
    long np = pT->ComServiceAdd(num1, num2, sum);
    pT->Release();
    CoUninitialize();
    return *sum;
}

int main(int argc, TCHAR* argv[])
{  
    LONG num1 = 10;
    LONG num2 = 20;
    LONG sum = 0;
    LONG np = TestAdd(num1, num2, &sum);
    printf("np = %ld\n", np);
    printf("sum = %ld\n", sum);
    return 0;
}

__CRT_UUID_DECL(IATLSimpleObject, 0xd2f10de5, 0x0003, 0x4000, 0xa2, 0xf6, 0xa8, 0x9f, 0xc7, 0xe1, 0xbd, 0x84)
__CRT_UUID_DECL(ATLSimpleObject, 0xa82dc89f, 0x0ac2, 0x4b2f, 0x80, 0x04, 0x9a, 0x1b, 0x5f, 0xe7, 0xa5, 0xf8)
