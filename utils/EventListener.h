#include<windows.h>
struct Event{
    POINT CursorPos;
    HWND Window;
    RECT WindowRect;
    POINT Offset;
    Event(){
        GetCursorPos(&CursorPos);
        Window=GetForegroundWindow();
        GetWindowRect(Window,&WindowRect);
        Offset.x=CursorPos.x-WindowRect.left;
        Offset.y=CursorPos.y-WindowRect.top;
    }
};
#define EventListener(Func) Func ## W
typedef void(*CallBack)(Event);
unsigned long __stdcall MouseMoveEventListener(void* fptr){
    CallBack callbackfn=(CallBack)fptr;
    POINT Before;
    POINT After;
    while(true){
        GetCursorPos(&After);
        if(Before.x!=After.x||Before.y!=After.y){
            Before=After;
            callbackfn(Event());
        }
        Sleep(1);
    }
}
void __fastcall MouseMoveEventListenerW(CallBack callbackfn){
    CloseHandle(
        CreateThread(NULL,0,MouseMoveEventListener,(void*)callbackfn,0,NULL)
    );
}
#define MouseMoveEventListener EventListener(MouseMoveEventListener)
unsigned long __stdcall LeftDownEventListener(void* fptr){
    CallBack callbackfn=(CallBack)fptr;
    INPUT_RECORD Rec;
    while(true){
        ReadConsoleInput(
            GetStdHandle(STD_INPUT_HANDLE),
            &Rec,
            sizeof(INPUT_RECORD),
            NULL
        );
        if(Rec.EventType==MOUSE_EVENT&&Rec.Event.MouseEvent.dwButtonState==FROM_LEFT_1ST_BUTTON_PRESSED)
            callbackfn(Event());
        Sleep(1);
    }
}
void __fastcall LeftDownEventListenerW(CallBack callbackfn){
    CloseHandle(
        CreateThread(NULL,0,LeftDownEventListener,(void*)callbackfn,0,NULL)
    );
}
#define LeftDownEventListener EventListener(LeftDownEventListener)
unsigned long __stdcall RightDownEventListener(void* fptr){
    CallBack callbackfn=(CallBack)fptr;
    INPUT_RECORD Rec;
    while(true){
        ReadConsoleInput(
            GetStdHandle(STD_INPUT_HANDLE),
            &Rec,
            sizeof(INPUT_RECORD),
            NULL
        );
        if(Rec.EventType==MOUSE_EVENT&&Rec.Event.MouseEvent.dwButtonState==RIGHTMOST_BUTTON_PRESSED)
            callbackfn(Event());
        Sleep(1);
    }
}
void __fastcall RightDownEventListenerW(CallBack callbackfn){
    CloseHandle(
        CreateThread(NULL,0,RightDownEventListener,(void*)callbackfn,0,NULL)
    );
}
#define RightDownEventListener EventListener(RightDownEventListener)
struct TimeoutTask{
	void(*callbackfn)(void*);
	DWORD timeout;
	void* param;
	TimeoutTask(void(CALLBACK *callbackfn)(void*),DWORD timeout,void* param=NULL){
		this->callbackfn=callbackfn;
		this->timeout=timeout;
		this->param=param;
	}
};
unsigned long __stdcall SetTimeout(void* fptr){
	TimeoutTask* task=(TimeoutTask*)fptr;
	Sleep(task->timeout);
	task->callbackfn(task->param);
	delete task;
}
void __fastcall SetTimeoutW(void(*callbackfn)(void*),DWORD timeout,void* param=NULL){
	CloseHandle(
		CreateThread(NULL,0,SetTimeout,(void*)new TimeoutTask(callbackfn,timeout,param),0,NULL)
	);
}
#define SetTimeout EventListener(SetTimeout)
