typedef float Float;

#define PI 3.1415926535897932384626433832795

#define Sqrt(x) ((Float)__builtin_sqrt(x))
#define Sin(x) ((Float)__builtin_sin(x))
#define Cos(x) ((Float)__builtin_cos(x))

class Vector3;
class Quaternion;

class Vector3
{
public:
    Float x;
    Float y;
    Float z;

    static const Vector3 up;
    static const Vector3 down;
    static const Vector3 left;
    static const Vector3 right;
    static const Vector3 forward;
    static const Vector3 back;

    Vector3();
    Vector3(Vector3 &&) = default;
    Vector3(const Vector3 &) = default;
    Vector3 &operator=(Vector3 &&) = default;
    Vector3 &operator=(const Vector3 &) = default;
    ~Vector3();
    Vector3(Float, Float, Float);
    Vector3 operator+(Vector3) const;
    Vector3 operator-(Vector3) const;
    Vector3 operator*(Float) const;
    Vector3 operator/(Float) const;
    Vector3 &operator+=(Vector3);
    Vector3 &operator-=(Vector3);
    Vector3 &operator*=(Float);
    Vector3 &operator/=(Float);
    Vector3 operator-() const;
    Vector3 &operator*=(Quaternion);
    Vector3 &operator/=(Quaternion);
    
    static Float Dot(Vector3, Vector3);
    static Vector3 Cross(Vector3, Vector3);

    Vector3 Normal() const;
    Vector3 &Normalize();
    Float Magnitude() const;
    Float SqrMagnitude() const;
    Vector3 Reflect(Vector3) const;
};

class Quaternion
{
public:
    Float x;
    Float y;
    Float z;
    Float w;

    Quaternion();
    Quaternion(Quaternion &&) = default;
    Quaternion(const Quaternion &) = default;
    Quaternion &operator=(Quaternion &&) = default;
    Quaternion &operator=(const Quaternion &) = default;
    ~Quaternion();
    Quaternion(Float, Float, Float, Float);
    Quaternion(Vector3, Float);
    Quaternion operator*(Quaternion) const;
    Quaternion operator*(Float) const;
    Quaternion operator/(Quaternion) const;
    Quaternion operator/(Float) const;
    Quaternion &operator*=(Quaternion);
    Quaternion &operator*=(Float);
    Quaternion &operator/=(Quaternion);
    Quaternion &operator/=(Float);
    Quaternion operator-() const;
    Vector3 operator*(Vector3) const;

    static Quaternion FromTo(Vector3, Vector3);
    static Quaternion Reflection(Vector3, Vector3);
    static Quaternion AxisAngle(Vector3, Float);
    static Quaternion EulerZXY(Float, Float, Float);
    static Quaternion EulerZXY(Vector3);

    Quaternion Conjugate() const;
    Quaternion Inverse() const;
    Quaternion Normal() const;
    Quaternion &Normalize();
    Float Magnitude() const;
    Float SqrMagnitude() const;

private:
    Vector3 ToVector3() const;
};

Vector3::Vector3()
{
}

Vector3::~Vector3()
{
}

Vector3::Vector3(Float x, Float y, Float z):x(x), y(y), z(z)
{
}

const Vector3 Vector3::up       = { 0.0F, 1.0F, 0.0F};
const Vector3 Vector3::down     = { 0.0F,-1.0F, 0.0F};
const Vector3 Vector3::left     = {-1.0F, 0.0F, 0.0F};
const Vector3 Vector3::right    = { 1.0F, 0.0F, 0.0F};
const Vector3 Vector3::forward  = { 0.0F, 0.0F, 1.0F};
const Vector3 Vector3::back     = { 0.0F, 0.0F,-1.0F};

Vector3 Vector3::operator*(Float w) const
{
    return Vector3(x * w, y * w, z * w);
}

Vector3 &Vector3::operator*=(Float w)
{
    x *= w; y *= w; z *= w;
    return *this;
}

Vector3 Vector3::operator+(Vector3 v) const
{
    return Vector3(x + v.x, y + v.y, z + v.z);
}

Vector3 &Vector3::operator+=(Vector3 v)
{
    x += v.x; y += v.y; z += v.z;
    return *this;
}

Vector3 Vector3::operator-() const
{
    return Vector3(-x, -y, -z);
}

Vector3 Vector3::operator-(Vector3 v) const
{
    return Vector3(x - v.x, y - v.y, z - v.z);
}

Vector3 &Vector3::operator-=(Vector3 v)
{
    x -= v.x; y -= v.y; z -= v.z;
    return *this;
}

Vector3 Vector3::operator/(Float w) const
{
    w = 1.0F / w;
    return Vector3(x * w, y * w, z * w);
}

Vector3 &Vector3::operator/=(Float w)
{
    w = 1.0F / w;
    x *= w; y *= w; z *= w;
    return *this;
}

Vector3 &Vector3::operator*=(Quaternion q)
{
    return (*this = q * *this);
}

Vector3 &Vector3::operator/=(Quaternion q)
{
    return (*this = q.Inverse() * *this);
}

Vector3 Vector3::Cross(Vector3 v1, Vector3 v2)
{
    return Vector3( v1.y * v2.z - v1.z * v2.y,
                    v1.z * v2.x - v1.x * v2.z,
                    v1.x * v2.y - v1.y * v2.x);
}

Float Vector3::Dot(Vector3 v1, Vector3 v2)
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

Float Vector3::Magnitude() const
{
    return Sqrt(x * x + y * y + z * z);
}

Float Vector3::SqrMagnitude() const
{
    return x * x + y * y + z * z;
}

Vector3 Vector3::Normal() const
{
    return *this / this->Magnitude();
}

Vector3 &Vector3::Normalize()
{
    return (*this /= this->Magnitude());
}

Vector3 Vector3::Reflect(Vector3 v) const
{
    return Quaternion(Normal(), 0.0F) / Quaternion(v.Normal(), 0.0F) * v;
}

Quaternion::Quaternion()
{
}

Quaternion::~Quaternion()
{
}

Quaternion::Quaternion(Float x, Float y, Float z, Float w):x(x), y(y), z(z), w(w)
{
}

Quaternion::Quaternion(Vector3 v, Float w):x(v.x), y(v.y), z(v.z), w(w)
{
}

Quaternion Quaternion::operator*(Quaternion q) const
{
    return Quaternion(  w * q.x + x * q.w + y * q.z - z * q.y,
                        w * q.y - x * q.z + y * q.w + z * q.x,
                        w * q.z + x * q.y - y * q.x + z * q.w,
                        w * q.w - x * q.x - y * q.y - z * q.z);
}

Quaternion Quaternion::operator*(Float s) const
{
    return Quaternion(x * s, y * s, z * s, w * s);
}

Quaternion Quaternion::operator/(Quaternion q) const
{
    return *this * q.Inverse();
}

Quaternion Quaternion::operator/(Float s) const
{
    s = 1.0F / s;
    return Quaternion(x * s, y * s, z * s, w * s);
}

Quaternion &Quaternion::operator*=(Quaternion q)
{
    return (*this = *this * q);
}

Quaternion &Quaternion::operator*=(Float s)
{
    x *= s; y *= s; z *= s; w *= s;
    return *this;
}

Quaternion &Quaternion::operator/=(Quaternion q)
{
    return (*this *= *this * q.Inverse());
}

Quaternion &Quaternion::operator/=(Float s)
{
    s = 1.0F / s;
    x *= s; y *= s; z *= s; w *= s;
    return *this;
}

Quaternion Quaternion::operator-() const
{
    return Quaternion(-x, -y, -z, -w);
}

Vector3 Quaternion::operator*(Vector3 v) const
{
    return (*this * Quaternion(v, 0.0F) * this->Inverse()).ToVector3();
}

Quaternion Quaternion::FromTo(Vector3 from, Vector3 to)
{
    from.Normalize();
    to.Normalize();
    return Quaternion((from + to).Normal(), 0.0F) / Quaternion(from, 0.0F);
}

Quaternion Quaternion::Reflection(Vector3 axis, Vector3 v)
{
    return Quaternion(axis.Normal(), 0.0F) / Quaternion(v.Normal(), 0.0F);
}

Quaternion Quaternion::AxisAngle(Vector3 axis, Float angle)
{
    angle *= 0.00872664625997164788461845384244F;
    return Quaternion(axis.Normal() * Sin(angle), Cos(angle));
}

Quaternion Quaternion::EulerZXY(Vector3 angle)
{
    return  Quaternion::AxisAngle(Vector3::up, angle.y) *
            Quaternion::AxisAngle(Vector3::right, angle.x) *
            Quaternion::AxisAngle(Vector3::forward, angle.z);
}

Quaternion Quaternion::EulerZXY(Float x, Float y, Float z)
{
    return  Quaternion::AxisAngle(Vector3::up, y) *
            Quaternion::AxisAngle(Vector3::right, x) *
            Quaternion::AxisAngle(Vector3::forward, z);
}

Quaternion Quaternion::Conjugate() const
{
    return Quaternion(-x, -y, -z, w);
}

Quaternion Quaternion::Inverse() const
{
    return Quaternion(-x, -y, -z, w) / this->SqrMagnitude();
}

Quaternion Quaternion::Normal() const
{
    return *this / this->Magnitude();
}

Quaternion &Quaternion::Normalize()
{
    return (*this /= this->Magnitude());
}

Float Quaternion::Magnitude() const
{
    return Sqrt(x * x + y * y + z * z + w * w);
}

Float Quaternion::SqrMagnitude() const
{
    return x * x + y * y + z * z + w * w;
}

Vector3 Quaternion::ToVector3() const
{
    return Vector3(x, y, z);
}

#include <cstdio>
#include <windows.h>

int main(){
    HANDLE hOut=GetStdHandle(STD_OUTPUT_HANDLE);
    float a=0.0F;
    while(true){
        Quaternion q=Quaternion::EulerZXY(0.0F,0.0F,a);
        for(int i=-39;i<=39;i++)
            for(int j=-39;j<0;j++){
                if(i*i+j*j>=1600)
                    continue;
                float k=Sqrt(1600-i*i-j*j);
                char c=' ';
                SetConsoleCursorPosition(hOut,{SHORT(40+i),SHORT(-j)});
                Vector3 v=Vector3(i,j,-k).Reflect(Vector3::back);
                Float s=(40+j)/(-v.y);
                if(Sqrt(i*i+j*j)/k/s>0.05F)
                    c='#';
                else if(Sqrt(i*i+j*j)/k/s>0.01F)
                    c='.';
                Vector3 p(v.x*s+i,v.z*s-k,0.0F);
                p*=q;
                putchar(((((int)p.x+400000)%40<20)^(((int)p.y+400000)%40<20))?c:' ');
            }
        a+=3.0F;
    }
    getchar();
    return 0;
}
