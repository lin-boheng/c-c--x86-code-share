#include <iostream>

using namespace std;

int main() {
  struct {
    float x;
    float y;
    float z;
    float w;
  } a = {2.0f, 3.4f, -4.7f, 9.0f}, b = {4.4f, -10.8f, 1.2f, -3.1f};
  asm("movaps (%0),%%xmm0\n"
      "movaps (%1),%%xmm1\n"
      "mulps %%xmm1,%%xmm0\n"
      "movaps %%xmm0,(%0)\n" ::"r"(&a),
      "r"(&b));
  asm("flds (%0)\n"
      "flds (%1)\n"
      "fdivp %%st(0),%%st(1)\n"
      "fstps (%0)\n" ::"r"(&a),
      "r"(&b));
  asm("wait");
  asm("movl $0x3F800000,8(%0)" ::"r"(&a));
  cout << a.x << ' ' << a.y << ' ' << a.z << ' ' << a.w << endl;
  getchar();
  return 0;
  // single character (like 'a'): char
  // multiple character (like 'aaa'): int
  //"aaa": const char*
  // L"aaa": const wchar_t*
  // integer suffix: u => unsigned int => (too large) unsigned long long
  //                ul => unsigned long => (too large) unsigned long long
  //                ull => unsigned long long
  //                [none] => int => (too large) long long
  //                l => long => (too large) long long
  //                ll => long long
  //                i => complex int
  //                j => complex int
  //                tips: 1i == 1j == i (sign 'i' is the same to 'j')
  // float suffix: f(float) [none](double) l(long double) q(__float128)
  // [complex]: i, j
  // single:
  // 0-01111111-00000000000000000000000
  // double:
  // 0-01111111111-0000000000000000000000000000000000000000000000000000
  // long double:
  // 0-01111111111-00000000000000000000000000000000000000000000000000000000000000000000
  // float128:
  // 0-0111111111111111-000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  //_Complex type => memory: |real|imagine|
}