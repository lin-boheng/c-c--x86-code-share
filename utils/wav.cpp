#include<iostream>
#include<fstream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<map>
#include<windows.h>
using namespace std;
void gen(char* buf,char* data,int len){
	struct{
		char riff[4]={'R','I','F','F'};
		DWORD RiffSize;
		char wave[4]={'W','A','V','E'};
		char fmt[4]={'f','m','t',' '};
		DWORD fmtLen=0x10;
		SHORT tag=1;
		SHORT channels=1;
		DWORD samplesPerSec=44100;
		DWORD bytesPerSec=44100;
		SHORT blockAlign=1;
		SHORT bitsPerSample=8;
		char data[4]={'d','a','t','a'};
		DWORD dataSize;
	}wav;
	wav.dataSize=len;
	wav.RiffSize=wav.dataSize+36;
	memcpy(buf,&wav,sizeof(wav));
	memcpy(buf+sizeof(wav),data,len);
}
int getline(char* s){
	static bool eof=false;
	if(eof)
		return EOF;
	int p=-1;
	do{
		s[++p]=getchar();
		if(s[p]==EOF){
			eof=true;
			s[p]='\0';
			return p;
		}
	}while(s[p]!='\n');
	s[p]='\0';
	return p;
}
bool space(char c){
	return c==' '||c=='\t';
}
struct stream{
	int size=100;
	int rec=0;
	int offset=0;
	char* buffer=new char[100];
	stream(){
		memset(buffer,0x80,size);
	}
	~stream(){
		delete[] buffer;
	}
	char& operator[](int x){
		x+=offset;
		if(x<0)
			return *(char*)NULL;
		if(x>=size){
			int s=size;
			while(size<=x)size<<=1;
			char* newbuf=new char[size];
			memcpy(newbuf,buffer,s);
			memset(newbuf+s,0x80,size-s);
			delete[] buffer;
			buffer=newbuf;
		}
		rec=max(rec,x);
		return buffer[x];
	}
	int getLen(){
		return rec+1;
	}
};
map<string,stream*> cache;
void play(stream* wave){
	if(wave==NULL)
		return;
	char* buf=new char[wave->getLen()+44];
	gen(buf,wave->buffer,wave->getLen());
	PlaySound(buf,NULL,SND_MEMORY|SND_SYNC|SND_NODEFAULT);
	delete[] buf;
}
void out(stream* wave,FILE* f){
	if(wave==NULL||f==NULL)
		return;
	char* buf=new char[wave->getLen()+44];
	gen(buf,wave->buffer,wave->getLen());
	fwrite(buf,1,wave->getLen()+44,f);
	delete[] buf;
}
void loadfile(stream* wave,FILE* f){
	struct{
		char riff[4]={'R','I','F','F'};
		DWORD RiffSize;
		char wave[4]={'W','A','V','E'};
		char fmt[4]={'f','m','t',' '};
		DWORD fmtLen=0x10;
		SHORT tag=1;
		SHORT channels=1;
		DWORD samplesPerSec=44100;
		DWORD bytesPerSec=44100;
		SHORT blockAlign=1;
		SHORT bitsPerSample=8;
		char data[4]={'d','a','t','a'};
		DWORD dataSize;
	}wav;
	fread(&wav,sizeof(wav),1,f);
	if(strcmp(wav.riff,"RIFF")||
	   strcmp(wav.wave,"WAVE")||
	   strcmp(wav.fmt,"fmt ")||
	   wav.fmtLen!=0x10||
	   wav.tag!=1||
	   wav.channels!=1||
	   wav.samplesPerSec!=44100||
	   wav.bytesPerSec!=44100||
	   wav.blockAlign!=1||
	   wav.bitsPerSample!=8||
	   strcmp(wav.data,"data"))
	   	return;
	char* buf=new char[wav.dataSize];
	fread(buf,1,wav.dataSize,f);
	for(int i=0;i<wav.dataSize;i++)
		wave->operator[](i)+=(buf[i]-0x80);
	delete[] buf;
}
void exec(char** arg){
	static stream* seg=cache["main"];
	int p=0;
	if(!strcmp(arg[p],"offset")){
		if(arg[++p]==NULL)
			return;
		double t;
		if(!strcmp(arg[p],"+")){
			if(arg[++p]==NULL)
				return;
			sscanf(arg[p],"%lf",&t);
			seg->offset+=(int)(44100*t);
		}else if(!strcmp(arg[p],"-")){
			if(arg[++p]==NULL)
				return;
			sscanf(arg[p],"%lf",&t);
			seg->offset-=(int)(44100*t);
		}else if(!strcmp(arg[p],"=")){
			if(arg[++p]==NULL)
				return;
			sscanf(arg[p],"%lf",&t);
			seg->offset=(int)(44100*t);
		}
	}else if(!strcmp(arg[p],"seg")){
		if(arg[++p]==NULL)
			return;
		if(cache[arg[p]]==NULL)
			cache[arg[p]]=new stream();
		seg=cache[arg[p]];
	}else if(!strcmp(arg[p],"ends")){
		seg=cache["main"];
	}else if(!strcmp(arg[p],"load")){
		if(arg[++p]==NULL)
			return;
		if(cache[arg[p]]==NULL)
			return;
		stream* src=cache[arg[p]];
		stream* dst=cache["main"];
		for(int i=0;i<=src->rec;i++)
			dst->operator[](i)+=(src->buffer[i]-0x80);
	}else if(!strcmp(arg[p],"wav")){
		double begin;
		double len;
		double scale;
		double freq;
		if(arg[++p]==NULL)
			return;
		sscanf(arg[p],"%lf",&begin);
		if(arg[++p]==NULL)
			return;
		sscanf(arg[p],"%lf",&len);
		if(arg[++p]==NULL)
			return;
		sscanf(arg[p],"%lf",&scale);
		if(arg[++p]==NULL)
			return;
		sscanf(arg[p],"%lf",&freq);
		int b=(int)(44100*begin);
		int e=(int)(44100*len);
		freq=(freq*2.0*3.14159265358979323846/44100.0);
		for(int i=0;i<e;i++)
			seg->operator[](b+i)+=scale*sin(i*freq);
	}else if(!strcmp(arg[p],"play")){
		if(arg[++p]==NULL)
			play(cache["main"]);
		else
			play(cache[arg[p]]);
	}else if(!strcmp(arg[p],"out")){
		if(arg[++p]==NULL)
			return;
		FILE* f=fopen(arg[p],"wb");
		if(arg[++p]==NULL)
			out(cache["main"],f);
		else
			out(cache[arg[p]],f);
		fclose(f);
	}else if(!strcmp(arg[p],"point")){
		int index=0;
		while(arg[++p]!=NULL){
			int value;
			sscanf(arg[p],"%d",&value);
			seg->operator[](index)+=value;
			index++;
		}
	}else if(!strcmp(arg[p],"loadfile")){
		if(arg[++p]==NULL)
			return;
		FILE* f=fopen(arg[p],"rb");
		if(arg[++p]==NULL)
			loadfile(cache["main"],f);
		else{
			if(cache[arg[p]]==NULL)
				cache[arg[p]]=new stream();
			loadfile(cache[arg[p]],f);
		}
		fclose(f);
	}
}
int main(int argc,char** argv){
	if(argc!=2){
		printf("Usage: wav [file]\n");
		return 0;
	}
  	freopen(argv[1],"r",stdin);
  	cache["main"]=new stream();
  	int len;
  	char* s=new char[0x4000];
  	char** ptr=new char*[0x400];
	while((len=getline(s))!=EOF){
		int p=0;
		if(!space(s[0]))
			ptr[p++]=s;
		for(int i=1;i<len;i++)
			if(space(s[i-1])){
				if(!space(s[i]))
					ptr[p++]=s+i;
				s[i-1]='\0';
			}
		ptr[p]=NULL;
		exec(ptr);
	}
	delete[] s;
	delete[] ptr;
	for(map<string,stream*>::iterator i=cache.begin();i!=cache.end();++i)
		delete i->second;
	return 0;
}

