#include <windows.h>
#include <stdio.h>

DWORD calcSize(DWORD height, DWORD width, int BitCount){
    if(BitCount == 16){
        if(width & 1)
            width++;
        return height * width * 2;
    }else if(BitCount == 1){
        if((height * width) & 7)
            return ((height * width) >> 3) + 1;
        return ((height * width) >> 3);
    }else{
        return -1;
    }
}

void Save(HWND hWnd, LPRECT r, LPCSTR szName){
    HDC hdc = GetDC(hWnd);
    HDC hMem = CreateCompatibleDC(hdc);
    PBITMAPINFO bitmapInfo = (PBITMAPINFO)malloc(sizeof(BITMAPINFOHEADER) + 2 * sizeof(RGBQUAD));
    LPVOID lpBitmap;
    LONG Height = r->bottom - r->top, Width = r->right - r->left;
    RtlZeroMemory(bitmapInfo, sizeof(BITMAPINFO));
    bitmapInfo->bmiHeader.biSize = 40;
    bitmapInfo->bmiHeader.biWidth = Width;
    bitmapInfo->bmiHeader.biHeight = Height;
    bitmapInfo->bmiHeader.biPlanes = 1;
    bitmapInfo->bmiHeader.biBitCount = 1;
    bitmapInfo->bmiColors[0].rgbRed = 0;
    bitmapInfo->bmiColors[0].rgbGreen = 0;
    bitmapInfo->bmiColors[0].rgbBlue = 0;
    bitmapInfo->bmiColors[0].rgbReserved = 0;
    bitmapInfo->bmiColors[1].rgbRed = 0xFF;
    bitmapInfo->bmiColors[1].rgbGreen = 0xFF;
    bitmapInfo->bmiColors[1].rgbBlue = 0xFF;
    bitmapInfo->bmiColors[1].rgbReserved = 0;
    const DWORD imageSize = Height * 4;
    HBITMAP hBitmap = CreateDIBSection(hdc, bitmapInfo, DIB_RGB_COLORS, &lpBitmap, NULL, 0);
    HBITMAP hNew = (HBITMAP)SelectObject(hMem, hBitmap);
    BitBlt(hMem, 0, 0, Width, Height, hdc, r->left, r->top, SRCCOPY);
    DeleteObject(hBitmap);
    DeleteDC(hdc);
    FILE* f = fopen(szName, "wb");
    BITMAPFILEHEADER fileHdr;
    const DWORD offset = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + 2 * sizeof(RGBQUAD);
    RtlZeroMemory(&fileHdr, sizeof(BITMAPFILEHEADER));
    fileHdr.bfType = 'B' + ('M' << 8);
    fileHdr.bfSize = offset + imageSize;
    fileHdr.bfOffBits = offset;
    fwrite(&fileHdr, sizeof(BITMAPFILEHEADER), 1, f);
    fwrite(bitmapInfo, sizeof(BITMAPINFOHEADER) + 2 * sizeof(RGBQUAD), 1, f);
    free(bitmapInfo);
    fwrite(lpBitmap, imageSize, 1, f);
    fclose(f);
    DeleteDC(hMem);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam){
    switch (Msg) {
    case WM_CREATE:
        break;
    case WM_CLOSE:
        break;
    case WM_PAINT:
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    case WM_QUIT:
        DestroyWindow(hWnd);
        break;
    default:
        break;
    }
    return DefWindowProcA(hWnd, Msg, wParam, lParam);
}

HWND SetupWindow(HINSTANCE hInstance){
    return CreateWindowEx(
        WS_EX_CLIENTEDGE,
        TEXT("ascii"),
        TEXT("TEST"),
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT,
        200, 200,
        NULL,
        NULL,
        hInstance,
        NULL
    );
}


ATOM RegClass(HINSTANCE hInst){
    WNDCLASSEX wcex;
    wcex.cbSize        = sizeof(WNDCLASSEX);
    wcex.style         = 0;
    wcex.lpfnWndProc   = WndProc;
    wcex.cbClsExtra    = 0;
    wcex.cbWndExtra    = 0;
    wcex.hInstance     = hInst;
    wcex.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wcex.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName  = NULL;
    wcex.lpszClassName = "ascii";
    wcex.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);
    return RegisterClassEx(&wcex);
}

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd){
    MSG Msg;
    HWND hWnd;
    RegClass(hInstance);
    hWnd = SetupWindow(hInstance);
    //SetBkMode(GetDC(hWnd), TRANSPARENT);
    ShowWindow(hWnd, nShowCmd);
    SIZE size;
    HDC hdc = GetWindowDC(GetDesktopWindow());
    CHAR szName[10];
    wchar_t c = 0x8140;
    while(TRUE){
        if(PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE)){
            if(Msg.message == WM_QUIT)
                break;
            TranslateMessage(&Msg);
            DispatchMessage(&Msg);
        }else{
            if(c > 0xFEFE)
                continue;
            if((c & 0xFF) == 0x7F){
                c++;
                continue;
            }
            GetTextExtentPointW(hdc, &c, 1, &size);
            RECT r = {
                .left = 0,
                .top = 0,
                .right = size.cx,
                .bottom = size.cy
            };
            DrawTextW(hdc, &c, 1, &r, DT_LEFT);
            sprintf(szName, "%04X.bmp", c);
            Save(GetDesktopWindow(), &r, szName);
            c++;
        }
    }
    DestroyWindow(hWnd);
    UnregisterClassA("ascii", hInstance);
    ExitProcess(Msg.wParam);
}
