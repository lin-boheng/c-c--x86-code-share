#include <winsock2.h>
#include <stdio.h>

enum Message {
    _MSG_SETDLL,
    _MSG_SETFUNC,
    _MSG_CALL,
    _MSG_SENDSTR,
    _MSG_RECVSTR,
};

SOCKET sock, client;
SOCKADDR_IN cli_addr;
int addrlen;
WSADATA wsaData;

BOOL init(){
    SOCKADDR_IN addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(5243);
    addr.sin_addr.S_un.S_addr = htonl(INADDR_LOOPBACK);
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(bind(sock, (PSOCKADDR)&addr, sizeof(SOCKADDR)) == SOCKET_ERROR)
        return FALSE;
    listen(sock, 1);
    addrlen = sizeof(SOCKADDR);
    client = accept(sock, (PSOCKADDR)&cli_addr, &addrlen);
    return TRUE;
}

char* recv_str(){
    int len;
    char* str;
    recv(client, (PCHAR)&len, sizeof(int), 0);
    str = (char*)malloc(len + 1);
    recv(client, str, len, 0);
    str[len] = '\0';
    return str;
}

void send_str(const char* s){
    int len = strlen(s);
    send(client, (char*)&len, sizeof(int), 0);
    send(client, s, len, 0);
}

int main(){
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    init();
    HMODULE hDLL = GetModuleHandleA("kernel32.dll");
    printf("%p\n", hDLL);
    int msg;
    void* func;
    while(recv(client, (PCHAR)&msg, sizeof(int), 0) != SOCKET_ERROR){
        switch (msg) {
        case _MSG_SETDLL:{
            char* s = recv_str();
            hDLL = LoadLibraryA(s);
            printf("dll %s %p\n", s, hDLL);
            free(s);
        }
            break;
        case _MSG_SETFUNC:{
            char* s = recv_str();
            func = (void*)GetProcAddress(hDLL, s);
            if(!func){
                if((func = (void*)GetProcAddress(LoadLibraryA("ntdll.dll"), s))){
                    hDLL = LoadLibraryA("ntdll.dll");
                }else if((func = (void*)GetProcAddress(LoadLibraryA("kernel32.dll"), s))){
                    hDLL = LoadLibraryA("kernel32.dll");
                }else if((func = (void*)GetProcAddress(LoadLibraryA("shell32.dll"), s))){
                    hDLL = LoadLibraryA("shell32.dll");
                }else if((func = (void*)GetProcAddress(LoadLibraryA("user32.dll"), s))){
                    hDLL = LoadLibraryA("user32.dll");
                }
            }
            printf("func %s %p\n", s, func);
            free(s);
        }
            break;
        case _MSG_CALL:{
            void* res;
            void* rsp;
            int n;
            void* p[100];
            recv(client, (char*)&n, sizeof(int), 0);
            recv(client, (char*)p, n * sizeof(void*), 0);
            switch(n){
            case 0:
                res = ((void*(*)())func)();
                break;
            case 1:
                res = ((void*(*)(void*))func)(p[0]);
                break;
            case 2:
                res = ((void*(*)(void*,void*))func)(p[0],p[1]);
                break;
            case 3:
                res = ((void*(*)(void*,void*,void*))func)(p[0],p[1],p[2]);
                break;
            case 4:
                res = ((void*(*)(void*,void*,void*,void*))func)(p[0],p[1],p[2],p[3]);
                break;
            default:
                asm("mov %0,%%rcx"::"m"(p[0]));
                asm("mov %0,%%rdx"::"m"(p[1]));
                asm("mov %0,%%r8"::"m"(p[2]));
                asm("mov %0,%%r9"::"m"(p[3]));
                asm("mov %%rsp,%0"::"m"(rsp));
                asm("1:"
                    "pushq (%[p])\n"
                    "sub $8,%[p]\n"
                    "loop 1b\n"
                    "sub $32,%%rsp\n"
                    "call *%[f]\n":"=a"(res):[p]"r"(p + n - 1),"c"(n - 4),[f]"m"(func));
                asm("mov %0,%%rsp"::"m"(rsp));
                break;
            }
            printf("call\n");
            send(client, (char*)&res, sizeof(void*), 0);
        }
            break;
        case _MSG_SENDSTR:{
            void* p;
            int len;
            recv(client, (char*)&p, sizeof(void*), 0);
            recv(client, (char*)&len, sizeof(int), 0);
            recv(client, (char*)p, len, 0);
        }
            break;
        case _MSG_RECVSTR:{
            void* p;
            int len;
            recv(client, (char*)&p, sizeof(void*), 0);
            recv(client, (char*)&len, sizeof(int), 0);
            send(client, (char*)p, len, 0);
        }
            break;
        default:
            break;
        }
        msg = -1;
    }
    closesocket(client);
    closesocket(sock);
    WSACleanup();
    ExitProcess(0);
}