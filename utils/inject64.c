#include <windows.h>
#include <winternl.h>

#define SE_CREATE_TOKEN_PRIVILEGE 0X2
#define SE_ASSIGN_PRIMARY_TOKEN_PRIVILEGE 0X3
#define SE_LOCK_MEMORY_PRIVILEGE 0X4
#define SE_INCREASE_QUOTA_PRIVILEGE 0X5
#define SE_UNSOLICITED_INPUT_PRIVILEGE 0X0
#define SE_MACHINE_ACCOUNT_PRIVILEGE 0X6
#define SE_TCB_PRIVILEGE 0X7
#define SE_SECURITY_PRIVILEGE 0X8
#define SE_TAKE_OWNER_SHIP_PRIVILEGE 0X9
#define SE_LOAD_DRIVER_PRIVILEGE 0XA
#define SE_SYSTEM_PROFILE_PRIVILEGE 0XB
#define SE_SYSTEM_TIME_PRIVILEGE 0XC
#define SE_PROFILE_SINGLE_PROCESS_PRIVILEGE 0XD
#define SE_INCREASE_BASE_PRIORITY_PRIVILEGE 0XE
#define SE_CREATE_PAGE_FILE_PRIVILEGE 0XF
#define SE_CREATE_PERMANENT_PRIVILEGE 0X10
#define SE_BACKUP_PRIVILEGE 0X11
#define SE_RESTORE_PRIVILEGE 0X12
#define SE_SHUTDOWN_PRIVILEGE 0X13
#define SE_DEBUG_PRIVILEGE 0X14
#define SE_AUDIT_PRIVILEGE 0X15
#define SE_SYSTEM_ENVIRONMENT_PRIVILEGE 0X16
#define SE_CHANGE_NOTIFY_PRIVILEGE 0X17
#define SE_REMOTE_SHUTDOWN_PRIVILEGE 0X18
#define SE_UNDOCK_PRIVILEGE 0X19
#define SE_SYNC_AGENT_PRIVILEGE 0X1A
#define SE_ENABLE_DELEGATION_PRIVILEGE 0X1B
#define SE_MANAGE_VOLUME_PRIVILEGE 0X1C
#define SE_IMPERSONATE_PRIVILEGE 0X1D
#define SE_CREATE_GLOBAL_PRIVILEGE 0X1E
#define SE_TRUSTED_CRED_MAN_ACCESS_PRIVILEGE 0X1F
#define SE_RELABEL_PRIVILEGE 0X20
#define SE_INCREASE_WORK_INGSET_PRIVILEGE 0X21
#define SE_TIMEZONE_PRIVILEGE 0X22
#define SE_CREATE_SYMBOLIC_LINK_PRIVILEGE 0X23

BOOL inject64(HANDLE pid, LPCSTR szDll){
    HANDLE hps, hThread;
    DWORD dwLen;
    LPVOID lpvMem;
    HMODULE hKrnl, hDll;
    FARPROC lpfnLoad;
    DWORD dwState;

    hps = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE, FALSE, (DWORD_PTR)pid);
    if(hps == NULL){
        return FALSE;
    }
    dwLen = strlen(szDll) + 1;
    lpvMem = VirtualAllocEx(hps, NULL, dwLen, MEM_COMMIT, PAGE_READWRITE);
    if(lpvMem == NULL){
        CloseHandle(hps);
        return FALSE;
    }
    if(!WriteProcessMemory(hps, lpvMem, szDll, dwLen, NULL)){
        VirtualFreeEx(hps, lpvMem, dwLen, MEM_RELEASE);
        CloseHandle(hps);
        return FALSE;
    }
    hKrnl = GetModuleHandleA("kernel32.dll");
    lpfnLoad = GetProcAddress(hKrnl, "LoadLibraryA");
    hThread = CreateRemoteThread(hps, NULL, 0, (DWORD(CALLBACK*)(LPVOID))lpfnLoad, lpvMem, 0, NULL);
    if(!hThread){
        VirtualFreeEx(hps, lpvMem, dwLen, MEM_RELEASE);
        CloseHandle(hps);
        return FALSE;
    }
    dwState = WaitForSingleObject(hThread, 1000);
    if(dwState != WAIT_OBJECT_0){
        CloseHandle(hThread);
        CloseHandle(hps);
        return FALSE;
    }
    GetExitCodeThread(hThread, (LPDWORD)&hDll);
    CloseHandle(hThread);
    CloseHandle(hps);
    return hDll == NULL ? FALSE : TRUE;
}

int main(int argc, char** argv){
    HANDLE hOut;
    LPVOID lpvInfo;
    ULONG uLength;
    PSYSTEM_PROCESS_INFORMATION lpInfo;
    BOOLEAN bEnabled;

    hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    if(argc != 3){
        WriteFile(hOut, "Usage: inject64.exe [name] [DLLFullPath]\n", 41, NULL, NULL);
        ExitProcess(1);
    }

    typedef NTSTATUS(*fnNtQuerySystemInformation)(SYSTEM_INFORMATION_CLASS, PVOID, ULONG, PULONG);
    typedef NTSTATUS(*fnRtlUnicodeStringToAnsiString)(PANSI_STRING, PCUNICODE_STRING, BOOLEAN);
    typedef NTSTATUS(*fnNtQueryInformationProcess)(HANDLE, PROCESSINFOCLASS, PVOID, ULONG, PULONG);
    typedef NTSTATUS(*fnRtlAdjustPrivilege)(ULONG, BOOLEAN, BOOLEAN, PBOOLEAN);
    
    fnNtQuerySystemInformation _NtQuerySystemInformation = (fnNtQuerySystemInformation)GetProcAddress(GetModuleHandleA("ntdll.dll"), "NtQuerySystemInformation");
    fnRtlUnicodeStringToAnsiString _RtlUnicodeStringToAnsiString = (fnRtlUnicodeStringToAnsiString)GetProcAddress(GetModuleHandleA("ntdll.dll"), "RtlUnicodeStringToAnsiString");
    fnNtQueryInformationProcess _NtQueryInformationProcess = (fnNtQueryInformationProcess)GetProcAddress(GetModuleHandleA("ntdll.dll"), "NtQueryInformationProcess");
    fnRtlAdjustPrivilege _RtlAdjustPrivilege = (fnRtlAdjustPrivilege)GetProcAddress(GetModuleHandleA("ntdll.dll"), "RtlAdjustPrivilege");

    _RtlAdjustPrivilege(SE_DEBUG_PRIVILEGE, TRUE, FALSE, &bEnabled);

    _NtQuerySystemInformation(SystemProcessInformation, lpvInfo, 0, &uLength);
    lpvInfo = GlobalAlloc(GMEM_FIXED, uLength);
    _NtQuerySystemInformation(SystemProcessInformation, lpvInfo, uLength, NULL);

    lpInfo = (PSYSTEM_PROCESS_INFORMATION)lpvInfo;
    do {
        ANSI_STRING str;
        _RtlUnicodeStringToAnsiString(&str, &lpInfo->ImageName, TRUE);
        if(!strstr(str.Buffer, argv[1]))
            continue;
        WriteFile(hOut, "Trying to inject: ", 18, NULL, NULL);
        WriteFile(hOut, str.Buffer, str.Length, NULL, NULL);
        inject64(lpInfo->UniqueProcessId, argv[2]) ? WriteFile(hOut, " Success!\n", 10, NULL, NULL) : WriteFile(hOut, " Failed!\n", 9, NULL, NULL);
    }while (lpInfo->NextEntryOffset && (lpInfo = (PSYSTEM_PROCESS_INFORMATION)((LPBYTE)lpInfo + lpInfo->NextEntryOffset)));
    ExitProcess(0);
}
