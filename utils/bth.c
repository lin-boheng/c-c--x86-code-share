#include <stdio.h>
#include <winsock2.h>
#include <ws2bth.h>

#define BTH_LOOKUP_BUFFER_SIZE 0x10000

BOOL BthLookupDevice(){
    HANDLE hLookup;
    WSAQUERYSETW wsaq;
    PWSAQUERYSETW lpqsResults;
    PSOCKADDR_BTH lpsockAddr;
    BOOL bResult;
    DWORD dwSize = 0;

    RtlZeroMemory(&wsaq, sizeof(WSAQUERYSETW));
    wsaq.dwSize = sizeof(WSAQUERYSETW);
    wsaq.dwNameSpace = NS_BTH;
    wsaq.lpcsaBuffer = NULL;

    bResult = WSALookupServiceBeginW(&wsaq, LUP_CONTAINERS, &hLookup);
    if(bResult != ERROR_SUCCESS || hLookup == NULL)
        return FALSE;
    dwSize = BTH_LOOKUP_BUFFER_SIZE;
    lpqsResults = (PWSAQUERYSETW)GlobalAlloc(GMEM_FIXED, dwSize);
    if(lpqsResults == NULL)
        return FALSE;
    while(WSALookupServiceNextW(hLookup, LUP_RETURN_NAME | LUP_RETURN_ADDR, &dwSize, lpqsResults) == ERROR_SUCCESS){
        lpsockAddr = (PSOCKADDR_BTH)lpqsResults->lpcsaBuffer->RemoteAddr.lpSockaddr;
        wprintf(L"%s:%12llX\n", lpqsResults->lpszServiceInstanceName ? lpqsResults->lpszServiceInstanceName : L"<unnamed>", lpsockAddr->btAddr);
    }
    WSALookupServiceEnd(hLookup);
    GlobalFree(lpqsResults);
    return TRUE;
}

int main(){
    WSADATA wsaData;
    SOCKET btSock;
    SOCKADDR_BTH btAddr;
    int iRet;

    if(WSAStartup(MAKEWORD(2, 2), &wsaData)){
        WSACleanup();
        ExitProcess(1);
    }
    BthLookupDevice();
    ExitProcess(0);

    btSock = socket(AF_BTH, SOCK_STREAM, BTHPROTO_RFCOMM);

    btAddr.addressFamily = AF_BTH;
    btAddr.btAddr = 0x266217056D8D;
    btAddr.port = 0;
    btAddr.serviceClassId = RFCOMM_PROTOCOL_UUID;

    iRet = connect(btSock, (PSOCKADDR)&btAddr, sizeof(SOCKADDR_BTH));

    closesocket(btSock);
    WSACleanup();
    ExitProcess(0);
}