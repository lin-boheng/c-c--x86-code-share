#include "wav.h"

void gen(char* buf,char* data,int len){
	struct{
		char riff[4]={'R','I','F','F'};
		DWORD RiffSize;
		char wave[4]={'W','A','V','E'};
		char fmt[4]={'f','m','t',' '};
		DWORD fmtLen=0x10;
		SHORT tag=1;
		SHORT channels=1;
		DWORD samplesPerSec=44100;
		DWORD bytesPerSec=44100;
		SHORT blockAlign=1;
		SHORT bitsPerSample=8;
		char data[4]={'d','a','t','a'};
		DWORD dataSize;
	}__attribute__((packed)) wav;
	wav.dataSize=len;
	wav.RiffSize=wav.dataSize+36;
	memcpy(buf,&wav,sizeof(wav));
	memcpy(buf+sizeof(wav),data,len);
}
int getline(FILE* f,char* s){
	static bool eof=false;
	if(eof)
		return EOF;
	int p=-1;
	do{
		s[++p]=fgetc(f);
		if(s[p]==EOF){
			eof=true;
			s[p]='\0';
			return p;
		}
	}while(s[p]!='\n');
	s[p]='\0';
	return p;
}
bool space(char c){
	return c==' '||c=='\t';
}
static map<string,stream*> cache;
static map<string,Function*> table;
void play(stream* wave){
	if(wave==NULL)
		return;
	char* buf=new char[wave->getLen()+44];
	gen(buf,wave->buffer,wave->getLen());
	PlaySound(buf,NULL,SND_MEMORY|SND_SYNC|SND_NODEFAULT);
	delete[] buf;
}
void out(stream* wave,FILE* f){
	if(wave==NULL||f==NULL)
		return;
	char* buf=new char[wave->getLen()+44];
	gen(buf,wave->buffer,wave->getLen());
	fwrite(buf,1,wave->getLen()+44,f);
	delete[] buf;
}
int strcmp(const char* s1,const char* s2,int len){
	for(int i=0;i<len;i++){
		if(((s1[i]>='A'&&s1[i]<='Z')?s1[i]+'a'-'A':s1[i])!=((s2[i]>='A'&&s2[i]<='Z')?s2[i]+'a'-'A':s2[i]))
			return s1[i]>s2[i]?1:-1;
	}
	return 0;
}
void loadfile(stream* wave,FILE* f){
	if(wave==NULL||f==NULL)
		return;
	struct{
		char riff[4]={'R','I','F','F'};
		DWORD RiffSize;
		char wave[4]={'W','A','V','E'};
		char fmt[4]={'f','m','t',' '};
		DWORD fmtLen=0x10;
		SHORT tag=1;
		SHORT channels=1;
		DWORD samplesPerSec=44100;
		DWORD bytesPerSec=44100;
		SHORT blockAlign=1;
		SHORT bitsPerSample=8;
		char data[4]={'d','a','t','a'};
		DWORD dataSize;
	}__attribute__((packed)) wav;
	fread(&wav,sizeof(wav),1,f);
	if(strcmp(wav.riff,"RIFF",4)||
	   strcmp(wav.wave,"WAVE",4)||
	   strcmp(wav.fmt,"fmt ",4)||
	   wav.fmtLen!=0x10||
	   wav.tag!=1||
	   wav.channels!=1||
	   wav.samplesPerSec!=44100||
	   wav.bytesPerSec!=44100||
	   wav.blockAlign!=1||
	   wav.bitsPerSample!=8||
	   strcmp(wav.data,"data",4))
	   	return;
	char* buf=new char[wav.dataSize];
	fread(buf,1,wav.dataSize,f);
	for(int i=0;i<wav.dataSize;i++)
		wave->operator[](i)+=(buf[i]-0x80);
	delete[] buf;
}
void exec(const char** arg){
	static stream* seg=cache["main"];
	static Function* func=NULL;
	if(arg==NULL||arg[0]==NULL)
		return;
	int p=0;
	if(func!=NULL){
		if(!strcmp(arg[p],"endf")){
			func=NULL;
			return;
		}
		func->AddLine(arg);
		return;
	}
	if(!strcmp(arg[p],"offset")){
		if(arg[++p]==NULL)
			return;
		double t;
		if(!strcmp(arg[p],"+")){
			if(arg[++p]==NULL)
				return;
			sscanf(arg[p],"%lf",&t);
			seg->offset+=(int)(44100*t);
		}else if(!strcmp(arg[p],"-")){
			if(arg[++p]==NULL)
				return;
			sscanf(arg[p],"%lf",&t);
			seg->offset-=(int)(44100*t);
		}else if(!strcmp(arg[p],"=")){
			if(arg[++p]==NULL)
				return;
			sscanf(arg[p],"%lf",&t);
			seg->offset=(int)(44100*t);
		}
	}else if(!strcmp(arg[p],"seg")){
		if(arg[++p]==NULL)
			return;
		if(cache[arg[p]]==NULL)
			cache[arg[p]]=new stream();
		seg=cache[arg[p]];
	}else if(!strcmp(arg[p],"ends")){
		seg=cache["main"];
	}else if(!strcmp(arg[p],"load")){
		if(arg[++p]==NULL)
			return;
		if(cache[arg[p]]==NULL)
			return;
		stream* src=cache[arg[p]];
		stream* dst=cache["main"];
		for(int i=0;i<=src->rec;i++)
			dst->operator[](i)+=(src->buffer[i]-0x80);
	}else if(!strcmp(arg[p],"wav")){
		double begin;
		double len;
		double scale;
		double freq;
		if(arg[++p]==NULL)
			return;
		sscanf(arg[p],"%lf",&begin);
		if(arg[++p]==NULL)
			return;
		sscanf(arg[p],"%lf",&len);
		if(arg[++p]==NULL)
			return;
		sscanf(arg[p],"%lf",&scale);
		if(arg[++p]==NULL)
			return;
		sscanf(arg[p],"%lf",&freq);
		int b=(int)(44100*begin);
		int e=(int)(44100*len);
		freq=(freq*2.0*3.14159265358979323846/44100.0);
		for(int i=0;i<e;i++)
			seg->operator[](b+i)+=scale*sin(i*freq);
	}else if(!strcmp(arg[p],"play")){
		if(arg[++p]==NULL)
			play(cache["main"]);
		else
			play(cache[arg[p]]);
	}else if(!strcmp(arg[p],"out")){
		if(arg[++p]==NULL)
			return;
		FILE* f=fopen(arg[p],"wb");
		if(arg[++p]==NULL)
			out(cache["main"],f);
		else
			out(cache[arg[p]],f);
		fclose(f);
	}else if(!strcmp(arg[p],"point")){
		int index=0;
		while(arg[++p]!=NULL){
			int value;
			sscanf(arg[p],"%d",&value);
			seg->operator[](index)+=value;
			index++;
		}
	}else if(!strcmp(arg[p],"loadfile")){
		if(arg[++p]==NULL)
			return;
		FILE* f=fopen(arg[p],"rb");
		if(arg[++p]==NULL)
			loadfile(cache["main"],f);
		else{
			if(cache[arg[p]]==NULL)
				cache[arg[p]]=new stream();
			loadfile(cache[arg[p]],f);
		}
		fclose(f);
	}else if(!strcmp(arg[p],"func")){
		if(arg[++p]==NULL)
			return;
		if(table[arg[p]]!=NULL)
			delete table[arg[p]];
		table[arg[p]]=new Function(arg+2);
		func=table[arg[p]];
	}else if(!strcmp(arg[p],"call")){
		if(arg[++p]==NULL)
			return;
		if(table[arg[p]]==NULL)
			return;
		Function* target=table[arg[p]];
		map<string,const char*> args;
		map<string,StringDouble> vals;
		while(arg[++p]!=NULL){
			if(p-2>=target->args->len)
				break;
			args[target->args->operator[](p-2)]=arg[p];
			double value;
			sscanf(arg[p],"%lf",&value);
			vals[target->args->operator[](p-2)]=value;
		}
		const char** ptr=new const char*[0x400];
		for(int i=0;i<target->lines.size();i++){
			if(target->lines[i]->len==0||target->lines[i]->list[0]==NULL)
				continue;
			if(funcexec((const char**)target->lines[i]->list,vals,i))
				continue;
			*ptr=target->lines[i]->list[0];
			for(int j=1;j<target->lines[i]->len;j++){
				if(args[target->lines[i]->list[j]]!=NULL)
					ptr[j]=args[target->lines[i]->list[j]];
				else if(vals.find(target->lines[i]->list[j])!=vals.end())
					ptr[j]=vals[target->lines[i]->list[j]].GetString();
				else
					ptr[j]=target->lines[i]->list[j];
			}
			ptr[target->lines[i]->len]=NULL;
			exec(ptr);
		}
		delete[] ptr;
	}else if(!strcmp(arg[p],"rep")){
		if(arg[++p]==NULL)
			return;
		int times;
		sscanf(arg[p],"%d",&times);
		if(arg[++p]==NULL)
			return;
		for(int i=0;i<times;i++)
			exec(arg+p);
	}
}
bool funcexec(const char** arg,map<string,StringDouble>& vals,int& ip){
	static bool EF=false;
	static bool LF=false;
	static bool GF=false;
	if(arg==NULL||arg[0]==NULL)
		return false;
	if(arg[0][3]=='\0'&&
	  (arg[0][0]=='*'||arg[0][0]=='+'||arg[0][0]=='-')&&
	  (arg[0][1]=='*'||arg[0][1]=='+'||arg[0][1]=='-')&&
	  (arg[0][2]=='*'||arg[0][2]=='+'||arg[0][2]=='-'))
	{
		if(((arg[0][0]=='*')||((arg[0][0]=='-')^(EF)))&&
		   ((arg[0][1]=='*')||((arg[0][1]=='-')^(LF)))&&
		   ((arg[0][2]=='*')||((arg[0][2]=='-')^(GF))))
		{
			arg++;
			if(arg[0]==NULL)
				return false;
		}else return true;
	}
	int p=0;
	if(!strcmp(arg[p],"var")){
		while(arg[++p]!=NULL)
			vals[arg[p]]=0.0;
	}else if(!strcmp(arg[p],"mov")){
		if(arg[++p]==NULL)
			return true;
		if(vals.find(arg[p])==vals.end())
			return true;
		if(arg[++p]==NULL)
			return true;
		if(vals.find(arg[p])==vals.end()){
			double value;
			sscanf(arg[p],"%lf",&value);
			vals[arg[p-1]]=value;
		}else{
			vals[arg[p-1]]=vals[arg[p]].GetValue();
		}
	}else if(!strcmp(arg[p],"add")){
		if(arg[++p]==NULL)
			return true;
		if(vals.find(arg[p])==vals.end())
			return true;
		if(arg[++p]==NULL)
			return true;
		if(vals.find(arg[p])==vals.end()){
			double value;
			sscanf(arg[p],"%lf",&value);
			vals[arg[p-1]]=vals[arg[p-1]].GetValue()+value;
		}else{
			vals[arg[p-1]]=vals[arg[p-1]].GetValue()+vals[arg[p]].GetValue();
		}
	}else if(!strcmp(arg[p],"sub")){
		if(arg[++p]==NULL)
			return true;
		if(vals.find(arg[p])==vals.end())
			return true;
		if(arg[++p]==NULL)
			return true;
		if(vals.find(arg[p])==vals.end()){
			double value;
			sscanf(arg[p],"%lf",&value);
			vals[arg[p-1]]=vals[arg[p-1]].GetValue()-value;
		}else{
			vals[arg[p-1]]=vals[arg[p-1]].GetValue()-vals[arg[p]].GetValue();
		}
	}else if(!strcmp(arg[p],"mul")){
		if(arg[++p]==NULL)
			return true;
		if(vals.find(arg[p])==vals.end())
			return true;
		if(arg[++p]==NULL)
			return true;
		if(vals.find(arg[p])==vals.end()){
			double value;
			sscanf(arg[p],"%lf",&value);
			vals[arg[p-1]]=vals[arg[p-1]].GetValue()*value;
		}else{
			vals[arg[p-1]]=vals[arg[p-1]].GetValue()*vals[arg[p]].GetValue();
		}
	}else if(!strcmp(arg[p],"div")){
		if(arg[++p]==NULL)
			return true;
		if(vals.find(arg[p])==vals.end())
			return true;
		if(arg[++p]==NULL)
			return true;
		if(vals.find(arg[p])==vals.end()){
			double value;
			sscanf(arg[p],"%lf",&value);
			vals[arg[p-1]]=vals[arg[p-1]].GetValue()/value;
		}else{
			vals[arg[p-1]]=vals[arg[p-1]].GetValue()/vals[arg[p]].GetValue();
		}
	}else if(!strcmp(arg[p],"abs")){
		if(arg[++p]==NULL)
			return true;
		if(vals.find(arg[p])==vals.end())
			return true;
		vals[arg[p]]=abs(vals[arg[p]].GetValue());
	}else if(!strcmp(arg[p],"cmp")){
		if(arg[++p]==NULL)
			return true;
		if(vals.find(arg[p])==vals.end())
			return true;
		double val=vals[arg[p]].GetValue();
		if(arg[++p]==NULL)
			return true;
		double value;
		if(vals.find(arg[p])==vals.end())
			sscanf(arg[p],"%lf",&value);
		else
			value=vals[arg[p]].GetValue();
		EF=(((int)round(val))==((int)round(value)));
		LF=(val<value);
		GF=(val>value);
	}else if(!strcmp(arg[p],"ip")){
		if(arg[++p]==NULL)
			return true;
		if(arg[++p]==NULL)
			return true;
		int value;
		if(vals.find(arg[p])!=vals.end())
			value=(int)round(vals[arg[p]].GetValue());
		else
			sscanf(arg[p],"%d",&value);
		if(!strcmp(arg[p-1],"+")){
			ip+=value;
		}else if(!strcmp(arg[p-1],"-")){
			ip-=value;
		}else if(!strcmp(arg[p-1],"=")){
			ip=value;
		}else return true;
		ip--;
		return true;
	}else if(!strcmp(arg[p],"rep")){
		if(arg[++p]==NULL)
			return false;
		int times;
		if(vals.find(arg[p])!=vals.end())
			times=(int)round(vals[arg[p]].GetValue());
		else
			sscanf(arg[p],"%d",&times);
		if(arg[++p]==NULL)
			return false;
		for(int i=0;i<times;i++)
			if(funcexec(arg+p,vals,ip)==false)
				return false;
	}else return false;
	return true;
}
int main(int argc,char** argv){
	if(argc!=2){
		printf("Usage: wav [file]\n");
		return 0;
	}
	FILE** fstack=new FILE*[0x400];
	int fsp=0;
	fstack[fsp++]=fopen(argv[1],"r");
  	cache["main"]=new stream();
  	int len;
  	char* s=new char[0x4000]; 
	char* s2=new char[0x4000];
	bool* div=new bool[0x4000];
  	const char** ptr=new const char*[0x400];
	while(true){
		len=getline(fstack[fsp-1],s);
		if(len==EOF){
			fsp--;
			fclose(fstack[fsp]);
			if(fsp==0)
				break;
			continue;
		}
		if(len==0)
			continue;
		int p=0;
		int len2=0;
		for(int i=0,flag=0;i<len;i++){
			if(s[i]=='\"'){
				flag=!flag;
				continue;
			}else if(s[i]==';')
				break;
			if(flag)
				div[len2]=false;
			else
				div[len2]=space(s[i]);
			s2[len2++]=s[i];
		}
		if(len2==0)
			continue;
		s2[len2]='\0';
		if(!div[0])
			ptr[p++]=s2;
		for(int i=1;i<len2;i++)
			if(div[i-1]){
				if(!div[i])
					ptr[p++]=s2+i;
				s2[i-1]='\0';
			}
		if(p==0)
			continue;
		ptr[p]=NULL;
		if(!strcmp(ptr[0],"#include")){
			if(ptr[1]==NULL)
				continue;
			fstack[fsp++]=fopen(ptr[1],"r");
			if(fstack[fsp-1]==NULL)
				fsp--;
			continue;
		}
		exec(ptr);
	}
	delete[] s;
	delete[] s2;
	delete[] div;
	delete[] ptr;
	delete[] fstack;
	for(map<string,stream*>::iterator i=cache.begin();i!=cache.end();++i)
		delete i->second;
	for(map<string,Function*>::iterator i=table.begin();i!=table.end();++i)
		delete i->second;
	return 0;
}

