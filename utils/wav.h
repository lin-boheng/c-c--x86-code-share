#include<iostream>
#include<fstream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<map>
#include<vector>
#include<windows.h>
using namespace std;
struct stream{
	int size=100;
	int rec=0;
	int offset=0;
	char* buffer=new char[100];
	stream(){
		memset(buffer,0x80,size);
	}
	~stream(){
		delete[] buffer;
	}
	char& operator[](int x){
		x+=offset;
		if(x<0)
			return *(char*)NULL;
		if(x>=size){
			int s=size;
			while(size<=x)size<<=1;
			char* newbuf=new char[size];
			memcpy(newbuf,buffer,s);
			memset(newbuf+s,0x80,size-s);
			delete[] buffer;
			buffer=newbuf;
		}
		rec=max(rec,x);
		return buffer[x];
	}
	int getLen(){
		return rec+1;
	}
};
class CmdLine{
public:
	char** list=NULL;
	int len=0;
	CmdLine(const char** arg){
		while(arg[len]!=NULL)
			len++;
		list=new char*[len+1];
		for(int i=0;i<len;i++){
			list[i]=new char[strlen(arg[i])+1];
			strcpy(list[i],arg[i]);
		}
        list[len]=NULL;
	}
	~CmdLine(){
		for(int i=0;i<len;i++)
			delete[] list[i];
		delete[] list;
	}
	const char* operator[](int x){
		if(x<0||x>=len)
			return NULL;
		return list[x];
	}
};
class Function{
public:
	CmdLine* args=NULL;
	vector<CmdLine*> lines;
	Function(const char** arg){
		args=new CmdLine(arg);
	}
	~Function(){
		for(int i=0;i<lines.size();i++)
			delete lines[i];
		delete args;
	}
	void AddLine(const char** arg){
		lines.push_back(new CmdLine(arg));
	}
};
class StringDouble{
private:
    double val;
    char str[0x400];
public:
    StringDouble(){
        *str='\0';
    }
    ~StringDouble(){}
    double operator=(double x){
        val=x;
        sprintf(str,"%lf",x);
        return x;
    }
    const char* GetString(){
        return str;
    }
    double GetValue(){
        return val;
    }
};

void gen(char* buf,char* data,int len);
int getline(char* s);
bool space(char c);
void play(stream* wave);
void out(stream* wave,FILE* f);
int strcmp(const char* s1,const char* s2,int len);
void loadfile(stream* wave,FILE* f);
void exec(const char** arg);
bool funcexec(const char** arg,map<string,StringDouble>& vals,int& ip);