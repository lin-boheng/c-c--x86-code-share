#include <iostream>
#include <regex>
#include <windows.h>
using namespace std;
int main(int argc, char** argv) {
  SC_HANDLE SCMgr = OpenSCManagerW(NULL, NULL, SC_MANAGER_ALL_ACCESS);
  ENUM_SERVICE_STATUSA EnumSvc[0x40];
  DWORD dwNeed, ResumeHandle = 0, SvcCount;
  CHAR DisName[0x400], SvcName[0x400];
  EnumSvc->lpDisplayName = DisName;
  EnumSvc->lpServiceName = SvcName;
  string key;
  regex exp;
  if(argc >= 2){
    exp = regex(argv[1]);
  }else{
    cin >> key;
    exp = regex(key);
    getchar();
  }
  while (true) {
    EnumServicesStatusA(SCMgr, SERVICE_TYPE_ALL, SERVICE_STATE_ALL, EnumSvc,
                        0x400, &dwNeed, &SvcCount, &ResumeHandle);
    for (int i = 0; i < SvcCount; i++) {
      int len = strlen(EnumSvc[i].lpServiceName);
      if (regex_match(EnumSvc[i].lpServiceName, exp))
        cout << EnumSvc[i].lpServiceName << ' ' << EnumSvc[i].lpDisplayName
             << endl;
    }
    if (!dwNeed)
      break;
  }
  return 0;
}