#include <stdarg.h>
#include <string.h>
#include <vadefs.h>
#include <winsock2.h>
#include <stdio.h>

enum Message {
    _MSG_SETDLL,
    _MSG_SETFUNC,
    _MSG_CALL,
    _MSG_SENDSTR,
    _MSG_RECVSTR,
};

typedef DWORD_PTR intp;

SOCKET sock;

char* recv_str(){
    int len;
    char* str;
    recv(sock, (char*)&len, sizeof(int), 0);
    str = (char*)malloc(len + 1);
    recv(sock, str, sizeof(int), 0);
    str[len] = '\0';
    return str;
}

void send_str(const char* s){
    int len = strlen(s);
    // ***DEBUG***
    printf("%s %d\n", s, len);
    // ***
    send(sock, (char*)&len, sizeof(int), 0);
    send(sock, s, len, 0);
}

void send_int(int a){
    send(sock, (char*)&a, sizeof(int), 0);
}

void send_lint(intp a){
    send(sock, (char*)&a, sizeof(intp), 0);
}

int recv_int(){
    int res;
    recv(sock, (char*)&res, sizeof(int), 0);
    return res;
}

void set_dll(const char* s){
    send_int(_MSG_SETDLL);
    send_str(s);
}

void set_func(const char* s){
    send_int(_MSG_SETFUNC);
    send_str(s);
}

void write_str(intp p, const void* data, int len){
    send_int(_MSG_SENDSTR);
    send_lint(p);
    send_int(len);
    send(sock, (const char*)data, len, 0);
}

void read_str(intp p, void* data, int len){
    send_int(_MSG_RECVSTR);
    send_lint(p);
    send_int(len);
    recv(sock, (char*)data, len, 0);
}

intp __cdecl call(int n, ...){
    intp res;
    send_int(_MSG_CALL);
    send_int(n);
    send(sock, ((char*)&n) + 8, n * sizeof(void*), 0);
    recv(sock, (char*)&res, sizeof(intp), 0);
    return res;
}

intp __cdecl callf(const char* s, int n, ...){
    intp res;
    set_func(s);
    send_int(_MSG_CALL);
    send_int(n);
    send(sock, ((char*)&n) + 8, n * sizeof(void*), 0);
    recv(sock, (char*)&res, sizeof(intp), 0);
    return res;
}

intp const_str(const char* s){
    intp res;
    intp len;
    len = strlen(s);
    res = callf("GlobalAlloc", 2, GMEM_FIXED, len + 1);
    write_str(res, (const void*)s, len + 1);
    return res;
}

intp s0, s1;

void payload(){
    MessageBeep(MB_ICONERROR);
}

int init(){
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    SOCKADDR_IN addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(5243);
    addr.sin_addr.S_un.S_addr = htonl(INADDR_LOOPBACK);
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(connect(sock, (PSOCKADDR)&addr, sizeof(SOCKADDR)) == SOCKET_ERROR)
        return 1;
    return 0;
}

int main(){
    init();
    s0 = const_str("aaa");
    s1 = const_str("bbb");
    callf("MessageBeep", 1, MB_ICONERROR);
    callf("MessageBoxA", 4, NULL, s0, s1, MB_OK);
    intp f = callf("VirtualAlloc", 4, NULL, 1000, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
    write_str(f, (void*)payload, 1000);
    callf("CreateThread", 6, NULL, 0, f, NULL, 0, NULL);
    callf("Sleep", 1, 1000);
    closesocket(sock);
    WSACleanup();
    return 0;
}