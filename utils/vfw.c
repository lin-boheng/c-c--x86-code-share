#include <windows.h>
#include <vfw.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam){
    switch(Msg){
    case WM_CREATE:
        break;
    case WM_QUIT:
        PostQuitMessage(0);
        break;
    case WM_DESTROY:
        ExitProcess(0);
        break;
    }
    return DefWindowProc(hWnd, Msg, wParam, lParam);
}

void CapFrameCallback(HWND hWnd, LPVIDEOHDR lpVHdr){
    if(lpVHdr)
        MessageBoxA(NULL, "?", "?", MB_OK);
}

//HANDLE hEvent = CreateEventA(NULL, FALSE, FALSE, NULL);

void CapVideoCallback(HWND hWnd, LPVIDEOHDR lpVHdr){
    BITMAPFILEHEADER Header;
    BITMAPINFO Info;
    // HANDLE hFile = CreateFileA(
    //     "test.jpeg",
    //     FILE_GENERIC_READ | FILE_GENERIC_WRITE,
    //     FILE_SHARE_READ | FILE_SHARE_WRITE,
    //     NULL,
    //     CREATE_NEW,
    //     FILE_ATTRIBUTE_NORMAL,
    //     NULL
    // );
    int width, height, channel;
    stbi_uc* ImageBits = stbi_load_from_memory(lpVHdr->lpData, lpVHdr->dwBytesUsed, &width, &height, &channel, 0);
    int size = ((((width * channel) & 3 ? 4 : 0) + ((width * channel) & ~3)) * height) >> 2;
    LPDWORD RevBits = new DWORD[size];
    for(int i = 0; i < size; i++)
        RevBits[i] = ((LPDWORD)ImageBits)[size - i - 1];
    //WriteFile(hFile, lpVHdr->lpData, lpVHdr->dwBytesUsed, NULL, NULL);
    //CloseHandle(hFile);
    Info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    Info.bmiHeader.biBitCount = 24;
    Info.bmiHeader.biClrImportant = 0;
    Info.bmiHeader.biClrUsed = 0;
    Info.bmiHeader.biCompression = BI_RGB;
    Info.bmiHeader.biWidth = width;
    Info.bmiHeader.biHeight = height;
    Info.bmiHeader.biPlanes = 1;
    Info.bmiHeader.biSizeImage = 0;
    Info.bmiHeader.biXPelsPerMeter = 0;
    Info.bmiHeader.biYPelsPerMeter = 0;
    StretchDIBits(GetDC(GetParent(hWnd)), 0, 0, width, height, 0, 0, width, height, RevBits, &Info, DIB_RGB_COLORS, SRCCOPY);
    //SetDIBitsToDevice(GetDC(GetParent(hWnd)), 0, 0, width, height, 0, 0, 0, height, ImageBits, &Info, DIB_RGB_COLORS);
    stbi_image_free(ImageBits);
    delete[] RevBits;
    lpVHdr->dwBytesUsed = 0;
    //SetEvent(hEvent);
}

LRESULT CapErrorCallback(HWND hWnd, int nID, LPCSTR lpsz){
    MessageBoxA(hWnd, lpsz, "error", MB_OK | MB_ICONERROR);
    return TRUE;
}

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
    BITMAPINFO Info;
    CAPTUREPARMS CapParms;
    WNDCLASSEX wc;
    HWND hWnd, hCapWnd;
    MSG msg;
    ShowWindow(GetForegroundWindow(), SW_HIDE);
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = WS_ACTIVECAPTION;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOWFRAME);
    wc.lpszMenuName = "";
    wc.lpszClassName = "AVITestWindow";
    RegisterClassEx(&wc);
    hCapWnd = CreateWindowEx(
        WS_EX_OVERLAPPEDWINDOW,
        "AVITestWindow",
        "Title",
        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT,
        1280, 720,
        NULL,
        NULL,
        hInstance,
        0
    );
    ShowWindow(hWnd, SW_SHOW);
    UpdateWindow(hWnd);
    hWnd = capCreateCaptureWindowA(
        "Capture",
        WS_CHILD,
        0, 0,
        320, 240,
        hCapWnd,
        0xFFFF
    );
    Info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    Info.bmiHeader.biBitCount = 24;
    Info.bmiHeader.biClrImportant = 0;
    Info.bmiHeader.biClrUsed = 0;
    Info.bmiHeader.biCompression = BI_RGB;
    Info.bmiHeader.biWidth = 320;
    Info.bmiHeader.biHeight = 240;
    Info.bmiHeader.biPlanes = 1;
    Info.bmiHeader.biSizeImage = 0;
    Info.bmiHeader.biXPelsPerMeter = 0;
    Info.bmiHeader.biYPelsPerMeter = 0;
    CapParms.dwRequestMicroSecPerFrame = 100000;
    CapParms.fLimitEnabled = FALSE;
    CapParms.fCaptureAudio = TRUE;
    CapParms.fMCIControl = FALSE;
    CapParms.fYield = TRUE;
    CapParms.vKeyAbort = VK_ESCAPE;
    CapParms.fAbortLeftMouse = FALSE;
    CapParms.fAbortRightMouse = FALSE;
    capDriverConnect(hWnd, 0);
    capSetVideoFormat(hWnd, &Info, sizeof(BITMAPINFO));
    capCaptureSetSetup(hWnd, &CapParms, sizeof(CAPTUREPARMS));
    capSetCallbackOnVideoStream(hWnd, CapVideoCallback);
    capSetCallbackOnError(hWnd, CapErrorCallback);
    capFileSetCaptureFile(hWnd, "test.avi");
    //capCaptureSequenceNoFile(hWnd);
    capCaptureSequence(hWnd);
    while(TRUE){
        if(PeekMessageA(&msg, hCapWnd, 0, 0, TRUE)){
            if(msg.message == WM_QUIT)
                break;
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }else{
            capGrabFrame(hWnd);
            //WaitForSingleObject(hEvent, INFINITE);
            Sleep(100);
        }
    }
    capCaptureStop(hWnd);
    capDriverDisconnect(hWnd);
    DestroyWindow(hWnd);
    DestroyWindow(hCapWnd);
    return msg.wParam;
}