#include <cstdio>
#include <windows.h>

FILE* file;
DWORD dwBytesLen = 0;

const DWORD Channel = 1;
const DWORD Frequency = 44100;
const DWORD SampleSize = 2;

void waveInProc(HWAVEIN hwi, UINT uMsg, DWORD_PTR hInstance, DWORD_PTR Param1, DWORD_PTR Param2){
    switch(uMsg){
        case MM_WIM_OPEN:
            break;
        case MM_WIM_CLOSE:
            break;
        case MM_WIM_DATA:{
            LPWAVEHDR whdr = (LPWAVEHDR)Param1;

            DWORD sum = 0;
            if(SampleSize == 2)
                for(DWORD i = 0; i < whdr->dwBytesRecorded; i += 2){
                    if(((WORD*)whdr->lpData)[i] >= 0x8000)
                        sum += 0x10000 - ((WORD*)whdr->lpData)[i];
                    else
                        sum += ((WORD*)whdr->lpData)[i];
                }
            else if(SampleSize == 1){
                for(DWORD i = 0; i < whdr->dwBytesRecorded; i++){
                    if((BYTE)whdr->lpData[i] >= 0x79 && (BYTE)whdr->lpData[i] <= 0x81)
                        whdr->lpData[i] = 0x80;
                    sum += abs((BYTE)whdr->lpData[i] - 0x80);
                }
            }
            for(DWORD i = 0; i < sum / (SampleSize == 1 ? 0x400 : 0x40000); i++)
                putchar('#');
            putchar('\n');
            //printf("%u\n",sum);

            fwrite(whdr->lpData, 1, whdr->dwBytesRecorded, file);
            dwBytesLen += whdr->dwBytesRecorded;

            waveInAddBuffer(hwi, whdr, sizeof(WAVEHDR));
        }
            break;
    }
}

int main(){
    HWAVEIN hwi;
    WAVEFORMATEX wavForm;
    MMRESULT mmRet;

    wavForm.wFormatTag = WAVE_FORMAT_PCM;
    wavForm.nAvgBytesPerSec = Frequency * Channel * SampleSize;
    wavForm.nBlockAlign = Channel * SampleSize;
    wavForm.nChannels = Channel;
    wavForm.nSamplesPerSec = Frequency;
    wavForm.wBitsPerSample = 8 * Channel * SampleSize;
    wavForm.cbSize = 0;

    const DWORD bufLen = 0x1000;

    static WAVEHDR whdr1, whdr2;
    static BYTE buffer1[bufLen], buffer2[bufLen];

    mmRet = waveInOpen(&hwi, WAVE_MAPPER, &wavForm, (DWORD_PTR)waveInProc, 0, CALLBACK_FUNCTION);

    if(mmRet != MMSYSERR_NOERROR){
        char err[MAX_PATH + 1];
        err[MAX_PATH] = '\0';
        waveInGetErrorTextA(mmRet, err, MAX_PATH);
        MessageBoxA(NULL, err, "waveInOpen Error!", MB_OK | MB_ICONERROR);
        return 0;
    }

    whdr1.lpData = (LPSTR)buffer1;
    whdr1.dwBufferLength = bufLen;
    whdr1.dwBytesRecorded = 0;
    whdr1.dwUser = 0;
    whdr1.dwFlags = 0;
    whdr1.dwLoops = 1;
    whdr1.lpNext = NULL;
    whdr1.reserved = 0;

    whdr2.lpData = (LPSTR)buffer2;
    whdr2.dwBufferLength = bufLen;
    whdr2.dwBytesRecorded = 0;
    whdr2.dwUser = 0;
    whdr2.dwFlags = 0;
    whdr2.dwLoops = 1;
    whdr2.lpNext = NULL;
    whdr2.reserved = 0;

    waveInPrepareHeader(hwi, &whdr1, sizeof(WAVEHDR));
    waveInPrepareHeader(hwi, &whdr2, sizeof(WAVEHDR));

    waveInAddBuffer(hwi, &whdr1, sizeof(WAVEHDR));
    waveInAddBuffer(hwi, &whdr2, sizeof(WAVEHDR));

    struct{
        char riff[4] = {'R','I','F','F'};
        DWORD riffSize;
        char wave[4] = {'W','A','V','E'};
        char fmt[4] = {'f','m','t',' '};
        DWORD fmtLen = 0x10;
        SHORT format = WAVE_FORMAT_PCM;
        SHORT channels = Channel;
        DWORD samplesPerSec = Frequency;
        DWORD bytesPerSec = Frequency * Channel * SampleSize;
        SHORT blockAlign = Channel * SampleSize;
        SHORT bitsPerSample = 8 * Channel * SampleSize;
        char data[4]={'d','a','t','a'};
        DWORD dataSize;
    }wav;

    file = fopen("wave/record.wav", "wb");
    fseek(file, sizeof(wav), FILE_BEGIN);

    printf("Press 'Enter' to start...");
    getchar();
    waveInStart(hwi);

    printf("Press 'Enter' to terminate...");
    getchar();
    waveInStop(hwi);

    Sleep(100);

    waveInClose(hwi);

    wav.dataSize = dwBytesLen;
    wav.riffSize = dwBytesLen + 36;

    fseek(file, 0, FILE_BEGIN);
    fwrite(&wav, sizeof(wav), 1, file);

    return 0;
}