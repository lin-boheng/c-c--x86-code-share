#include <string.h>
#include <windows.h>

int main(int argc, char** argv){
    STARTUPINFOA Info;
    PROCESS_INFORMATION psInfo;
    CHAR szPath[MAX_PATH];
    HANDLE hOut;

    hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    if(argc != 2){
        WriteFile(hOut, "Usage: startps.exe [file] [args...]", 35, NULL, NULL);
        ExitProcess(0);
    }
    GetStartupInfoA(&Info);
    GetCurrentDirectoryA(MAX_PATH, szPath);
    CreateProcessA(argv[1], NULL, NULL, NULL, FALSE, 0, NULL, szPath, &Info, &psInfo);
    ExitProcess(0);
}