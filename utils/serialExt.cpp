#include <windows.h>

static HANDLE hSerial = INVALID_HANDLE_VALUE;
static TCHAR szName[MAX_PATH];

BOOL DllMain(HINSTANCE hInst, DWORD dwReason, DWORD dwReserved){
    return TRUE;
}

extern "C" BOOL __declspec(dllexport) __cdecl SerialTimeout(){
    COMMTIMEOUTS commTO;
    GetCommTimeouts(hSerial, &commTO);
    commTO.ReadIntervalTimeout = MAXDWORD;
    commTO.ReadTotalTimeoutConstant = 0;
    commTO.ReadTotalTimeoutMultiplier = 0;
    return SetCommTimeouts(hSerial, &commTO);
}

extern "C" BOOL __declspec(dllexport) __cdecl SerialExists(DWORD no){
    if (hSerial != INVALID_HANDLE_VALUE)
        return FALSE;
    wsprintf(szName, TEXT("\\\\.\\COM%u"), no);
    hSerial = CreateFile(
        szName,
        GENERIC_READ | GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        0,
        NULL
    );
    if (hSerial == INVALID_HANDLE_VALUE)
        return FALSE;
    CloseHandle(hSerial);
    hSerial = INVALID_HANDLE_VALUE;
    return TRUE;
}

extern "C" BOOL __declspec(dllexport) __cdecl SerialOpen(DWORD no){
    DCB commDCB;
    if (hSerial != INVALID_HANDLE_VALUE)
        return FALSE;
    wsprintf(szName, TEXT("\\\\.\\COM%u"), no);
    hSerial = CreateFile(
        szName,
        GENERIC_READ | GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        0,
        NULL
    );
    if (hSerial == INVALID_HANDLE_VALUE)
        return FALSE;
    commDCB.DCBlength = sizeof(DCB);
    commDCB.fAbortOnError = FALSE;
    commDCB.fBinary = TRUE;
    commDCB.BaudRate = 115200;                  //clock frequency
    commDCB.ByteSize = 8;                       //8 bits in a byte
    commDCB.StopBits = 0;                       //STOPBITS_10
    commDCB.fParity = FALSE;                    //no parity check
    commDCB.Parity = 0;                         //PARITY_NONE
    commDCB.fDtrControl = DTR_CONTROL_DISABLE;  //pin DTR
    commDCB.fDsrSensitivity = FALSE;            //pin DSR
    commDCB.fRtsControl = RTS_CONTROL_DISABLE;  //pin RTS
    commDCB.fOutxCtsFlow = FALSE;               //pin CTS
    commDCB.fOutX = TRUE;                       //pin TX ->
    commDCB.fInX = TRUE;                        //pin RX <-
    SetCommState(hSerial, &commDCB);
    SerialTimeout();
    return TRUE;
}

extern "C" BOOL __declspec(dllexport) __cdecl SerialClose(){
    if (hSerial == INVALID_HANDLE_VALUE)
        return FALSE;
    return CloseHandle(hSerial);
}

extern "C" BOOL __declspec(dllexport) __cdecl SerialRead(LPVOID buf, DWORD len, LPDWORD read){
    return ReadFile(hSerial, buf, len, read, NULL);
}

extern "C" BOOL __declspec(dllexport) __cdecl SerialWrite(LPCVOID buf, DWORD len, LPDWORD written){
    return WriteFile(hSerial, buf, len, written, NULL);
}
