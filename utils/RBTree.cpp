#include <iostream>
#include <algorithm>
#include <map>
#include <windows.h>
using namespace std;
template <typename Key, typename Value>
class Node{
public:
    static Node<Key, Value>* root;
    Node<Key, Value>* s[2];
    Node<Key, Value>* f;
    Key k;
    Value v;
    bool black;
    Node<Key, Value>(){
        s[0]=NULL;s[1]=NULL;
        black=false;
    }
    Node<Key, Value>(Key key, Value val){
        s[0]=NULL;s[1]=NULL;
        k=key;v=val;
        black=false;
    }
    Node<Key, Value>* query(Key key){
        if(k==key) return this;
        Node* n=s[k<key];
        return n?n->query(key):NULL;
    }
    bool pos(){ return f->s[1]==this; }
    Node* rela(){ return f->s[!pos()]; }
    void rot(bool l){
        Node* t=s[l];
        if(f) f->s[pos()]=t;
        t->f=f;
        f=t;
        s[l]=t->s[!l];
        t->s[!l]=this;
    }
    void update(){
        while(root->f) root=root->f;
        root->black=true;
        if(f&&!f->black){
            if(f->rela()&&!f->rela()->black){
                f->black=true;
                f->rela()->black=true;
                f->f->black=false;
                f->f->update();
                return;
            }else{
                if(f->pos()^pos()){
                    f->rot(pos());
                    s[pos()]->update();
                    return;
                }
                f->black=true;
                f->f->black=false;
                f->f->rot(pos());
                return;
            }
        }
    }
    Node<Key, Value>* insert(Key key,Value val){
        if(k==key){
            v=val;
            return this;
        }
        Node<Key, Value>*& n=s[k<key];
        if(!n){
            n=new Node<Key, Value>(key,val);
            n->f=this;
            n->update();
            return n;
        }
        return n->insert(key, val);
    }
};
template<typename Key, typename Value>
Node<Key, Value>* Node<Key, Value>::root=NULL;
template<typename Key, typename Value>
class Root{
public:
    static Node<Key, Value>* query(Key key){
        if(!Node<Key, Value>::root) return NULL;
        return Node<Key, Value>::root->query(key);
    }
    static Node<Key, Value>* insert(Key key,Value val){
        if(!Node<Key, Value>::root){
            Node<Key, Value>::root=new Node<Key, Value>(key,val);
            Node<Key, Value>::root->f=NULL;
            Node<Key, Value>::root->black=true;
            return Node<Key, Value>::root;
        }
        Node<Key, Value>* ret=Node<Key, Value>::root->insert(key, val);
        while(Node<Key, Value>::root->f) Node<Key, Value>::root=Node<Key, Value>::root->f;
        Node<Key, Value>::root->black=true;
        return ret;
    }
    static void dels(Node<Key, Value>* t,Node<Key, Value>*& s){
        t->k=s->k;
        t->v=s->v;
        delete s;
        s=NULL;
    }
    static void del(Key key){
        if(!Node<Key, Value>::root) return;
        Node<Key, Value>* t=Node<Key, Value>::root->query(key);
        if(!t) return;
        if(t->s[0]){
            if(t->s[1]){
                Node<Key, Value>* s=t->s[1];
                while(s->s[0]) s=s->s[0];
                dels(t,s);
                return;
            }
            dels(t,t->s[0]);
        }else if(t->s[1]){
            dels(t,t->s[1]);
        }else{
            if(t==Node<Key, Value>::root) Node<Key, Value>::root=NULL;
            delete t;
        }
    }
};
template<typename Key, typename Value>
void out(Node<Key, Value>* r){
    if(r){
        cout<<'('<<r->k<<','<<r->v<<','<<(r->black?"B":"R")<<')';
        cout<<'{';
        out(r->s[0]);
        cout<<',';
        out(r->s[1]);
        cout<<'}';
    }else cout<<"/";
}
HANDLE hPipe;
TCHAR szIns[MAX_PATH];
TCHAR arg0[MAX_PATH];
int arg1, arg2;
void(*read)();
void read_pipe(){
    int p=0;
    TCHAR c;
    do{
        ReadFile(hPipe, &c, 1, NULL, NULL);
        szIns[p++]=c;
    }while(c!='\n');
    szIns[p]='\0';
    sscanf_s(szIns, "%s %d %d", arg0, &arg1, &arg2);
}
void read_cin(){
    scanf_s("%s", arg0);
    if(!strcmp(arg0, "insert")){
        scanf_s("%d%d", &arg1, &arg2);
    }else if(!strcmp(arg0, "query")){
        scanf_s("%d", &arg1);
    }else if(!strcmp(arg0, "delete")){
        scanf_s("%d", &arg1);
    }
}
void init(){
    cout<<"RBTree"<<endl;
    hPipe = CreateFile(
        TEXT("\\\\.\\pipe\\RBTreeIn"),
        FILE_GENERIC_READ,
        FILE_SHARE_READ,
        NULL,
        OPEN_EXISTING,
        0,
        NULL
    );
    if(hPipe==INVALID_HANDLE_VALUE){
        read=read_cin;
        return;
    }
    cout<<hPipe<<endl;
    SetStdHandle(STD_INPUT_HANDLE, hPipe);
    read=read_pipe;
}
int main(int argc, char** argv) {
    map<string, void(*)()> cmd;
    string s;
    init();
    cmd["insert"]=[](){
        Root<int, int>::insert(arg1, arg2);
        #ifdef DEBUG
        out(Node::root);
        cout<<endl;
        #endif
    };
    cmd["query"]=[](){
        Node<int, int>* r=Root<int, int>::query(arg1);
        r?cout<<r->v<<endl:cout<<"NULL\n";
    };
    cmd["delete"]=[](){
        Root<int, int>::del(arg1);
        #ifdef DEBUG
        out(Node<int, int>::root);
        cout<<endl;
        #endif
    };
    while(true){
        read();
        transform(s.begin(),s.end(),s.begin(),::tolower);
        if(cmd.find(arg0)==cmd.end()||cmd.find(arg0)->second==NULL){
            cout<<"command not found!"<<endl;
            continue;
        }else cmd[arg0]();
    }
    return 0;
}