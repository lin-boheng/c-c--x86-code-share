#define UNICODE
#include <windows.h>

int main(){
    SC_HANDLE hSCManager;
    SC_HANDLE hService;
    BYTE ch;

    HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    HANDLE hIn = GetStdHandle(STD_INPUT_HANDLE);

    hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);

    if(hSCManager){
        hService = CreateService(
            hSCManager,
            L"MySvc",
            L"MySvc",
            SERVICE_ALL_ACCESS,
            SERVICE_KERNEL_DRIVER,
            SERVICE_DEMAND_START,
            SERVICE_ERROR_NORMAL,
            L"C:\\Users\\lenovo\\Desktop\\USBDriver1.sys",
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
        );

        CloseServiceHandle(hSCManager);

        if(hService == NULL || hService == INVALID_HANDLE_VALUE){
            WriteFile(hOut, "Create service failed!\n", 23, NULL, NULL);
            ReadFile(hIn, &ch, 1, NULL, NULL);
            return 1;
        }
    }else{
        WriteFile(hOut, "Open service manager failed!\n", 29, NULL, NULL);
        ReadFile(hIn, &ch, 1, NULL, NULL);
        return 1;
    }
    
    CloseServiceHandle(hService);

    ReadFile(hIn, &ch, 1, NULL, NULL);

    return 0;
}