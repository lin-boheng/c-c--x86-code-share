#include <cstdio>
#include <windows.h>
#include <vfw.h>

LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam){
    switch(Msg){
    case WM_CREATE:
        break;
    case WM_QUIT:
        PostQuitMessage(0);
        break;
    case WM_DESTROY:
        ExitProcess(0);
        break;
    }
    return DefWindowProc(hWnd, Msg, wParam, lParam);
}

int WinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,
    int nCmdShow
){
    WNDCLASSEX wc;
    HWND hWnd;
    HWND hCapWnd;
    MSG msg;

    ShowWindow(GetForegroundWindow(), SW_HIDE);

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = WS_ACTIVECAPTION;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOWFRAME);
    wc.lpszMenuName = "";
    wc.lpszClassName = "AvicapTestWindow";

    RegisterClassEx(&wc);

    hWnd = CreateWindowEx(
        WS_EX_OVERLAPPEDWINDOW,
        "AvicapTestWindow",
        "Title",
        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT,
        340, 280,
        NULL,
        NULL,
        hInstance,
        0
    );

    ShowWindow(hWnd, SW_SHOW);
    UpdateWindow(hWnd);

    hCapWnd = capCreateCaptureWindowA(
        "title",
        WS_CHILD | WS_VISIBLE | WS_THICKFRAME,
        CW_USEDEFAULT, CW_USEDEFAULT,
        320, 240,
        hWnd,
        0xFFFF
    );

    printf("%016llX\n", (DWORD_PTR)hCapWnd);

    CAPTUREPARMS capParams;

    capDriverConnect(hCapWnd, 0);

    capFileSetCaptureFile(hCapWnd, "test.avi");

    //capCaptureGetSetup(hCapWnd, &capParams, sizeof(CAPTUREPARMS));
    //capParams.dwRequestMicroSecPerFrame = 100000;
    //capCaptureSetSetup(hCapWnd, &capParams, sizeof(CAPTUREPARMS));

    capPreview(hCapWnd, TRUE);
    capPreviewRate(hCapWnd, 30);
    capPreviewScale(hCapWnd, TRUE);

    capCaptureSequence(hCapWnd);
    
    while(GetMessageA(&msg, hWnd, 0, 0)){
        if(msg.message == WM_QUIT)
            break;
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    capCaptureStop(hCapWnd);

    capDriverDisconnect(hCapWnd);

    DestroyWindow(hWnd);
    ExitProcess(0);

    return 0;
}