#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <windows.h>
using namespace std;
struct GDTEntry{
    unsigned short limit_low;
    unsigned short base_low;
    unsigned char base_middle;
    unsigned char access;
    unsigned char granularity;
    unsigned char base_high;
} __attribute__((packed));
struct GDTPointer{
    unsigned short limit;
    unsigned int base;
} __attribute__((packed));
typedef struct _UNICODE_STRING{
    USHORT Length;
    USHORT MaximumLength;
    PWSTR Buffer;
} UNICODE_STRING, *PUNICODE_STRING;
typedef enum _SECTION_INHERIT{
    ViewShare = 1,
    ViewUnmap = 2
} SECTION_INHERIT, *PSECTION_INHERIT;
typedef struct _OBJECT_ATTRIBUTES{
    ULONG Length;
    HANDLE RootDirectory;
    PUNICODE_STRING ObjectName;
    ULONG Attributes;
    PVOID SecurityDescriptor;
    PVOID SecurityQualityOfService;
} OBJECT_ATTRIBUTES, *POBJECT_ATTRIBUTES;
#define InitializeObjectAttributes( p, n, a, r, s ){\
    (p)->Length = sizeof( OBJECT_ATTRIBUTES );\
    (p)->RootDirectory = r;\
    (p)->Attributes = a;\
    (p)->ObjectName = n;\
    (p)->SecurityDescriptor = s;\
    (p)->SecurityQualityOfService = NULL;\
}
typedef NTSTATUS (WINAPI *ZwOpenSectionProc)(
    PHANDLE SectionHandle,
    DWORD DesiredAccess,
    POBJECT_ATTRIBUTES ObjectAttributes
);
typedef NTSTATUS (WINAPI *ZwMapViewOfSectionProc)(
    HANDLE SectionHandle,
    HANDLE ProcessHandle,
    PVOID *BaseAddress,
    ULONG ZeroBits,
    ULONG CommitSize,
    PLARGE_INTEGER SectionOffset,
    PULONG ViewSize,
    SECTION_INHERIT InheritDisposition,
    ULONG AllocationType,
    ULONG Protect
);
typedef NTSTATUS (WINAPI *ZwUnmapViewOfSectionProc)(
    HANDLE ProcessHandle,
    PVOID BaseAddress
);
typedef VOID (WINAPI *RtlInitUnicodeStringProc)(
    IN OUT PUNICODE_STRING DestinationString,
    IN PCWSTR SourceString
);
HANDLE hMemory;
HMODULE ntdll;
ZwOpenSectionProc ZwOpenSection;
ZwMapViewOfSectionProc ZwMapViewOfSection;
ZwUnmapViewOfSectionProc ZwUnmapViewOfSection;
RtlInitUnicodeStringProc RtlInitUnicodeString;
void ExitPhysicalMemory(){
    if (hMemory != NULL){
        CloseHandle(hMemory);
    }
    if (ntdll != NULL){
        FreeLibrary(ntdll);
    }
}
BOOL ReadPhysicalMemory(PVOID buffer, DWORD address, DWORD length){
    DWORD outlen;            // 输出长度，根据内存分页大小可能大于要求的长度
    PVOID vaddress;          // 映射的虚地址
    NTSTATUS status;         // NTDLL函数返回的状态
    LARGE_INTEGER base;      // 物理内存地址
 
    vaddress = 0;
    outlen = length;
    base.QuadPart = (ULONGLONG)(address);
    // 映射物理内存地址到当前进程的虚地址空间
    status = ZwMapViewOfSection(hMemory,
        (HANDLE) -1,
        (PVOID *)&vaddress,
        0,
        length,
        &base,
        &outlen,
        ViewShare,
        0,
        PAGE_READONLY);
    if (status < 0){
        return FALSE;
    }
    // 当前进程的虚地址空间中，复制数据到输出缓冲区
    memmove(buffer, vaddress, length);
    // 完成访问，取消地址映射
    status = ZwUnmapViewOfSection((HANDLE)-1, (PVOID)vaddress);
    return (status >= 0);
}
void print(char* GDT){
    WORD GDTLimit=*(LPWORD)GDT;
    DWORD GDTBase=*(LPDWORD)(GDT+2);
    printf("%04X %08X\n",GDTLimit,GDTBase);
    if(hMemory==INVALID_HANDLE_VALUE){
        printf("Open memory error: %d\n",GetLastError());
        return;
    }
    UCHAR Buffer[0x1000];
    if(!ReadPhysicalMemory(Buffer, GDTBase, 0x1000)){
        puts("Read memory error.");
        return;
    }
    for(DWORD i=0;i<(GDTLimit+1)/8;i++){
        puts("=======================");
        if(i>=0x200)
            break;
        LPBYTE Item=Buffer+(i<<3);
        DWORD Base=0;
        DWORD Limit=0;
        Base|=(DWORD)(*(LPWORD)(Item+2));
        Base|=((DWORD)*(LPBYTE)(Item+4))<<16;
        Base|=((DWORD)*(LPBYTE)(Item+7))<<24;
        Limit|=(DWORD)(*(LPWORD)(Item));
        Limit|=((DWORD)(*(LPBYTE)(Item+6))&0x0F)<<16;
        printf("Base: %08X\n",Base);
        printf("Seg Limit: %05X\n",Limit);
    }
}
BOOL init(){
    ntdll=LoadLibraryA("ntdll.dll");
    ZwOpenSection=(ZwOpenSectionProc)GetProcAddress(ntdll,"ZwOpenSection");
    ZwMapViewOfSection=(ZwMapViewOfSectionProc)GetProcAddress(ntdll,"ZwMapViewOfSection");
    ZwUnmapViewOfSection=(ZwUnmapViewOfSectionProc)GetProcAddress(ntdll,"ZwUnmapViewOfSection");
    RtlInitUnicodeString=(RtlInitUnicodeStringProc)GetProcAddress(ntdll,"RtlInitUnicodeString");
    WCHAR PhysicalMemoryName[] = L"//Device//PhysicalMemory";
    UNICODE_STRING PhysicalMemoryString;
    OBJECT_ATTRIBUTES attributes;
    RtlInitUnicodeString(&PhysicalMemoryString, PhysicalMemoryName);
    InitializeObjectAttributes(&attributes, &PhysicalMemoryString, 0, NULL, NULL);
    NTSTATUS status = ZwOpenSection(&hMemory, SECTION_MAP_READ, &attributes );
    return (status >= 0);
}
int main(){
    char* GDT;
    GDT=new char[6];
    asm("push %rax");
    asm("mov -0x8(%rbp),%rax");
    asm("sgdt 0(%rax)");
    asm("pop %rax");
    printf("%04X %08X\n",*(LPWORD)GDT,*(LPDWORD)(GDT+2));
    if(!init()){
        puts("Init failed.");
        getchar();
        return 0;
    }
    print(GDT);
    delete[] GDT;
    ExitPhysicalMemory();
    getchar();
    return 0;
}