#include <wchar.h>
#include <windows.h>
#include <winternl.h>
#include <stdio.h>
#include <tlhelp32.h>

#define GOOD_HANDLE(x) (x != INVALID_HANDLE_VALUE && x != NULL)
 
typedef struct
{
	ULONG          AllocationSize;
	ULONG          ActualSize;
	ULONG          Flags;
	ULONG          Unknown1;
	UNICODE_STRING Unknown2;
	HANDLE         InputHandle;
	HANDLE         OutputHandle;
	HANDLE         ErrorHandle;
	UNICODE_STRING CurrentDirectory;
	HANDLE         CurrentDirectoryHandle;
	UNICODE_STRING SearchPaths;
	UNICODE_STRING ApplicationName;
	UNICODE_STRING CommandLine;
	PVOID          EnvironmentBlock;
	ULONG          Unknown[9];
	UNICODE_STRING Unknown3;
	UNICODE_STRING Unknown4;
	UNICODE_STRING Unknown5;
	UNICODE_STRING Unknown6;
} PROCESS_PARAMETERS, *PPROCESS_PARAMETERS;

int main(){
    HANDLE hOut;
    LPVOID lpvInfo;
    LPVOID lpvStr;
    ULONG uLength;
    PSYSTEM_PROCESS_INFORMATION lpInfo;
    HANDLE hSnapShot;
    PROCESSENTRY32W ps;
    PWSTR pwLine;

    hOut = GetStdHandle(STD_OUTPUT_HANDLE);

    typedef NTSTATUS(*fnNtQuerySystemInformation)(SYSTEM_INFORMATION_CLASS, PVOID, ULONG, PULONG);
    typedef NTSTATUS(*fnRtlUnicodeToMultiByteSize)(PULONG, PWSTR, ULONG);
    typedef NTSTATUS(*fnNtQueryInformationProcess)(HANDLE, PROCESSINFOCLASS, PVOID, ULONG, PULONG);

    fnNtQuerySystemInformation _NtQuerySystemInformation = (fnNtQuerySystemInformation)GetProcAddress(GetModuleHandleA("ntdll.dll"), "NtQuerySystemInformation");
    fnRtlUnicodeToMultiByteSize _RtlUnicodeToMultiByteSize = (fnRtlUnicodeToMultiByteSize)GetProcAddress(GetModuleHandleA("ntdll.dll"), "RtlUnicodeToMultiByteSize");
    fnNtQueryInformationProcess _NtQueryInformationProcess = (fnNtQueryInformationProcess)GetProcAddress(GetModuleHandleA("ntdll.dll"), "NtQueryInformationProcess");
    
    _NtQuerySystemInformation(SystemProcessInformation, lpvInfo, 0, &uLength);
    lpvInfo = GlobalAlloc(GMEM_FIXED, uLength);
    _NtQuerySystemInformation(SystemProcessInformation, lpvInfo, uLength, NULL);

    lpInfo = (PSYSTEM_PROCESS_INFORMATION)lpvInfo;
    lpvStr = GlobalAlloc(GMEM_FIXED, MAX_PATH);
    pwLine = (PWSTR)GlobalAlloc(GMEM_FIXED | GMEM_ZEROINIT, 0x10000);
    
    do {
        HANDLE hProcess;
        PROCESS_BASIC_INFORMATION pbi;
        PEB peb;
        PROCESS_PARAMETERS param;

        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, (DWORD_PTR)lpInfo->UniqueProcessId);
        if (GOOD_HANDLE(hProcess)) {
            _NtQueryInformationProcess(hProcess, ProcessBasicInformation, &pbi, sizeof(PROCESS_BASIC_INFORMATION), NULL);
            ReadProcessMemory(hProcess, pbi.PebBaseAddress, &peb, sizeof(PEB), NULL);
            ReadProcessMemory(hProcess, peb.ProcessParameters, &param, sizeof(PROCESS_PARAMETERS), NULL);
            ReadProcessMemory(hProcess, param.CommandLine.Buffer, pwLine, param.CommandLine.Length, NULL);
            wprintf(L"%s\n", pwLine);
            CloseHandle(hProcess);
        }
    }while (lpInfo->NextEntryOffset && (lpInfo = (PSYSTEM_PROCESS_INFORMATION)((LPBYTE)lpInfo + lpInfo->NextEntryOffset)));
    GlobalFree(pwLine);

    // ps.dwSize = sizeof(PROCESSENTRY32W);
    // hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    // Process32FirstW(hSnapShot, &ps);
    // do {
    //     HANDLE hProcess;
    //     PROCESS_BASIC_INFORMATION pbi;
    //     PEB peb;
    //     PROCESS_PARAMETERS param;
        
    //     wprintf(L"%d %s\n", ps.th32ProcessID, ps.szExeFile);
    //     hProcess = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, ps.th32ProcessID);
    //     if (GOOD_HANDLE(hProcess)) {
    //         _NtQueryInformationProcess(hProcess, ProcessBasicInformation, &pbi, sizeof(PROCESS_BASIC_INFORMATION), NULL);
    //         ReadProcessMemory(hProcess, pbi.PebBaseAddress, &peb, sizeof(PEB), NULL);
    //         ReadProcessMemory(hProcess, peb.ProcessParameters, &param, sizeof(PROCESS_PARAMETERS), NULL);
    //         ReadProcessMemory(hProcess, param.CommandLine.Buffer, pwLine, param.CommandLine.Length, NULL);
    //         wprintf(L"%s\n", pwLine);
    //         CloseHandle(hProcess);
    //     }
    // }while (Process32NextW(hSnapShot, &ps));
    // CloseHandle(hSnapShot);

    // GlobalFree(lpvInfo);
    // GlobalFree(lpvStr);

    ExitProcess(0);
}