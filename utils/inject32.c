#include <windows.h>
#include <tlhelp32.h>

#define SE_CREATE_TOKEN_PRIVILEGE 0X2
#define SE_ASSIGN_PRIMARY_TOKEN_PRIVILEGE 0X3
#define SE_LOCK_MEMORY_PRIVILEGE 0X4
#define SE_INCREASE_QUOTA_PRIVILEGE 0X5
#define SE_UNSOLICITED_INPUT_PRIVILEGE 0X0
#define SE_MACHINE_ACCOUNT_PRIVILEGE 0X6
#define SE_TCB_PRIVILEGE 0X7
#define SE_SECURITY_PRIVILEGE 0X8
#define SE_TAKE_OWNER_SHIP_PRIVILEGE 0X9
#define SE_LOAD_DRIVER_PRIVILEGE 0XA
#define SE_SYSTEM_PROFILE_PRIVILEGE 0XB
#define SE_SYSTEM_TIME_PRIVILEGE 0XC
#define SE_PROFILE_SINGLE_PROCESS_PRIVILEGE 0XD
#define SE_INCREASE_BASE_PRIORITY_PRIVILEGE 0XE
#define SE_CREATE_PAGE_FILE_PRIVILEGE 0XF
#define SE_CREATE_PERMANENT_PRIVILEGE 0X10
#define SE_BACKUP_PRIVILEGE 0X11
#define SE_RESTORE_PRIVILEGE 0X12
#define SE_SHUTDOWN_PRIVILEGE 0X13
#define SE_DEBUG_PRIVILEGE 0X14
#define SE_AUDIT_PRIVILEGE 0X15
#define SE_SYSTEM_ENVIRONMENT_PRIVILEGE 0X16
#define SE_CHANGE_NOTIFY_PRIVILEGE 0X17
#define SE_REMOTE_SHUTDOWN_PRIVILEGE 0X18
#define SE_UNDOCK_PRIVILEGE 0X19
#define SE_SYNC_AGENT_PRIVILEGE 0X1A
#define SE_ENABLE_DELEGATION_PRIVILEGE 0X1B
#define SE_MANAGE_VOLUME_PRIVILEGE 0X1C
#define SE_IMPERSONATE_PRIVILEGE 0X1D
#define SE_CREATE_GLOBAL_PRIVILEGE 0X1E
#define SE_TRUSTED_CRED_MAN_ACCESS_PRIVILEGE 0X1F
#define SE_RELABEL_PRIVILEGE 0X20
#define SE_INCREASE_WORK_INGSET_PRIVILEGE 0X21
#define SE_TIMEZONE_PRIVILEGE 0X22
#define SE_CREATE_SYMBOLIC_LINK_PRIVILEGE 0X23

BOOL inject32(DWORD pid, LPCTSTR szDll){
    HANDLE hps, hThread;
    DWORD dwLen;
    LPVOID lpvMem;
    HMODULE hKrnl, hDll;
    FARPROC lpfnLoad;
    DWORD dwState;

    hps = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE, FALSE, pid);
    if(hps == NULL)
        return FALSE;
    dwLen = (strlen(szDll) + 1) * sizeof(TCHAR);
    lpvMem = VirtualAllocEx(hps, NULL, dwLen, MEM_COMMIT, PAGE_READWRITE);
    if(lpvMem == NULL){
        CloseHandle(hps);
        return FALSE;
    }
    if(!WriteProcessMemory(hps, lpvMem, szDll, dwLen, NULL)){
        VirtualFreeEx(hps, lpvMem, dwLen, MEM_RELEASE);
        CloseHandle(hps);
        return FALSE;
    }
    hKrnl = GetModuleHandle(TEXT("kernel32.dll"));
    lpfnLoad = GetProcAddress(hKrnl, "LoadLibraryA");
    hThread = CreateRemoteThread(hps, NULL, 0, (DWORD(CALLBACK*)(LPVOID))lpfnLoad, lpvMem, 0, NULL);
    dwState = WaitForSingleObject(hThread, 1000);
    if(dwState != WAIT_OBJECT_0){
        CloseHandle(hThread);
        CloseHandle(hps);
        return FALSE;
    }
    GetExitCodeThread(hThread, (LPDWORD)&hDll);
    CloseHandle(hThread);
    CloseHandle(hps);
    return hDll == NULL ? FALSE : TRUE;
}

int main(int argc, char** argv){
    HANDLE hOut;
    HANDLE hSnap;
    PROCESSENTRY32 pe;
    BOOL bExist;
    BOOLEAN bEnabled;

    hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    if(argc != 3){
        WriteFile(hOut, "Usage: inject32.exe [name] [DLLFullPath]\n", 41, NULL, NULL);
        ExitProcess(1);
    }
    typedef NTSTATUS(*fnRtlAdjustPrivilege)(ULONG, BOOLEAN, BOOLEAN, PBOOLEAN);
    fnRtlAdjustPrivilege _RtlAdjustPrivilege = (fnRtlAdjustPrivilege)GetProcAddress(GetModuleHandleA("ntdll.dll"), "RtlAdjustPrivilege");
    _RtlAdjustPrivilege(SE_DEBUG_PRIVILEGE, TRUE, FALSE, &bEnabled);
    hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if(hSnap == INVALID_HANDLE_VALUE){
        WriteFile(hOut, "CreateToolhelp32Snapshot error\n", 31, NULL, NULL);
        ExitProcess(1);
    }
    pe.dwSize = sizeof(PROCESSENTRY32);
    bExist = Process32First(hSnap, &pe);
    while(bExist){
        if(strstr(pe.szExeFile, argv[1])){
            WriteFile(hOut, "Trying to inject: ", 18, NULL, NULL);
            WriteFile(hOut, pe.szExeFile, strlen(pe.szExeFile), NULL, NULL);
            inject32(pe.th32ProcessID, argv[2]) ? WriteFile(hOut, " Success!\n", 10, NULL, NULL) : WriteFile(hOut, " Failed!\n", 9, NULL, NULL);
        }
        bExist = Process32Next(hSnap, &pe);
    }
    CloseHandle(hSnap);
    ExitProcess(0);
}
