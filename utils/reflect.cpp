#include <windows.h>
#include <stdio.h>

#define __push(x) __asm__("push %0"::"r"(x))
#define __pushm(x) __asm__("push %0"::"m"(x))
#define __call(x) __asm__("call *%0"::"r"(x))
#define __popm(x) __asm__("pop %0"::"m"(x))
#define __popn(x) __asm__("add %0,%%esp"::"r"(x))
#define __ebp(x) __asm__("mov %%ebp,%0"::"m"(x))
#define __esp(x) __asm__("mov %%esp,%0"::"m"(x))
#define __sebp(x) __asm__("mov %0,%%ebp"::"m"(x))
#define __sesp(x) __asm__("mov %0,%%esp"::"m"(x))
#define __ret(x) __asm__("mov %%eax,%0":"m"(x))
#define __caller(x,ebp) __asm__("mov 4(%1),%0;mov (%1),%1":"=r"(x),"+r"(ebp))

void*(*Reflect)(LPCSTR);

BOOL APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpvReserved){
    return TRUE;
}

extern "C" BOOL foo(UINT uType){
    MessageBeep(uType);
    //这里由于MessageBeep返回到eax但未处理
    //后面函数直接返回却没有操作eax而作为返回值
    //所以MessageBeep前加不加return没区别
}

extern "C" int entry(void* lpfnReflect){
    Reflect = (void*(*)(const char*))lpfnReflect;
    void* foo = Reflect("foo");
    printf("%s\n", ((int(*)())foo)() ? "TRUE" : "FALSE");
    printf("%s\n", ((int(*)(int))foo)(MB_ICONERROR) ? "TRUE" : "FALSE");
    int(*getret)()=(int(*)())VirtualAlloc(NULL, 1, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
    *(char*)getret=0xC3;//x86 ret
    //FlushInstructionCache(GetCurrentProcess(), (void*)getret, 1);//可要可不要
    int i, j=5;
    __esp(j);
    for(i=0;i<=j;i+=0x100000)
        __push(MB_ICONERROR);
    __call(foo);
    __popn(4*((j>>20)+1));
    printf("%s\n", getret() ? "TRUE" : "FALSE");
    VirtualFree((void*)getret, 1, MEM_RELEASE);
    return 0;
}

//C++ extern "C" 自带DLL导出
//C/C++ 函数默认__cdecl
//应使用_declspec(dllexport)单个'_'
//.exe不能Get自己的函数，但是.dll可以
//同样嵌入asm，C进行远调用时不明白的复杂，C++的call压栈传参与asm同步
