// 这次我要尝试录制用C++代码构造Java的Class文件
// 有可能会失败，但是我之前已经实现过
// 就当是分享一下码代码一点点大致思路吧

#include <cstdio>
#include <cstring>
#include <fstream>

#include <vector>

#pragma region Types

// 这是一些基本Class文件用到的变量类型
typedef unsigned int jmagic;
typedef unsigned short jversion;
typedef unsigned short jtype;
typedef unsigned short jindex;
typedef unsigned short jflag;
typedef unsigned short jcount;
typedef unsigned int jlength;
typedef unsigned char jbyte;

#pragma endregion

// 主要是Debug使用,若是不需要调试输出时去掉后面"printf x",再重新编译即可
#define debug_printf(x) printf x

// 这里摆下一些重要的常量
#pragma region Constants

// Class文件字节码
enum class Code {
    nop = 0x00,
    aconst_null = 0x01,
    iconst_m1 = 0x02,
    iconst_0 = 0x03,
    iconst_1 = 0x04,
    iconst_2 = 0x05,
    iconst_3 = 0x06,
    iconst_4 = 0x07,
    iconst_5 = 0x08,
    lconst_0 = 0x09,
    lconst_1 = 0x0A,
    fconst_0 = 0x0B,
    fconst_1 = 0x0C,
    fconst_2 = 0x0D,
    dconst_0 = 0x0E,
    dconst_1 = 0x0F,
    bipush = 0x10,//[signed char]
    sipush = 0x11,//[signed short]
    ldc = 0x12,//[byte]
    ldc_w = 0x13,//[word]
    ldc2_w = 0x14,//[word]
    aload = 0x19,//[byte]
    aload_0 = 0x2A,
    aload_1 = 0x2B,
    aload_2 = 0x2C,
    aload_3 = 0x2D,
    iload = 0x15,//[byte]
    iload_0 = 0x1A,
    iload_1 = 0x1B,
    iload_2 = 0x1C,
    iload_3 = 0x1D,
    lload = 0x16,//[byte]
    lload_0 = 0x1E,
    lload_1 = 0x1F,
    lload_2 = 0x20,
    lload_3 = 0x21,
    fload = 0x17,//[byte]
    fload_0 = 0x22,
    fload_1 = 0x23,
    fload_2 = 0x24,
    fload_3 = 0x25,
    dload = 0x18,//[byte]
    dload_0 = 0x26,
    dload_1 = 0x27,
    dload_2 = 0x28,
    dload_3 = 0x29,
    aaload = 0x32,
    iaload = 0x2E,
    laload = 0x2F,
    faload = 0x30,
    daload = 0x31,
    baload = 0x33,
    caload = 0x34,
    saload = 0x35,
    astore = 0x3A,//[byte]
    astore_0 = 0x4B,
    astore_1 = 0x4C,
    astore_2 = 0x4D,
    astore_3 = 0x4E,
    istore = 0x36,//[byte]
    istore_0 = 0x3B,
    istore_1 = 0x3C,
    istore_2 = 0x3D,
    istore_3 = 0x3E,
    lstore = 0x37,//[byte]
    lstore_0 = 0x3F,
    lstore_1 = 0x40,
    lstore_2 = 0x41,
    lstore_3 = 0x42,
    fstore = 0x38,//[byte]
    fstore_0 = 0x43,
    fstore_1 = 0x44,
    fstore_2 = 0x45,
    fstore_3 = 0x46,
    dstore = 0x39,//[byte]
    dstore_0 = 0x47,
    dstore_1 = 0x48,
    dstore_2 = 0x49,
    dstore_3 = 0x4A,
    aastore = 0x53,
    iastore = 0x4F,
    lastore = 0x50,
    fastore = 0x51,
    dastore = 0x52,
    bastore = 0x54,
    castore = 0x55,
    sastore = 0x56,
    pop = 0x57,
    pop2 = 0x58,
    dup = 0x59,
    dup_x1 = 0x5A,
    dup_x2 = 0x5B,
    dup2 = 0x5C,
    dup2_x1 = 0x5D,
    dup2_x2 = 0x5E,
    swap = 0x5F,
    iadd = 0x60,
    ladd = 0x61,
    fadd = 0x62,
    dadd = 0x63,
    isub = 0x64,
    lsub = 0x65,
    fsub = 0x66,
    dsub = 0x67,
    imul = 0x68,
    lmul = 0x69,
    fmul = 0x6A,
    dmul = 0x6B,
    idiv = 0x6C,
    ldiv_ = 0x6D,
    fdiv = 0x6E,
    ddiv = 0x6F,
    irem = 0x70,
    lrem = 0x71,
    frem = 0x72,
    drem = 0x73,
    ineg = 0x74,
    lneg = 0x75,
    fneg = 0x76,
    dneg = 0x77,
    ishl = 0x78,
    lshl = 0x79,
    ishr = 0x7A,
    lshr = 0x7B,
    iushr = 0x7C,
    lushr = 0x7D,
    iand = 0x7E,
    land = 0x7F,
    ior = 0x80,
    lor = 0x81,
    ixor = 0x82,
    lxor = 0x83,
    iinc = 0x84,//[byte(index)] [signed char(value)]
    i2l = 0x85,
    i2f = 0x86,
    i2d = 0x87,
    l2i = 0x88,
    l2f = 0x89,
    l2d = 0x8A,
    f2i = 0x8B,
    f2l = 0x8C,
    f2d = 0x8D,
    d2i = 0x8E,
    d2l = 0x8F,
    d2f = 0x90,
    i2b = 0x91,
    i2c = 0x92,
    i2s = 0x93,
    lcmp = 0x94,
    fcmpl = 0x95,
    fcmpg = 0x96,
    dcmpl = 0x97,
    dcmpg = 0x98,
    ifeq = 0x99,//[signed short(position relative to the head of this instruction)]
    ifne = 0x9A,//[signed short(position relative to the head of this instruction)]
    iflt = 0x9B,//[signed short(position relative to the head of this instruction)]
    ifge = 0x9C,//[signed short(position relative to the head of this instruction)]
    ifgt = 0x9D,//[signed short(position relative to the head of this instruction)]
    ifle = 0x9E,//[signed short(position relative to the head of this instruction)]
    if_icmpeq = 0x9F,//[signed short(position relative to the head of this instruction)]
    if_icmpne = 0xA0,//[signed short(position relative to the head of this instruction)]
    if_icmplt = 0xA1,//[signed short(position relative to the head of this instruction)]
    if_icmpge = 0xA2,//[signed short(position relative to the head of this instruction)]
    if_icmpgt = 0xA3,//[signed short(position relative to the head of this instruction)]
    if_icmple = 0xA4,//[signed short(position relative to the head of this instruction)]
    if_acmpeq = 0xA5,//[signed short(position relative to the head of this instruction)]
    if_acmpne = 0xA6,//[signed short(position relative to the head of this instruction)]
    goto_ = 0xA7,//[signed short(position relative to the head of this instruction)]
    jsr = 0xA8,
    ret = 0xA9,
    tableswitch = 0xAA,
    lookupswitch = 0xAB,
    ireturn = 0xAC,
    lreturn = 0xAD,
    freturn = 0xAE,
    dreturn = 0xAF,
    areturn = 0xB0,
    return_ = 0xB1,
    getstatic = 0xB2,//[word(index to a field)]
    putstatic = 0xB3,//[word(index to a field)]
    getfield = 0xB4,//[word(index to a field)]
    putfield = 0xB5,//[word(index to a field)]
    invokevirtual = 0xB6,//invokevirtual (WORD)[method index] __thiscall调用约定 CallObjectMethod
    invokespecial = 0xB7,//invokespecial (WORD)[method index] __thiscall调用约定 <init>
    invokestatic = 0xB8,//invokestatic (WORD)[method index] __stdcall调用约定 static
    invokeinterface = 0xB9,//invokeinterface (WORD)[method index] __thiscall调用约定 CallObjectMethod
    new_ = 0xBB,//[word(index to a class)] 如果要初始化该实例，则push入引用再以返回void的方法用invokespecial调用构造器
    newarray = 0xBC,
    anewarray = 0xBD,
    arraylength = 0xBE,
    athrow = 0xBF,
    checkcast = 0xC0,
    instanceof = 0xC1,
    monitorenter = 0xC2,
    monitorexit = 0xC3,
    wide = 0xC4,
    multianewarray = 0xC5,
    ifnull = 0xC6,
    ifnonnull = 0xC7,
    goto_w = 0xC8,//[signed int(position relative to the head of this instruction)]
    jsr_w = 0xC9,
};

#define CLASS_FILE_MAGIC 0xCAFEBABE

#define CLASS_CONSTANT_UTF8 1
#define CLASS_CONSTANT_INTEGER 3
#define CLASS_CONSTANT_FLOAT 4
#define CLASS_CONSTANT_LONG 5
#define CLASS_CONSTANT_DOUBLE 6
#define CLASS_CONSTANT_CLASS_REF 7
#define CLASS_CONSTANT_STRING 8
#define CLASS_CONSTANT_FIELD_REF 9
#define CLASS_CONSTANT_METHOD_REF 10
#define CLASS_CONSTANT_INTERFACE_METHOD_REF 11
#define CLASS_CONSTANT_NAME_AND_TYPE 12
#define CLASS_CONSTANT_METHOD_HANDLE 15
#define CLASS_CONSTANT_METHOD_TYPE 16
#define CLASS_CONSTANT_INVOKE_DYNAMIC 18

#define CLASS_ACC_PUBLIC 0x0001
#define CLASS_ACC_FINAL 0x0010
#define CLASS_ACC_SUPER 0x0020
#define CLASS_ACC_INTERFACE 0x0200
#define CLASS_ACC_ABSTRACT 0x0400
#define CLASS_ACC_SYNTHETIC 0x1000
#define CLASS_ACC_ANNOTATION 0x2000
#define CLASS_ACC_ENUM 0x4000

#define FIELD_ACC_PUBLIC 0x0001
#define FIELD_ACC_PRIVATE 0x0002
#define FIELD_ACC_PROTECTED 0x0004
#define FIELD_ACC_STATIC 0x0008
#define FIELD_ACC_FINAL 0x0010
#define FIELD_ACC_VOLATILE 0x0040
#define FIELD_ACC_TRANSIENT 0x0080
#define FIELD_ACC_SYNTHETIC 0x0100
#define FIELD_ACC_ENUM 0x0400

#define METHOD_ACC_PUBLIC 0x0001
#define METHOD_ACC_PRIVATE 0x0002
#define METHOD_ACC_PROTECTED 0x0004
#define METHOD_ACC_STATIC 0x0008
#define METHOD_ACC_FINAL 0x0010
#define METHOD_ACC_SYNCHRONIZED 0x0020
#define METHOD_ACC_BRIDGE 0x0040
#define METHOD_ACC_VARARGS 0x0080
#define METHOD_ACC_NATIVE 0x0100
#define METHOD_ACC_ABSTRACT 0x0400
#define METHOD_ACC_STRICTFP 0x0800
#define METHOD_ACC_SYNTHETIC 0x1000

#pragma endregion

using std::vector;

// 这是一个序列化接口,用于将类对象序列化后写入文件
class Serializable {
public:
    // 纯虚函数,类似于Java的接口
    virtual void Serialize(FILE* f) const = 0;
};

// 一些方便编程的方法
class Utils {
public:
    template <class T>
    static void Write(FILE* f, T val){
        fwrite(&val, sizeof(val), 1, f);
    }

    // Class文件中高低位是颠倒的,这里是颠倒写入文件的函数(方法)
    // 为了增大IO速率,最好还是整块写入文件,但是留着以后改进吧
    template <class T>
    static void WriteSwap(FILE* f, T val){
        char* start = reinterpret_cast<char*>(&val);
        for(int i = 0; i < sizeof(val); i++){
            fwrite(start + sizeof(val) - i - 1, 1, 1, f);
        }
    }

    template <class T>
    static void WriteVector(FILE* f, const vector<T*>& vec){
        WriteSwap(f, (jcount)vec.size());
        for(size_t i = 0; i < vec.size(); i++){
            if(vec[i] != NULL){
                // 这里假设vector中对象实现了Serializable接口
                // 若没有实现则报错
                vec[i]->Serialize(f);
            }
        }
    }

    template <class T>
    static void WriteVector(FILE* f, const vector<T>& vec){
        WriteSwap(f, (jcount)vec.size());
        for(size_t i = 0; i < vec.size(); i++){
            WriteSwap(f, vec[i]);
        }
    }

    // 更常用的类型还是unsigned short
    static uint16_t SwapShort(uint16_t x){
        return (x << 8) | (x >> 8);
    }

    static uint32_t SwapInt(uint32_t x){
        return (SwapShort(x << 16) << 16) | SwapShort((x & 0xFFFF0000) >> 16);
    }

    template <class T>
    static void Free(vector<T*>& vec){
        for(size_t i = 0; i < vec.size(); i++){
            if(vec[i] != NULL){
                delete vec[i];
            }
        }
    }
};

// Java中一个常量的结构
// 字节数
//   1    常量类型
//   x    其他
class JavaConstant : Serializable {
private:
    // 常量类型
    jbyte Type;
    // 这里用一个void指针指向的内存块存储常量信息
    void* Data;
    uint32_t Length;

    // 私有构造器内部使用,因为构造对象需要经过多种不同类型常量的处理,如填充数据内存块
    JavaConstant(jbyte type) : Type(type) {}

public:

    ~JavaConstant(){
        delete[] (char*)Data;
    }

    virtual void Serialize(FILE* f) const override {
        Utils::Write(f, Type);
        fwrite(Data, Length, 1, f);
    }

    // 这里使用静态全局方法构造
    // 这里先简单点,写几个基本的
    static JavaConstant* String(const char* s){
        JavaConstant* res = new JavaConstant(CLASS_CONSTANT_UTF8);
        uint32_t len = strlen(s);
        res->Data = (void*)new char[len + 2];
        res->Length = len + 2;
        *(uint16_t*)res->Data = (uint16_t)Utils::SwapShort(len);
        memcpy((uint16_t*)res->Data + 1, s, len);
        return res;
    }

    static JavaConstant* ClassRef(jindex index){
        JavaConstant* res = new JavaConstant(CLASS_CONSTANT_CLASS_REF);
        res->Data = (void*)new jindex(Utils::SwapShort(index));
        res->Length = sizeof(jindex);
        return res;
    }
};


// 这里是一些Class文件中的一些属性
#pragma region Attributes

class Attribute : public Serializable {
public:
    jindex Name;
    jlength Length;

    // 这里不实现Serialize方法,因为这些只是属性所包含的基础部分,只是一个基类
    // 稍后使用类继承共享这一共用部分
};

#pragma endregion

// 这里我提供一下变量域和方法的描述结构
class FieldAttribute : Serializable {
public:
    jflag AccessFlag;
    jindex Name;
    jindex Descriptor;
    std::vector<Attribute*> AttributeTable;

    FieldAttribute(){}

    ~FieldAttribute(){
        Utils::Free(AttributeTable);
    }

    virtual void Serialize(FILE* f) const override {
        debug_printf(("FieldAttribute::Serialize\n"));
        Utils::WriteSwap(f, AccessFlag);
        Utils::WriteSwap(f, Name);
        Utils::WriteSwap(f, Descriptor);
        Utils::WriteVector(f, AttributeTable);
    }
};

class MethodAttribute : Serializable {
public:
    jflag AccessFlag;
    jindex Name;
    jindex Descriptor;
    std::vector<Attribute*> AttributeTable;

    MethodAttribute(){}

    ~MethodAttribute(){
        Utils::Free(AttributeTable);
    }

    virtual void Serialize(FILE* f) const override {
        debug_printf(("MethodAttribute::Serialize\n"));
        Utils::WriteSwap(f, AccessFlag);
        Utils::WriteSwap(f, Name);
        Utils::WriteSwap(f, Descriptor);
        Utils::WriteVector(f, AttributeTable);
    }
};

// 这里构造一个类,用于代表序列化出一个Class文件所有所需信息
class JavaClass : Serializable {
private:
public:
    // 这里先介绍一下Class文件的结构
    // 字节数
    //   4    魔法值
    jmagic Magic;
    //   2    次版本号
    jversion MinorVersion;
    //   2    主版本号
    jversion MajorVersion;
    //   2    常量个数+1 我也不清楚为什么会这样设计
    //   x    常量表
    // 重要提示：double和long类型Java常量算两个
    // 其实Java中常量最好去重,我打算用HashMap,但是现在懒得写
    vector<JavaConstant*> ConstantTable;
    //   2    类访问标记
    jflag AccessFlag;
    //   2    当前类
    jindex ThisClass;
    //   2    基类 默认java/lang/Object
    jindex SuperClass;
    //   2    接口数量
    //   x    接口
    vector<jindex> Interfaces;
    //   2    变量域数量
    //   x    变量域
    vector<FieldAttribute*> FieldAttributes;
    //   2    方法数量
    //   x    方法
    vector<MethodAttribute*> MethodAttributes;
    //   2    全局属性数量
    //   x    全局属性
    vector<Attribute*> GlobalAttributes;

    JavaClass(){
        Magic = CLASS_FILE_MAGIC;
        // 我的Java编译出来就是这样的版本
        MinorVersion = 0;
        MajorVersion = 0x34;
    }

    ~JavaClass(){
        for(jcount i = 0; i < ConstantTable.size(); i++){
            if(ConstantTable[i] != NULL){
                delete ConstantTable[i];
            }
        }
    }

    virtual void Serialize(FILE* f) const override {
        Utils::WriteSwap(f, Magic);
        Utils::WriteSwap(f, MinorVersion);
        Utils::WriteSwap(f, MajorVersion);
        Utils::WriteSwap(f, (jcount)(ConstantTable.size() + 1));
        for(jcount i = 0; i < ConstantTable.size(); i++){
            if(ConstantTable[i] != NULL){
                ConstantTable[i]->Serialize(f);
            }
        }
        Utils::WriteSwap(f, AccessFlag);
        Utils::WriteSwap(f, ThisClass);
        Utils::WriteSwap(f, SuperClass);
        Utils::WriteVector(f, Interfaces);
        Utils::WriteVector(f, FieldAttributes);
        Utils::WriteVector(f, MethodAttributes);
        Utils::WriteVector(f, GlobalAttributes);
    }

    jindex PutString(const char* s){
        JavaConstant* constant = JavaConstant::String(s);
        ConstantTable.push_back(constant);
        debug_printf(("JavaClass::PutString %d %s\n", ConstantTable.size(), s));
        return ConstantTable.size();
    }

    jindex PutClassRef(jindex index){
        JavaConstant* constant = JavaConstant::ClassRef(index);
        ConstantTable.push_back(constant);
        debug_printf(("JavaClass::PutClassRef %d %d\n", ConstantTable.size(), index));
        return ConstantTable.size();
    }

    jindex PutClassRef(const char* s){
        // Class常量从1开始索引,vector从0开始,push_back后正好对应String的索引
        return PutClassRef(PutString(s));
    }
};

int main(){
    // 第一次测试准备
    // 我现在是使用自制类分析工具测试
    FILE* f = fopen("Main.class", "wb");
    JavaClass* clazz = new JavaClass();
    clazz->AccessFlag = CLASS_ACC_SUPER | CLASS_ACC_PUBLIC;// 存在基类(java/lang/Object)
    clazz->ThisClass = clazz->PutClassRef("test/Main");
    clazz->SuperClass = clazz->PutClassRef("java/lang/Object");
    clazz->Serialize(f);
    delete clazz;
    fclose(f);
    return 0;
}
