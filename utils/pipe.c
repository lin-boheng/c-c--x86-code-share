#include <stdio.h>
#include <windows.h>

int main(){
    STARTUPINFO info;
    SECURITY_ATTRIBUTES attr;
    PROCESS_INFORMATION ps;
    HANDLE hStdOut;
    HANDLE hOut, hCliIn, hPipe;
    TCHAR szLine[MAX_PATH];
    hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
    attr.nLength = sizeof(SECURITY_ATTRIBUTES);
    attr.bInheritHandle = TRUE;
    attr.lpSecurityDescriptor = NULL;
    CreatePipe(&hCliIn, &hOut, &attr, 0);
    hPipe = CreateNamedPipe(
        TEXT("\\\\.\\pipe\\RBTreeIn"),
        PIPE_ACCESS_OUTBOUND,
        PIPE_TYPE_BYTE,
        PIPE_UNLIMITED_INSTANCES,
        MAX_PATH,
        MAX_PATH,
        INFINITE,
        &attr
    );
    printf("%p\n", hPipe);
    RtlZeroMemory(&info, sizeof(STARTUPINFO));
    info.cb = sizeof(STARTUPINFO);
    info.hStdInput = hCliIn;
    info.hStdOutput = hStdOut;
    info.hStdError = hStdOut;
    wsprintf(szLine, TEXT("RBTree.exe %p"), hCliIn);
    CreateProcess(NULL, szLine, NULL, NULL, TRUE, 0, NULL, NULL, &info, &ps);
    const TCHAR* szIns[] = {
        TEXT("insert"),
        TEXT("query"),
        TEXT("delete"),
    };
    while(1){
        LARGE_INTEGER size;
        DWORD len;
        Sleep(1000);
        switch (rand() % 3) {
        case 0:
            len = wsprintf(szLine, TEXT("%s %d %d\n"), szIns[0], rand(), rand());
            WriteFile(hStdOut, szLine, len, NULL, NULL);
            WriteFile(hPipe, szLine, len, NULL, NULL);
            break;
        case 1:
            len = wsprintf(szLine, TEXT("%s %d\n"), szIns[1], rand());
            WriteFile(hStdOut, szLine, len, NULL, NULL);
            WriteFile(hPipe, szLine, len, NULL, NULL);
            break;
        case 2:
            len = wsprintf(szLine, TEXT("%s %d\n"), szIns[2], rand());
            WriteFile(hStdOut, szLine, len, NULL, NULL);
            WriteFile(hPipe, szLine, len, NULL, NULL);
            break;
        }
    }
}