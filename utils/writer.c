#include <stdio.h>

int main(){
    freopen("a.S", "w", stdout);
    printf("global _ResASCIICharset\n");
    printf("_ResASCIICharset:\n");
    for(int i=0;i<0x20;i++)
        printf("    dd 0\n");
    for(int i=0x20;i<=0x7E;i++)
        printf("    dd _ResASCIICharset_%02X\n", i);
    for(int i=0x20;i<=0x7E;i++){
        printf("global _ResASCIICharset_%02X\n", i);
        printf("_ResASCIICharset_%02X:\n", i);
        printf("    incbin \"res/ascii/%02X.bmp\"\n", i);
    }
}