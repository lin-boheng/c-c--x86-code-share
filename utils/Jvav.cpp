#include <iostream>
#include <windows.h>

typedef unsigned long long INDEX;
typedef void *REF;
typedef void(*Callback)();

typedef char SBYTE;
typedef short SWORD;
typedef int SDWORD;
typedef long long SQWORD;
typedef unsigned char BYTE;
//typedef unsigned short WORD;
//typedef unsigned int DWORD;
typedef unsigned long long QWORD;
typedef float REAL4;
typedef double REAL8;

//typedef char CHAR;

#define STACK_SIZE 0x10000
#define EVAL_SIZE 0x10000

static BYTE ss[STACK_SIZE];
static INDEX sp = 0ull;

static BYTE es[EVAL_SIZE];
static INDEX ep = 0ull;

static Callback ins[0x100];
static Callback sys[0x100];

static BYTE cons[0x10000];
static REF tempss[0x10000];
static INDEX tempsp = 0ull;
static REF temp = new QWORD[0x100];

#define PUSH(Type,Value)\
    *(Type*)(ss + sp) = Value;\
    sp += sizeof(Type);
#define POP(Type,Var)\
    sp -= sizeof(Type);\
    Var = *(Type*)(ss + sp);

#define NEXTB (es[ep++])
#define NEXTSB ((char)es[ep++])

#define NEXTW (*(WORD*)(es + ep))
#define NEXTSW (*(SWORD*)(es + ep))
#define NEXTDW (*(DWORD*)(es + ep))
#define NEXTSDW (*(SDWORD*)(es + ep))
#define NEXTQW (*(QWORD*)(es + ep))
#define NEXTSQW (*(SQWORD*)(es + ep))

#define TEMP(Type,Index)\
    (*(Type*)((QWORD*)temp + Index))

enum{
    nop = 0x00,
    aconst_null = 0x01,
    iconst_m1 = 0x02,
    iconst_0 = 0x03,
    iconst_1 = 0x04,
    iconst_2 = 0x05,
    iconst_3 = 0x06,
    iconst_4 = 0x07,
    iconst_5 = 0x08,
    lconst_0 = 0x09,
    lconst_1 = 0x0A,
    fconst_0 = 0x0B,
    fconst_1 = 0x0C,
    fconst_2 = 0x0D,
    dconst_0 = 0x0E,
    dconst_1 = 0x0F,
    bipush = 0x10,
    sipush = 0x11,
    ldc = 0x12,
    ldc_w = 0x13,
    ldc2_w = 0x14,
    aload = 0x19,
    aload_0 = 0x2A,
    aload_1 = 0x2B,
    aload_2 = 0x2C,
    aload_3 = 0x2D,
    iload = 0x15,
    iload_0 = 0x1A,
    iload_1 = 0x1B,
    iload_2 = 0x1C,
    iload_3 = 0x1D,
    lload = 0x16,
    lload_0 = 0x1E,
    lload_1 = 0x1F,
    lload_2 = 0x20,
    lload_3 = 0x21,
    fload = 0x17,
    fload_0 = 0x22,
    fload_1 = 0x23,
    fload_2 = 0x24,
    fload_3 = 0x25,
    dload = 0x18,
    dload_0 = 0x26,
    dload_1 = 0x27,
    dload_2 = 0x28,
    dload_3 = 0x29,
    aaload = 0x32,
    iaload = 0x2E,
    laload = 0x2F,
    faload = 0x30,
    daload = 0x31,
    baload = 0x33,
    caload = 0x34,
    saload = 0x35,
    astore = 0x3A,
    astore_0 = 0x4B,
    astore_1 = 0x4C,
    astore_2 = 0x4D,
    astore_3 = 0x4E,
    istore = 0x36,
    istore_0 = 0x3B,
    istore_1 = 0x3C,
    istore_2 = 0x3D,
    istore_3 = 0x3E,
    lstore = 0x37,
    lstore_0 = 0x3F,
    lstore_1 = 0x40,
    lstore_2 = 0x41,
    lstore_3 = 0x42,
    fstore = 0x38,
    fstore_0 = 0x43,
    fstore_1 = 0x44,
    fstore_2 = 0x45,
    fstore_3 = 0x46,
    dstore = 0x39,
    dstore_0 = 0x47,
    dstore_1 = 0x48,
    dstore_2 = 0x49,
    dstore_3 = 0x4A,
    aastore = 0x53,
    iastore = 0x4F,
    lastore = 0x50,
    fastore = 0x51,
    dastore = 0x52,
    bastore = 0x54,
    castore = 0x55,
    sastore = 0x56,
    pop = 0x57,
    pop2 = 0x58,
    dup = 0x59,
    dup_x1 = 0x5A,
    dup_x2 = 0x5B,
    dup2 = 0x5C,
    dup2_x1 = 0x5D,
    dup2_x2 = 0x5E,
    swap = 0x5F,
    iadd = 0x60,
    ladd = 0x61,
    fadd = 0x62,
    dadd = 0x63,
    isub = 0x64,
    lsub = 0x65,
    fsub = 0x66,
    dsub = 0x67,
    imul = 0x68,
    lmul = 0x69,
    fmul = 0x6A,
    dmul = 0x6B,
    idiv = 0x6C,
    ldiv_ = 0x6D,
    fdiv = 0x6E,
    ddiv = 0x6F,
    irem = 0x70,
    lrem = 0x71,
    frem = 0x72,
    drem = 0x73,
    ineg = 0x74,
    lneg = 0x75,
    fneg = 0x76,
    dneg = 0x77,
    ishl = 0x78,
    lshl = 0x79,
    ishr = 0x7A,
    lshr = 0x7B,
    iushr = 0x7C,
    lushr = 0x7D,
    iand = 0x7E,
    land = 0x7F,
    ior = 0x80,
    lor = 0x81,
    ixor = 0x82,
    lxor = 0x83,
    iinc = 0x84,
    i2l = 0x85,
    i2f = 0x86,
    i2d = 0x87,
    l2i = 0x88,
    l2f = 0x89,
    l2d = 0x8A,
    f2i = 0x8B,
    f2l = 0x8C,
    f2d = 0x8D,
    d2i = 0x8E,
    d2l = 0x8F,
    d2f = 0x90,
    i2b = 0x91,
    i2c = 0x92,
    i2s = 0x93,
    lcmp = 0x94,
    fcmpl = 0x95,
    fcmpg = 0x96,
    dcmpl = 0x97,
    dcmpg = 0x98,
    ifeq = 0x99,
    ifne = 0x9A,
    iflt = 0x9B,
    ifge = 0x9C,
    ifgt = 0x9D,
    ifle = 0x9E,
    if_icmpeq = 0x9F,
    if_icmpne = 0xA0,
    if_icmplt = 0xA1,
    if_icmpge = 0xA2,
    if_icmpgt = 0xA3,
    if_icmple = 0xA4,
    if_acmpeq = 0xA5,
    if_acmpne = 0xA6,
    goto_ = 0xA7,
    jsr = 0xA8,
    ret = 0xA9,
    tableswitch = 0xAA,
    lookupswitch = 0xAB,
    ireturn = 0xAC,
    lreturn = 0xAD,
    freturn = 0xAE,
    dreturn = 0xAF,
    areturn = 0xB0,
    return_ = 0xB1,
    getstatic = 0xB2,
    putstatic = 0xB3,
    getfield = 0xB4,
    putfield = 0xB5,
    invokevirtual = 0xB6,
    invokespecial = 0xB7,
    invokestatic = 0xB8,
    invokeinterface = 0xB9,
    new_ = 0xBB,
    newarray = 0xBC,
    anewarray = 0xBD,
    arraylength = 0xBE,
    athrow = 0xBF,
    checkcast = 0xC0,
    instanceof = 0xC1,
    monitorenter = 0xC2,
    monitorexit = 0xC3,
    wide = 0xC4,
    multianewarray = 0xC5,
    ifnull = 0xC6,
    ifnonnull = 0xC7,
    goto_w = 0xC8,
    jsr_w = 0xC9,
    binput = 0xD0,
    boutput = 0xD1,
    iinput = 0xD2,
    ioutput = 0xD3,
    linput = 0xD4,
    loutput = 0xD5,
    finput = 0xD6,
    foutput = 0xD7,
    dinput = 0xD8,
    doutput = 0xD9,
    sys_ = 0xE0,
    call = 0xE1,
    exit_ = 0xFF
};

void init(){

ins[0x00] = [](){};

ins[0x01] = [](){
    PUSH(REF, nullptr);
};
ins[0x02] = [](){
    PUSH(SDWORD, -1);
};
ins[0x03] = [](){
    PUSH(SDWORD, 0);
};
ins[0x04] = [](){
    PUSH(SDWORD, 1);
};
ins[0x05] = [](){
    PUSH(SDWORD, 2);
};
ins[0x06] = [](){
    PUSH(SDWORD, 3);
};
ins[0x07] = [](){
    PUSH(SDWORD, 4);
};
ins[0x08] = [](){
    PUSH(SDWORD, 5);
};
ins[0x09] = [](){
    PUSH(SQWORD, 0LL);
};
ins[0x0A] = [](){
    PUSH(SQWORD, 1LL);
};
ins[0x0B] = [](){
    PUSH(REAL4, 0.0F);
};
ins[0x0C] = [](){
    PUSH(REAL4, 1.0F);
};
ins[0x0D] = [](){
    PUSH(REAL4, 2.0F);
};
ins[0x0E] = [](){
    PUSH(REAL8, 0.0);
};
ins[0x0F] = [](){
    PUSH(REAL8, 1.0);
};
ins[0x10] = [](){
    PUSH(SDWORD, NEXTSB);
};
ins[0x11] = [](){
    PUSH(SDWORD, NEXTSW);
    ep += 2;
};
ins[0x12] = [](){
    PUSH(SDWORD, cons[NEXTB]);
};
ins[0x13] = [](){
    PUSH(SDWORD, cons[NEXTW]);
    ep += 2;
};
ins[0x14] = [](){
    PUSH(SQWORD, cons[NEXTW]);
    ep += 2;
};

ins[0x19] = [](){
    PUSH(REF, TEMP(REF, NEXTB));
};
ins[0x2A] = [](){
    PUSH(REF, TEMP(REF, 0));
};
ins[0x2B] = [](){
    PUSH(REF, TEMP(REF, 1));
};
ins[0x2C] = [](){
    PUSH(REF, TEMP(REF, 2));
};
ins[0x2D] = [](){
    PUSH(REF, TEMP(REF, 3));
};
ins[0x15] = [](){
    PUSH(SDWORD, TEMP(SDWORD, NEXTB));
};
ins[0x1A] = [](){
    PUSH(SDWORD, TEMP(SDWORD, 0));
};
ins[0x1B] = [](){
    PUSH(SDWORD, TEMP(SDWORD, 1));
};
ins[0x1C] = [](){
    PUSH(SDWORD, TEMP(SDWORD, 2));
};
ins[0x1D] = [](){
    PUSH(SDWORD, TEMP(SDWORD, 3));
};
ins[0x16] = [](){
    PUSH(SQWORD, TEMP(SQWORD, NEXTB));
};
ins[0x1E] = [](){
    PUSH(SQWORD, TEMP(SQWORD, 0));
};
ins[0x1F] = [](){
    PUSH(SQWORD, TEMP(SQWORD, 1));
};
ins[0x20] = [](){
    PUSH(SQWORD, TEMP(SQWORD, 2));
};
ins[0x21] = [](){
    PUSH(SQWORD, TEMP(SQWORD, 3));
};
ins[0x17] = [](){
    PUSH(REAL4, TEMP(REAL4, NEXTB));
};
ins[0x22] = [](){
    PUSH(REAL4, TEMP(REAL4, 0));
};
ins[0x23] = [](){
    PUSH(REAL4, TEMP(REAL4, 1));
};
ins[0x24] = [](){
    PUSH(REAL4, TEMP(REAL4, 2));
};
ins[0x25] = [](){
    PUSH(REAL4, TEMP(REAL4, 3));
};
ins[0x18] = [](){
    PUSH(REAL8, TEMP(REAL8, NEXTB));
};
ins[0x26] = [](){
    PUSH(REAL8, TEMP(REAL8, 0));
};
ins[0x27] = [](){
    PUSH(REAL8, TEMP(REAL8, 1));
};
ins[0x28] = [](){
    PUSH(REAL8, TEMP(REAL8, 2));
};
ins[0x29] = [](){
    PUSH(REAL8, TEMP(REAL8, 3));
};
ins[0x32] = [](){
    register SDWORD i;
    register REF ref;
    POP(SDWORD, i);
    POP(REF, ref);
    PUSH(REF, *((REF*)ref + i));
};
ins[0x2E] = [](){
    register SDWORD i;
    register REF ref;
    POP(SDWORD, i);
    POP(REF, ref);
    PUSH(SDWORD, *((SDWORD*)ref + i));
};
ins[0x2F] = [](){
    register SDWORD i;
    register REF ref;
    POP(SDWORD, i);
    POP(REF, ref);
    PUSH(SQWORD, *((SQWORD*)ref + i));
};
ins[0x30] = [](){
    register SDWORD i;
    register REF ref;
    POP(SDWORD, i);
    POP(REF, ref);
    PUSH(REAL4, *((REAL4*)ref + i));
};
ins[0x31] = [](){
    register SDWORD i;
    register REF ref;
    POP(SDWORD, i);
    POP(REF, ref);
    PUSH(REAL8, *((REAL8*)ref + i));
};
ins[0x33] = [](){
    register SDWORD i;
    register REF ref;
    POP(SDWORD, i);
    POP(REF, ref);
    PUSH(SDWORD, *((SBYTE*)ref + i));
};
ins[0x34] = [](){
    register SDWORD i;
    register REF ref;
    POP(SDWORD, i);
    POP(REF, ref);
    PUSH(SDWORD, *((CHAR*)ref + i));
};
ins[0x35] = [](){
    register SDWORD i;
    register REF ref;
    POP(SDWORD, i);
    POP(REF, ref);
    PUSH(SWORD, *((SWORD*)ref + i));
};

ins[0x3A] = [](){
    POP(REF, TEMP(REF, NEXTB));
};
ins[0x4B] = [](){
    POP(REF, TEMP(REF, 0));
};
ins[0x4C] = [](){
    POP(REF, TEMP(REF, 1));
};
ins[0x4D] = [](){
    POP(REF, TEMP(REF, 2));
};
ins[0x4E] = [](){
    POP(REF, TEMP(REF, 3));
};
ins[0x36] = [](){
    POP(SDWORD, TEMP(SDWORD, NEXTB));
};
ins[0x3B] = [](){
    POP(SDWORD, TEMP(SDWORD, 0));
};
ins[0x3C] = [](){
    POP(SDWORD, TEMP(SDWORD, 1));
};
ins[0x3D] = [](){
    POP(SDWORD, TEMP(SDWORD, 2));
};
ins[0x3E] = [](){
    POP(SDWORD, TEMP(SDWORD, 3));
};
ins[0x37] = [](){
    POP(SQWORD, TEMP(SQWORD, NEXTB));
};
ins[0x3F] = [](){
    POP(SQWORD, TEMP(SQWORD, 0));
};
ins[0x40] = [](){
    POP(SQWORD, TEMP(SQWORD, 1));
};
ins[0x41] = [](){
    POP(SQWORD, TEMP(SQWORD, 2));
};
ins[0x42] = [](){
    POP(SQWORD, TEMP(SQWORD, 3));
};
ins[0x38] = [](){
    POP(REAL4, TEMP(REAL4, NEXTB));
};
ins[0x43] = [](){
    POP(REAL4, TEMP(REAL4, 0));
};
ins[0x44] = [](){
    POP(REAL4, TEMP(REAL4, 1));
};
ins[0x45] = [](){
    POP(REAL4, TEMP(REAL4, 2));
};
ins[0x46] = [](){
    POP(REAL4, TEMP(REAL4, 3));
};
ins[0x39] = [](){
    POP(REAL8, TEMP(REAL8, NEXTB));
};
ins[0x47] = [](){
    POP(REAL8, TEMP(REAL8, 0));
};
ins[0x48] = [](){
    POP(REAL8, TEMP(REAL8, 1));
};
ins[0x49] = [](){
    POP(REAL8, TEMP(REAL8, 2));
};
ins[0x4A] = [](){
    POP(REAL8, TEMP(REAL8, 3));
};
ins[0x53] = [](){
    register REF ref;
    register SDWORD i;
    register REF val;
    POP(REF, val);
    POP(SDWORD, i);
    POP(REF, ref);
    *((REF*)ref + i) = val;
};
ins[0x4F] = [](){
    register REF ref;
    register SDWORD i;
    register SDWORD val;
    POP(SDWORD, val);
    POP(SDWORD, i);
    POP(REF, ref);
    *((SDWORD*)ref + i) = val;
};
ins[0x50] = [](){
    register REF ref;
    register SDWORD i;
    register SQWORD val;
    POP(SQWORD, val);
    POP(SDWORD, i);
    POP(REF, ref);
    *((SQWORD*)ref + i) = val;
};
ins[0x51] = [](){
    register REF ref;
    register SDWORD i;
    register REAL4 val;
    POP(REAL4, val);
    POP(SDWORD, i);
    POP(REF, ref);
    *((REAL4*)ref + i) = val;
};
ins[0x52] = [](){
    register REF ref;
    register SDWORD i;
    register REAL8 val;
    POP(REAL8, val);
    POP(SDWORD, i);
    POP(REF, ref);
    *((REAL8*)ref + i) = val;
};
ins[0x54] = [](){
    register REF ref;
    register SDWORD i;
    register SBYTE val;
    POP(SDWORD, val);
    POP(SDWORD, i);
    POP(REF, ref);
    *((SBYTE*)ref + i) = val;
};
ins[0x55] = [](){
    register REF ref;
    register SDWORD i;
    register CHAR val;
    POP(SDWORD, val);
    POP(SDWORD, i);
    POP(REF, ref);
    *((CHAR*)ref + i) = val;
};
ins[0x56] = [](){
    register REF ref;
    register SDWORD i;
    register SWORD val;
    POP(SDWORD, val);
    POP(SDWORD, i);
    POP(REF, ref);
    *((SWORD*)ref + i) = val;
};

ins[0x57] = [](){
    sp -= sizeof(SDWORD);
};
ins[0x58] = [](){
    sp -= sizeof(SQWORD);
};
ins[0x59] = [](){
    register DWORD i;
    POP(DWORD, i);
    PUSH(DWORD, i);
    PUSH(DWORD, i);
};
ins[0x5A] = [](){
    register DWORD i1, i2;
    POP(DWORD, i2);
    POP(DWORD, i1);
    PUSH(DWORD, i2);
    PUSH(DWORD, i1);
    PUSH(DWORD, i2);
};
ins[0x5B] = [](){
    register DWORD i1, i2, i3;
    POP(DWORD, i3);
    POP(DWORD, i2);
    POP(DWORD, i1);
    PUSH(DWORD, i3);
    PUSH(DWORD, i1);
    PUSH(DWORD, i2);
    PUSH(DWORD, i3);
};
ins[0x5C] = [](){
    register QWORD l;
    POP(QWORD, l);
    PUSH(QWORD, l);
    PUSH(QWORD, l);
};
ins[0x5D] = [](){
    register QWORD l;
    register DWORD i;
    POP(QWORD, l);
    POP(DWORD, i);
    PUSH(QWORD, l);
    PUSH(DWORD, i);
    PUSH(QWORD, l);
};
ins[0x5E] = [](){
    register QWORD l1, l2;
    POP(QWORD, l2);
    POP(QWORD, l1);
    PUSH(QWORD, l2);
    PUSH(QWORD, l1);
    PUSH(QWORD, l2);
};
ins[0x5F] = [](){
    register DWORD i1, i2;
    POP(DWORD, i2);
    POP(DWORD, i1);
    PUSH(DWORD, i2);
    PUSH(DWORD, i1);
};

ins[0x60] = [](){
    register SDWORD i1, i2;
    POP(SDWORD, i2);
    POP(SDWORD, i1);
    PUSH(SDWORD, i1 + i2);
};
ins[0x64] = [](){
    register SDWORD i1, i2;
    POP(SDWORD, i2);
    POP(SDWORD, i1);
    PUSH(SDWORD, i1 - i2);
};
ins[0x68] = [](){
    register SDWORD i1, i2;
    POP(SDWORD, i2);
    POP(SDWORD, i1);
    PUSH(SDWORD, i1 * i2);
};
ins[0x6C] = [](){
    register SDWORD i1, i2;
    POP(SDWORD, i2);
    POP(SDWORD, i1);
    PUSH(SDWORD, i1 / i2);
};
ins[0x70] = [](){
    register SDWORD i1, i2;
    POP(SDWORD, i2);
    POP(SDWORD, i1);
    PUSH(SDWORD, i1 % i2);
};
ins[0x74] = [](){
    register SDWORD i;
    POP(SDWORD, i);
    PUSH(SDWORD, -i);
};
ins[0x61] = [](){
    register SQWORD l1, l2;
    POP(SQWORD, l2);
    POP(SQWORD, l1);
    PUSH(SQWORD, l1 + l2);
};
ins[0x65] = [](){
    register SQWORD l1, l2;
    POP(SQWORD, l2);
    POP(SQWORD, l1);
    PUSH(SQWORD, l1 - l2);
};
ins[0x69] = [](){
    register SQWORD l1, l2;
    POP(SQWORD, l2);
    POP(SQWORD, l1);
    PUSH(SQWORD, l1 * l2);
};
ins[0x6D] = [](){
    register SQWORD l1, l2;
    POP(SQWORD, l2);
    POP(SQWORD, l1);
    PUSH(SQWORD, l1 / l2);
};
ins[0x71] = [](){
    register SQWORD l1, l2;
    POP(SQWORD, l2);
    POP(SQWORD, l1);
    PUSH(SQWORD, l1 % l2);
};
ins[0x75] = [](){
    register SQWORD l;
    POP(SQWORD, l);
    PUSH(SQWORD, -l);
};
ins[0x62] = [](){
    register REAL4 f1, f2;
    POP(REAL4, f2);
    POP(REAL4, f1);
    PUSH(REAL4, f1 + f2);
};
ins[0x66] = [](){
    register REAL4 f1, f2;
    POP(REAL4, f2);
    POP(REAL4, f1);
    PUSH(REAL4, f1 - f2);
};
ins[0x6A] = [](){
    register REAL4 f1, f2;
    POP(REAL4, f2);
    POP(REAL4, f1);
    PUSH(REAL4, f1 * f2);
};
ins[0x6E] = [](){
    register REAL4 f1, f2;
    POP(REAL4, f2);
    POP(REAL4, f1);
    PUSH(REAL4, f1 / f2);
};
ins[0x75] = [](){
    register REAL4 f;
    POP(REAL4, f);
    PUSH(REAL4, -f);
};
ins[0x63] = [](){
    register REAL8 d1, d2;
    POP(REAL8, d2);
    POP(REAL8, d1);
    PUSH(REAL8, d1 + d2);
};
ins[0x67] = [](){
    register REAL8 d1, d2;
    POP(REAL8, d2);
    POP(REAL8, d1);
    PUSH(REAL8, d1 - d2);
};
ins[0x6B] = [](){
    register REAL8 d1, d2;
    POP(REAL8, d2);
    POP(REAL8, d1);
    PUSH(REAL8, d1 * d2);
};
ins[0x6F] = [](){
    register REAL8 d1, d2;
    POP(REAL8, d2);
    POP(REAL8, d1);
    PUSH(REAL8, d1 / d2);
};
ins[0x77] = [](){
    register REAL8 d;
    POP(REAL8, d);
    PUSH(REAL8, -d);
};

ins[0x7E] = [](){
    DWORD i1, i2;
    POP(DWORD, i2);
    POP(DWORD, i1);
    PUSH(DWORD, i1 & i2);
};
ins[0x7F] = [](){
    QWORD l1, l2;
    POP(QWORD, l2);
    POP(QWORD, l1);
    PUSH(QWORD, l1 & l2);
};
ins[0x80] = [](){
    DWORD i1, i2;
    POP(DWORD, i2);
    POP(DWORD, i1);
    PUSH(DWORD, i1 | i2);
};
ins[0x81] = [](){
    QWORD l1, l2;
    POP(QWORD, l2);
    POP(QWORD, l1);
    PUSH(QWORD, l1 | l2);
};
ins[0x82] = [](){
    DWORD i1, i2;
    POP(DWORD, i2);
    POP(DWORD, i1);
    PUSH(DWORD, i1 ^ i2);
};
ins[0x83] = [](){
    QWORD l1, l2;
    POP(QWORD, l2);
    POP(QWORD, l1);
    PUSH(QWORD, l1 ^ l2);
};

ins[0x84] = [](){
    TEMP(SDWORD, NEXTB) += (SBYTE)NEXTB;
};

ins[0x86] = [](){
    register SDWORD i;
    POP(SDWORD, i);
    PUSH(REAL4, (REAL4)i);
};
ins[0x85] = [](){
    register SDWORD i;
    POP(SDWORD, i);
    PUSH(SQWORD, (SQWORD)i);
};
ins[0x87] = [](){
    register SDWORD i;
    POP(SDWORD, i);
    PUSH(REAL8, (REAL8)i);
};
ins[0x8B] = [](){
    register REAL4 f;
    POP(REAL4, f);
    PUSH(SDWORD, (SDWORD)f);
};
ins[0x8C] = [](){
    register REAL4 f;
    POP(REAL4, f);
    PUSH(SQWORD, (SQWORD)f);
};
ins[0x8D] = [](){
    register REAL4 f;
    POP(REAL4, f);
    PUSH(REAL8, (REAL8)f);
};
ins[0x88] = [](){
    register SQWORD l;
    POP(SQWORD, l);
    PUSH(SDWORD, (SDWORD)l);
};
ins[0x89] = [](){
    register SQWORD l;
    POP(SQWORD, l);
    PUSH(REAL4, (REAL4)l);
};
ins[0x8A] = [](){
    register SQWORD l;
    POP(SQWORD, l);
    PUSH(REAL8, (REAL8)l);
};
ins[0x8E] = [](){
    register REAL8 d;
    POP(REAL8, d);
    PUSH(SDWORD, (SDWORD)d);
};
ins[0x90] = [](){
    register REAL8 d;
    POP(REAL8, d);
    PUSH(REAL4, (REAL4)d);
};
ins[0x8F] = [](){
    register REAL8 d;
    POP(REAL8, d);
    PUSH(SQWORD, (SQWORD)d);
};
ins[0x91] = [](){
    register SDWORD i;
    POP(SDWORD, i);
    PUSH(SDWORD, (SBYTE)i);
};
ins[0x92] = [](){
    register SDWORD i;
    POP(SDWORD, i);
    PUSH(SDWORD, (CHAR)i);
};
ins[0x93] = [](){
    register SDWORD i;
    POP(SDWORD, i);
    PUSH(SDWORD, (SWORD)i);
};

ins[0x99] = [](){
    SDWORD i;
    POP(SDWORD, i);
    if(i == 0)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0x9A] = [](){
    SDWORD i;
    POP(SDWORD, i);
    if(i != 0)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0x9B] = [](){
    SDWORD i;
    POP(SDWORD, i);
    if(i < 0)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0x9E] = [](){
    SDWORD i;
    POP(SDWORD, i);
    if(i <= 0)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0x9D] = [](){
    SDWORD i;
    POP(SDWORD, i);
    if(i > 0)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0x9C] = [](){
    SDWORD i;
    POP(SDWORD, i);
    if(i >= 0)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0x9F] = [](){
    SDWORD i1, i2;
    POP(SDWORD, i2);
    POP(SDWORD, i1);
    if(i1 == i2)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0xA0] = [](){
    SDWORD i1, i2;
    POP(SDWORD, i2);
    POP(SDWORD, i1);
    if(i1 != i2)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0xA1] = [](){
    SDWORD i1, i2;
    POP(SDWORD, i2);
    POP(SDWORD, i1);
    if(i1 < i2)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0xA4] = [](){
    SDWORD i1, i2;
    POP(SDWORD, i2);
    POP(SDWORD, i1);
    if(i1 <= i2)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0xA3] = [](){
    SDWORD i1, i2;
    POP(SDWORD, i2);
    POP(SDWORD, i1);
    if(i1 > i2)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0xA2] = [](){
    SDWORD i1, i2;
    POP(SDWORD, i2);
    POP(SDWORD, i1);
    if(i1 >= i2)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0xC6] = [](){
    REF ref;
    POP(REF, ref);
    if(ref == nullptr)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0xC7] = [](){
    REF ref;
    POP(REF, ref);
    if(ref != nullptr)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0xA5] = [](){
    REF ref1, ref2;
    POP(REF, ref2);
    POP(REF, ref1);
    if(ref1 == ref2)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};
ins[0xA6] = [](){
    REF ref1, ref2;
    POP(REF, ref2);
    POP(REF, ref1);
    if(ref1 != ref2)
        ep = NEXTW;
    else
        ep += sizeof(WORD);
};

ins[0xAC] = [](){
    register SDWORD i;
    POP(SDWORD, i);
    POP(DWORD, ep);
    PUSH(SDWORD, i);
    free(temp);
    temp = tempss[--tempsp];
};
ins[0xAD] = [](){
    register SQWORD l;
    POP(SQWORD, l);
    POP(DWORD, ep);
    PUSH(SQWORD, l);
    free(temp);
    temp = tempss[--tempsp];
};
ins[0xAE] = [](){
    register REAL4 f;
    POP(REAL4, f);
    POP(DWORD, ep);
    PUSH(REAL4, f);
    free(temp);
    temp = tempss[--tempsp];
};
ins[0xAF] = [](){
    register REAL8 d;
    POP(REAL8, d);
    POP(DWORD, ep);
    PUSH(REAL8, d);
    free(temp);
    temp = tempss[--tempsp];
};
ins[0xB0] = [](){
    register REF ref;
    POP(REF, ref);
    POP(DWORD, ep);
    PUSH(REF, ref);
    free(temp);
    temp = tempss[--tempsp];
};
ins[0xB1] = [](){
    POP(DWORD, ep);
    free(temp);
    temp = tempss[--tempsp];
};

ins[0xA7] = [](){
    ep = NEXTW;
};
ins[0xC8] = [](){
    ep = NEXTDW;
};

ins[0xD0] = [](){
    PUSH(SDWORD, getchar());
};
ins[0xD1] = [](){
    SDWORD c;
    POP(SDWORD, c);
    putchar(c);
};
ins[0xD2] = [](){
    SDWORD i;
    scanf("%d",&i);
    PUSH(SDWORD, i);
};
ins[0xD3] = [](){
    SDWORD i;
    POP(SDWORD, i);
    printf("%d",i);
};
ins[0xD4] = [](){
    SQWORD l;
    scanf("%lld",&l);
    PUSH(SQWORD, l);
};
ins[0xD5] = [](){
    SQWORD l;
    POP(SQWORD, l);
    printf("%lld",l);
};
ins[0xD6] = [](){
    REAL4 f;
    scanf("%f",&f);
    PUSH(REAL4, f);
};
ins[0xD7] = [](){
    REAL4 f;
    POP(REAL4, f);
    printf("%f",f);
};
ins[0xD8] = [](){
    REAL8 d;
    scanf("%lf",&d);
    PUSH(REAL8, d);
};
ins[0xD9] = [](){
    REAL8 d;
    POP(REAL8, d);
    printf("%lf",d);
};

ins[0xE0] = [](){
    sys[NEXTB]();
};
ins[0xE1] = [](){
    PUSH(DWORD, (DWORD)(ep + sizeof(DWORD)));
    ep = NEXTDW;
    tempss[tempsp++] = temp;
    temp = malloc(0x100 * sizeof(QWORD));
};

ins[0xFF] = [](){
    register SDWORD v;
    POP(SDWORD, v);
    exit(v);
};

sys[0x00] = [](){
    register DWORD dwMilliseconds;
    POP(DWORD, dwMilliseconds);
    Sleep(dwMilliseconds);
};
sys[0x01] = [](){
    register QWORD qwSize;
    POP(QWORD, qwSize);
    PUSH(REF, malloc(qwSize));
};
sys[0x02] = [](){
    register REF refMemory;
    POP(REF, refMemory);
    free(refMemory);
};
sys[0x03] = [](){
    register REF refCommand;
    POP(REF, refCommand);
    PUSH(SDWORD, system((const char*)refCommand));
};

}

#define word(x) (x & 0xFF),(x >> 8)

int main(){
    init();
    FILE* f = fopen("Jvav.bin", "rb");
    fread(es, sizeof(BYTE), sizeof(es), f);
    fclose(f);
    while(true)
        ins[NEXTB]();
    return 0;
}