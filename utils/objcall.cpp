#include <iostream>
using namespace std;
class Object {};
class A {
public:
    void test(){ cout << "A::test" << endl; }
    void test2(){ cout << "A::test2" << endl; }
};
union B {
    void(A::*p)();
    struct {
        void* p1;
        void* p2;
    };
    void(Object::*po)();
};
// 发现
// 1. 若父类同名函数为虚函数，则子类不用声明virtual也为虚函数
// 2. 无论编译时成员函数指针类型是什么，调用方式一致，调用效果仅与类实体实际数据有关
// 3. 最有效的强制转换还是union联合体，基于内存中实际数据的转换，类似reinterpret_cast
int main(){
    A a;
    B b1, b2;
    Object* o = (Object*)&a;
    b1.p = A::test;
    b2.p = A::test2;
    printf("%p %p\n", b1.p1, b1.p2);
    printf("%p %p\n", b2.p1, b2.p2);
    (o->*b1.po)();
    (o->*b2.po)();
}