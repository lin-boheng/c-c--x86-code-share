#include <iostream>
#include <windows.h>
using namespace std;

typedef struct _PARTITION_ENTRY //分区表结构
{
    UCHAR active;        //状态（是否被激活）   重要
    UCHAR StartHead;     //分区起始磁头号
    USHORT StartSecCyli; //与63相位与得出的是开始扇区，把它右移6位就是开始柱面
    UCHAR PartitionType; // 分区类型   重要
    UCHAR EndHead;       //分区结束磁头号
    USHORT EndSecCyli;   //与63相位与得出的就是结束扇区，把它右移6位就是结束柱面
    ULONG StartLBA;      // 扇区起始逻辑地址（相对扇区号）   重要
    ULONG TotalSector;   // 分区大小      重要
} PARTITION_ENTRY, *PPARTITION_ENTRY;
//磁头8bit 柱面10bit 扇区6bit 扇区512bytes
//8G
typedef struct _MBR_SECTOR
{
    UCHAR BootCode[440];          //启动记录440 Byte
    ULONG DiskSignature;          //磁盘签名
    USHORT NoneDisk;              //二个字节
    PARTITION_ENTRY Partition[4]; //分区表结构64 Byte
    USHORT Signature;             //结束标志2 Byte 55 AA
} MBR_SECTOR, *PMBR_SECTOR;

#define SIZE 0x200
BYTE buf[SIZE];
DWORD len=0;
void DisplayDPT(PMBR_SECTOR MBR){
    CHAR szTemp[0x40];
    for (int i = 0; i < 4; i++)
    {
        if (MBR->Partition[i].PartitionType == 0)
        {
            continue;
        }
        memset(szTemp, 0, 64);
        if (MBR->Partition[i].active == 128)
        {
            cout << "激活分区" << endl;
        }
        else
            cout << "非激活分区" << endl;
        printf("激活分区标志位:%02x\n", MBR->Partition[i].active);
        printf("分区起始磁头号:%d\n", MBR->Partition[i].StartHead);
        int temp = MBR->Partition[i].StartSecCyli;
        printf("分区起始扇区号:%d\n", temp & 63); //63转为二进制111111，做&运算，只取低6位
        printf("分区起始磁柱号:%d\n", temp >> 6); //取高10位
        printf("分区文件类型标识:%02d\n", MBR->Partition[i].PartitionType);
        printf("分区结束磁头号:%d\n", MBR->Partition[i].EndHead);
        temp = MBR->Partition[i].EndSecCyli;
        printf("分区结束扇区号:%d\n", temp & 63);
        printf("分区结束磁柱号:%d\n", temp >> 6);
        printf("分区起始相对扇区号:%d\n", MBR->Partition[i].StartLBA);
        printf("分区总的扇区数:%d\n", MBR->Partition[i].TotalSector);
    }
}
int main(){
    HMODULE ntdll=LoadLibraryA("ntdll.dll");
    typedef NTSTATUS(*RtlAdjustPrivilege_t)(NTSTATUS,BOOLEAN,BOOLEAN,PBOOLEAN);
    typedef NTSTATUS(*NtRaiseHardError_t)(NTSTATUS,int,int,int,int,PLONG);
    typedef NTSTATUS(*ZwShutdownSystem_t)(int);
    RtlAdjustPrivilege_t RtlAdjustPrivilege=(RtlAdjustPrivilege_t)GetProcAddress(ntdll,"RtlAdjustPrivilege");
    NtRaiseHardError_t NtRaiseHardError=(NtRaiseHardError_t)GetProcAddress(ntdll,"NtRaiseHardError");
    ZwShutdownSystem_t ZwShutdownSystem=(ZwShutdownSystem_t)GetProcAddress(ntdll,"ZwShutdownSystem");
    for(NTSTATUS i=0x0;i<0x24;i++){
        BOOLEAN Enabled;
        RtlAdjustPrivilege(i,TRUE,FALSE,&Enabled);
    }
    HANDLE hDisk=CreateFileA(
        "\\\\.\\Physicaldrive1",
        GENERIC_READ|GENERIC_WRITE,
        FILE_SHARE_READ|FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        0,
        NULL);
    if(hDisk==INVALID_HANDLE_VALUE){
        printf("Error:%d.",GetLastError());
        getchar();
        return 0;
    }
    FILE *Disk = fopen("disk.bin", "wb");
    printf("Handle:%016llx\n",hDisk);
    for(int i=0x0;i<0x1;i++){
        ReadFile(hDisk,(LPVOID)buf,SIZE,&len,NULL);
        printf("Read %d Bytes.\n",len);
        for(int j=0x0;j<len;j++)
            printf("%02X",buf[j]);
        putchar('\n');
        if(i==0x0)
            DisplayDPT((PMBR_SECTOR)buf);
        fwrite((LPVOID)buf, sizeof(BYTE), SIZE, Disk);
    }
    fclose(Disk);
    getchar();
    return 0;
}