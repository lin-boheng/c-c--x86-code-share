#include <stdint.h>
#include <stdlib.h>

static double RandomDouble(double Low, double High) {
  return Low + ((rand() * 0.000030517578125) / (High - Low));
}

static double RandomSpec() { return rand() * 0.00006103515625 - 1.0; }

static double Linear(double x) { return x; }

static double ReLU(double x) { return x > 0.0 ? x : 0.0; }

static double Sigmoid(double x) { return 1.0 / (1.0 + __builtin_exp(-x)); }

static double Tanh(double x) { return __builtin_tanh(x); }

enum class OperationType {
  NONE = 0,
  CONVOLUTION = 1,
  MAP = 2,
  MAX_POOL = 3,
  AVERAGE_POOL = 4,
};

class Operation {
public:
  Operation(uint32_t Channel, uint32_t Len, uint32_t Num)
      : Channel(Channel), Type(OperationType::CONVOLUTION), Next(NULL) {
    Param.Convolution.FilterLen = Len;
    Param.Convolution.FilterNum = Num;
    Param.Convolution.FilterData = new double[Len * Channel * Num];
  }

  Operation(uint32_t Channel, double (*MapFunc)(double))
      : Channel(Channel), Type(OperationType::MAP), Next(NULL) {
    Param.Map.MapFunction = MapFunc;
  }

  Operation(uint32_t Channel, OperationType Type)
      : Channel(Channel), Type(Type), Next(NULL) {}

  ~Operation() {
    switch (Type) {
    case OperationType::CONVOLUTION:
      delete[] Param.Convolution.FilterData;
      break;
    default:
      break;
    }
  }

  OperationType Type;
  uint32_t Channel;
  Operation *Next;
  union {
    struct {
      uint32_t FilterLen;
      uint32_t FilterNum;
      double *FilterData;
    } Convolution;
    struct {
      double (*MapFunction)(double);
    } Map;
    struct {
      uint32_t Scale;
    } MaxPool;
    struct {
      uint32_t Scale;
    } AveragePool;
  } Param;
};

class ConvolutionalNeuralNetwork {
private:
  const uint32_t InputChannel;
  uint32_t OutputChannel;
  Operation *Start;
  Operation *End;

public:
  ConvolutionalNeuralNetwork(const uint32_t Channel = 1)
      : InputChannel(Channel), OutputChannel(Channel),
        Start(new Operation(Channel, OperationType::NONE)), End(Start) {}

  ~ConvolutionalNeuralNetwork() {
    Operation *Temp;
    while (Start) {
      Temp = Start;
      Start = Start->Next;
      delete Temp;
    }
  }

  void AddFilter(uint32_t Len, uint32_t Num) {
    End->Next = new Operation(OutputChannel, Len, Num);
    End = End->Next;
    OutputChannel = Num;
  }

  void AddMap(double (*MapFunc)(double) = ReLU) {
    End->Next = new Operation(OutputChannel, MapFunc);
    End = End->Next;
  }

  void AddMaxPooling(uint32_t Scale) {
    End->Next = new Operation(OutputChannel, OperationType::MAX_POOL);
    End = End->Next;
    End->Param.MaxPool.Scale = Scale;
  }

  void AddAveragePooling(uint32_t Scale) {
    End->Next = new Operation(OutputChannel, OperationType::AVERAGE_POOL);
    End = End->Next;
    End->Param.AveragePool.Scale = Scale;
  }

  void RandomChange(double Size = 1.0) {
    for (Operation *op = Start; op; op = op->Next) {
      switch (op->Type) {
      case OperationType::CONVOLUTION: {
        uint32_t Size;
        Size = op->Channel * op->Param.Convolution.FilterLen *
               op->Param.Convolution.FilterNum;
        for (uint32_t i = 0; i < Size; i++)
          op->Param.Convolution.FilterData[i] += RandomDouble(-Size, Size);
      } break;
      default:
        break;
      }
    }
  }

  void CalCulate(double *Data, uint32_t Len) {
    for (Operation *op = Start; op; op = op->Next) {
      switch (op->Type) {
      case OperationType::NONE:
        break;
      case OperationType::CONVOLUTION:
        break;
      case OperationType::MAP:
        break;
      case OperationType::MAX_POOL:
        break;
      case OperationType::AVERAGE_POOL:
        break;
      }
    }
  }
};