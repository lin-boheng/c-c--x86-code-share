#include <iostream>
#include <windows.h>

using namespace std;

BOOL CALLBACK proc(HWND hWnd, LPARAM lParam){
    TCHAR s[MAX_PATH];
    EnumChildWindows(hWnd, proc, (LPARAM)0);
    GetWindowTextA(hWnd, s, MAX_PATH);
    //cout<<hWnd<<':'<<s<<endl;
    if(!strcmp(s, "X")){
        cout<<hWnd<<':'<<s<<endl;
        while(true){
            HWND hw = GetParent(hWnd);
            HDC hdc = GetDC(hw);
            LONG l;
            l = GetWindowLongA(hw, GWL_STYLE);
            SetWindowLongA(hw, GWL_STYLE, l | WS_CAPTION | WS_THICKFRAME);
            SetWindowPos(hWnd,NULL,0,0,200,200,SWP_NOMOVE|SWP_NOSIZE|SWP_FRAMECHANGED);
            HDC hd = GetDC(hWnd);
            SendMessageA(hWnd, WM_MOUSEHOVER, 0, 0);
            SendMessageA(hWnd, WM_LBUTTONDOWN, 0, 0);
            SendMessageA(hWnd, WM_LBUTTONUP, 0, 0);
            SendMessageA(hWnd, WM_MOUSELEAVE, 0, 0);
            SendMessageA(hWnd, WM_LBUTTONDBLCLK, 0, 0);
            for(int i=0;i<0x10000;i++){
                SendMessageA(hw, WM_COMMAND, MAKEWPARAM(i, BN_CLICKED), 0);
            }
            RECT r, r2;
            GetWindowRect(hw, &r);
            GetWindowRect(hWnd, &r2);
            BitBlt(GetDC(GetDesktopWindow()),0,0,r2.right-r2.left,r2.bottom-r2.top,hd,0,0,NOTSRCCOPY);
            BitBlt(hdc,0,0,r.right-r.left,r.bottom-r.top,hdc,0,0,SRCERASE);
            cout<<r.left<<' '<<r.right<<' '<<r.top<<' '<<r.bottom<<endl;
            cout<<r2.left<<' '<<r2.right<<' '<<r2.top<<' '<<r2.bottom<<endl;
            Sleep(100);
        }
    }
    return TRUE;
}

int main(){
    //BitBlt(GetDC(GetDesktopWindow()),0,0,100,100,GetDC(GetDesktopWindow()),0,0,SRCERASE);
    //getchar();
    while(true)
        EnumChildWindows(GetDesktopWindow(), proc, (LPARAM)0);
    getchar();
    ExitProcess(0);
}