#include <windows.h>

int main(){
    ((long(*)(int, int, int, int*))GetProcAddress(GetModuleHandleA("ntdll.dll"), "RtlAdjustPrivilege"))(0x13, 1, 0, 0);
    __asm__(
        //Windows system call arguments: rcx rdx r8 r9 [stack]...
        "sub $0x30, %rsp\n"
        "mov $0xC000008E, %ecx\n"
        "xor %edx, %edx\n"
        "xor %r8d, %r8d\n"
        "xor %r9d, %r9d\n"
        "movq $0, 0x20(%rsp)\n"
        "movq $0, 0x28(%rsp)\n"
        "lea (%rip), %rax\n"
        "push %rax\n"
        //Service No.0x167
        "mov %rcx, %r10\n"
        "mov $0x167, %eax\n"
        "syscall\n"
        "ret\n"
    );
}