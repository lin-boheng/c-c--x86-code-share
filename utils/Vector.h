#ifndef _VECTOR_
#define _VECTOR_
#include<iostream>
#include<cstdlib>
#include<cmath>
#include<initializer_list>
using namespace std;
const double eps=1e-6;
const double pi=3.14159265358979323846;
template<typename T>
T degree(T x){return x*pi/180.0;}
template<typename T>
T todegree(T x){return x*180.0/pi;}
template<typename T>
T abs(T x){return x<0?-x:x;}
template<typename T>
int sgn(T x){return abs(x)<eps?0:x>0?1:-1;}
template<typename T>
T sq(T x){return x*x;}
template<int size,typename T=double>
class Vector{
	private:
		T begin[size];
	public:
		Vector(){}
		Vector(initializer_list<T> x){
			register int s=0;
			for(decltype(x.begin()) it=x.begin();it!=x.end()&&s<size;++it,++s)
				begin[s]=*it;
		}
		T& operator[](register int x){return begin[x];}
		template<typename Tp>
		Vector<size,T>& operator=(register Tp* x){memcpy(begin,x,size*sizeof(T));return *this;}
		template<typename Tp>
		Vector<size,T>& operator=(register const Tp* x){memcpy(begin,x,size*sizeof(T));return *this;}
		template<typename Tp>
		Vector<size,T>& operator=(register Vector<size,Tp>& x){for(register int i=0;i<size;i++)begin[i]=x[i];return *this;}
		template<typename Tp>
		Vector<size,T>& operator()(register Vector<size,Tp>& x){for(register int i=0;i<size;i++)begin[i]=x[i];return *this;}
};
typedef Vector<1,double> Point1;
typedef Vector<2,double> Point2;
typedef Vector<3,double> Point3;
template<int height,int width,typename T=double>
class Matrix{
	private:
		T begin[height*width];
	public:
		Matrix(){}
		Matrix(initializer_list<initializer_list<T> > x){
			register int h=0;
			for(decltype(x.begin()) it1=x.begin();it1!=x.end()&&h<height;++it1,++h){
				register int w=0;
				for(decltype(it1->begin()) it2=it1->begin();it2!=it1->end()&&w<width;++it2,++w)
					begin[h*width+w]=*it2;
			}
		}
		T* operator[](register int x){return begin+x*width;}
		template<typename Tp>
		Matrix<height,width,T>& operator=(Matrix<height,width,Tp>& x){
			for(register int i=0;i<height;i++)
				for(register int j=0;j<width;j++)
					this->operator[](i)[j]=x[i][j];
			return *this;
		}
		template<typename Tp>
		Matrix<height,width,T>& operator()(Matrix<height,width,Tp>& x){return *this=x;}
		void Union(Matrix<height,width,T> x){begin=x[0];}
};
template<int size,typename T>
T sq(Vector<size,T> x){return x*x;}
template<int size,typename T>
Matrix<size,size,T> sq(Matrix<size,size,T> x){return x*x;}
template<int size,typename T>
T operator*(Vector<size,T> x,Vector<size,T> y){
	T ret=0;
	for(register int i=0;i<size;i++)
		ret+=x[i]*y[i];
	return ret;
}
template<int size,typename T>
Vector<size,T> operator!(Vector<size,T> x){
	T len=0;
	Vector<size,T> ret;
	for(register int i=0;i<size;i++)
		len+=x[i]*x[i];
	len=sqrt(len);
	for(register int i=0;i<size;i++)
		ret[i]=x[i]/len;
	return x;
}
template<int size,typename T>
Vector<size,T> operator-(Vector<size,T> x){
	Vector<size,T> ret;
	for(register int i=0;i<size;i++)
		ret[i]=-x[i];
	return ret;
}
template<int size,typename T>
Vector<size,T> operator+(Vector<size,T> x,Vector<size,T> y){
	Vector<size,T> ret;
	for(register int i=0;i<size;i++)
		ret[i]=x[i]+y[i];
	return ret;
}
template<int size,typename T>
Vector<size,T> operator-(Vector<size,T> x,Vector<size,T> y){
	Vector<size,T> ret;
	for(register int i=0;i<size;i++)
		ret[i]=x[i]-y[i];
	return ret;
}
template<int size,typename T>
T len(Vector<size,T> x){return sqrt(x*x);}
template<int size,typename T>
T cos(Vector<size,T> x,Vector<size,T> y){return x*y/(len(x)*len(y));}
template<int size,typename T>
T sin(Vector<size,T> x,Vector<size,T> y){return sqrt(1.0-sq(cos(x,y)));}
template<int size,typename T>
T angle(Vector<size,T> x,Vector<size,T> y){return acos(cos(x,y));}
template<int size,typename T,typename Tp>
Vector<size,T> operator*(Tp x,Vector<size,T> y){
	Vector<size,T> ret;
	for(register int i=0;i<size;i++)
		ret[i]=y[i]*x;
	return ret;
}
template<int size,typename T,typename Tp>
Vector<size,T> operator*(Vector<size,T> x,Tp y){
	Vector<size,T> ret;
	for(register int i=0;i<size;i++)
		ret[i]=x[i]*y;
	return ret;
}
template<int size,typename T,typename Tp>
Vector<size,T> operator/(Vector<size,T> x,Tp y){
	Vector<size,T> ret;
	for(register int i=0;i<size;i++)
		ret[i]=x[i]/y;
	return ret;
}
template<int height,int width,typename T>
Vector<height,T> operator*(Matrix<height,width,T> x,Vector<width,T> y){
	Vector<height,T> ret;
	for(register int i=0;i<height;i++)
		for(register int j=0;j<width;j++)
			ret[i]+=x[i][j]*y[j];
	return ret;
}
template<int out,int mid,int in,typename T>
Matrix<out,in,T> operator*(Matrix<out,mid,T> x,Matrix<mid,in,T> y){
	Matrix<out,in,T> ret;
	for(register int i=0;i<out;i++)
		for(register int j=0;j<in;j++)
			for(register int k=0;k<mid;k++)
				ret[i][j]+=x[i][k]*y[k][j];
	return ret;
}
template<int height,int width,typename T,typename Tp>
Matrix<height,width,T> operator*(Matrix<height,width,T> x,Tp y){
	Matrix<height,width,T> ret;
	for(register int i=0;i<height;i++)
		for(register int j=0;j<width;j++)
			ret[i][j]=x[i][j]*y;
	return ret;
}
template<int height,int width,typename T,typename Tp>
Matrix<height,width,T> operator*(Tp x,Matrix<height,width,T> y){
	Matrix<height,width,T> ret;
	for(register int i=0;i<height;i++)
		for(register int j=0;j<width;j++)
			ret[i][j]=y[i][j]*x;
	return ret;
}
template<int height,int width,typename T,typename Tp>
Matrix<height,width,T> operator/(Matrix<height,width,T> x,Tp y){
	Matrix<height,width,T> ret;
	for(register int i=0;i<height;i++)
		for(register int j=0;j<width;j++)
			ret[i][j]=x[i][j]/y;
	return ret;
}
template<int height,int width,typename T,typename Tp>
Matrix<height,width,T> operator+(Matrix<height,width,T> x,Matrix<height,width,Tp> y){
	Matrix<height,width,T> ret;
	for(register int i=0;i<height;i++)
		for(register int j=0;j<width;j++)
			ret[i][j]=x[i][j]+y[i][j];
	return ret;
}
template<int height,int width,typename T,typename Tp>
Matrix<height,width,T> operator-(Matrix<height,width,T> x,Matrix<height,width,Tp> y){
	Matrix<height,width,T> ret;
	for(register int i=0;i<height;i++)
		for(register int j=0;j<width;j++)
			ret[i][j]=x[i][j]-y[i][j];
	return ret;
}
template<int height,int width,typename Tp>
Matrix<width,height,Tp> operator~(Matrix<height,width,Tp> x){
	Matrix<width,height,Tp> ret;
	for(register int i=0;i<height;i++)
		for(register int j=0;j<width;j++)
			ret[j][i]=x[i][j];
	return ret;
}
template<int height,int width,typename Tp>
Matrix<width,height,Tp> T(Matrix<height,width,Tp> x){
	Matrix<width,height,Tp> ret;
	for(register int i=0;i<height;i++)
		for(register int j=0;j<width;j++)
			ret[j][i]=x[i][j];
	return ret;
}
template<int size,typename T,typename Tp>
Vector<size,T> Shadow(Vector<size,T> x,Vector<size,Tp> y){return len(x)*cos(x,y)*(!y);}
template<int size,typename T,typename Tp>
Vector<size,T> Dislodge(Vector<size,T> x,Vector<size,Tp> y){return x-Shadow(x,y);}
template<int size,typename T>
T det(Matrix<size,size,T>& x){
	if(size==1)
		return x[0][0];
	if(size==2)
		return x[0][0]*x[1][1]-x[1][0]*x[0][1];
	T ret=0;
	Matrix<(size>=3?size-1:2),(size>=3?size-1:2),T> son;
	for(register int i=0;i<size;i++){
		for(register int j=1;j<size;j++)
			for(register int k=1;k<size;k++)
				son[j-1][k-1]=x[(i+j)%size][k];
		ret+=x[i][0]*det(son);
	}
	return ret;
}
template<int size,typename T>
Matrix<size,size,T> DCT(Matrix<size,size,T> x){
	Matrix<size,size,T> ret;
	Matrix<size,size,T> A;
	for(register int i=0;i<size;i++)
		for(register int j=0;j<size;j++)
			A[i][j]=(i==0?sqrt(1.0/size):sqrt(2.0/size))*cos(i*(j+0.5)*pi/size);
	ret=A*x*T(A);
	return ret;
}
template<int size,typename T>
Matrix<size,size,T> IDCT(Matrix<size,size,T> x){
	Matrix<size,size,T> ret;
	Matrix<size,size,T> A;
	for(register int i=0;i<size;i++)
		for(register int j=0;j<size;j++)
			A[i][j]=(i==0?sqrt(1.0/size):sqrt(2.0/size))*cos(i*(j+0.5)*pi/size);
	ret=T(A)*x*A;
	return ret;
}
template<typename T>
T cross(Vector<2,T> x,Vector<2,T> y){return x[0]*y[1]-x[1]*y[0];}
template<typename T>
Vector<3,T> cross(Vector<3,T> x,Vector<3,T> y){
	Vector<3,T> ret;
	ret[0]=x[1]*y[2]-x[2]*y[1];
	ret[1]=x[2]*y[0]-x[0]*y[2];
	ret[2]=x[0]*y[1]-x[1]*y[0];
	return ret;
}
template<int size,typename T,typename Tp>
Vector<size,T>& operator+=(Vector<size,T>& x,Vector<size,Tp> y){return x=x+y;}
template<int size,typename T,typename Tp>
Vector<size,T>& operator-=(Vector<size,T>& x,Vector<size,Tp> y){return x=x-y;}
template<int size,typename T,typename Tp>
Vector<size,T>& operator*=(Vector<size,T>& x,Tp y){return x=x*y;}
template<int size,typename T,typename Tp>
Vector<size,T>& operator/=(Vector<size,T>& x,Tp y){return x=x/y;}
template<int size,typename T>
istream& operator>>(istream& cin,Vector<size,T>& x){
	for(register int i=0;i<size;i++)
		cin>>x[i];
	return cin;
}
template<int size,typename T>
ostream& operator<<(ostream& cout,Vector<size,T> x){
	for(register int i=0;i<size;i++)
		cout<<x[i]<<' ';
	cout<<endl;
	return cout;
}
template<int height,int width,typename T>
istream& operator>>(istream& cin,Matrix<height,width,T>& x){
	for(register int i=0;i<height;i++)
		for(register int j=0;j<width;j++)
			cin>>x[i][j];
	return cin;
}
template<int height,int width,typename T>
ostream& operator<<(ostream& cout,Matrix<height,width,T> x){
	for(register int i=0;i<height;i++){
		for(register int j=0;j<width;j++)
			cout<<x[i][j]<<' ';
		cout<<endl;
	}
	return cout;
}
template<int height,int width,typename T,typename Tp>
Matrix<height,width,T>& operator+=(Matrix<height,width,T>& x,Matrix<height,width,Tp> y){return x=x+y;}
template<int height,int width,typename T,typename Tp>
Matrix<height,width,T>& operator-=(Matrix<height,width,T>& x,Matrix<height,width,Tp> y){return x=x-y;}
template<int height,int width,typename T,typename Tp>
Matrix<height,width,T>& operator*=(Matrix<height,width,T>& x,Matrix<height,width,Tp> y){return x=x*y;}
template<int height,int width,typename T,typename Tp>
Matrix<height,width,T>& operator*=(Matrix<height,width,T>& x,Tp y){return x=x*y;}
template<int height,int width,typename T,typename Tp>
Matrix<height,width,T>& operator/=(Matrix<height,width,T>& x,Tp y){return x=x/y;}
template<int size,typename T,typename Tp>
Matrix<size,size,T> pow(Matrix<size,size,T> x,Tp y){
	Matrix<size,size,T> ret;
	for(register int i=0;i<size;i++)
		ret[i][i]=1;
	while(y){
		if(y&1)
			ret=ret*x;
		x=x*x;
		y>>=1;
	}
	return ret;
}
template<int size,typename T=double>
class Line{
	public:
		Vector<size,T> start;
		Vector<size,T> dir;
	public:
		Line(){dir[0]=1;}
		template<typename T1,typename T2>
		Line(Vector<size,T1> start,Vector<size,T2> dir):start(start),dir(dir){}
		~Line(){}
		template<typename Tp>
		void SetStart(Vector<size,Tp> x){start=x;}
		template<typename Tp>
		void SetDir(Vector<size,Tp> x){dir=x;}
};
typedef Line<2,double> Line2;
typedef Line<3,double> Line3;
template<int size,typename T=double>
class Segment{
	public:
		Vector<size,T> start;
		Vector<size,T> end;
	public:
		Segment(){end[0]=1;}
		template<typename Tp>
		Segment(Vector<size,Tp> end):end(end){}
		template<typename T1,typename T2>
		Segment(Vector<size,T1> start,Vector<size,T2> end):start(start),end(end){}
		~Segment(){}
		template<typename Tp>
		void SetStart(Vector<size,Tp> x){start=x;}
		template<typename Tp>
		void SetEnd(Vector<size,Tp> x){end=x;}
};
typedef Segment<2,double> Segment2;
typedef Segment<2,double> Segment3;
template<int size,typename T>
T len(Segment<size,T> x){return len(x.end-x.start);}
template<int size=3,typename T=double>
class Plane{
	public:
		Vector<size,T> start;
		Vector<size,T> dir1;
		Vector<size,T> dir2;
	public:
		Plane(){dir1[0]=1;dir2[1]=1;}
		template<typename T1,typename T2,typename T3>
		Plane(Vector<size,T1> start,Vector<size,T2> dir1,Vector<size,T3> dir2):start(start),dir1(dir1),dir2(dir2){}
		~Plane(){}
		template<typename Tp>
		void SetStart(Vector<size,Tp> x){start=x;}
		template<typename Tp>
		void SetDir1(Vector<size,Tp> x){dir1=x;}
		template<typename Tp>
		void SetDir2(Vector<size,Tp> x){dir2=x;}
};
typedef Plane<3,double> Plane3;
template<typename T=double>
class NewPlane{
	public:
		Vector<3,T> start;
		Vector<3,T> LawVector;
	public:
		NewPlane(){LawVector[2]=1;}
		template<typename T1,typename T2>
		NewPlane(Vector<3,T1> start,Vector<3,T2> LawVector):start(start),LawVector(LawVector){}
		~NewPlane(){}
		template<typename Tp>
		void SetStart(Vector<3,Tp> x){start=x;}
		template<typename Tp>
		void SetLawVector(Vector<3,Tp> x){LawVector=x;}
};
template<int size,typename T=double>
class NDCircle{
	public:
		Vector<size,T> center;
		T radius;
	public:
		NDCircle(){}
		template<typename T1,typename T2>
		NDCircle(Vector<size,T1> center,T2 radius):center(center),radius(radius){}
		~NDCircle(){}
		template<typename Tp>
		void SetCenter(Vector<size,Tp> x){center=x;}
		template<typename Tp>
		void SetRadius(Tp x){radius=x;}
};
typedef NDCircle<2,double> Circle;
typedef NDCircle<3,double> Ball;
template<int size,typename T,typename Tp>
T dis2(Vector<size,T> x,Vector<size,Tp> y){return sq(x-y);}
template<int size,typename T,typename Tp>
T dis(Vector<size,T> x,Vector<size,Tp> y){return sqrt(sq(x-y));}
template<int size,typename T,typename Tp>
T dis(Vector<size,T> x,Line<size,Tp> y){return dis(x,y.start)*sin(angle(x-y.start,y.dir));}
template<int size,typename T,typename Tp>
Vector<size,T> Shadow(Vector<size,T> x,Line<size,Tp> y){return y.start+Shadow(x-y.start,y.dir);}
template<int size,typename T,typename Tp>
T sin(Line<size,T> x,Line<size,Tp> y){return sin(x.dir,y.dir);}
template<int size,typename T,typename Tp>
T cos(Line<size,T> x,Line<size,Tp> y){return cos(x.dir,y.dir);}
template<int size,typename T,typename Tp>
T angle(Line<size,T> x,Line<size,Tp> y){return angle(x.dir,y.dir);}
template<int size,typename T,typename Tp>
T sin(Line<size,T> x,Vector<size,Tp> y){return sin(x.dir,y);}
template<int size,typename T,typename Tp>
T cos(Line<size,T> x,Vector<size,Tp> y){return cos(x.dir,y);}
template<int size,typename T,typename Tp>
T angle(Line<size,T> x,Vector<size,Tp> y){return angle(x.dir,y);}
template<typename T,typename Tp>
Vector<2,T> Intersect(Line<2,T> x,Line<2,Tp> y){return x.start+(sgn(cos(Dislodge(y.start-x.start,y.dir),x.dir))==1?1:-1)*(dis(x.start,y)/sin(x,y))*(!x.dir);}
template<typename T>
Vector<3,T> LawVector(Plane<3,T> x){return cross(x.dir1,x.dir2);}
template<typename T,typename Tp>
T sin(Vector<3,T> x,Plane<3,Tp> y){return abs(cos(x,LawVector(y)));}
template<typename T,typename Tp>
T cos(Vector<3,T> x,Plane<3,Tp> y){return sqrt(1.0-sq(sin(x,y)));}
template<typename T,typename Tp>
T angle(Vector<3,T> x,Plane<3,Tp> y){return asin(sin(x,y));}
template<typename T,typename Tp>
T sin(Line<3,T> x,Plane<3,Tp> y){return abs(cos(x.dir,LawVector(y)));}
template<typename T,typename Tp>
T cos(Line<3,T> x,Plane<3,Tp> y){return sqrt(1.0-sq(sin(x,y)));}
template<typename T,typename Tp>
T angle(Line<3,T> x,Plane<3,Tp> y){return asin(sin(x,y));}
template<typename T,typename Tp>
T dis(Vector<3,T> x,Plane<3,Tp> y){return len(Shadow(x-y.start,LawVector(y)));}
template<typename T,typename Tp>
Vector<3,T> Shadow(Vector<3,T> x,Plane<3,Tp> y){Vector<3,T> ret=x-Shadow(x-y.start,LawVector(y));ret.g=true;return ret;}
template<typename T,typename Tp>
Vector<3,T> Intersect(Line<3,T> x,Plane<3,Tp> y){return x.start+(cos(Dislodge(y.start-x.start),LawVector(y))*dis(x.start,y)/cos(x.dir,LawVector(y)))*x.dir;}
template<typename T,typename Tp>
Line<3,T> Shadow(Line<3,T> x,Plane<3,Tp> y){return Line<3,T>(y.start+Dislodge(x.start-y.start,LawVector(y)),Dislodge(x.dir,LawVector(y)));}
template<int size,typename T=double>
class equation{
	private:
		Matrix<size,size+1,T> formula;
	public:
		equation(){}
		equation(Matrix<size,size+1,T> x):formula(x){}
		~equation(){}
		T* operator[](int x){return formula[x];}
		Vector<size,T> solve_clime(){
			Matrix<size,size,T> copy;
			for(register int i=0;i<size;i++)
				for(register int j=0;j<size;j++)
					copy[i][j]=formula[i][j];
			T div=det(copy);
			Vector<size,T> ret;
			for(register int i=0;i<size;i++){
				if(i>0)
					for(register int j=0;j<size;j++)
						copy[j][i-1]=formula[j][i-1];
				for(register int j=0;j<size;j++)
					copy[j][i]=formula[j][size];
				ret[i]=det(copy)/div;
			}
			copy.Free();
			return ret;
		}
		Vector<size,T> solve(){
			Matrix<size,size+1,T> copy=formula;
			Vector<size,T> ret;
			for(register int i=0;i<size;i++){
				T div1=copy[i][i];
				for(register int j=i;j<=size;j++)
					copy[i][j]/=div1;
				for(register int j=i+1;j<size;j++){
					T div2=copy[j][i];
					for(register int k=i;k<=size;k++)
						copy[j][k]-=div2*copy[i][k];
				}
			}
			for(register int i=size-1;i>=0;i--){
				for(register int j=i-1;j>=0;j--){
					copy[j][size]-=copy[j][i]*copy[i][size];
					copy[j][i]=0;
				}
			}
			for(register int i=0;i<size;i++)
				ret[i]=copy[i][size];
			return ret;
		}
};
template<int size,typename T,typename Tp>
bool Perpendicular(Vector<size,T> x,Vector<size,Tp> y){return sgn(cos(x,y))==0;}
template<int size,typename T,typename Tp>
bool Perpendicular(Line<size,T> x,Line<size,Tp> y){return sgn(cos(x.dir,y.dir))==0;}
template<int size,typename T,typename Tp>
bool Perpendicular(Vector<size,T> x,Line<size,Tp> y){return sgn(cos(x,y.dir))==0;}
template<int size,typename T,typename Tp>
bool Perpendicular(Line<size,T> x,Vector<size,Tp> y){return sgn(cos(x.dir,y))==0;}
template<int size,typename T,typename Tp>
bool Parallel(Vector<size,T> x,Vector<size,Tp> y){return sgn(sin(x,y))==0;}
template<int size,typename T,typename Tp>
bool Parallel(Line<size,T> x,Line<size,Tp> y){return sgn(sin(x.dir,y.dir))==0;}
template<int size,typename T,typename Tp>
bool Parallel(Vector<size,T> x,Line<size,Tp> y){return sgn(sin(x,y.dir))==0;}
template<int size,typename T,typename Tp>
bool Parallel(Line<size,T> x,Vector<size,Tp> y){return sgn(sin(x.dir,y))==0;}
template<typename T,typename Tp>
void Rotate(Vector<2,T>& x,Tp y){x=Vector<2,T>(x[0]*cos(y)-x[1]*sin(y),x[0]*sin(y)+x[1]*cos(y));}
template<typename T1,typename T2,typename T3>
void Rotate(Vector<3,T1>& x,Vector<3,T2> LawVector,T3 y){x=Dislodge(x,LawVector)*cos(y)+cross(LawVector,x)*sin(y)+Shadow(x,LawVector);}
#endif
