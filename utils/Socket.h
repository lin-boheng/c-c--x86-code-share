#include<winsock2.h>
class WSAInit{
    public:
        WSADATA wsaData;
        WSAInit(){
            WSAStartup(MAKEWORD(2,2),&wsaData);
        }
        ~WSAInit(){
            WSACleanup();
        }
}WSAInit;
class Socket{
    protected:
        sockaddr Address;
        SOCKET Sock;
        int Af;
        int Type;
        int Protocol;
        bool connected;
    public:
    	Socket(){}
        Socket(int af,int type,int protocol=IPPROTO_TCP):
            Af(af),
            Type(type),
            Protocol(protocol),
            Sock(socket(af,type,protocol)),
            connected(false)
        {}
        ~Socket(){
            this->Close();
        }
        int GetAf(){return Af;}
        int GetType(){return Type;}
        int GetProtocol(){return Protocol;}
        int Close(){
            this->connected=false;
            return closesocket(Sock);
        }
        sockaddr_in GetAddress(){return *(sockaddr_in*)&Address;}
        void Sethost(const char* host){
            sockaddr_in* addr=(sockaddr_in*)&Address;
            addr->sin_addr.S_un.S_addr=inet_addr(host);
        }
        void Setport(unsigned short port){
            sockaddr_in* addr=(sockaddr_in*)&Address;
            addr->sin_port=htons(port);
        }
        int Connect(const char* host,unsigned short port){
            sockaddr_in* addr=(sockaddr_in*)&Address;
            addr->sin_family=Af;
            addr->sin_addr.S_un.S_addr=inet_addr(host);
            addr->sin_port=htons(port);
            int retval=connect(Sock,&Address,sizeof(sockaddr));
            if(retval!=SOCKET_ERROR)
                this->connected=true;
            return retval;
        }
        bool isConnected(){
            return this->connected;
        }
        int Bind(const char* host,unsigned short port){
            sockaddr_in* addr=(sockaddr_in*)&Address;
            addr->sin_family=Af;
            addr->sin_addr.S_un.S_addr=inet_addr(host);
            addr->sin_port=htons(port);
            int retval=bind(Sock,&Address,sizeof(sockaddr));
            if(retval!=SOCKET_ERROR)
                this->connected=true;
            return retval;
        }
        int Bind(unsigned short port){
            sockaddr_in* addr=(sockaddr_in*)&Address;
            addr->sin_family=Af;
            char host[MAX_PATH];
            gethostname(host,MAX_PATH);
            hostent *list=gethostbyname(host);
            addr->sin_addr=*(in_addr*)list->h_addr_list[0];
            addr->sin_port=htons(port);
            int retval=bind(Sock,&Address,sizeof(sockaddr));
            if(retval!=SOCKET_ERROR)
                this->connected=true;
            return retval;
        }
        int Listen(int backlog){
            return listen(Sock,backlog);
        }
        Socket Accept(){
            Socket RetSock;
            RetSock.Af=Af;
            RetSock.Type=Type;
            RetSock.Protocol=Protocol;
            int addrlen=sizeof(sockaddr);
            RetSock.Sock=accept(Sock,&RetSock.Address,&addrlen);
            RetSock.connected=true;
            return RetSock;
        }
        int Send(const char* buf,int len){
        	int retval=send(Sock,buf,len,0);
            if(retval==SOCKET_ERROR){
                this->connected=false;
                this->Close();
            }
            return retval;
		}
        int Receive(char* buf,int len){
            int retval=recv(Sock,buf,len,0);
            if (retval==SOCKET_ERROR){
                this->connected=false;
                this->Close();
            }
            return retval;
        }
        int SendTo(const char* buf,int len){
            return sendto(Sock,buf,len,0,&Address,sizeof(sockaddr));
        }
        int ReceiveFrom(char* buf,int len){
            int addrlen=sizeof(sockaddr);
            return recvfrom(Sock,buf,len,0,&Address,&addrlen);
        }
};
