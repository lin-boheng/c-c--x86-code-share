#include <windows.h>
#include <stdio.h>

#define __push(x) __asm__("push %0"::"r"(x))
#define __pushm(x) __asm__("push %0"::"m"(x))
#define __call(x) __asm__("call *%0"::"r"(x))
#define __popm(x) __asm__("pop %0"::"m"(x))
#define __popn(x) __asm__("add %0,%%esp"::"r"(x))
#define __ebp(x) __asm__("mov %%ebp,%0"::"m"(x))
#define __esp(x) __asm__("mov %%esp,%0"::"m"(x))
#define __sebp(x) __asm__("mov %0,%%ebp"::"m"(x))
#define __sesp(x) __asm__("mov %0,%%esp"::"m"(x))
#define __ret(x) __asm__("mov %%eax,%0":"m"(x))
#define __caller(x,ebp) __asm__("mov 4(%1),%0;mov (%1),%1":"=r"(x),"+r"(ebp))

HMODULE hDLL;

extern "C" FARPROC IGetProcAddressA(LPCSTR lpProcName){
    return GetProcAddress(hDLL, lpProcName);
}

int main(){
    hDLL = LoadLibrary("reflect.dll");
    FARPROC entry = GetProcAddress(hDLL, "entry");
    __push(IGetProcAddressA);
    __call(entry);
    __popn(4);
    FreeLibrary(hDLL);
    printf("Exit...\n");
}