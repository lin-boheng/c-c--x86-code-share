#include <cstdlib>
#include <fstream>
#include <initializer_list>

static double RandomDouble(double Low, double High) {
  return Low + ((rand() * 0.000030517578125) / (High - Low));
}

static double RandomSpec() { return rand() * 0.00006103515625 - 1.0; }

static double Linear(double x) { return x; }

static double ReLU(double x) { return x > 0.0 ? x : 0.0; }

static double Sigmoid(double x) { return 1.0 / (1.0 + __builtin_exp(-x)); }

static double Tanh(double x) { return __builtin_tanh(x); }

static double DeltaLinear(double x) { return 1.0; }

static double DeltaReLU(double x) { return x > 0.0 ? 1.0 : 0.0; }

static double DeltaSigmoid(double x) { return x * (1.0 - x); }

static double DeltaTanh(double x) { return (1.0 + x) * (1.0 - x); }

double (*const ActivationFunc[])(double) = {Linear, ReLU, Sigmoid, Tanh};
double (*const ActivationFuncDelta[])(double) = {DeltaLinear, DeltaReLU,
                                                 DeltaSigmoid, DeltaTanh};

enum class ActFunc {
  LINEAR = 0,
  RELU = 1,
  SIGMOID = 2,
  TANH = 3,
};

class BPNeuralNetwork {
private:
  unsigned int LayerCnt;
  unsigned int *LayerConfig;
  ActFunc ActFun;
  double (*Act)(double);
  double (*ActDelta)(double);
  double **MatrixPtr;
  double **OffsetPtr;
  double **OutCache;
  double **DeltaCache;

  void AllocateMemory() {
    MatrixPtr = new double *[LayerCnt];
    OffsetPtr = new double *[LayerCnt];
    OutCache = new double *[LayerCnt + 1];
    DeltaCache = new double *[LayerCnt + 1];
    for (unsigned int i = 0; i < LayerCnt; i++) {
      MatrixPtr[i] = new double[LayerConfig[i] * LayerConfig[i + 1]];
      OffsetPtr[i] = new double[LayerConfig[i + 1]];
      OutCache[i] = new double[LayerConfig[i]];
      DeltaCache[i] = new double[LayerConfig[i]];
      for (unsigned int j = 0; j < LayerConfig[i] * LayerConfig[i + 1]; j++)
        MatrixPtr[i][j] = RandomSpec();
      for (unsigned int j = 0; j < LayerConfig[i + 1]; j++)
        OffsetPtr[i][j] = RandomSpec();
    }
    OutCache[LayerCnt] = new double[LayerConfig[LayerCnt]];
    DeltaCache[LayerCnt] = new double[LayerConfig[LayerCnt]];
  }

  void FreeMemory() {
    for (unsigned int i = 0; i < LayerCnt; i++) {
      delete[] MatrixPtr[i];
      delete[] OffsetPtr[i];
      delete[] OutCache[i];
      delete[] DeltaCache[i];
    }
    delete[] OutCache[LayerCnt];
    delete[] DeltaCache[LayerCnt];
    delete[] MatrixPtr;
    delete[] OffsetPtr;
    delete[] OutCache;
    delete[] DeltaCache;
    delete[] LayerConfig;
  }

public:
  BPNeuralNetwork(std::initializer_list<unsigned int> LayerConfigList,
                  ActFunc ActFun = ActFunc::SIGMOID)
      : Act(ActivationFunc[(unsigned int)ActFun]),
        ActDelta(ActivationFuncDelta[(unsigned int)ActFun]),
        LayerCnt(LayerConfigList.size() - 1), ActFun(ActFun) {
    LayerConfig = new unsigned int[LayerConfigList.size()];
    for (unsigned int i = 0; i <= LayerCnt; i++)
      LayerConfig[i] = LayerConfigList.begin()[i];
    AllocateMemory();
  }

  BPNeuralNetwork(FILE *File) {
    std::fread(&ActFun, sizeof(ActFunc), 1, File);
    Act = ActivationFunc[(unsigned int)ActFun];
    ActDelta = ActivationFuncDelta[(unsigned int)ActFun];
    std::fread(&LayerCnt, sizeof(unsigned int), 1, File);
    LayerConfig = new unsigned int[LayerCnt + 1];
    std::fread(LayerConfig, sizeof(unsigned int), LayerCnt + 1, File);
    AllocateMemory();
    for (unsigned int i = 0; i < LayerCnt; i++) {
      std::fread(MatrixPtr[i], sizeof(double),
                 LayerConfig[i] * LayerConfig[i + 1], File);
      std::fread(OffsetPtr[i], sizeof(double), LayerConfig[i + 1], File);
    }
  }

  ~BPNeuralNetwork() { FreeMemory(); }

  void Train(double *Input, double *Output, double LearnRate = 1.0) {
    for (unsigned int i = 0; i < *LayerConfig; i++)
      (*OutCache)[i] = Input[i];
    for (unsigned int i = 0; i < LayerCnt; i++) {
      for (unsigned int j = 0; j < LayerConfig[i + 1]; j++)
        OutCache[i + 1][j] = 0.0;
      for (unsigned int j = 0; j < LayerConfig[i + 1]; j++) {
        double *Weight = MatrixPtr[i] + j * LayerConfig[i];
        for (unsigned int k = 0; k < LayerConfig[i]; k++)
          OutCache[i + 1][j] += Weight[k] * OutCache[i][k];
        OutCache[i + 1][j] = Act(OutCache[i + 1][j] + OffsetPtr[i][j]);
      }
    }
    for (unsigned int i = 0; i < LayerConfig[LayerCnt]; i++)
      DeltaCache[LayerCnt][i] = ActDelta(OutCache[LayerCnt][i] - Output[i]);
    for (unsigned int i = LayerCnt; i > 0; i--) {
      for (unsigned int j = 0; j < LayerConfig[i - 1]; j++)
        DeltaCache[i - 1][j] = 0.0;
      for (unsigned int j = 0; j < LayerConfig[i]; j++) {
        double *Weight = MatrixPtr[i - 1] + j * LayerConfig[i - 1];
        for (unsigned int k = 0; k < LayerConfig[i - 1]; k++) {
          DeltaCache[i - 1][k] += Weight[k] * DeltaCache[i][j];
          Weight[k] -= LearnRate * OutCache[i - 1][k] * DeltaCache[i][j];
        }
      }
      for (unsigned int j = 0; j < LayerConfig[i]; j++)
        OffsetPtr[i - 1][j] -= LearnRate * DeltaCache[i][j];
      for (unsigned int j = 0; j < LayerConfig[i - 1]; j++)
        DeltaCache[i - 1][j] *= ActDelta(OutCache[i - 1][j]);
    }
  }

  void Calculate(double *Input, double *Output) {
    std::swap(*OutCache, Input);
    std::swap(OutCache[LayerCnt], Output);
    for (unsigned int i = 0; i < LayerCnt; i++) {
      for (unsigned int j = 0; j < LayerConfig[i + 1]; j++)
        OutCache[i + 1][j] = 0.0;
      for (unsigned int j = 0; j < LayerConfig[i + 1]; j++) {
        double *Weight = MatrixPtr[i] + j * LayerConfig[i];
        for (unsigned int k = 0; k < LayerConfig[i]; k++)
          OutCache[i + 1][j] += Weight[k] * OutCache[i][k];
        OutCache[i + 1][j] = Act(OutCache[i + 1][j] + OffsetPtr[i][j]);
      }
    }
    std::swap(*OutCache, Input);
    std::swap(OutCache[LayerCnt], Output);
  }

  void SaveToFile(FILE *File) {
    std::fwrite(&ActFun, sizeof(ActFunc), 1, File);
    std::fwrite(&LayerCnt, sizeof(unsigned int), 1, File);
    std::fwrite(LayerConfig, sizeof(unsigned int), LayerCnt + 1, File);
    for (unsigned int i = 0; i < LayerCnt; i++) {
      std::fwrite(MatrixPtr[i], sizeof(double),
                  LayerConfig[i] * LayerConfig[i + 1], File);
      std::fwrite(OffsetPtr[i], sizeof(double), LayerConfig[i + 1], File);
    }
  }
};
