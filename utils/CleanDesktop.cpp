#include <iostream>
#include <cstdlib>
#include <windows.h>
#include <shlwapi.h>
#include <psapi.h>
#include <tlhelp32.h>
#include <commctrl.h>
using namespace std;
int main(){
    for(int i = 0; i < 1000; i++){
        HWND hWnd = GetDesktopWindow();
        HDC hDC = GetWindowDC(hWnd);
        RECT rect;
        GetWindowRect(hWnd, &rect);
        int size = rand() % min(rect.right, rect.bottom);
        int x1 = rand() % (rect.right - size);
        int x2 = rand() % (rect.right - size);
        int y1 = rand() % (rect.bottom - size);
        int y2 = rand() % (rect.bottom - size);
        BitBlt(hDC, x1, y1, size, size, hDC, x2, y2, SRCCOPY);
        ReleaseDC(hWnd, hDC);
        Sleep(10);
    }
    return 0;
}