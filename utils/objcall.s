	.file	"objcall.cpp"
	.text
.lcomm _ZStL8__ioinit,1,1
	.section .rdata,"dr"
.LC0:
	.ascii "A::test\0"
	.section	.text$_ZN1A4testEv,"x"
	.linkonce discard
	.align 2
	.globl	_ZN1A4testEv
	.def	_ZN1A4testEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN1A4testEv
_ZN1A4testEv:
.LFB1788:
	pushq	%rbp
	.seh_pushreg	%rbp
	movq	%rsp, %rbp
	.seh_setframe	%rbp, 0
	leaq	-32(%rsp), %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, 16(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, %rdx
	movq	.refptr._ZSt4cout(%rip), %rax
	movq	%rax, %rcx
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rcx
	movq	.refptr._ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_(%rip), %rax
	movq	%rax, %rdx
	call	_ZNSolsEPFRSoS_E
	nop
	leaq	32(%rsp), %rsp
	popq	%rbp
	ret
	.seh_endproc
	.section .rdata,"dr"
.LC1:
	.ascii "A::test2\0"
	.section	.text$_ZN1A5test2Ev,"x"
	.linkonce discard
	.align 2
	.globl	_ZN1A5test2Ev
	.def	_ZN1A5test2Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN1A5test2Ev
_ZN1A5test2Ev:
.LFB1789:
	pushq	%rbp
	.seh_pushreg	%rbp
	movq	%rsp, %rbp
	.seh_setframe	%rbp, 0
	leaq	-32(%rsp), %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, 16(%rbp)
	leaq	.LC1(%rip), %rax
	movq	%rax, %rdx
	movq	.refptr._ZSt4cout(%rip), %rax
	movq	%rax, %rcx
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rcx
	movq	.refptr._ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_(%rip), %rax
	movq	%rax, %rdx
	call	_ZNSolsEPFRSoS_E
	nop
	leaq	32(%rsp), %rsp
	popq	%rbp
	ret
	.seh_endproc
	.text
	.globl	_Z4callP6ObjectMS_FvvE
	.def	_Z4callP6ObjectMS_FvvE;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z4callP6ObjectMS_FvvE
_Z4callP6ObjectMS_FvvE:
.LFB1790:
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rbx
	.seh_pushreg	%rbx
	leaq	-56(%rsp), %rsp
	.seh_stackalloc	56
	leaq	48(%rsp), %rbp
	.seh_setframe	%rbp, 48
	.seh_endprologue
	movq	%rcx, 32(%rbp)
	movq	%rdx, %rbx
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	%rax, -16(%rbp)
	movq	%rdx, -8(%rbp)
	movq	-16(%rbp), %rax
	andl	$1, %eax
	testq	%rax, %rax
	je	.L4
	movq	-8(%rbp), %rax
	movq	%rax, %rdx
	movq	32(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movq	-16(%rbp), %rax
	subq	$1, %rax
	addq	%rdx, %rax
	movq	(%rax), %rax
	jmp	.L5
.L4:
	movq	-16(%rbp), %rax
.L5:
	movq	-8(%rbp), %rdx
	movq	%rdx, %rcx
	movq	32(%rbp), %rdx
	addq	%rcx, %rdx
	movq	%rdx, %rcx
	call	*%rax
	nop
	leaq	56(%rsp), %rsp
	popq	%rbx
	popq	%rbp
	ret
	.seh_endproc
	.globl	_Z4call12UniversalPtr22UniversalMemberFuncPtr
	.def	_Z4call12UniversalPtr22UniversalMemberFuncPtr;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z4call12UniversalPtr22UniversalMemberFuncPtr
_Z4call12UniversalPtr22UniversalMemberFuncPtr:
.LFB1791:
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rbx
	.seh_pushreg	%rbx
	leaq	-104(%rsp), %rsp
	.seh_stackalloc	104
	leaq	96(%rsp), %rbp
	.seh_setframe	%rbp, 96
	.seh_endprologue
	movq	%rcx, 32(%rbp)
	movq	%rdx, %rbx
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	%rax, -48(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-48(%rbp), %rax
	movq	-40(%rbp), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	leaq	-64(%rbp), %rdx
	leaq	-32(%rbp), %rax
	movq	%rax, %rcx
	call	_ZN22UniversalMemberFuncPtrC1IM6ObjectFvvEEET_
	movq	32(%rbp), %rdx
	leaq	-8(%rbp), %rax
	movq	%rax, %rcx
	call	_ZN12UniversalPtrC1IPvEET_
	movq	-32(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	leaq	-64(%rbp), %rax
	movq	-8(%rbp), %rcx
	movq	%rax, %rdx
	call	_Z4call12UniversalPtr22UniversalMemberFuncPtr
	nop
	leaq	104(%rsp), %rsp
	popq	%rbx
	popq	%rbp
	ret
	.seh_endproc
	.def	__main;	.scl	2;	.type	32;	.endef
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
.LFB1792:
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	leaq	-128(%rsp), %rsp
	.seh_stackalloc	128
	leaq	128(%rsp), %rbp
	.seh_setframe	%rbp, 128
	.seh_endprologue
	call	__main
	leaq	16+_ZTV1A(%rip), %rax
	movq	%rax, -72(%rbp)
	leaq	_ZN1A4testEv(%rip), %rax
	movq	%rax, %r12
	movl	$0, %r13d
	movq	%r12, -96(%rbp)
	movq	%r13, -88(%rbp)
	leaq	-96(%rbp), %rdx
	leaq	-64(%rbp), %rax
	movq	%rax, %rcx
	call	_ZN22UniversalMemberFuncPtrC1IM1AFvvEEET_
	leaq	-72(%rbp), %rdx
	leaq	-40(%rbp), %rax
	movq	%rax, %rcx
	call	_ZN12UniversalPtrC1IP1AEET_
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	leaq	-96(%rbp), %rax
	movq	-40(%rbp), %rcx
	movq	%rax, %rdx
	call	_Z4call12UniversalPtr22UniversalMemberFuncPtr
	movl	$1, %esi
	movl	$0, %edi
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	leaq	-96(%rbp), %rdx
	leaq	-32(%rbp), %rax
	movq	%rax, %rcx
	call	_ZN22UniversalMemberFuncPtrC1IM1AFvvEEET_
	leaq	-72(%rbp), %rdx
	leaq	-8(%rbp), %rax
	movq	%rax, %rcx
	call	_ZN12UniversalPtrC1IP1AEET_
	movq	-32(%rbp), %rax
	movq	-24(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	leaq	-96(%rbp), %rax
	movq	-8(%rbp), %rcx
	movq	%rax, %rdx
	call	_Z4call12UniversalPtr22UniversalMemberFuncPtr
	movl	$0, %eax
	leaq	128(%rsp), %rsp
	popq	%rsi
	popq	%rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	ret
	.seh_endproc
	.section	.text$_ZN12UniversalPtrC1IPvEET_,"x"
	.linkonce discard
	.align 2
	.globl	_ZN12UniversalPtrC1IPvEET_
	.def	_ZN12UniversalPtrC1IPvEET_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN12UniversalPtrC1IPvEET_
_ZN12UniversalPtrC1IPvEET_:
.LFB2061:
	pushq	%rbp
	.seh_pushreg	%rbp
	movq	%rsp, %rbp
	.seh_setframe	%rbp, 0
	.seh_endprologue
	movq	%rcx, 16(%rbp)
	movq	%rdx, 24(%rbp)
	movq	16(%rbp), %rax
	movq	24(%rbp), %rdx
	movq	%rdx, (%rax)
	nop
	popq	%rbp
	ret
	.seh_endproc
	.section	.text$_ZN22UniversalMemberFuncPtrC1IM6ObjectFvvEEET_,"x"
	.linkonce discard
	.align 2
	.globl	_ZN22UniversalMemberFuncPtrC1IM6ObjectFvvEEET_
	.def	_ZN22UniversalMemberFuncPtrC1IM6ObjectFvvEEET_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN22UniversalMemberFuncPtrC1IM6ObjectFvvEEET_
_ZN22UniversalMemberFuncPtrC1IM6ObjectFvvEEET_:
.LFB2064:
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rbx
	.seh_pushreg	%rbx
	leaq	-24(%rsp), %rsp
	.seh_stackalloc	24
	leaq	16(%rsp), %rbp
	.seh_setframe	%rbp, 16
	.seh_endprologue
	movq	%rcx, 32(%rbp)
	movq	%rdx, %rbx
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	%rax, -16(%rbp)
	movq	%rdx, -8(%rbp)
	nop
	leaq	24(%rsp), %rsp
	popq	%rbx
	popq	%rbp
	ret
	.seh_endproc
	.section	.text$_ZN12UniversalPtrC1IP1AEET_,"x"
	.linkonce discard
	.align 2
	.globl	_ZN12UniversalPtrC1IP1AEET_
	.def	_ZN12UniversalPtrC1IP1AEET_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN12UniversalPtrC1IP1AEET_
_ZN12UniversalPtrC1IP1AEET_:
.LFB2067:
	pushq	%rbp
	.seh_pushreg	%rbp
	movq	%rsp, %rbp
	.seh_setframe	%rbp, 0
	.seh_endprologue
	movq	%rcx, 16(%rbp)
	movq	%rdx, 24(%rbp)
	movq	16(%rbp), %rax
	movq	24(%rbp), %rdx
	movq	%rdx, (%rax)
	nop
	popq	%rbp
	ret
	.seh_endproc
	.section	.text$_ZN22UniversalMemberFuncPtrC1IM1AFvvEEET_,"x"
	.linkonce discard
	.align 2
	.globl	_ZN22UniversalMemberFuncPtrC1IM1AFvvEEET_
	.def	_ZN22UniversalMemberFuncPtrC1IM1AFvvEEET_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZN22UniversalMemberFuncPtrC1IM1AFvvEEET_
_ZN22UniversalMemberFuncPtrC1IM1AFvvEEET_:
.LFB2070:
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rbx
	.seh_pushreg	%rbx
	leaq	-24(%rsp), %rsp
	.seh_stackalloc	24
	leaq	16(%rsp), %rbp
	.seh_setframe	%rbp, 16
	.seh_endprologue
	movq	%rcx, 32(%rbp)
	movq	%rdx, %rbx
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	%rax, -16(%rbp)
	movq	%rdx, -8(%rbp)
	nop
	leaq	24(%rsp), %rsp
	popq	%rbx
	popq	%rbp
	ret
	.seh_endproc
	.globl	_ZTV1A
	.section	.rdata$_ZTV1A,"dr"
	.linkonce same_size
	.align 8
_ZTV1A:
	.quad	0
	.quad	_ZTI1A
	.quad	_ZN1A5test2Ev
	.globl	_ZTI1A
	.section	.rdata$_ZTI1A,"dr"
	.linkonce same_size
	.align 8
_ZTI1A:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS1A
	.quad	_ZTI6Object
	.globl	_ZTS1A
	.section	.rdata$_ZTS1A,"dr"
	.linkonce same_size
_ZTS1A:
	.ascii "1A\0"
	.text
	.def	__tcf_0;	.scl	3;	.type	32;	.endef
	.seh_proc	__tcf_0
__tcf_0:
.LFB2311:
	pushq	%rbp
	.seh_pushreg	%rbp
	movq	%rsp, %rbp
	.seh_setframe	%rbp, 0
	leaq	-32(%rsp), %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	_ZStL8__ioinit(%rip), %rax
	movq	%rax, %rcx
	call	_ZNSt8ios_base4InitD1Ev
	nop
	leaq	32(%rsp), %rsp
	popq	%rbp
	ret
	.seh_endproc
	.def	_Z41__static_initialization_and_destruction_0ii;	.scl	3;	.type	32;	.endef
	.seh_proc	_Z41__static_initialization_and_destruction_0ii
_Z41__static_initialization_and_destruction_0ii:
.LFB2310:
	pushq	%rbp
	.seh_pushreg	%rbp
	movq	%rsp, %rbp
	.seh_setframe	%rbp, 0
	leaq	-32(%rsp), %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movl	%ecx, 16(%rbp)
	movl	%edx, 24(%rbp)
	cmpl	$1, 16(%rbp)
	jne	.L16
	cmpl	$65535, 24(%rbp)
	jne	.L16
	leaq	_ZStL8__ioinit(%rip), %rax
	movq	%rax, %rcx
	call	_ZNSt8ios_base4InitC1Ev
	leaq	__tcf_0(%rip), %rax
	movq	%rax, %rcx
	call	atexit
.L16:
	nop
	leaq	32(%rsp), %rsp
	popq	%rbp
	ret
	.seh_endproc
	.globl	_ZTI6Object
	.section	.rdata$_ZTI6Object,"dr"
	.linkonce same_size
	.align 8
_ZTI6Object:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS6Object
	.globl	_ZTS6Object
	.section	.rdata$_ZTS6Object,"dr"
	.linkonce same_size
	.align 8
_ZTS6Object:
	.ascii "6Object\0"
	.text
	.def	_GLOBAL__sub_I__Z4callP6ObjectMS_FvvE;	.scl	3;	.type	32;	.endef
	.seh_proc	_GLOBAL__sub_I__Z4callP6ObjectMS_FvvE
_GLOBAL__sub_I__Z4callP6ObjectMS_FvvE:
.LFB2312:
	pushq	%rbp
	.seh_pushreg	%rbp
	movq	%rsp, %rbp
	.seh_setframe	%rbp, 0
	leaq	-32(%rsp), %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movl	$65535, %edx
	movl	$1, %ecx
	call	_Z41__static_initialization_and_destruction_0ii
	nop
	leaq	32(%rsp), %rsp
	popq	%rbp
	ret
	.seh_endproc
	.section	.ctors,"w"
	.align 8
	.quad	_GLOBAL__sub_I__Z4callP6ObjectMS_FvvE
	.ident	"GCC: (GNU) 11.1.0"
	.def	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc;	.scl	2;	.type	32;	.endef
	.def	_ZNSolsEPFRSoS_E;	.scl	2;	.type	32;	.endef
	.def	_ZNSt8ios_base4InitD1Ev;	.scl	2;	.type	32;	.endef
	.def	_ZNSt8ios_base4InitC1Ev;	.scl	2;	.type	32;	.endef
	.def	atexit;	.scl	2;	.type	32;	.endef
	.section	.rdata$.refptr._ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, "dr"
	.globl	.refptr._ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	.linkonce	discard
.refptr._ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_:
	.quad	_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	.section	.rdata$.refptr._ZSt4cout, "dr"
	.globl	.refptr._ZSt4cout
	.linkonce	discard
.refptr._ZSt4cout:
	.quad	_ZSt4cout
