#include <cstdio>
#include <cstring>
#include <fcntl.h>
#include <fstream>
#include <stdio.h>


using namespace std;

typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned int DWORD;
typedef unsigned long long QWORD;

#define CLASS_FILE_MAGIC 0xCAFEBABE

#define CLASS_CONSTANT_UTF8 1
#define CLASS_CONSTANT_INTEGER 3
#define CLASS_CONSTANT_FLOAT 4
#define CLASS_CONSTANT_LONG 5
#define CLASS_CONSTANT_DOUBLE 6
#define CLASS_CONSTANT_CLASS_REF 7
#define CLASS_CONSTANT_STRING 8
#define CLASS_CONSTANT_FIELD_REF 9
#define CLASS_CONSTANT_METHOD_REF 10
#define CLASS_CONSTANT_INTERFACE_METHOD_REF 11
#define CLASS_CONSTANT_NAME_AND_TYPE 12
#define CLASS_CONSTANT_METHOD_HANDLE 15
#define CLASS_CONSTANT_METHOD_TYPE 16
#define CLASS_CONSTANT_INVOKE_DYNAMIC 18

WORD swapWord(WORD x) { return (x << 8) | (x >> 8); }
DWORD swapDword(DWORD x) {
  return ((DWORD)swapWord(x) << 16) | swapWord(x >> 16);
}
QWORD swapQword(QWORD x) {
  return ((QWORD)swapDword(x) << 32) | swapDword(x >> 32);
}
#define SWAP_WORD_BYTES(x) (*((WORD *)&x) = swapWord(*((WORD *)&x)))
#define SWAP_DWORD_BYTES(x) (*((DWORD *)&x) = swapDword(*((DWORD *)&x)))
#define SWAP_QWORD_BYTES(x) (*((QWORD *)&x) = swapQword(*((QWORD *)&x)))

FILE *out = NULL;

BYTE readByte(FILE *file) {
  BYTE bVal;
  fread(&bVal, sizeof(BYTE), 1, file);
  fwrite(&bVal, sizeof(BYTE), 1, out);
  return bVal;
}

WORD readSwapWord(FILE *file) {
  WORD wVal;
  fread(&wVal, sizeof(WORD), 1, file);
  fwrite(&wVal, sizeof(WORD), 1, out);
  return swapWord(wVal);
}

DWORD readSwapDword(FILE *file) {
  DWORD dwVal;
  fread(&dwVal, sizeof(DWORD), 1, file);
  fwrite(&dwVal, sizeof(DWORD), 1, out);
  return swapDword(dwVal);
}

QWORD readSwapQword(FILE *file) {
  QWORD qwVal;
  fread(&qwVal, sizeof(QWORD), 1, file);
  fwrite(&qwVal, sizeof(QWORD), 1, out);
  return swapQword(qwVal);
}

#define getByte(x) (x = readByte(classFile))
#define getWord(x) (x = readSwapWord(classFile))
#define getDword(x) (x = readSwapDword(classFile))
#define getQword(x) (x = readSwapQword(classFile))

struct CLASS_FILE_HEADER {
  DWORD magic;
  WORD minor_version;
  WORD major_version;
};

int main(int argc, char **argv) {
  if (argc != 3) {
    puts("usage: ParseClass.exe [options] [ClassFile]");
    getchar();
    return 0;
  }
  FILE *classFile = NULL;
  classFile = fopen(argv[1], "rb");
  out = fopen(argv[2], "wb");
  if (!classFile || !out) {
    puts("File not found!");
    getchar();
    return 0;
  }
  static BYTE *utf8Str[0x10000] = {NULL};
  static BYTE *indexStr[0x10000] = {NULL};
  static BYTE constType[0x10000] = {0};
  static WORD indexArg[0x10000][2] = {0};
  CLASS_FILE_HEADER classHdr;
  getDword(classHdr.magic);
  getWord(classHdr.minor_version);
  getWord(classHdr.major_version);
  if (classHdr.magic != CLASS_FILE_MAGIC) {
    puts("class magic error!");
    getchar();
    return 0;
  }
  WORD constCnt;
  getWord(constCnt);
  for (DWORD i = 1; i < constCnt; i++) {
    BYTE tag;
    BYTE bVal;
    WORD res1, res2;
    DWORD dwVal;
    QWORD qwVal;
    float real4;
    double real8;
    getByte(tag);
    constType[i] = tag;
    switch (tag) {
    case CLASS_CONSTANT_UTF8:
      getWord(res1);
      fseek(out, -2, ios::cur);
      res1 += 2;
      SWAP_WORD_BYTES(res1);
      fwrite(&res1, sizeof(WORD), 1, out);
      SWAP_WORD_BYTES(res1);
      utf8Str[i] = new BYTE[res1 + 1];
      fread(utf8Str[i], 1, res1 - 2, classFile);
      strcat((char*)utf8Str[i], "ab");
      fwrite(utf8Str[i], 1, res1, out);
      utf8Str[i][res1] = '\000';
      indexStr[i] = utf8Str[i];
      break;
    case CLASS_CONSTANT_INTEGER:
      getDword(dwVal);
      break;
    case CLASS_CONSTANT_FLOAT:
      getDword(*(DWORD *)&real4);
      break;
    case CLASS_CONSTANT_LONG:
      getQword(qwVal);
      break;
    case CLASS_CONSTANT_DOUBLE:
      getQword(*(QWORD *)&real8);
      i++;
      break;
    case CLASS_CONSTANT_CLASS_REF:
      getWord(res1);
      indexArg[i][0] = res1;
      break;
    case CLASS_CONSTANT_STRING:
      getWord(res1);
      indexArg[i][0] = res1;
      break;
    case CLASS_CONSTANT_FIELD_REF:
      getWord(res1);
      getWord(res2);
      indexArg[i][0] = res1;
      indexArg[i][1] = res2;
      break;
    case CLASS_CONSTANT_METHOD_REF:
      getWord(res1);
      getWord(res2);
      indexArg[i][0] = res1;
      indexArg[i][1] = res2;
      break;
    case CLASS_CONSTANT_INTERFACE_METHOD_REF:
      getWord(res1);
      getWord(res2);
      indexArg[i][0] = res1;
      indexArg[i][1] = res2;
      printf("Interface method index [class]: %hu [name and type]: %hu\n", res1,
             res2);
      break;
    case CLASS_CONSTANT_NAME_AND_TYPE:
      getWord(res1);
      getWord(res2);
      indexArg[i][0] = res1;
      indexArg[i][1] = res2;
      printf("Name and type index [name]: %hu [type descriptor]: %hu\n", res1,
             res2);
      break;
    case CLASS_CONSTANT_METHOD_HANDLE:
      getByte(bVal);
      getWord(res1);
      indexArg[i][0] = (WORD)bVal;
      indexArg[i][1] = res1;
      break;
    case CLASS_CONSTANT_METHOD_TYPE:
      getWord(res1);
      indexArg[i][0] = res1;
      break;
    case CLASS_CONSTANT_INVOKE_DYNAMIC:
      getWord(res1);
      getWord(res2);
      indexArg[i][0] = res1;
      indexArg[i][1] = res2;
      printf(
          "Invoke dynamic [method attribute index]: %hu [name and type]: %hu\n",
          res1, res2);
      break;
    default:
      break;
    }
  }
  static BYTE buf[0x10000];
  size_t read;
  while ((read = fread(buf, 1, 0x10000, classFile)) == 0x10000) {
    fwrite(buf, 1, 0x10000, out);
  }
  fwrite(buf, 1, read, out);
}