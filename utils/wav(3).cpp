#include <iostream>
#include <fstream>
#include <math.h>
#include <windows.h>
using namespace std;
typedef char data_t;
class WaveMaker{
protected:
    struct WavHead{
        char RIFF[4];    //头部分那个RIFF
        long int size0;//存的是后面所有文件大小
        char WAVE[4];
        char FMT[4];
        long int size1;//存的是fmt保存的大小，包含这之后，data前面几个，共16个
        short int fmttag;
        short int channel;
        long int samplespersec;//每秒采样数，用的是11025
        long int bytepersec;
        short int blockalign;
        short int bitpersamples;
        char DATA[4];
        long int size2;//剩下文件大小，也就是声音采样是大小，因为是一秒钟的内容，那么就是11025了。
    };
    long Frequency;
    data_t* Data;
    long DataLen;
public:
    WaveMaker(double Sec,long Freq):Frequency(Freq){
        DataLen=(long)ceil(Sec*Freq);
        Data=new data_t[DataLen];
        for(long i=0;i<DataLen;i++)
            Data[i]=(sizeof(data_t)==1?0x80:0x8000);
    }
    WaveMaker(const char* Filename){
        FILE* f=fopen(Filename,"rb");
        WavHead head;
        fread(&head,sizeof(WavHead),1,f);
        Frequency=head.samplespersec;
        DataLen=head.size1/(head.blockalign);
        Data=new data_t[DataLen];
        data_t* File=new data_t[head.size1];
        fread(File,sizeof(data_t),head.size1/sizeof(data_t),f);
        if(sizeof(data_t)==1){
            if(head.blockalign/head.channel==2)
                for(long i=0;i<DataLen;i++)
                    Data[i]=File[i*head.blockalign+1l];
            else
                for(long i=0;i<DataLen;i++)
                    Data[i]=File[i*head.blockalign];
        }else{
            if(head.blockalign/head.channel==2)
                for(long i=0;i<DataLen;i++)
                    Data[i]=File[i*head.channel];
            else{
                if(head.channel==1)
                    for(long i=0;i<DataLen;i++)
                        Data[i]=File[i]&0xFF00;
                else
                    for(long i=0;i<DataLen;i++)
                        Data[i]=(File[i>>1]&(i&1==0?0x00FF:0xFF00))*(i&1==0?0x100:0x1);
            }
        }
        delete[] File;
    }
    ~WaveMaker(){
        delete[] Data;
    }
    bool Play(bool async=false){
        WavHead head={{'R','I','F','F'},0,{'W','A','V','E'},{'f','m','t',' '},16,
                1,1,Frequency,Frequency,1,8,{'d','a','t','a'},0};
        head.size0=DataLen+24;
        head.size2=DataLen;
        char* Memory=new char[sizeof(head)+sizeof(data_t)*DataLen];
        memcpy(Memory,&head,sizeof(head));
        memcpy(Memory+sizeof(head),Data,sizeof(data_t)*DataLen);
        bool suc=!PlaySoundA(Memory,NULL,(async?SND_ASYNC:SND_SYNC)|SND_NODEFAULT|SND_MEMORY);
        delete[] Memory;
        return suc;
    }
    bool CreateWaveFile(const char* FileName){
        WavHead head={{'R','I','F','F'},0,{'W','A','V','E'},{'f','m','t',' '},16,
                1,1,Frequency,Frequency,1,8,{'d','a','t','a'},0};
        head.size0=DataLen*sizeof(data_t)+24;
        head.size2=DataLen*sizeof(data_t);
        ofstream ocout;
        ocout.open(FileName,ios::out|ios::binary);
        if(ocout.fail()){
            ocout.close();
            return false;
        }
        ocout.write((char*)&head,sizeof(head));
        ocout.write(Data,DataLen*sizeof(data_t));
        ocout.close();
        return true;
    }
    void CreateWave(double start,double len,double scale,double freq,double k){
        long s=(long)(Frequency*start);
        long l=(long)(Frequency*len);
        for(long i=0;i<l;i++)
            Data[s+i]+=scale*sin((2.0*M_PI*freq*i)/Frequency)*exp((k*i)/Frequency);
    }
    void CreateWave(double start,double len,double scale,double freq){
        long s=(long)(Frequency*start);
        long l=(long)(Frequency*len);
        for(long i=0;i<l;i++)
            Data[s+i]+=scale*sin((2.0*M_PI*freq*i)/Frequency);
    }
    void AddWave(double start,WaveMaker& wav){
        long s=(long)(Frequency*start);
        for(int i=0;i<wav.DataLen;i++)
            Data[s+i]+=(wav.Data[i]-(sizeof(data_t)==1?0x80:0x8000));
    }
    data_t& operator[](int x){
        return Data[x];
    }
    data_t& operator[](long x){
        return Data[x];
    }
    data_t& operator[](double x){
        return Data[(long)(Frequency*x)];
    }
    data_t* GetData(long offset){
        return Data+offset;
    }
    data_t* GetData(double offset){
        return Data+(long)(Frequency*offset);
    }
    data_t* GetData(){
        return Data;
    }
};
int main() {
    WaveMaker a(1.0,44100);
    a.Play();
    a.CreateWaveFile("123.wav");
    return 0;
}