#include <iostream>
#include <winsock2.h>
#include "Socket.h"

using namespace std;

int main(){
	while(true){
		Socket s(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if(s.Connect("127.0.0.1", 2333) == SOCKET_ERROR){
			puts("Connect failed.");
			Sleep(200);
			continue;
		}
		puts("Connect success.");
		HWND hwnd = GetDesktopWindow();
		RECT rect;
		HDC hDC = GetWindowDC(hwnd);
		LONG nWidth, nHeight;
		nWidth = GetDeviceCaps(hDC, HORZRES);
		nHeight = GetDeviceCaps(hDC, VERTRES);

		HBITMAP hBitmap = CreateCompatibleBitmap(hDC, nWidth, nHeight);
		HDC hMemDC = CreateCompatibleDC(hDC);
		HBITMAP hBitTemp = (HBITMAP) SelectObject(hMemDC, hBitmap);
		BitBlt(hMemDC, 0, 0, nWidth, nHeight, hDC, 0, 0, SRCCOPY);
		hBitmap = (HBITMAP) SelectObject(hMemDC, hBitTemp);
		BITMAPINFO info;
		info.bmiHeader.biSize = sizeof(info.bmiHeader);
		BYTE* buf = NULL;
		DeleteObject(hBitTemp);
		DeleteObject(hMemDC);
		GetDIBits(hDC, hBitmap, 0, nHeight, NULL, &info, DIB_RGB_COLORS);
		buf = new BYTE[info.bmiHeader.biSizeImage];
		GetDIBits(hDC, hBitmap, 0, nHeight, buf, &info, DIB_RGB_COLORS);

		info.bmiHeader.biHeight = nHeight;
		info.bmiHeader.biWidth = nWidth;

		if(s.Send((char*)&info, sizeof(BITMAPINFO)) == SOCKET_ERROR ||
		   s.Send((char*)buf, info.bmiHeader.biSizeImage) == SOCKET_ERROR){
			puts("Send data failed.");
		}else{
			puts("Send data success.");
		}
		s.Close();
		delete[] buf;
		Sleep(5000);
	}
	return 0;
}

