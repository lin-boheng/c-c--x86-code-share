#include <fstream>
#include <stdio.h>
#include <windows.h>
#include <wingdi.h>

DWORD calcSize(DWORD height, DWORD width, int BitCount){
    if(BitCount == 16){
        if(width & 1)
            width++;
        return height * width * 2;
    }else{
        return EOF;
    }
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam){
    switch(Msg){
        case WM_QUIT:
            PostQuitMessage(0);
            break;
    }
    return DefWindowProcA(hWnd, Msg, wParam, lParam);
}

int WinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,
    int nCmdShow
){
    HWND hwnd = GetDesktopWindow();
    HDC hdc = GetDC(hwnd);
    HDC hMem = CreateCompatibleDC(hdc);
    static BITMAPINFO bitmapInfo;
    static void* lpBitmap;
    RtlZeroMemory(&bitmapInfo, sizeof(BITMAPINFO));
    int BitCount = GetDeviceCaps(hdc, BITSPIXEL);
    static int Height = GetDeviceCaps(hdc, VERTRES);
    static int Width = GetDeviceCaps(hdc, HORZRES);
    int NumColors = GetDeviceCaps(hdc, NUMCOLORS);
    bitmapInfo.bmiHeader.biSize = 40;
    bitmapInfo.bmiHeader.biWidth = Width;
    bitmapInfo.bmiHeader.biHeight = Height;
    bitmapInfo.bmiHeader.biPlanes = 1;
    bitmapInfo.bmiHeader.biBitCount = 16;
    const DWORD imageSize = calcSize(Height, Width, 16);
    HBITMAP hBitmap = CreateDIBSection(hdc, &bitmapInfo, DIB_RGB_COLORS, &lpBitmap, NULL, 0);
    HBITMAP hNew = (HBITMAP)SelectObject(hMem, hBitmap);
    void* imagePtr = VirtualAlloc(NULL, imageSize * 2, MEM_COMMIT, PAGE_READWRITE);
    PrintWindow(hwnd, hMem, 0);
    BitBlt(hMem, 0, 0, Width, Height, hdc, 0, 0, SRCCOPY);
    DeleteObject(hBitmap);
    DeleteDC(hdc);
    FILE* f = fopen("desktop.bmp", "wb");
    BITMAPFILEHEADER fileHdr;
    const DWORD offset = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
    RtlZeroMemory(&fileHdr, sizeof(BITMAPFILEHEADER));
    fileHdr.bfType = 'B' + ('M' << 8);
    fileHdr.bfSize = offset + imageSize;
    fileHdr.bfOffBits = offset;
    fwrite(&fileHdr, sizeof(BITMAPFILEHEADER), 1, f);
    fwrite(&bitmapInfo.bmiHeader, sizeof(BITMAPINFOHEADER), 1, f);
    fwrite(lpBitmap, imageSize, 1, f);
    fclose(f);
    VirtualFree(imagePtr, 0, MEM_RELEASE);
    EnumChildWindows(hwnd, [](HWND hWnd, LPARAM lParam)->BOOL{
        HDC hdc = GetWindowDC(hWnd);
        //SetDIBitsToDevice(hdc, 0, 0, Width, Height, 0, 0, 0, Height, lpBitmap, &bitmapInfo, DIB_RGB_COLORS);
        StretchDIBits(hdc, 0, 0, GetDeviceCaps(hdc, HORZRES), GetDeviceCaps(hdc, VERTRES), 0, 0, Width, Height, lpBitmap, &bitmapInfo, DIB_RGB_COLORS, NOTSRCCOPY);
        RECT rect;
        rect.top = 200;
        rect.bottom = 250;
        rect.left = 200;
        rect.right = 500;
        DrawTextW(hdc, L"你好，世界！", EOF, &rect, DT_CENTER);
        return TRUE;
    }, (LPARAM)NULL);
    return 0;
}