        org 0x7c00
        jmp start
new_int_0x70:
        push ax
        push bx
        push cx
        push dx
        push es
.w0:
        mov al,0x0a
        or al,0x80
        out 0x70,al
        in al,0x71          
        test al,0x80
        jnz .w0
        xor al,al
        or al,0x80
        out 0x70,al
        in al,0x71
        push ax
        mov al,2
        or al,0x80
        out 0x70,al
        in al,0x71
        push ax
        mov al,4
        or al,0x80
        out 0x70,al
        in al,0x71
        push ax
        mov al,0x0c
        out 0x70,al
        in al,0x71
        mov ax,0xb800
        mov es,ax
        pop ax
        call bcd_to_ascii
        mov bx,12*160+36*2
        mov [es:bx],ah
        mov [es:bx+2],al
        mov byte [es:bx+4],':'      
        not byte [es:bx+5]
        pop ax
        call bcd_to_ascii
        mov [es:bx+6],ah
        mov [es:bx+8],al
        mov byte [es:bx+10],':'     
        not byte [es:bx+11]
        pop ax
        call bcd_to_ascii
        mov [es:bx+12],ah
        mov [es:bx+14],al
        mov al,0x20
        out 0xa0,al
        out 0x20,al
        pop es
        pop dx
        pop cx
        pop bx
        pop ax
        iret                    
bcd_to_ascii:
    mov ah,al
    and al,0x0f         
    add al,0x30
    shr ah,4
    and ah,0x0f
    add ah,0x30
    ret
start:
    mov ax,cs
    mov ss,ax
    mov ds,ax
    mov sp,0x7c00
    mov bp,sp
    mov bx,init_msg
    call put_string
    mov al,0x70
    mov bl,4
    mul bl
    mov bx,ax
    cli
    push es
    mov ax,0x0000
    mov es,ax
    mov word [es:bx],new_int_0x70
    mov word [es:bx+2],cs
    pop es

    mov al,0x8b
    out 0x70,al
    mov al,0x12
    out 0x71,al
    mov al,0x0c
    out 0x70,al
    in al,0x71
    in al,0xa1
    and al,0xfe
    out 0xa1,al
    sti
    mov cx,0xb800
    mov ds,cx
    mov byte [12*160+32*2],'@'
    xor dx,dx
    .idle:
            hlt
            not byte [12*160+32*2+1]
            inc dx
            mov bl,dh
            call .char_code
            mov byte [12*160+26*2],ah
            mov byte [12*160+27*2],al
            mov bl,dl
            call .char_code
            mov byte [12*160+28*2],ah
            mov byte [12*160+29*2],al
            jmp .idle
    .char_code:
            mov al,bl
            and al,0x0f
            cmp al,0x0A
            jl .char_code_less_al
            add al,'A'-10
            jmp .char_code_next
    .char_code_less_al:
            add al,'0'
    .char_code_next:
            mov ah,bl
            mov cl,4
            shr ah,cl
            cmp ah,0x0A
            jl .char_code_less_ah
            add ah,'A'-10
            jmp .char_code_ret
    .char_code_less_ah:
            add ah,'0'
    .char_code_ret:
            ret
put_string:
         mov cl,[bx]
         or cl,cl
         jz .exit
         call put_char
         inc bx
         jmp put_string
   .exit:
         ret
put_char:
             push ax
             push bx
             push cx
             push dx
             push ds
             push es
             mov dx,0x3d4
             mov al,0x0e
             out dx,al
             mov dx,0x3d5
             in al,dx
             mov ah,al
             mov dx,0x3d4
             mov al,0x0f
             out dx,al
             mov dx,0x3d5
             in al,dx
             mov bx,ax
             cmp cl,0x0d
             jnz .put_0a
             mov ax,bx
             mov bl,80                       
             div bl
             mul bl
             mov bx,ax
             jmp .set_cursor
.put_0a:
             cmp cl,0x0a
             jnz .put_other
             add bx,80
             jmp .roll_screen
.put_other:
             mov ax,0xb800
             mov es,ax
             shl bx,1
             mov [es:bx],cl
             shr bx,1
             add bx,1
.roll_screen:
             cmp bx,2000
             jl .set_cursor
             mov ax,0xb800
             mov ds,ax
             mov es,ax
             cld
             mov si,0xa0
             mov di,0x00
             mov cx,1920
             rep movsw
             mov bx,3840
             mov cx,80
.cls:
             mov word[es:bx],0x0720
             add bx,2
             loop .cls
             mov bx,1920
.set_cursor:
             mov dx,0x3d4
             mov al,0x0e
             out dx,al
             mov dx,0x3d5
             mov al,bh
             out dx,al
             mov dx,0x3d4
             mov al,0x0f
             out dx,al
             mov dx,0x3d5
             mov al,bl
             out dx,al
             pop es
             pop ds
             pop dx
             pop cx
             pop bx
             pop ax
             ret
init_msg: db 'Starting...',0x0d,0x0a,0
    times 510-($-$$) db 0
    dw 0xaa55