    org 7c00h
    cli
    ;设置栈
    mov sp,1000h
    mov bp,sp
    ;设置段
    mov ax,cs
    mov ds,ax
    mov ss,ax
    ;注册中断(00h)除数为0时的异常中断
    ;mov ax,0000h
    ;mov es,ax
    ;mov bx,00h*4
    ;mov word [es:bx],int_00
    ;mov word [es:bx+2],cs
    sti
    ;int 00h

    mov ax,0b800h
    ;mov ax,0e00h;图形模式的文字段
    mov es,ax
    mov di,00h
    mov cx,500h
cls:
    mov byte [es:di],00h
    add di,02h
    loop cls
    
    xor bx,bx
    mov byte [es:bx],30h
    
    ;mov dx,43h
    ;mov al,01110111b
    ;01(counter 1 select)
    ;11(r/w counter bits 0-7 first,then 8-15)
    ;x11(mode 3 select - square wave generator)
    ;1(BCD counter)
    ;out dx,al
    ;mov dx,40h;counter 0,counter divisor
    ;mov ax,2500h;2.5MHz/2500=1000Hz
    ;out dx,ax
    
    ;mov dx,21h
    ;in al,dx
    ;and al,0feh
    ;out dx,al
    ;enable timer interrupt

    ;磁头8bit 柱面10bit 扇区6bit 扇区512bytes
    ;8G
    mov al,0a0h;01f6h (101)[固定]0(硬盘)0000(磁头)
    mov dx,01f6h
    out dx,al
    mov al,1;01f2h 读写扇区数量
    mov dx,01f2h
    out dx,al
    mov al,2;01f3h 读写扇区号
    inc dx
    out dx,al
    mov al,0;01f4h 柱面低8位
    inc dx
    out dx,al
    mov al,0;01f5h 柱面高2位
    inc dx
    out dx,al
    mov al,20h;01f7h 读命令
    mov dx,01f7h
    out dx,al

    xor cx,cx
    jmp .read_wait
    .display_cx:
        push ax
        mov al,ch
        call .bcd
        mov [es:bx],ah
        mov [es:bx+2],al
        mov al,cl
        call .bcd
        mov [es:bx+4],ah
        mov [es:bx+6],al
        pop ax
        ret
    .bcd:
        push cx
        mov ah,al
        and ax,0f00fh
        mov cl,4
        shr ah,cl
        cmp ah,10
        jb .below_ah
        jmp .above_ah
        .below_ah:
            add ah,30h
            jmp .end_ah
        .above_ah:
            add ah,'A'-10
        .end_ah:
        cmp al,10
        jb .below_al
        jmp .above_al
        .below_al:
            add al,30h
            jmp .end_al
        .above_al:
            add al,'A'-10
        .end_al:
        pop cx
        ret
    .read_wait:
        in al,dx
        ;01f7h 操作后状态
        ;第7位          控制器忙碌  
        ;第6位          磁盘驱动器准备好了  
        ;第5位          写入错误  
        ;第4位          搜索完成  
        ;第3位          为1时扇区缓冲区没有准备好  
        ;第2位          是否正确读取磁盘数据  
        ;第1位          磁盘每转一周将此位设为1
        ;第0位          之前的命令因发生错误而结束
        test al,08h
        jz .read_wait
    ;mov ax,0013h;这两行使显示模式为图形模式 13h是其中一种
    ;int 10h
    ;push es
    ;mov ax,0a000h;图形模式所处段为0a000h
    ;mov es,ax
    mov di,00a0h
    mov cx,100h
    mov dx,01f0h
    rep insw;(insw)将dx端口读入es:di并di++,(rep)cx--,cx==0时结束
    ;pop es
    ;0xA1号端口可以控制一些中断的开关 读/写
    ;bit 7 = 0  reserved 
    ;bit 6 = 0  enable fixed disk interrupt 
    ;bit 5 = 0  enable coprocessor exception interrupt 
    ;bit 4 = 0  enable mouse interrupt 
    ;bit 3 = 0  reserved 
    ;bit 2 = 0  reserved 
    ;bit 1 = 0  enable redirect cascade 
    ;bit 0 = 0  enable real-time clock interrupt
    .idle:
        hlt
        inc cx
        mov bx,10h
        call .display_cx
        call int_00
        jmp .idle
int_00:
    push es
    push ax
    push bx
    push cx
    mov ax,0b800h
    mov es,ax
    xor bx,bx
    not byte [es:bx+1]
    mov al,00h
    out 70h,al
    in al,71h
    mov ah,al
    and al,0fh
    add al,30h
    mov byte [es:bx+6],al
    and ah,0f0h
    mov cl,4
    shr ah,cl
    add ah,30h
    mov byte [es:bx+4],ah
    pop cx
    pop bx
    pop ax
    pop es
    ;iret
    ret
    times 510-($-$$) db 0
    dw 0xaa55
    db "H",4,"e",2,"l",1,"l",4,"o",2," ",1,"w",4,"o",2,"r",1,"l",4,"d",2,"!",1