    org 7c00h
start:
    cli
    mov sp,0a00h
    mov bp,sp
    mov ax,cs
    mov ds,ax
    mov ss,ax
    add ax,0200h
    mov fs,ax
    call init_8253
    xor ax,ax
    mov es,ax
    mov bx,0020h
    mov word [es:bx],int_0x08
    mov word [es:bx+2],cs
    call init_8259A
    mov ax,0b800h
    mov es,ax
    xor di,di
    mov cx,07d0h
    .cls:
        mov byte [es:di],00h
        add di,0002h
        loop .cls
    sti
    .create_threads:
    cmp word [task_count],001ah
    ja .idle
    mov eax,task_00
    call CreateThread
    jmp .create_threads
    .idle:
        hlt
        jmp .create_threads
init_8253:
    mov al,00110110b
    out 43h,al
    ;base: 1.1932MHz
    mov ax,1193
    out 40h,al
    mov al,ah
    out 40h,al
    ret
init_8259A:
    mov al,00010011b
    out 20h,al
    mov al,00001000b
    out 21h,al
    mov al,00000001b
    out 21h,al
    in al,21h
    and al,11111110b
    out 21h,al
    ret
task_00:
    mov bx,01e1h
    mov cx,[task_index]
    shl cx,2
    add bx,cx
    mov cx,0010h
    .loop_label:
        not byte [es:bx]
        add bx,00a0h
        loop .loop_label
    mov cl,[task_index]
    mov eax,10h
    shl eax,cl
    call Sleep
    jmp task_00
;@param eax => cs:ip
CreateThread:
    pushfw
    cli
    push di
    mov di,[task_count]
    shl di,6
    mov [fs:di],eax
    mov word [fs:di+04h],0000001000000010b
    mov word [fs:di+06h],di
    mov word [fs:di+08h],1000h
    mov dword [fs:di+0ah],00000000h
    inc dword [task_count]
    pop di
    popfw
    ret
;@param eax => MilliSec
Sleep:
    pushfw
    cli
    push di
    mov di,[task_index]
    shl di,6
    mov dword [fs:di+0ah],eax
    pop di
    popfw
    hlt
    ret
screen_index:dw 0x0001
task_index:dd 0x00000000
task_count:dd 0x00000001
int_0x08:
    push edi
    mov di,[task_index]
    shl di,6
    mov [fs:di+10h],eax
    mov [fs:di+14h],ebx
    mov [fs:di+18h],ecx
    mov [fs:di+1ch],edx
    pop eax
    mov [fs:di+20h],eax
    mov [fs:di+24h],esi
    mov [fs:di+28h],ebp
    mov di,[screen_index]
    not byte [es:di]
    add word [screen_index],0002h
    cmp word [screen_index],0140h
    jb .skip_0x00
    mov word [screen_index],0001h
    .skip_0x00:

    mov bx,[task_index]
    mov di,bx
    shl di,6
    mov cx,5
    .pop_data:
        pop ax
        mov [fs:di],ax
        add edi,2
        loop .pop_data
    
    mov ax,0000h
    mov cx,[task_count]
    .reduce_delay:
        mov si,ax
        shl si,6
        cmp dword [fs:si+0ah],00000000h
        jz .skip_0x02
        dec dword [fs:si+0ah]
        .skip_0x02:
        inc ax
        loop .reduce_delay
    
    mov cx,[task_count]
    .find_task:
        inc bx
        cmp bx,[task_count]
        jb .skip_0x03
        xor bx,bx
        .skip_0x03:
        mov si,bx
        shl si,6
        cmp dword [fs:si+0ah],00000000h
        jz .skip_0x01
        loop .find_task
    .skip_0x01:
    
    mov [task_index],bx
    mov si,bx
    shl si,6
    mov cx,5
    add si,10
    .push_data:
        sub si,2
        mov ax,[fs:si]
        push ax
        loop .push_data

    mov si,[task_index]
    shl si,6
    mov eax,[fs:si+24h]
    push eax
    mov eax,[fs:si+10h]
    mov ebx,[fs:si+14h]
    mov ecx,[fs:si+18h]
    mov edx,[fs:si+1ch]
    mov edi,[fs:si+20h]
    mov ebp,[fs:si+28h]
    pop esi

    push ax
    mov al,20h
    out 20h,al
    pop ax

    iret
    times 510-($-$$) db 0
    dw 0xaa55
