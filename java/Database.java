import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.function.BiConsumer;

public class Database {
    private HashMap<String, String> Data;

    public Database() {
        this.Data = new HashMap<String, String>();
    }

    public boolean Contain(String s, String type) {
        return this.Data.get(type + "_" + s) != null;
    }

    public Set<String> List(String type) {
        final Set<String> res = new HashSet<String>();
        res.clear();
        this.Data.forEach(new BiConsumer<String, String>() {
            @Override
            public void accept(String key, String val) {
                if (key.substring(0, 1).equals(type))
                    res.add(val);
            }
        });
        return res;
    }

    public boolean Erase(String s, String type) {
        return this.Data.remove(type + "_" + s) != null;
    }

    public void Clear() {
        this.Data.clear();
    }

    public void Load(File f) throws IOException {
        final Scanner scanner = new Scanner(new FileInputStream(f));
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            int split = -1;
            for (int i = 1; i < line.length(); i++)
                if (line.charAt(i) == '@' && line.charAt(i - 1) != '_') {
                    split = i;
                    break;
                }
            if (split == -1)
                continue;
            this.Data.put(line.substring(0, split).replace("_@", "@").replace("_@", "_"),
                    line.substring(split + 1).replace("_@", "@").replace("_@", "_"));
        }
        scanner.close();
    }

    public void Save(File f) throws IOException {
        final PrintWriter printWriter = new PrintWriter(new FileOutputStream(f));
        // _=>__@ @=>_@ split character:'@'
        this.Data.forEach(new BiConsumer<String, String>() {
            @Override
            public void accept(String key, String val) {
                printWriter.println(
                        key.replace("_", "_@").replace("@", "_@") + "@" + val.replace("_", "_@").replace("@", "_@"));
            }
        });
        printWriter.close();
    }

    public int Qint(String s) throws NullPointerException {
        String res = this.Data.get("i_" + s);
        if (res == null)
            throw new NullPointerException();
        try {
            return Integer.parseInt(res);
        } catch (Exception e) {
            throw new NullPointerException();
        }
    }

    public boolean Qb(String s) throws NullPointerException {
        String res = this.Data.get("b_" + s);
        if (res == null)
            throw new NullPointerException();
        return Boolean.parseBoolean(res);
    }

    public String Qs(String s) throws NullPointerException {
        String res = this.Data.get("s_" + s);
        if (res == null)
            throw new NullPointerException();
        return res;
    }

    public void Sint(String s, int i) {
        this.Data.put("i_" + s, Integer.toString(i));
    }

    public void Sb(String s, boolean b) {
        this.Data.put("b_" + s, Boolean.toString(b));
    }

    public void Ss(String s, String s1) {
        this.Data.put("s_" + s, s1);
    }
}