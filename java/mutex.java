import java.util.concurrent.atomic.AtomicBoolean;

class node {
    public node next = null;
    public Thread thread;

    public node(Thread t) {
        this.thread = t;
    }
}

public class mutex {
    private volatile AtomicBoolean tag = new AtomicBoolean(false);
    private volatile AtomicBoolean sync = new AtomicBoolean(false);

    private node head = null;
    private node tail = null;

    public mutex() {
    }

    public void lock() {
        if (tag.compareAndSet(false, true)) {
            return;
        }
        while (sync.compareAndSet(true, true))
            ;
        node temp = new node(Thread.currentThread());
        temp.thread.suspend();
        if (head == null) {
            this.head = temp;
            this.tail = temp;
        } else {
            this.tail.next = temp;
            this.tail = temp;
        }
        sync.set(false);
    }

    public void unlock() {
        while (sync.compareAndSet(true, true))
            ;
        if (head == null) {
            tag.set(false);
            return;
        }
        this.head.thread.resume();
        this.head = this.head.next;
        sync.set(false);
    }

    public boolean trylock() {
        return tag.compareAndSet(false, true);
    }
}