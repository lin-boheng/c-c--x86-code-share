package sac.extension.BasicNeuralNetwork;
//TODO 注意修改

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.Scanner;

static class Utils {
    public static Random rand = new Random(System.currentTimeMillis());
    public static File file = null;
    public static Scanner scanner = null;
    public static FileOutputStream os = null;

    public static void SetFile(File f) {
        file = f;
        scanner = new Scanner(f);
        os = new FileOutputStream(f);
    }

    public static void CloseFile() {
        scanner.close();
        os.close();
        file = null;
        scanner = null;
        os = null;
    }

    public static double Square(double x) {
        return x * x;
    }

    public static boolean Rate(double x) {
        return rand.nextInt(0x40000000) < x * (double) 0x40000000;
    }

    public static double Random(double low, double high) {
        return low + (rand.nextInt(0x40000000) / (double) 0x40000000) * (high - low);
    }

    public static int ReadFileInt() {
        if (!scanner.hasNextInt())
            return -1;
        return scanner.nextInt();
    }

    public static double ReadFileDouble() {
        if (!scanner.hasNextDouble())
            return -1;
        return scanner.nextDouble();
    }

    public static void WriteFile(int x) {
        try {
            byte[] Buffer = (Integer.toString(x) + " ").getBytes();
            os.write(Buffer);
        } catch (Exception exception) {
        }
    }

    public static void WriteFile(double x) {
        try {
            byte[] Buffer = (Double.toString(x) + " ").getBytes();
            os.write(Buffer);
        } catch (Exception exception) {
        }
    }

    public static double Tanh(double x) {
        return (Math.exp(x) - Math.exp(-x)) / (Math.exp(x) + Math.exp(-x));
    }

    public static double Sigmoid(double x) {
        return 1.0D / (1.0D + Math.exp(-x));
    }

    public static double ReLU(double x) {
        return x > 0.0D ? x : 0.0D;
    }

    public static double Linear(double x) {
        return x;
    }
}

class NerveUnit {
    int Count;
    double[] Params;
    double Offset;

    public NerveUnit(int cnt) {
        this.Count = cnt;
        this.Params = new double[this.Count];
        for (int i = 0; i < this.Count; i++)
            this.Params[i] = Utils.Random(-1.0D, 1.0D);
        this.Offset = Utils.Random(-1.0D, 1.0D);
    }

    public NerveUnit(int cnt, boolean flag) {
        this.Count = cnt;
        this.Params = new double[this.Count];
        if (flag) {
            for (int i = 0; i < this.Count; i++)
                this.Params[i] = Utils.Random(-1.0D, 1.0D);
            this.Offset = Utils.Random(-1.0D, 1.0D);
        } else {
            for (int i = 0; i < this.Count; i++)
                this.Params[i] = 0.0D;
            this.Offset = 0.0D;
        }
    }

    public void AddMemory() {
        this.Count++;
        double[] NewParams = new double[this.Count];
        for (int i = 0; i < this.Count - 1; i++)
            NewParams[i] = this.Params[i];
        NewParams[this.Count - 1] = Utils.Random(-1.0D, 1.0D);
        this.Params = NewParams;
    }

    public void DeleteParam(int Position) {
        this.Count--;
        double[] NewParams = new double[this.Count];
        for (int i = 0; i < this.Count; i++)
            NewParams[i] = this.Params[i >= Position ? i + 1 : i];
        this.Params = NewParams;
    }
}

public class BasicNeuralNetwork {
    int LayerCount;
    int[] NodeCount;
    NerveUnit[][] Network;
    int InputCount;
    int OutputCount;

    public void load(File f) {
        try {
            Utils.SetFile(f);
            this.InputCount = Utils.ReadFileInt();
            this.OutputCount = Utils.ReadFileInt();
            this.LayerCount = Utils.ReadFileInt();
            this.NodeCount = new int[this.LayerCount];
            this.Network = new NerveUnit[this.LayerCount + 1][];
            for (int i = 0; i < this.LayerCount; i++) {
                this.NodeCount[i] = Utils.ReadFileInt();
                this.Network[i] = new NerveUnit[this.NodeCount[i]];
                for (int j = 0; j < this.NodeCount[i]; j++) {
                    int Count;
                    Count = Utils.ReadFileInt();
                    this.Network[i][j] = new NerveUnit(Count, false);
                    for (int k = 0; k < this.Network[i][j].Count; k++)
                        this.Network[i][j].Params[k] = Utils.ReadFileDouble();
                    this.Network[i][j].Offset = Utils.ReadFileDouble();
                }
            }
            this.Network[this.LayerCount] = new NerveUnit[this.OutputCount];
            for (int i = 0; i < this.OutputCount; i++) {
                int Count;
                Count = Utils.ReadFileInt();
                this.Network[this.LayerCount][i] = new NerveUnit(Count, false);
                for (int j = 0; j < this.Network[this.LayerCount][i].Count; j++)
                    this.Network[this.LayerCount][i].Params[j] = Utils.ReadFileDouble();
                this.Network[this.LayerCount][i].Offset = Utils.ReadFileDouble();
            }
            Utils.CloseFile();
        } catch (Exception exception) {
        }
    }

    public void save(File f) {
        try {
            Utils.SetFile(f);
            Utils.WriteFile(this.InputCount);
            Utils.WriteFile(this.OutputCount);
            Utils.WriteFile(this.LayerCount);
            for (int i = 0; i < this.LayerCount; i++) {
                Utils.WriteFile(this.NodeCount[i]);
                for (int j = 0; j < this.NodeCount[i]; j++) {
                    Utils.WriteFile(this.Network[i][j].Count);
                    for (int k = 0; k < this.Network[i][j].Count; k++)
                        Utils.WriteFile(this.Network[i][j].Params[k]);
                    Utils.WriteFile(this.Network[i][j].Offset);
                }
            }
            for (int i = 0; i < this.OutputCount; i++) {
                Utils.WriteFile(this.Network[this.LayerCount][i].Count);
                for (int j = 0; j < this.Network[this.LayerCount][i].Count; j++)
                    Utils.WriteFile(this.Network[this.LayerCount][i].Params[j]);
                Utils.WriteFile(this.Network[this.LayerCount][i].Offset);
            }
            Utils.CloseFile();
        } catch (Exception exception) {
        }
    }

    public BasicNeuralNetwork() {
        BasicNeuralNetwork(25, 1, 3, new int[] { 15, 9, 3 });
        // TODO 出问题就自定义一下
    }

    public BasicNeuralNetwork(File f) {
        this.load(f);
    }

    private BasicNeuralNetwork(int Input, int Output, int Layer, int[] Node) {
        this.LayerCount = Layer;
        this.InputCount = Input;
        this.OutputCount = Output;
        this.NodeCount = new int[this.LayerCount];
        this.Network = new NerveUnit[this.LayerCount + 1][];
        for (int i = 0; i < this.LayerCount; i++) {
            this.NodeCount[i] = Node[i];
            this.Network[i] = new NerveUnit[this.NodeCount[i]];
            for (int j = 0; j < this.NodeCount[i]; j++)
                this.Network[i][j] = new NerveUnit(i == 0 ? this.InputCount : this.NodeCount[i - 1]);
        }
        this.Network[this.LayerCount] = new NerveUnit[this.OutputCount];
        for (int i = 0; i < OutputCount; i++)
            this.Network[this.LayerCount][i] = new NerveUnit(this.NodeCount[this.LayerCount - 1]);
    }

    private void Calculate(double[] InputValues, double[] ReturnValues) {
        double[] Input;
        double[] Output;
        Input = new double[this.InputCount];
        for (int i = 0; i < this.InputCount; i++)
            Input[i] = InputValues[i];
        for (int i = 0; i < this.LayerCount; i++) {
            Output = new double[this.NodeCount[i]];
            for (int j = 0; j < this.NodeCount[i]; j++) {
                Output[j] = 0.0;
                for (int k = 0; k < this.Network[i][j].Count; k++)
                    Output[j] += this.Network[i][j].Params[k] * Input[k];
                Output[j] += this.Network[i][j].Offset;
                Output[j] = Utils.Tanh(Output[j]);
            }
            Input = Output;
        }
        for (int i = 0; i < this.OutputCount; i++) {
            ReturnValues[i] = 0.0;
            for (int j = 0; j < this.Network[this.LayerCount][i].Count; j++)
                ReturnValues[i] += this.Network[this.LayerCount][i].Params[j] * Input[j];
            ReturnValues[i] += this.Network[this.LayerCount][i].Offset;
            ReturnValues[i] = Utils.Tanh(ReturnValues[i]);
        }
        return;
    }

    private void RandomChange(double x, double num) {
        for (int i = 0; i < LayerCount; i++)
            for (int j = 0; j < NodeCount[i]; j++)
                if (Utils.Rate(num)) {
                    for (int k = 0; k < this.Network[i][j].Count; k++)
                        this.Network[i][j].Params[k] += Utils.Random(-x, x);
                    this.Network[i][j].Offset += Utils.Random(-x, x);
                }
        for (int i = 0; i < OutputCount; i++)
            if (Utils.Rate(num)) {
                for (int j = 0; j < this.Network[this.LayerCount][i].Count; j++)
                    this.Network[this.LayerCount][i].Params[j] += Utils.Random(-x, x);
                this.Network[this.LayerCount][i].Offset += Utils.Random(-x, x);
            }
        return;
    }

    private void RandomChange(double x) {
        for (int i = 0; i < LayerCount; i++)
            for (int j = 0; j < NodeCount[i]; j++) {
                for (int k = 0; k < this.Network[i][j].Count; k++)
                    this.Network[i][j].Params[k] += Utils.Random(-x, x);
                this.Network[i][j].Offset += Utils.Random(-x, x);
            }
        for (int i = 0; i < OutputCount; i++) {
            for (int j = 0; j < this.Network[this.LayerCount][i].Count; j++)
                this.Network[this.LayerCount][i].Params[j] += Utils.Random(-x, x);
            this.Network[this.LayerCount][i].Offset += Utils.Random(-x, x);
        }
        return;
    }

    private boolean AddNode(int Layer) {
        if (Layer >= this.LayerCount)
            return false;
        this.NodeCount[Layer]++;
        NerveUnit[] NewLayer = new NerveUnit[this.NodeCount[Layer]];
        for (int i = 0; i < this.NodeCount[Layer] - 1; i++)
            NewLayer[i] = this.Network[Layer][i];
        NewLayer[this.NodeCount[Layer] - 1] = new NerveUnit(Layer == 0 ? this.InputCount : this.NodeCount[Layer - 1]);
        this.Network[Layer] = NewLayer;
        if (Layer == this.LayerCount - 1) {
            for (int i = 0; i < this.OutputCount; i++)
                this.Network[this.LayerCount][i].AddMemory();
        } else {
            for (int i = 0; i < this.NodeCount[Layer + 1]; i++)
                this.Network[Layer + 1][i].AddMemory();
        }
        return true;
    }

    private boolean DeleteNode(int Layer, int Position) {
        if (Layer >= this.LayerCount)
            return false;
        if (Position >= this.NodeCount[Layer])
            return false;
        this.NodeCount[Layer]--;
        NerveUnit[] NewLayer = new NerveUnit[this.NodeCount[Layer]];
        for (int i = 0; i < NodeCount[Layer]; i++)
            NewLayer[i] = this.Network[Layer][i >= Position ? i + 1 : i];
        this.Network[Layer] = NewLayer;
        if (Layer == this.LayerCount - 1) {
            for (int i = 0; i < this.OutputCount; i++)
                this.Network[this.LayerCount][i].DeleteParam(Position);
        } else {
            for (int i = 0; i < NodeCount[Layer + 1]; i++)
                this.Network[Layer + 1][i].DeleteParam(Position);
        }
        return true;
    }

    private BasicNeuralNetwork Copy() {
        BasicNeuralNetwork Return = new BasicNeuralNetwork(InputCount, OutputCount, LayerCount, NodeCount);
        for (int i = 0; i < this.LayerCount; i++)
            for (int j = 0; j < this.NodeCount[i]; j++) {
                for (int k = 0; k < this.Network[i][j].Count; k++)
                    Return.Network[i][j].Params[k] = this.Network[i][j].Params[k];
                Return.Network[i][j].Offset = this.Network[i][j].Offset;
            }
        for (int i = 0; i < this.OutputCount; i++) {
            for (int j = 0; j < this.Network[this.LayerCount][i].Count; j++)
                Return.Network[this.LayerCount][i].Params[j] = this.Network[this.LayerCount][i].Params[j];
            Return.Network[this.LayerCount][i].Offset = this.Network[this.LayerCount][i].Offset;
        }
        return Return;
    }

    private void AddLayer() {
        this.LayerCount++;
        NerveUnit[][] NewNetwork = new NerveUnit[this.LayerCount + 1][];
        for (int i = 0; i < this.LayerCount - 1; i++)
            NewNetwork[i] = this.Network[i];
        NewNetwork[this.LayerCount] = this.Network[this.LayerCount - 1];
        NewNetwork[this.LayerCount - 1] = new NerveUnit[2];
        this.Network = NewNetwork;
        int[] NewNodeCount = new int[this.LayerCount];
        for (int i = 0; i < this.LayerCount - 1; i++)
            NewNodeCount[i] = this.NodeCount[i];
        NewNodeCount[this.LayerCount - 1] = 2;
        this.NodeCount = NewNodeCount;
        this.Network[this.LayerCount - 1][0] = new NerveUnit(this.NodeCount[this.LayerCount - 2]);
        this.Network[this.LayerCount - 1][1] = new NerveUnit(this.NodeCount[this.LayerCount - 2]);
        for (int i = 0; i < OutputCount; i++) {
            this.Network[this.LayerCount][i] = new NerveUnit(2);
        }
    }

    private boolean DeleteLayer() {
        if (this.LayerCount == 0)
            return false;
        this.LayerCount--;
        NerveUnit[][] NewNetwork = new NerveUnit[this.LayerCount + 1][];
        for (int i = 0; i < this.LayerCount; i++)
            NewNetwork[i] = this.Network[i];
        NewNetwork[this.LayerCount] = this.Network[this.LayerCount + 1];
        this.Network = NewNetwork;
        int[] NewNodeCount = new int[this.LayerCount];
        for (int i = 0; i < this.LayerCount; i++)
            NewNodeCount[i] = this.NodeCount[i];
        this.NodeCount = NewNodeCount;
        for (int i = 0; i < OutputCount; i++) {
            this.Network[this.LayerCount][i] = new NerveUnit(
                    this.LayerCount > 0 ? this.NodeCount[this.LayerCount - 1] : this.InputCount);
        }
        return true;
    }

    private void CopyFrom(BasicNeuralNetwork src) {
        BasicNeuralNetwork temp = src.Copy();
        this.InputCount = temp.InputCount;
        this.OutputCount = temp.OutputCount;
        this.Network = temp.Network;
        this.NodeCount = temp.NodeCount;
        this.OutputCount = temp.OutputCount;
    }

    public void train(double[] data, boolean positive) {
        for (int i = 0; i < 10; i++) {
            double[] target = new double[1];
            double[] result = new double[1];
            target[0] = positive ? 1.0D : -1.0D;
            double error1 = 0.0D;
            double error2 = 0.0D;
            BasicNeuralNetwork temp = this.Copy();
            temp.RandomChange(Math.exp(Utils.Random(-3.0D, 3.0D)));
            // TODO 这是学习率,也是个很重要的参数,出问题也可以试着调一下
            this.Calculate(data, result);
            error1 += Utils.Square(result[0] - target[0]);
            temp.Calculate(data, result);
            error2 += Utils.Square(result[0] - target[0]);
            if (error1 >= error2) {
                this.CopyFrom(temp);
                return;
            }
        }
    }

    public double judge(double[] data) {
        double[] result = new double[1];
        this.Calculate(data, result);
        return 0.5D + 0.5D * result[0];
    }
};