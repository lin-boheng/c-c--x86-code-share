import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

public class BPNeuralNetwork {
    private int LayerCnt;
    private int[] LayerConfig;
    private float[][] MatrixPtr;
    private float[][] OffsetPtr;
    private float[][] OutCache;
    private float[][] DeltaCache;

    public interface MapFunc {
        public float map(float x);
    }

    public MapFunc Act;
    public MapFunc ActDelta;

    private static Random random;

    static {
        random = new Random(Runtime.getRuntime());
    }

    private static float RandomFloat(float Low, float High) {
        return random.nextFloat() * (High - Low) + Low;
    }

    private static float RandomSpec() {
        return 2.0 * random.nextFloat() - 1.0;
    }

    private void AllocateMemory() {
        MatrixPtr = new float[LayerCnt];
        OffsetPtr = new float[LayerCnt];
        OutCache = new float[LayerCnt + 1];
        DeltaCache = new float[LayerCnt + 1];
        for (int i = 0; i < LayerCnt; i++) {
            MatrixPtr[i] = new float[LayerConfig[i] * LayerConfig[i + 1]];
            OffsetPtr[i] = new float[LayerConfig[i + 1]];
            OutCache[i] = new float[LayerConfig[i]];
            DeltaCache[i] = new float[LayerConfig[i]];
            for (int j = 0; j < LayerConfig[i] * LayerConfig[i + 1]; j++)
                MatrixPtr[i][j] = RandomSpec();
            for (int j = 0; j < LayerConfig[i + 1]; j++)
                OffsetPtr[i][j] = RandomSpec();
        }
        OutCache[LayerCnt] = new float[LayerConfig[LayerCnt]];
        DeltaCache[LayerCnt] = new float[LayerConfig[LayerCnt]];
    }

    public BPNeuralNetwork(int[] LayerConfigList) {
        LayerCnt = LayerConfigList.length - 1;
        LayerConfig = new int[LayerConfigList.length];
        for (int i = 0; i <= LayerCnt; i++)
            LayerConfig[i] = LayerConfigList[i];
        AllocateMemory();
        Act = new MapFunc() {
            @Override
            public float map(float x) {
                return 1.0 / (2.0 + Math.expm1(-x));
            }
        };
        ActDelta = new MapFunc() {
            @Override
            public float map(float x) {
                return x * (1.0 - x);
            }
        };
    }

    public BPNeuralNetwork(File file) {
        Scanner sc = new Scanner(new FileInputStream(file));
        LayerCnt = sc.nextInt();
        LayerConfig = new int[LayerCnt + 1];
        for (int i = 0; i < LayerConfig.length; i++) {
            LayerConfig[i] = sc.nextInt();
        }
        AllocateMemory();
        for (int i = 0; i < LayerCnt; i++) {
            for (int j = 0; j < MatrixPtr[i].length; j++) {
                MatrixPtr[i][j] = sc.nextFloat();
            }
            for (int j = 0; j < OffsetPtr[i].length; j++) {
                OffsetPtr[i][j] = sc.nextFloat();
            }
        }
        sc.close();
    }

    public void Train(float[] Input, float[] Output, float LearnRate) {
        for (int i = 0; i < LayerConfig[0]; i++)
            OutCache[0][i] = Input[i];
        for (int i = 0; i < LayerCnt; i++) {
            for (int j = 0; j < LayerConfig[i + 1]; j++)
                OutCache[i + 1][j] = 0.0;
            for (int j = 0; j < LayerConfig[i + 1]; j++) {
                for (int k = 0; k < LayerConfig[i]; k++)
                    OutCache[i + 1][j] += MatrixPtr[i][j * LayerConfig[i] + k] * OutCache[i][k];
                OutCache[i + 1][j] = Act.map(OutCache[i + 1][j] + OffsetPtr[i][j]);
            }
        }
        for (int i = 0; i < LayerConfig[LayerCnt]; i++)
            DeltaCache[LayerCnt][i] = ActDelta.map(OutCache[LayerCnt][i] - Output[i]);
        for (int i = LayerCnt; i > 0; i--) {
            for (int j = 0; j < LayerConfig[i - 1]; j++)
                DeltaCache[i - 1][j] = 0.0;
            for (int j = 0; j < LayerConfig[i]; j++) {
                for (int k = 0; k < LayerConfig[i - 1]; k++) {
                    DeltaCache[i - 1][k] += MatrixPtr[i - 1][j * LayerConfig[i - 1] + k] * DeltaCache[i][j];
                    MatrixPtr[i - 1][j * LayerConfig[i - 1] + k] -= LearnRate * OutCache[i - 1][k] * DeltaCache[i][j];
                }
            }
            for (int j = 0; j < LayerConfig[i]; j++)
                OffsetPtr[i - 1][j] -= LearnRate * DeltaCache[i][j];
            for (int j = 0; j < LayerConfig[i - 1]; j++)
                DeltaCache[i - 1][j] *= ActDelta.map(OutCache[i - 1][j]);
        }
    }

    public void Calculate(float[] Input, float[] Output) {
        float[] temp;
        temp = OutCache[0];
        OutCache[0] = Input;
        Input = temp;
        for (int i = 0; i < LayerCnt; i++) {
            for (int j = 0; j < LayerConfig[i + 1]; j++)
                OutCache[i + 1][j] = 0.0;
            for (int j = 0; j < LayerConfig[i + 1]; j++) {
                for (int k = 0; k < LayerConfig[i]; k++)
                    OutCache[i + 1][j] += MatrixPtr[i][j * LayerConfig[i] + k] * OutCache[i][k];
                OutCache[i + 1][j] = Act.map(OutCache[i + 1][j] + OffsetPtr[i][j]);
            }
        }
        temp = OutCache[0];
        OutCache[0] = Input;
        Input = temp;
        temp = OutCache[LayerCnt];
        OutCache[LayerCnt] = Output;
        Output = temp;
    }

    public void SaveToFile(File file) {
        PrintWriter pWriter = new PrintWriter(new FileOutputStream(file));
        pWriter.println(LayerCnt);
        for (int config : LayerConfig) {
            pWriter.println(config);
        }
        for (int i = 0; i < LayerCnt; i++) {
            for (float val : MatrixPtr[i]) {
                pWriter.println(val);
            }
            for (float val : OffsetPtr[i]) {
                pWriter.println(val);
            }
        }
        pWriter.close();
    }
};
