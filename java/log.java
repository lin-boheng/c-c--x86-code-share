package com.mcml.space;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

class log implements Runnable {
    public final int port = 1234;
    public ServerSocket serverSocket = null;
    public String string = null;

    public log() {
    }

    public void Startup() throws IOException {
        this.serverSocket = new ServerSocket(this.port, 10);
    }

    public boolean isNew() {
        return this.string != null;
    }

    public String Receive() {
        String ret = this.string;
        this.string = null;
        return ret;
    }

    public void Send(String host, String content) throws UnknownHostException, IOException {
        Socket sock = new Socket(host, this.port);
        DataOutputStream os = new DataOutputStream(sock.getOutputStream());
        os.writeBytes(content);
        os.close();
        sock.close();
    }

    public void run() {
        while (this.serverSocket == null)
            try {
                wait(100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        while (true) {
            try {
                Socket sock = this.serverSocket.accept();
                InputStream iStream = sock.getInputStream();
                int len = iStream.available();
                byte[] bytes = new byte[len];
                iStream.read(bytes, 0, len);
                this.string = new String(bytes);
                sock.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}