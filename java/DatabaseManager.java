import java.util.HashMap;

import Database;

public class DatabaseManager {
    private HashMap<String, Database> Data;

    public DatabaseManager() {
        this.Data = new HashMap<String, Database>();
    }

    public synchronized Database get(String s) {
        Database res = this.Data.get(s);
        if (res == null) {
            res = new Database();
            this.Data.put(s, res);
        }
        return res;
    }
}