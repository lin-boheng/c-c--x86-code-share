#include <windows.h>
#include <windowsx.h>

#include <al.h>
#include <alc.h>

#include <gl/gl.h>

bool _alCheckError(const char* tag, const char* file, int line){
	int loopCnt = 0;
	for (ALenum error = alGetError(); loopCnt < 32 && error != AL_NO_ERROR; error = alGetError(), ++loopCnt){
		const char* pMsg;
		switch (error){
		case AL_INVALID_NAME: pMsg = "invalid name"; break;
		case AL_INVALID_ENUM: pMsg = "invalid enum"; break;
		case AL_INVALID_VALUE: pMsg = "invalid value"; break;
		case AL_INVALID_OPERATION: pMsg = "invalid operation"; break;
		case AL_OUT_OF_MEMORY: pMsg = "out of memory"; break;
		default: pMsg = "unknown error";
		}
		__builtin_printf("[OpenAL Error] %s %s(0x%x) at %s:%d\n", tag, pMsg, error, file, line);
	}
	return loopCnt != 0;
}

#define alCheckError(tag) _alCheckError(tag, __FILE__, __LINE__)

HWND hWnd;
HDC hDC;
HGLRC hRC;

void EnableOpenGL(HWND hWnd, HDC* hDC, HGLRC* hRC){
    PIXELFORMATDESCRIPTOR pfd;

    *hDC = GetDC(hWnd);

    RtlZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iLayerType = PFD_MAIN_PLANE;

    int iFormat = ChoosePixelFormat(*hDC, &pfd);
    SetPixelFormat(*hDC, iFormat, &pfd);

    *hRC = wglCreateContext(*hDC);
    wglMakeCurrent(*hDC, *hRC);
}

void DisableOpenGL(HWND hWnd, HDC hDC, HGLRC hRC){
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(hRC);
    ReleaseDC(hWnd, hDC);
}

LRESULT ALWindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
    switch (uMsg){
    case WM_CREATE:
        break;
    case WM_CLOSE:
        PostQuitMessage(0);
        break;
    }
    return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void ALDisplayWindow(){
    WNDCLASSEX wc;
    HINSTANCE hInst = GetModuleHandle(NULL);

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hIcon = LoadIcon(hInst, IDI_APPLICATION);
    wc.hIconSm = LoadIcon(hInst, IDI_APPLICATION);
    wc.hInstance = hInst;
    wc.lpfnWndProc = ALWindowProc;
    wc.lpszClassName = "ALWindow";
    wc.lpszMenuName = "";
    wc.style = 0;

    RegisterClassEx(&wc);

    hWnd = CreateWindowEx(
        0,
        "ALWindow",
        "ALWindow",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT,
        1024, 512,
        NULL,
        NULL,
        hInst,
        NULL
    );

    EnableOpenGL(hWnd, &hDC, &hRC);

    ShowWindow(hWnd, SW_SHOW);
    UpdateWindow(hWnd);
}

#define PI 3.1415926535897932384626433832795

void FFT(_Complex float* input, int sizebit, bool inv, bool div){
    int size = 1 << sizebit;
    int* rev = new int[size];
    rev[0] = 0;
    for (int i = 1; i < size; i++){
        rev[i] = (rev[i >> 1] >> 1) | ((i & 1) ? (1 << (sizebit - 1)) : 0);
    }
    for (int i = 0; i < size; i++){
        if (i < rev[i]){
            _Complex float tmp;
            tmp = input[i];
            input[i] = input[rev[i]];
            input[rev[i]] = tmp;
        }
    }
    delete[] rev;
    for (int i = 1; i <= sizebit; i++){
        int half = (1 << (i - 1)), num = (1 << i);
        for (int j = 0; j < (1 << (sizebit - i)); j++){
            _Complex float o, m;
            m = 1.0f + 0.0if;
            o = __builtin_sin(PI / num) * (inv ? -1.0if : 1.0if) + __builtin_cos(PI / num);
            for (int k = (j << i); k < (j << i) + half; k++){
                _Complex float A = input[k], B = input[k + half];
                input[k] = A + B * o;//单位根份数
                input[k + half] = A - B * o;
                m *= o;
            }
        }
    }
    if (div){
        for (int i = 0; i < size; i++){
            input[i] /= size;
        }
    }
}

void FileTrans(){
    BY_HANDLE_FILE_INFORMATION info;
    HANDLE hFile = CreateFile(
        "b.bin",
        GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL
    );
    HANDLE hOut = CreateFile(
        "test.wav",
        GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL,
        NULL
    );
    GetFileInformationByHandle(hFile, &info);
    DWORD temp;
    WriteFile(hOut, "RIFF", 4, NULL, NULL);
    temp = info.nFileSizeLow + 36;
    WriteFile(hOut, &temp, 4, NULL, NULL);
    WriteFile(hOut, "WAVE", 4, NULL, NULL);
    WriteFile(hOut, "fmt ", 4, NULL, NULL);
    temp = 0x10;
    WriteFile(hOut, &temp, 4, NULL, NULL);
    WAVEFORMATEX wav;
    wav.wFormatTag = WAVE_FORMAT_PCM;
    wav.nChannels = 1;
    wav.nBlockAlign = 2;
    wav.nAvgBytesPerSec = 22050;
    wav.nSamplesPerSec = 11025;
    wav.wBitsPerSample = 16;
    WriteFile(hOut, &wav, 0x10, NULL, NULL);
    WriteFile(hOut, "data", 4, NULL, NULL);
    WriteFile(hOut, &info.nFileSizeLow, 4, NULL, NULL);
    char* buf = new char[info.nFileSizeLow];
    ReadFile(hFile, buf, info.nFileSizeLow, NULL, NULL);
    WriteFile(hOut, buf, info.nFileSizeLow, NULL, NULL);
    delete[] buf;
    CloseHandle(hFile);
    CloseHandle(hOut);
}

union Complex {
    _Complex float comp;
    float real, imag;

    Complex(_Complex float comp) : comp(comp) {}
    float r2(){ return real * real + imag * imag; }
};

void FFTLoad(){
    ALCcontext* ctx;
    ALCdevice* dev;
    ALuint src;
    ALuint buf;
    short data[65536];
    _Complex float comp[65536];

    HANDLE hFile = CreateFile(
        "a.bin",
        GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL
    );
    HANDLE hOut = CreateFile(
        "b.bin",
        GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL,
        NULL
    );

    dev = alcOpenDevice(NULL);
    ctx = alcCreateContext(dev, NULL);

    alcMakeContextCurrent(ctx);

    alListener3f(AL_POSITION, 0.0f, 0.0f, 0.0f);
    alListener3f(AL_VELOCITY, 0.0f, 0.0f, 0.0f);
    alListener3f(AL_ORIENTATION, 0.0f, 1.0f, 0.0f);

    alGenSources(1, &src);
    alGenBuffers(1, &buf);

    ReadFile(hFile, data, sizeof(data), NULL, NULL);
    for (int i = 0; i < 65536; i++){
        comp[i] = data[i];
    }
    FFT(comp, 16, false, true);
    for (int i = 32767; i >= 0; i--){
        comp[i << 1] = comp[i];
        comp[(i << 1) | 1] = 0.0f;
    }
    FFT(comp, 16, true, false);
    for (int i = 0; i < 65536; i++){
        data[i] = (Complex(comp[i])).real;
    }

    alBufferData(buf, AL_FORMAT_MONO16, data, sizeof(data), 11025);

    alSourceQueueBuffers(src, 1, &buf);
    alSourcePlay(src);
    WriteFile(hOut, data, sizeof(data), NULL, NULL);
    Sleep(4000);
    alSourceStop(src);

    alDeleteBuffers(1, &buf);
    alDeleteSources(1, &src);

    alcDestroyContext(ctx);
    alcCloseDevice(dev);

    CloseHandle(hFile);
    CloseHandle(hOut);
}

float lerp(float a, float b, float t){
    return a + (b - a) * t;
}

int clamp(int min, int max, int x){
    if (x < min)
        return min;
    if (x > max)
        return max;
    return x;
}

void FFTRealTime(){
    const int bit = 16;

    ALCdevice* capDev;
    ALint sample;
    short capBuf[1 << bit], samBuf[1 << bit];

    ALCcontext* ctx;
    ALCdevice* dev;
    ALuint src;
    ALuint buf[2];
    int forwIdx = 0, backIdx = 0;

    MSG Msg;

    _Complex float comp[1 << bit];
    _Complex float output[1 << (bit + 1)];

    HANDLE hFile = CreateFile(
        "output.wav",
        GENERIC_READ | GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL,
        NULL
    );
    DWORD temp;

    WriteFile(hFile, "RIFF", 4, NULL, NULL);
    temp = 0x1000000 + 36;
    WriteFile(hFile, &temp, 4, NULL, NULL);
    WriteFile(hFile, "WAVE", 4, NULL, NULL);
    WriteFile(hFile, "fmt ", 4, NULL, NULL);
    temp = 0x10;
    WriteFile(hFile, &temp, 4, NULL, NULL);

    WAVEFORMATEX wav;
    wav.wFormatTag = WAVE_FORMAT_PCM;
    wav.nChannels = 1;
    wav.nBlockAlign = 2;
    wav.nAvgBytesPerSec = 88200;
    wav.nSamplesPerSec = 44100;
    wav.wBitsPerSample = 16;

    WriteFile(hFile, &wav, 0x10, NULL, NULL);
    WriteFile(hFile, "data", 4, NULL, NULL);
    temp = 0x1000000;
    WriteFile(hFile, &temp, 4, NULL, NULL);

    capDev = alcCaptureOpenDevice(NULL, 44100, AL_FORMAT_MONO16, sizeof(capBuf) << 1);
    dev = alcOpenDevice(NULL);
    ctx = alcCreateContext(dev, NULL);

    alcMakeContextCurrent(ctx);

    alListener3f(AL_POSITION, 0.0f, 0.0f, 0.0f);
    alListener3f(AL_VELOCITY, 0.0f, 0.0f, 0.0f);
    alListener3f(AL_ORIENTATION, 0.0f, 1.0f, 0.0f);

    alGenSources(1, &src);
    alGenBuffers(2, buf);

    alcCaptureStart(capDev);
    while (true){
        alcGetIntegerv(capDev, ALC_CAPTURE_SAMPLES, 1, &sample);
        if (sample < (1 << bit)){
            if (PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE)){
                if (Msg.message == WM_QUIT){
                    __builtin_puts("quit");
                    DisableOpenGL(hWnd, hDC, hRC);
                    DestroyWindow(hWnd);
                    break;
                }
                TranslateMessage(&Msg);
                DispatchMessage(&Msg);
            }
            Sleep(1);
            continue;
        }
        alcCaptureSamples(capDev, capBuf, (1 << bit));
        __builtin_printf("sample\n");
        for (int i = 0; i < (1 << bit); i++){
            float samPos = i * 2.5f;
            int samIdx = __builtin_floor(samPos);
            samPos -= samIdx;
            samIdx %= ((1 << bit) - 1);
            samBuf[i] = capBuf[samIdx] * (1.0f - samPos) + capBuf[samIdx + 1] * samPos;
        }
        WriteFile(hFile, capBuf, (1 << (bit + 1)), NULL, NULL);
        for (int i = 0; i < (1 << bit); i++){
            comp[i] = capBuf[i];
            output[i] = 0.0f;
        }
        FFT(comp, bit, false, true);
        // 显示
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0.0, 1024.0, 0.0, 10.0, -10.0, 1.0);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glLineWidth(1.0f);
        glBegin(GL_LINES);
        for (int i = 0; i < 1024; i++){
            glColor3f(0.0f, 0.0f, 1.0f);
            glVertex2f(i, 0.0f);
            glVertex2f(i, __builtin_log(Complex(comp[i << (bit - 11)]).r2()));
        }
        glEnd();
        //TODO 频域变换
        // 0-22050Hz
        // for (int i = 0; i < (1 << bit); i++){
        //     output[i] = 5.0f * comp[i];
        // }
        // // 显示2
        // glBegin(GL_LINES);
        // for (int i = 0; i < 1024; i++){
        //     glColor3f(1.0f, 0.0f, 0.0f);
        //     glVertex2f(i, 0.0f);
        //     glVertex2f(i, __builtin_log(Complex(output[i << (bit - 11)]).r2()));
        // }
        // glEnd();
        //FFT(output, bit, true, false);
        SwapBuffers(hDC);

        ALint cnt;
        ALint state;

        // for (int i = 0; i < (1 << bit); i++){
        //     capBuf[i] = (Complex(output[i])).real;
        // }
        alGetSourcei(src, AL_SOURCE_STATE, &state);
        if (state == AL_PLAYING)
            alSourceStop(src);
        alGetSourcei(src, AL_BUFFERS_QUEUED, &cnt);
        if (cnt)
            alSourceUnqueueBuffers(src, 1, &buf[(backIdx++) & 1]);
        alBufferData(buf[forwIdx & 1], AL_FORMAT_MONO16, samBuf, sizeof(samBuf), 44100);
        alSourceQueueBuffers(src, 1, &buf[(forwIdx++) & 1]);
        alSourcePlay(src);
    }
    alcCaptureStop(capDev);

    alcCaptureCloseDevice(capDev);

    alcDestroyContext(ctx);
    alcCloseDevice(dev);

    CloseHandle(hFile);
}

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
    ALDisplayWindow();
    FFTRealTime();
    return 0;
}