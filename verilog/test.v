module test();
reg clk;
reg rstn;
wire[7:0] cnt;
counter i1(
	.clk(clk),
	.count(cnt),
	.reset(rstn)
);
initial begin
	clk = 1'b0;
	rstn = 1'b0;
	#51;
	rstn = 1'b1;
	$display("test");
end
always #10 clk = ~clk;
endmodule