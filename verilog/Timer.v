module timer(clk);
output reg clk;

parameter cycle = 50;

initial begin
	clk = 1'b0;
end

always begin
	clk = #cycle ~clk;
end

endmodule
