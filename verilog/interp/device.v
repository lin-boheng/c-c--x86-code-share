`timescale 1ns / 1ps

module device (rst,clk);

input rst;
input clk;

$attribute(clk, "PAD", "P39");
$attribute(rst, "PAD", "0");

endmodule