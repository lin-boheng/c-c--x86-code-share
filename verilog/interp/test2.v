`timescale 1ns / 1ps

module test2;

reg rst;
reg clk;

always #10 clk <= ~clk;

parameter x1 = 16'd0, x2 = 16'd10, x3 = 16'd90,
          y1 = 16'd0, y2 = 16'd45, y3 = 16'd20;

scheduler s0(
    .rst(rst),
    .clk(clk),
    .x1(x1), .x2(x2), .x3(x3),
    .y1(y1), .y2(y2), .y3(y3)
);

initial begin
    $dumpfile("testwave.vcd");
    $dumpvars(0, test2);
end

initial begin
    rst <= 1'b1;
    clk <= 1'b0;
    rst = 1'b0;
    s0.block_inst.prog[0] <= 64'h0000390139000101;
    s0.block_inst.prog[1] <= 64'h0000390139000002;
    s0.block_inst.prog[2] <= 64'h0000390139000103;
    s0.block_inst.prog[3] <= 64'h0000000000000000;
    $display("Fire Triangle (%d,%d)(%d,%d)(%d,%d)", x1, y1, x2, y2, x3, y3);
    s0.fire_triangle();
    $display("Fire Triangle End");
    $finish;
end

endmodule