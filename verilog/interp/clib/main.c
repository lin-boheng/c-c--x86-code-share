#include <iverilog/vpi_user.h>

int alert_comp(char* msg){
    return 0;
}

int alert(char* msg){
    return 0;
}

#define vpi_reg(s, x)\
    s.type = vpiSysTask;\
    s.tfname = "$" #x;\
    s.calltf = x;\
    s.compiletf = x ## _comp;\
    s.sizetf = NULL;\
    s.user_data = #x "_user_data";\
    vpi_register_systf(&s);

void vpi_init(){
    s_vpi_systf_data data;
    vpi_reg(data, alert);
}

void (*vlog_startup_routines[])() = {
    vpi_init, NULL
};