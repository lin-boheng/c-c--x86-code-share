`timescale 1ns / 1ps

module test;

reg rst;
reg clk;

always #10 clk <= ~clk;

reg [31:0] angle;
wire signed [31:0] sin;
wire signed [31:0] cos;

sin_cos sincos (
    .clk(clk),
    .angle(angle),
    .sin(sin),
    .cos(cos)
);

initial begin
    $dumpfile("testwave.vcd");
    $dumpvars(0, test);
end

initial begin
    rst <= 1'b1;
    clk <= 1'b0;
    angle <= 32'd0;
end

always @(negedge clk) begin
    angle <= angle + 1;
    $display("angle %f sin %f cos %f", angle / 65536.0, sin / 65536.0, cos / 65536.0);
end

endmodule