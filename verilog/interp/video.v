`timescale 1ns / 1ps

module vga_output (clk,vSync,hSync,counterX,counterY);

input clk;
output vSync;
output hSync;

output reg [9:0] counterX;
output reg [9:0] counterY;

wire counterXmax = (counterX == 10'd1023);
wire counterYmax = (counterY == 10'd767);

always@(posedge clk) begin
    if(counterXmax)
        counterX <= 0;
    else
        counterX <= counterX + 1;
end

always@(posedge clk) begin
    if(counterXmax)
        counterY <= counterY + 1;
    if(counterYmax)
        counterY <= 0;
end

reg vsync, hsync;

always@(posedge clk) begin
    vsync <= (counterX[9:4] == 0);
    hsync <= (counterY == 0);
end

assign vSync = ~vsync;
assign hSync = ~hsync;

endmodule