`timescale 1ns / 1ns

module test3;

reg rst;
reg clk;

always #5 clk <= ~clk;

reg [15:0] x[2:0], y[2:0];

wire enable;

controller c0(
    .rst(rst),
    .clk(clk),
    .x(x), .y(y),
    .enable(enable)
);

initial begin
    x[0] <= 0; x[1] <= 10; x[2] <= 90;
    y[0] <= 0; y[1] <= 45; y[2] <= 20;
end

initial begin
    $dumpfile("testwave.vcd");
    $dumpvars(0, test3);
end

initial begin
    clk <= 1'b0;
    rst <= 1'b1;
    #1;
    rst <= 1'b0;
    #1;
    rst <= 1'b1;
    #3;
    c0.enable = 1'b1;
    wait(c0.enable == 1'b0);
    $finish;
end

endmodule