`timescale 1ns / 1ns

module calc_core(
    input wire rst,
    input wire clk,
    input wire [63:0] ins,
    input wire signed [15:0] xpos,
    input wire signed [15:0] ypos,
    input wire signed [15:0] x[2:0],
    input wire signed [15:0] y[2:0],
    input wire busy,
    output reg mask
);

// sint32 16(integer)16(frac)

// ins: type(8) tar(6) sat(1) condition(1) src(index(6),order(8),abs(1),neg(1))(3)

reg signed [3:0][31:0] regfile[63:0];// SIMD

wire signed [3:0][31:0] res;
reg signed [3:0][31:0] res_ori;

wire signed [3:0][31:0] src[2:0];
wire signed [3:0][31:0] src_abs[2:0];
wire signed [3:0][31:0] src_order[2:0];
wire signed [3:0][31:0] src_index[2:0];

genvar i, j;

generate
    for (i = 0; i < 3; i = i + 1) begin : shuffle
        assign src_index[i] = regfile[ins[(i << 4) + 21 : (i << 4) + 16]];
        assign src_order[i] = {src_index[i][ins[(i << 4) + 29 : (i << 4) + 28]],
                               src_index[i][ins[(i << 4) + 27 : (i << 4) + 26]],
                               src_index[i][ins[(i << 4) + 25 : (i << 4) + 24]],
                               src_index[i][ins[(i << 4) + 23 : (i << 4) + 22]]};
        assign src_abs[i] = ins[(i << 4) + 30] ? {(src_order[i][3] < 32'd0 ? src_order[i][3] : 32'd0),
                                                  (src_order[i][2] < 32'd0 ? src_order[i][2] : 32'd0),
                                                  (src_order[i][1] < 32'd0 ? src_order[i][1] : 32'd0),
                                                  (src_order[i][0] < 32'd0 ? src_order[i][0] : 32'd0)} : src_order[i];
        assign src[i] = ins[(i << 4) + 31] ? {-src_abs[i][3], -src_abs[i][2], -src_abs[i][1], -src_abs[i][0]} : src_abs[i];
    end
endgenerate

wire [31:0] idx_str[2:0];

generate
    for (i = 0; i < 3; i = i + 1) begin : idxX
        for (j = 0; j < 4; j = j + 1) begin : idxY
            assign idx_str[i][(j << 3) + 7 : (j << 3)] = 
                (ins[(i << 4) + (j << 1) + 23] ?
                (ins[(i << 4) + (j << 1) + 22] ? "w" : "z") :
                (ins[(i << 4) + (j << 1) + 22] ? "y" : "x"));
        end
    end
endgenerate

wire signed [31:0] g[2:0];
wire valid;

assign g[0] = (x[2] - xpos) * (y[1] - ypos) - (x[1] - xpos) * (y[2] - ypos);
assign g[1] = (x[0] - xpos) * (y[2] - ypos) - (x[2] - xpos) * (y[0] - ypos);
assign g[2] = (x[1] - xpos) * (y[0] - ypos) - (x[0] - xpos) * (y[1] - ypos);

assign valid = ((g[0] >= 32'sd0) && (g[1] >= 32'sd0) && (g[2] >= 32'sd0));

reg regout, sat;

generate
    for (i = 0; i < 4; i = i + 1) begin : satLayer
        assign res[i] = (sat ? (res_ori[i] < 32'sd0 ? 32'sd0 : (res_ori[i] > 32'sh10000 ? 32'sh10000 : res_ori[i])) : res_ori[i]);
    end
endgenerate

parameter STATE_IDLE = 1'b0,
          STATE_RUN = 1'b1;
reg state;

reg [5:0] index;

always @(posedge clk or negedge rst) begin
    if (!rst) begin
        mask <= 1'b0;
        state <= STATE_IDLE;
    end
    else begin
        case (state)
        STATE_IDLE: begin
            if (busy) begin
                mask <= valid;
                if (valid) begin
                    regout <= 1'b0;
                    state <= STATE_RUN;
                    // 测试
                    regfile[0][0] <= g[0];
                    regfile[0][1] <= g[1];
                    regfile[0][2] <= g[2];
                    regfile[0][3] <= 32'sh10000;
                    regfile[1][0] <= 32'sh10000;
                    regfile[1][1] <= 32'sh10000;
                    regfile[1][2] <= 32'sh10000;
                    regfile[1][3] <= 32'sh10000;
                end
            end
        end
        STATE_RUN: begin
            index <= ins[13:8];
            if (regout) begin
                regfile[index] <= res;
            end
            if (!busy) begin
                state <= STATE_IDLE;
            end
            else begin
                regout <= 1'b1;
                sat <= ins[14];
                case (ins[7:0])
                8'h00: $display("Core %d %d nop", xpos, ypos);
                8'h01: begin
                    $display("Core %d %d add r%02d,r%02d.%s,r%02d.%s", xpos, ypos, ins[13:8], ins[21:16], idx_str[0], ins[37:32], idx_str[1]);
                    res_ori <= {
                        src[0][0] + src[1][0],
                        src[0][1] + src[1][1],
                        src[0][2] + src[1][2],
                        src[0][3] + src[1][3]
                    };
                end
                8'h02: begin
                    $display("Core %d %d sub r%02d,r%02d.%s,r%02d.%s", xpos, ypos, ins[13:8], ins[21:16], idx_str[0], ins[37:32], idx_str[1]);
                    res_ori <= {
                        src[0][0] - src[1][0],
                        src[0][1] - src[1][1],
                        src[0][2] - src[1][2],
                        src[0][3] - src[1][3]
                    };
                end
                8'h03: begin
                    $display("Core %d %d mul r%02d,r%02d.%s,r%02d.%s", xpos, ypos, ins[13:8], ins[21:16], idx_str[0], ins[37:32], idx_str[1]);
                    res_ori <= {
                        (src[0][0] * src[1][0]) >>> 16,
                        (src[0][1] * src[1][1]) >>> 16,
                        (src[0][2] * src[1][2]) >>> 16,
                        (src[0][3] * src[1][3]) >>> 16
                    };
                end
                8'h04: begin
                    $display("Core %d %d div r%02d,r%02d.%s,r%02d.%s", xpos, ypos, ins[13:8], ins[21:16], idx_str[0], ins[37:32], idx_str[1]);
                    res_ori <= {
                        (src[0][0] <<< 16) / src[1][0],
                        (src[0][1] <<< 16) / src[1][1],
                        (src[0][2] <<< 16) / src[1][2],
                        (src[0][3] <<< 16) / src[1][3]
                    };
                end
                8'h05: begin
                    $display("Core %d %d def r%02d.%c,%f", xpos, ypos, ins[13:8], idx_str[0][7:0], ins[47:16] / 65536.0);
                    res_ori <= regfile[ins[13:8]];
                    res_ori[ins[15:14]] <= ins[47:16];
                    sat <= 1'b0;
                end
                default: begin
                    $display("Core %d %d #UD %x", xpos, ypos, ins[7:0]);
                    regout <= 1'b0;
                    sat <= 1'b0;
                end
                endcase
                // 测试
                $display("r0 x:%f y:%f z:%f w:%f", regfile[0][0] / 65536.0, regfile[0][1] / 65536.0, regfile[0][2] / 65536.0, regfile[0][3] / 65536.0);
                $display("r1 x:%f y:%f z:%f w:%f", regfile[1][0] / 65536.0, regfile[1][1] / 65536.0, regfile[1][2] / 65536.0, regfile[1][3] / 65536.0);
            end
        end
        endcase
    end
end

endmodule

module calc_block #(
    parameter X = 8,
    parameter Y = 8
)
(
    input wire rst,
    input wire clk,
    input wire signed [15:0] xst, yst,
    input wire signed [15:0] x[2:0],
    input wire signed [15:0] y[2:0],
	 input wire boot,
    output reg busy
);

reg [63:0] prog[255:0];
reg [7:0] pc, pcend;

wire [63:0] ins;
wire mask[7:0][7:0];

assign ins = prog[pc];

genvar i, j;

// SIMT
generate
    for (i = 0; i < X; i = i + 1) begin : coreX
        for (j = 0; j < Y; j = j + 1) begin : coreY
            calc_core core_inst(
                .rst(rst),
                .clk(clk),
                .ins(ins),
                .xpos(xst + i[15:0]), .ypos(yst + j[15:0]),
                .x(x), .y(y),
                .busy(busy),
                .mask(mask[i][j])
            );
        end
    end
endgenerate

localparam STATE_IDLE = 1'b0,
           STATE_RUN = 1'b1;
reg state;

always @(posedge clk or negedge rst) begin
    if (!rst) begin
        busy <= 1'b0;
        state <= STATE_IDLE;
    end
    else begin
        case (state)
            STATE_IDLE: begin
                if (busy) begin
                    pc <= 8'd0;
                    state <= STATE_RUN;
                    // 测试
                    pcend <= 8'd3;
                    prog[0] <= 64'h000006C106C00101;
                    prog[1] <= 64'h000006C106C00002;
                    prog[2] <= 64'h000006C106C00103;
                    prog[3] <= 64'h0000000000000000;
                end
            end
            STATE_RUN: begin
                if (pc == pcend) begin
                    busy <= 1'b0;
                    state <= STATE_IDLE;
                end
                else begin
                    pc <= pc + 8'd1;
                end
            end
        endcase
    end
end

endmodule

module controller #(
    parameter [15:0] RESO_X = 16'd128,
    parameter [15:0] RESO_Y = 16'd64
)
(
    input wire rst,
    input wire clk,
    input wire signed [15:0] x[2:0],
    input wire signed [15:0] y[2:0],
	 input wire boot,
    output reg enable
);

reg signed [15:0] xst, yst, xed, yed;
reg signed [15:0] xp, yp;
reg block_boot;
wire busy;

calc_block block_inst(
    .rst(rst),
    .clk(clk),
    .x(x), .y(y),
    .xst(xp), .yst(yp),
	 .boot(block_boot),
    .busy(busy)
);

reg signed [15:0] xmin, xmax, ymin, ymax;// 这会不会被综合成wire?

always @* begin
    xmin = (x[0] < x[1] ? (x[0] < x[2] ? x[0] : x[2]) : (x[1] < x[2] ? x[1] : x[2]));
    xmax = (x[0] > x[1] ? (x[0] > x[2] ? x[0] : x[2]) : (x[1] > x[2] ? x[1] : x[2]));
    ymin = (y[0] < y[1] ? (y[0] < y[2] ? y[0] : y[2]) : (y[1] < y[2] ? y[1] : y[2]));
    ymax = (y[0] > y[1] ? (y[0] > y[2] ? y[0] : y[2]) : (y[1] > y[2] ? y[1] : y[2]));
    xst = (xmin < 16'd0 ? 16'd0 : xmin);
    xed = (xmax > RESO_X ? RESO_X : xmax);
    yst = (ymin < 16'd0 ? 16'd0 : ymin);
    yed = (ymax > RESO_Y ? RESO_Y : ymax);
end

localparam [15:0] STEP_X = 16'd8,
                  STEP_Y = 16'd8;

reg signed [31:0] a[2:0], b[2:0], c[2:0];// 这是不是也会变成wire?
reg [2:0] v;
reg valid;

always @* begin
    integer i, j;
    for (i = 0; i < 3; i = i + 1) begin
        j = (i + 1) % 3;
        a[i] = y[i] - y[j];
        b[i] = x[j] - x[i];
        c[i] = x[i] * y[j] - x[j] * y[i];
        v[i] = (a[i] * xp + b[i] * yp + c[i] <= 32'sd0) ||
               (a[i] * (xp + STEP_X - 1) + b[i] * yp + c[i] <= 32'sd0) ||
               (a[i] * xp + b[i] * (yp + STEP_Y - 1) + c[i] <= 32'sd0) ||
               (a[i] * (xp + STEP_X - 1) + b[i] * (yp + STEP_Y - 1) + c[i] <= 32'sd0);
    end
    valid = &v;
end

localparam [1:0] STATE_IDLE = 2'b00,
                 STATE_BOUND = 2'b01,
                 STATE_RUN = 2'b10,
                 STATE_WAIT = 2'b11;
reg [1:0] state;

always @(posedge clk or negedge rst) begin
    if (!rst) begin
        state <= STATE_IDLE;
    end
    else begin
        case (state)
            STATE_IDLE: begin
                if (boot) begin
					     enable <= 1'b1;
                    state <= STATE_BOUND;
                end
            end
            STATE_BOUND: begin
                xp <= xst; yp <= yst;
                state <= STATE_RUN;
            end
            STATE_RUN: begin
                if (valid) begin
                    block_boot <= 1'b1;
                    state <= STATE_WAIT;
                end
                else begin
                    if (xp + STEP_X >= xed) begin
                        if (yp + STEP_Y >= yed) begin
                            enable <= 1'b0;
                            state <= STATE_IDLE;
                        end
                        else begin
                            xp <= xst;
                            yp <= yp + STEP_Y;
                        end
                    end
                    else begin
                        xp <= xp + STEP_X;
                    end
                end
            end
            STATE_WAIT: begin
					 if (block_boot) begin
					     if (busy) block_boot <= 1'b0;
					 end
                else if (!busy) begin
                    state <= STATE_RUN;
                    if (xp + STEP_X >= xed) begin
                        if (yp + STEP_Y >= yed) begin
                            enable <= 1'b0;
                            state <= STATE_IDLE;
                        end
                        else begin
                            xp <= xst;
                            yp <= yp + STEP_Y;
                        end
                    end
                    else begin
                        xp <= xp + STEP_X;
                    end
                end
            end
        endcase
    end
end

endmodule

module core2(
	input wire rst,
	input wire clk
);

wire signed [15:0] x[2:0];
wire signed [15:0] y[2:0];
reg boot;

wire enable;

controller ctl(
	.rst(rst), .clk(clk),
	.x(x), .y(y),
	.boot(boot),
	.enable(enable)
);

assign x[0] = 16'd0, x[1] = 16'd10, x[2] = 16'd90;
assign y[0] = 16'd0, y[1] = 16'd45, y[2] = 16'd20;

always @(posedge clk or negedge rst) begin
	 if(!rst) begin
		  boot <= 1'b1;
	 end
	 else begin
		  if(enable) boot <= 1'b0;
	 end
end

endmodule