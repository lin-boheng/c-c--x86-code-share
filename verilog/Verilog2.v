module clock(clk);
output clk;
always begin
assign #10 clk = ~clk;
end
endmodule