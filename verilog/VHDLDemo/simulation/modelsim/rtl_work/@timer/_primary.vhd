library verilog;
use verilog.vl_types.all;
entity Timer is
    generic(
        CYCLE_TIME      : integer := 10
    );
    port(
        clk             : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of CYCLE_TIME : constant is 1;
end Timer;
