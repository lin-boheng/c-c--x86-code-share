library verilog;
use verilog.vl_types.all;
entity Memory is
    generic(
        MEMORY_DELAY    : integer := 5
    );
    port(
        addr            : in     vl_logic_vector(7 downto 0);
        \out\           : out    vl_logic_vector(7 downto 0);
        \in\            : in     vl_logic_vector(7 downto 0);
        read            : in     vl_logic;
        write           : in     vl_logic;
        state           : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of MEMORY_DELAY : constant is 1;
end Memory;
