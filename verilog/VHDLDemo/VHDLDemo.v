module VHDLDemo(in);

wire clk;

reg[7:0] addr;
reg[7:0] out;
output wire[7:0] in;
reg read;
reg write;
wire state;

Timer t0(.clk(clk));
Memory m0(
	.addr(addr),
	.in(out),
	.out(in),
	.read(read),
	.write(write),
	.state(state)
);

task write_byte;
	input [7:0] address;
	input [7:0] val;
	begin
		addr = address;
		out = val;
		write = 1;
		wait (state);
		write = 0;
		wait (~state);
	end
endtask

task read_byte;
	input [7:0] address;
	begin
		wait (~state);
		addr = address;
		read = 1;
		wait (state);
		read = 0;
		wait (~state);
	end
endtask

reg[7:0] regx, regy;
reg[7:0] pc;

reg initialized;

// mov regx, 23		00000100 00010111
// mov regy, 1			00000101 00000001
// add regx, regy		00000001
// add regy, regx		00000010
//// mov [regx], regy  00001101
// jmp short -5		00010000 11111011

// mov [regx], 23 	00000110 00010111
// mov [regy], 1 		00000111 00000001

// mov regx, regx 	00001000
// mov regx, regy 	00001001
// mov regy, regx 	00001010
// mov regy, regy 	00001011

// mov [regx], regx 	00001100
// mov [regx], regy 	00001101
// mov [regy], regx 	00001110
// mov [regy], regy 	00001111

initial begin
	initialized = 1'b0;
	pc = 8'h00;
	regx = 8'h00;
	regy = 8'h00;
	write_byte(0, 8'b00000100);
	write_byte(1, 8'b00010111);
	write_byte(2, 8'b00000101);
	write_byte(3, 8'b00000001);
	write_byte(4, 8'b00000001);
	write_byte(5, 8'b00000010);
	//write_byte(6, 8'b00001101);
	//write_byte(7, 8'b00010000);
	//write_byte(8, 8'b11111011);
	write_byte(6, 8'b00010000);
	write_byte(7, 8'b11111100);
	initialized = 1'b1;
end

always @(posedge clk) begin
	if(initialized) begin
		read_byte(pc);
		casex(in)
		8'b00000001: regx = regx + regy;
		8'b00000010: regy = regy + regx;
		8'b00000100: begin
				pc = pc + 1;
				read_byte(pc);
				regx = in;
			end
		8'b00000101: begin
				pc = pc + 1;
				read_byte(pc);
				regy = in;
			end
		8'b00001000: regx = regx;
		8'b00001001: regx = regy;
		8'b00001010: regy = regx;
		8'b00001011: regy = regy;
		8'bxxxx1100: write_byte(regx, regx);
		8'bxxxx1101: write_byte(regx, regy);
		8'bxxxx1110: write_byte(regy, regx);
		8'bxxxx1111: write_byte(regy, regy);
		8'b00010000: begin
				pc = pc + 1;
				read_byte(pc);
				pc = in + pc;
			end
		default: $display("unknown instruction!!!");
		endcase
		pc = pc + 1;
	end
end

endmodule