module Timer(clk);

output reg clk = 1'b0;

parameter CYCLE_TIME = 10;

always clk = #CYCLE_TIME ~clk;

endmodule