module Memory(addr, out, in, read, write, state);

input wire[7:0] addr;
output reg[7:0] out;
input wire[7:0] in;
input wire read;
input wire write;
output reg state;

reg[7:0] memory[8'hFF:0];

parameter MEMORY_DELAY = 5;

integer i;

initial begin
	out = 8'h00;
	state = 1'b0;
	for(i = 0; i <= 8'hFF; i = i + 1) begin
		memory[i] = 8'b0;
	end
end

always @(posedge read or posedge write) begin
	state = 1'b1;
	if(read == 1'b1) begin
		for(i = 0; i <= 8'hFF; i = i + 1) begin
			if(i == addr) begin
				out = #MEMORY_DELAY memory[i];
			end
		end
	end
	else if(write == 1'b1) begin
		for(i = 0; i <= 8'hFF; i = i + 1) begin
			if(i == addr) begin
				memory[i] = #MEMORY_DELAY in;
			end
		end
	end
	state = 1'b0;
end

endmodule