module adder(Ai, Bi, So, Co, clk);
input Ai, Bi;
output reg So, Co;
input clk;

always @ (posedge clk) begin
	So = Ai ^ Bi;
	Co = Ai & Bi;
end

endmodule