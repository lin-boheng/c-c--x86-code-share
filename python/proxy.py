import requests
import sys

proxy = '127.0.0.1:7891'
if len(sys.argv) > 1:
  proxy = sys.argv[1]
proxies = {
    'http' : 'socks5://' + proxy,
    'https' : 'socks5://' + proxy
}
print(requests.get("https://www.baidu.com", proxies=proxies).content)
