from PyQt5.QtCore import QRect, QUrl, Qt, pyqtSignal
from PyQt5.QtGui import QDragEnterEvent, QDropEvent, QIcon
from PyQt5.QtNetwork import QNetworkCookie
from PyQt5.QtWidgets import QApplication, QDialog, QFrame, QHBoxLayout, QLabel, QLineEdit, QMainWindow, QPushButton, QTabWidget, QVBoxLayout, QWidget
from PyQt5.QtWebEngineWidgets import QWebEngineSettings, QWebEngineView, QWebEnginePage

import sys

def run(coroutine):
    try:
        coroutine.send(None)
    except StopIteration as e:
        return e.value

class MyWebView(QWebEngineView):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
    
    def dragEnterEvent(self, e: QDragEnterEvent):
        print(self, ".dragEnterEvent(", e, ")", sep='')
        if e.mimeData().hasText():
            e.accept()
        else:
            e.ignore()
    
    def dropEvent(self, e: QDropEvent):
        print(self, ".dropEvent(", e, ")", sep='')
        filePathList = e.mimeData().text()
        filePath = filePathList.split('\n')
        for file in filePath:
            file = file.replace('file:///', '', 1)
            print("Receive File: " + file)
            if file.endswith(".js"):
                print("Execute JavaScript: " + file)
                with open(file, "r") as f:
                    script = f.read()
                    self.page().runJavaScript(script)
            if file.endswith(".py"):
                print("Execute Python: " + file)
                with open(file, "r") as f:
                    script = f.read()
                    exec(script)

class Browser(QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setWindowTitle('My Browser')
        self.setWindowIcon(QIcon('icons/favicon.ico'))
        self.resize(1200, 600)

        self.center = QWidget(self)

        self.boxLayout = QHBoxLayout(self.center)
        self.boxLayout.setSpacing(0)
        self.boxLayout.setContentsMargins(1, 1, 1, 1)
        
        self.frame = QFrame(self.center)

        self.gridLayout = QVBoxLayout(self.frame)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)

        self.horizontalLayout = QHBoxLayout()
        self.tb_url = QLineEdit(self.frame)
        self.bt_back = QPushButton(self.frame)
        self.bt_ahead = QPushButton(self.frame)
 
        self.bt_back.setIcon(QIcon.fromTheme("go-previous"))
        self.bt_ahead.setIcon(QIcon.fromTheme("go-next"))
 
        self.horizontalLayout.addWidget(self.bt_back)
        self.horizontalLayout.addWidget(self.bt_ahead)
        self.horizontalLayout.addWidget(self.tb_url)
        self.gridLayout.addLayout(self.horizontalLayout)

        self.html = MyWebView(self.frame)
        self.set_webview_attr(self.html)

        self.gridLayout.addWidget(self.html)
        self.boxLayout.addWidget(self.frame)
        self.setCentralWidget(self.center)
 
        self.tb_url.returnPressed.connect(self.browse)
        self.bt_back.clicked.connect(self.html.back)
        self.bt_ahead.clicked.connect(self.html.forward)
        self.html.urlChanged.connect(self.url_changed)

        self.default_url = "https://www.baidu.com/"

        self.setCentralWidget(self.center)
    
    def set_webview_attr(self, view : QWebEngineView):
        view.settings().setAttribute(QWebEngineSettings.WebAttribute.JavascriptEnabled, True)
        view.settings().setAttribute(QWebEngineSettings.WebAttribute.LocalContentCanAccessRemoteUrls, True)
        view.settings().setAttribute(QWebEngineSettings.WebAttribute.WebGLEnabled, True)
        view.settings().setAttribute(QWebEngineSettings.WebAttribute.Accelerated2dCanvasEnabled, True)
        view.page().profile().cookieStore().cookieAdded.connect(self.cookie_added)
        view.page().profile().cookieStore().cookieRemoved.connect(self.cookie_removed)

    def cookie_to_string(cookie : QNetworkCookie) -> str:
        return cookie.name().data().decode("utf-8") + " => " + cookie.value().data().decode("utf-8")
    
    def cookie_added(self, cookie : QNetworkCookie):
        print(self, ".cookie_added(", Browser.cookie_to_string(cookie), ")", sep='')
    
    def cookie_removed(self, cookie : QNetworkCookie):
        print(self, ".cookie_removed(", Browser.cookie_to_string(cookie), ")", sep='')

    def create_window(self, type : QWebEnginePage.WebWindowType):
        tab = QDialog(self)
        tab.setWindowTitle("New Tab")
        layout = QVBoxLayout(tab)
        layout.addWidget(self.html.createWindow(type))
        tab.setLayout(layout)
        tab.show()

    def cur_url(self):
        return self.tb_url.text() if self.tb_url.text() else self.default_url
    
    def cur_cookie(self):
        return self.html.page().profile().cookieStore()
    
    def browse(self, url=None):
        print(self, ".browse(", url, ")", sep='')
        url = self.cur_url() if url is None else self.default_url
        self.html.load(QUrl(url))
        self.html.show()
    
    def url_changed(self, url : QUrl):
        print(self, ".url_changed(", url, ")", sep='')
        self.tb_url.setText(url.toString())
    
    def set_cookie(self, key : str, val : str):
        self.cur_cookie().setCookie(QNetworkCookie(key.encode("utf-8"), val.encode("utf-8")), QUrl(self.cur_url()))


class CookieInput(QDialog):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        self.setWindowTitle("Cookie Settings")
        self.setWindowIcon(QIcon('icons/favicon.ico'))
        self.resize(300, 100)

        self.cookie1 = ""
        self.cookie2 = ""

        self.gridLayout = QVBoxLayout(self)
        self.bt_confirm = QPushButton(self)
        self.bt_confirm.setText("Confirm")
        self.bt_confirm.clicked.connect(self.confirm)
        
        self.item1 = QHBoxLayout()
        self.l_cookie1 = QLabel(self)
        self.l_cookie1.setText("passport_auth_status")
        self.tb_val1 = QLineEdit(self)

        self.item2 = QHBoxLayout()
        self.l_cookie2 = QLabel(self)
        self.l_cookie2.setText("passport_csrf_token")
        self.tb_val2 = QLineEdit(self)

        self.item1.addWidget(self.l_cookie1)
        self.item1.addWidget(self.tb_val1)

        self.item2.addWidget(self.l_cookie2)
        self.item2.addWidget(self.tb_val2)

        self.gridLayout.addLayout(self.item1)
        self.gridLayout.addLayout(self.item2)
        self.gridLayout.addWidget(self.bt_confirm)

        self.setLayout(self.gridLayout)
    
    def confirm(self):
        self.cookie1 = self.tb_val1.text() if self.tb_val1.text() else ""
        self.cookie2 = self.tb_val2.text() if self.tb_val2.text() else ""
        self.accept()

app = QApplication(sys.argv)
browser = Browser()
browser.show()
input = CookieInput()
input.exec()
browser.set_cookie("passport_auth_status", input.cookie1)
browser.set_cookie("passport_auth_status_ss", input.cookie1)
browser.set_cookie("passport_csrf_token", input.cookie2)
browser.set_cookie("passport_csrf_token_default", input.cookie2)
browser.browse()
sys.exit(app.exec())
