# coding=utf-8
from urllib import request, parse
from hashlib import md5
import threading
import queue
import time
import re

urls = queue.Queue()
record = {}
htmlreg = re.compile(r'<!DOCTYPE [hH][tT][mM][lL]')


def getcontent(url: str):
    print("accessing web page {0}".format(url))
    try:
        res = request.urlopen(url, timeout=1.0)
        ret = res.read()
        return ret
    except Exception as e:
        print("access {0} failed:{1}".format(url, e))
        return None


def getpageurls(url: str):
    global urls
    pagecode = getcontent(url)
    if not pagecode:
        return
    page = pagecode.decode(encoding="UTF-8", errors="replace")
    if not htmlreg.match(page):
        return
    reg = r'"((http|https)://[^"]*)"'
    urlize = re.compile(reg)
    relist = urlize.findall(page)
    for url in relist:
        print("processing url {0}".format(url[0]))
        urls.put(url[0])
        part = str.split(url[0], '/')
        name = part[len(part) - 1].split('?')[0]
        if name == "":
            continue
        name = "download/" + name
        print("downloading file {0} from url {1}".format(name, url[0]))
        try:
            with open(name, "wb") as f:
                f.write(pagecode)
        except Exception as e:
            print("download {0} to file {1} failed:{2}".format(url[0], name, e))


def main():
    global urls
    cnt = 0
    urls.put(input("start url:"))
    while True:
        if urls.empty():
            time.sleep(1.0)
            cnt = cnt + 1
            if cnt >= 60:
                return
            continue
        cnt = 0
        url = urls.get()
        #if url.find(".sfacg.") == -1:
        #    continue
        dig = md5(url.encode("UTF-8")).digest().hex()
        try:
            if record[dig]:
                continue
        except Exception as e:
            record[dig] = True
        thread = threading.Thread(target=getpageurls, args=(url,), daemon=True)
        thread.start()


if __name__ == "__main__":
    main()

print("WebServerWorm process exited,press 'Enter' to leave")
input()
