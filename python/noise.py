from PIL import Image
import numpy as np

def lerp(x, y, w):
    return x + (y - x) * w

def fade(t):
    return t * t * t * (t * (t * 6 - 15) + 10)

def interpolate(l1, l2, r1, r2, wh, wv):
    return lerp(lerp(l1, r1, wh), lerp(l2, r2, wh), wv)

def random_noise(width, height, scale):
    '''Generator a random noise image from numpy.array.

    If nc is 1, the Grayscale image will be created.
    If nc is 3, the RGB image will be generated.

    Args:
        nc (int): (1 or 3) number of channels.
        width (int): width of output image.
        height (int): height of output image.
    Returns:
        PIL Image.
    '''
    img = (np.random.rand(width+1, height+1, 1)*255.0)
    new = np.random.rand(width*scale, height*scale)
    for i in range(0,width):
        for j in range(0,height):
            for k in range(0,scale):
                for l in range(0,scale):
                    new[i*scale+k][j*scale+l] = interpolate(img[i][j], img[i][j+1], img[i+1][j], img[i+1][j+1], k/scale, l/scale)
    return Image.fromarray(new.astype(np.uint8), mode="L")



if __name__ == '__main__':
    random_noise(16, 16, 16).save('random-noise.jpg')