# from bs4 import BeautifulSoup
import requests
# import easyocr
# import re
import threading
import time

host = []
out = open('proxies.txt', 'w')

def check_host(host:str):
    print('Test:' + host)
    try:
        p = { 'http' : "http://" + host[i] , 'https' : "https://" + host[i] }
        res = requests.get("https://www.baidu.com", proxies=p, timeout=10.0)
        if res.ok:
            print('Success:' + host)
            out.write(host + '\n')
            return
        res = requests.get("http://www.baidu.com", proxies=p, timeout=10.0)
        if res.ok:
            print('Success:' + host)
            out.write(host + '\n')
            return
        print("Failed:" + host)
    except Exception as e:
        pass

with open('http_proxies.txt', 'r') as f:
    host = f.readlines()

for i in range(0, len(host)):
    host[i] = host[i][0:len(host[i])-1]
    t = threading.Thread(target=check_host, args=(host[i],), daemon=False)
    t.start()
    time.sleep(1.0)
